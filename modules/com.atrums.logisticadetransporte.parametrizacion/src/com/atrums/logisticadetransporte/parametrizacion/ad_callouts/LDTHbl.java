package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class LDTHbl extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    if (vars.commandIn("DEFAULT")) {
      String strChanged = vars.getStringParameter("inpLastFieldChanged");
      if (log4j.isDebugEnabled())
        log4j.debug("CHANGED: " + strChanged);

      String strLdtHbl = vars.getStringParameter("inpldtHblId");
      String strOrgId = vars.getStringParameter("inpadOrgId");
      try {
        printPage(response, vars, strLdtHbl, strOrgId);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strLdtHbl,
      String strOrgId) throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: dataSheet");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "com/atrums/logisticadetransporte/parametrizacion/ad_callouts/CallOut").createXmlDocument();

    LDTHblData[] data = LDTHblData.selectHbl(this, strLdtHbl);

    StringBuffer resultado = new StringBuffer();
    resultado.append("var calloutName='LDTHbl';\n\n");

    if (data == null || data.length == 0) {
      resultado.append("var respuesta = new Array(new Array(\"inpldtHblId\", null),");
      resultado.append("new Array(\"inpisactive\",null),");
      resultado.append("new Array(\"inpprocessed\",null),");
      resultado.append("new Array(\"inpnumHbl\",null),");
      resultado.append("new Array(\"inpldtRoutingOrderId\",null),");
      resultado.append("new Array(\"inpcBpartnerId\",null)");
      resultado.append("new Array(\"inpvalorFlete\",null),");
      resultado.append("new Array(\"inpestado\",null),");
      resultado.append("new Array(\"inpnumEmbarque\",null),");
      resultado.append("new Array(\"inpcartas\",null),");
      resultado.append("new Array(\"inpvendedorId\",null));");
    } else {
      if (data != null && data.length > 0) {
        resultado.append("var respuesta = new Array(");
        resultado.append("new Array(\"inpldtHblId\", \"" + data[0].getField("idhbl") + "\"),");
        resultado.append("new Array(\"inpadOrgId\", \"" + data[0].getField("sucursal") + "\"),");
        resultado.append("new Array(\"inpisactive\", \"" + data[0].getField("activo") + "\"),");
        resultado.append("new Array(\"inpprocessed\", \"" + data[0].getField("processed") + "\"),");
        resultado.append("new Array(\"inpnumHbl\", \"" + data[0].getField("numhbl") + "\"),");
        resultado.append("new Array(\"inpldtRoutingOrderId\", \""
            + data[0].getField("routing_order") + "\"),");
        resultado.append("new Array(\"inpcBpartnerId\", \"" + data[0].getField("cliente") + "\"),");
        resultado.append("new Array(\"inpvalorFlete\", \"" + data[0].getField("valor_flete")
            + "\"),");
        resultado.append("new Array(\"inpestado\", \"" + data[0].getField("estado") + "\"),");
        resultado.append("new Array(\"inpnumEmbarque\", \"" + data[0].getField("num_embarque")
            + "\"),");
        resultado.append("new Array(\"inpcartas\", \"" + data[0].getField("cartas") + "\"),");
        resultado.append("new Array(\"inpvendedorId\", \"" + data[0].getField("vendedor") + "\")");
      } else {
        resultado.append("null");
      }

      resultado.append(");");
    }

    xmlDocument.setParameter("array", resultado.toString());
    xmlDocument.setParameter("frameName", "appFrame");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

}
