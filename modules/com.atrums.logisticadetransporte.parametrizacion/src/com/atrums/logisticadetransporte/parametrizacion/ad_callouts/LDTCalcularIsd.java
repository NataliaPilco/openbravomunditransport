package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import javax.servlet.ServletException;

import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.erpCommon.ad_callouts.SimpleCallout;
import org.openbravo.erpCommon.utility.Utility;

public class LDTCalcularIsd extends SimpleCallout {

  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;

  @Override
  protected void execute(CalloutInfo info) throws ServletException {
    VariablesSecureApp vars = info.vars;

    BigDecimal isd = BigDecimal.ZERO;

    DecimalFormat numberFormat = Utility.getFormat(vars, "priceRelation");

    BigDecimal notaDebito = new BigDecimal(vars.getStringParameter("inpndAgente", "0").replace(
        String.valueOf(numberFormat.getDecimalFormatSymbols().getMonetaryDecimalSeparator()), ""));

    isd = notaDebito.multiply(new BigDecimal(0.05));

    try {
      info.addResult("inpisd", isd);
    } catch (Exception e) {
      log4j.info("No default info for the selected payment method");
    }
  }

}
