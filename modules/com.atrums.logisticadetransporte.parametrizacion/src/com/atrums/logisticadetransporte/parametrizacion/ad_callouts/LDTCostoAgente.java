package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import javax.servlet.ServletException;

import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.erpCommon.ad_callouts.SimpleCallout;
import org.openbravo.erpCommon.utility.Utility;

public class LDTCostoAgente extends SimpleCallout {

  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;

  @Override
  protected void execute(CalloutInfo info) throws ServletException {
    VariablesSecureApp vars = info.vars;

    DecimalFormat numberFormat = Utility.getFormat(vars, "priceRelation");

    BigDecimal total = BigDecimal.ZERO;

    // BigDecimal precioUnitario = new BigDecimal(vars.getNumericParameter("inpldtCostoAgente",
    // "0"));
    // BigDecimal cantidad = new BigDecimal(vars.getStringParameter("inpcantidad", "0"));
    BigDecimal cantidad = new BigDecimal(vars.getStringParameter("inpcantidad", "0").replace(
        String.valueOf(numberFormat.getDecimalFormatSymbols().getMonetaryDecimalSeparator()), ""));

    BigDecimal precioUnitario = new BigDecimal(vars.getStringParameter("inpldtCostoAgente", "0")
        .replace(
            String.valueOf(numberFormat.getDecimalFormatSymbols().getMonetaryDecimalSeparator()),
            ""));

    total = cantidad.multiply(precioUnitario);

    try {
      info.addResult("inpldtTotalAgente", total);
    } catch (Exception e) {
      log4j.info("No default info for the selected payment method");
    }
  }

}
