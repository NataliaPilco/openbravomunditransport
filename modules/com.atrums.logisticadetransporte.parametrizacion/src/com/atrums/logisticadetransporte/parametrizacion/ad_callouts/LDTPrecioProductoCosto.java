package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class LDTPrecioProductoCosto extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    if (vars.commandIn("DEFAULT")) {
      String strChanged = vars.getStringParameter("inpLastFieldChanged");
      if (log4j.isDebugEnabled())
        log4j.debug("CHANGED: " + strChanged);

      String strProducto = vars.getStringParameter("inpmProductId");
      String strPricelistVersion = vars.getStringParameter("inpmPricelistVersionId");
      String strWarehouse = vars.getStringParameter("inpmWarehouseId");
      String strOrgId = vars.getStringParameter("inpadOrgId");
      String strClientId = vars.getStringParameter("inpadClientId");
      try {
        printPage(response, vars, strProducto, strPricelistVersion, strWarehouse, strOrgId,
            strClientId);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strProducto,
      String strPricelistVersion, String strWarehouse, String strOrgId, String strClientId)
      throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: dataSheet");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "com/atrums/logisticadetransporte/parametrizacion/ad_callouts/CallOut").createXmlDocument();

    LDTPrecioProductoCostoData[] data = LDTPrecioProductoCostoData.selectPrecio(this, strProducto,
        strPricelistVersion);

    StringBuffer resultado = new StringBuffer();
    resultado.append("var calloutName='LDTPrecioProductoCosto';\n\n");

    if (data == null || data.length == 0) {
      resultado.append("var respuesta = new Array(new Array(\"inpprecioUnit\", null),");
      resultado.append("var respuesta = new Array(new Array(\"inpldtMinimo\", null),");
      resultado.append("new Array(\"inpprecioStd\", null));");

    } else {
      if (data != null && data.length > 0) {
        resultado.append("var respuesta = new Array(");
        resultado.append("new Array(\"inpprecioUnit\", \"" + data[0].getField("PRECIO") + "\"),");
        resultado.append("new Array(\"inpldtMinimo\", \"" + data[0].getField("MINIMO") + "\"),");
        resultado.append("new Array(\"inpprecioStd\", \"" + data[0].getField("PRECIO_EST") + "\")");
      } else {
        resultado.append("null");
      }
      resultado.append(");");
    }

    xmlDocument.setParameter("array", resultado.toString());
    xmlDocument.setParameter("frameName", "appFrame");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

}