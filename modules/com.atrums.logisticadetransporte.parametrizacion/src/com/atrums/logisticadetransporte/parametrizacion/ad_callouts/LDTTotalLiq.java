package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import javax.servlet.ServletException;

import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.erpCommon.ad_callouts.SimpleCallout;
import org.openbravo.erpCommon.utility.Utility;

public class LDTTotalLiq extends SimpleCallout {

  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;

  @Override
  protected void execute(CalloutInfo info) throws ServletException {
    VariablesSecureApp vars = info.vars;

    BigDecimal total_1 = BigDecimal.ZERO;
    BigDecimal total_2 = BigDecimal.ZERO;
    BigDecimal utilidad = BigDecimal.ZERO;

    DecimalFormat numberFormat = Utility.getFormat(vars, "priceRelation");

    BigDecimal val_fact_flete = new BigDecimal(vars.getStringParameter("inpvalFactFlete", "0")
        .replace(
            String.valueOf(numberFormat.getDecimalFormatSymbols().getMonetaryDecimalSeparator()),
            ""));
    BigDecimal co_locales_iva = new BigDecimal(vars.getStringParameter("inpcoLocalesIva", "0")
        .replace(
            String.valueOf(numberFormat.getDecimalFormatSymbols().getMonetaryDecimalSeparator()),
            ""));
    BigDecimal total1 = new BigDecimal(vars.getStringParameter("inptotal1", "0").replace(
        String.valueOf(numberFormat.getDecimalFormatSymbols().getMonetaryDecimalSeparator()), ""));
    BigDecimal cas = new BigDecimal(vars.getStringParameter("inpcas", "0").replace(
        String.valueOf(numberFormat.getDecimalFormatSymbols().getMonetaryDecimalSeparator()), ""));
    BigDecimal mdt = new BigDecimal(vars.getStringParameter("inpmdt", "0").replace(
        String.valueOf(numberFormat.getDecimalFormatSymbols().getMonetaryDecimalSeparator()), ""));

    BigDecimal flete_mbl = new BigDecimal(vars.getStringParameter("inpfleteMbl", "0").replace(
        String.valueOf(numberFormat.getDecimalFormatSymbols().getMonetaryDecimalSeparator()), ""));
    BigDecimal thc = new BigDecimal(vars.getStringParameter("inpthc", "0").replace(
        String.valueOf(numberFormat.getDecimalFormatSymbols().getMonetaryDecimalSeparator()), ""));
    BigDecimal costo_local_nav = new BigDecimal(vars.getStringParameter("inpcostoLocalNav", "0")
        .replace(
            String.valueOf(numberFormat.getDecimalFormatSymbols().getMonetaryDecimalSeparator()),
            ""));
    BigDecimal isd = new BigDecimal(vars.getStringParameter("inpisd", "0").replace(
        String.valueOf(numberFormat.getDecimalFormatSymbols().getMonetaryDecimalSeparator()), ""));
    BigDecimal cod = new BigDecimal(vars.getStringParameter("inpcod", "0").replace(
        String.valueOf(numberFormat.getDecimalFormatSymbols().getMonetaryDecimalSeparator()), ""));
    BigDecimal otros1 = new BigDecimal(vars.getStringParameter("inpotros1", "0").replace(
        String.valueOf(numberFormat.getDecimalFormatSymbols().getMonetaryDecimalSeparator()), ""));

    total_1 = val_fact_flete.add(co_locales_iva).add(total1).add(cas).add(mdt);
    total_2 = flete_mbl.add(thc).add(costo_local_nav).add(isd).add(cod).add(otros1);
    utilidad = total_1.subtract(total_2);

    try {
      info.addResult("inputilidad", utilidad);
    } catch (Exception e) {
      log4j.info("No default info for the selected payment method");
    }
  }

}
