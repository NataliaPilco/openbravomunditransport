package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class LDTCotizacion extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    if (vars.commandIn("DEFAULT")) {
      String strChanged = vars.getStringParameter("inpLastFieldChanged");
      if (log4j.isDebugEnabled())
        log4j.debug("CHANGED: " + strChanged);

      String strLdtCotizacion = vars.getStringParameter("inpldtCotizacionId");
      String strOrgId = vars.getStringParameter("inpadOrgId");
      try {
        printPage(response, vars, strLdtCotizacion, strOrgId);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars,
      String strLdtCotizacion, String strOrgId) throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: dataSheet");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "com/atrums/logisticadetransporte/parametrizacion/ad_callouts/CallOut").createXmlDocument();

    LDTCotizacionData[] data = LDTCotizacionData.selectCotizacion(this, strLdtCotizacion);
    String varResultado = data[0].getField("es_fechas");

    StringBuffer resultado = new StringBuffer();
    resultado.append("var calloutName='LDTCotizacion';\n\n");

    resultado.append("var respuesta = new Array(");

    if (varResultado.equals("S")) {
      if (data == null || data.length == 0) {
        resultado.append("new Array(\"inpvendedorId\", null),");
        resultado.append("new Array(\"inpclienteId\",null),");
        resultado.append("new Array(\"inpmodalidad\",null),");
        resultado.append("new Array(\"inptipo\",null),");
        resultado.append("new Array(\"inpmPricelistComId\",null),");
        resultado.append("new Array(\"inpmPricelistId\",null),");
        resultado.append("new Array(\"inpmPricelistVersionId\",null),");
        resultado.append("new Array(\"inptarifaVenta\",null),");
        resultado.append("new Array(\"inpcostosLocales\",null),");
        resultado.append("new Array(\"inpldtPuertoTerminalId\",null),");
        resultado.append("new Array(\"inpldtCod\",null),");
        resultado.append("new Array(\"inpagenteId\",null),");
        resultado.append("new Array(\"inptarifaNegociada\",null),");
        resultado.append("new Array(\"inptarifaVentaEur\",null),");
        resultado.append("new Array(\"inpcostosLocalesEur\",null),");
        resultado.append("new Array(\"inptarifaNegociadaEur\",null));");
      } else {
        if (data != null && data.length > 0) {
          resultado.append("new Array(\"inpvendedorId\", \"" + data[0].getField("vendedor")
              + "\"),");
          resultado.append("new Array(\"inpclienteId\", \"" + data[0].getField("cliente") + "\"),");
          resultado.append("new Array(\"inppuertoTerminalOrigenId\", \"" + data[0].pruetoorigen
              + "\"),");
          resultado.append("new Array(\"inppuertoTerminalDestinoId\", \"" + data[0].pruetodestino
              + "\"),");
          resultado.append("new Array(\"inpmodalidad\", \"" + data[0].getField("modalidad")
              + "\"),");
          resultado.append("new Array(\"inptipo\", \"" + data[0].getField("tipo") + "\"),");
          resultado.append("new Array(\"inpcIncotermsId\", \"" + data[0].getField("incoterms")
              + "\"),");
          resultado.append("new Array(\"inpmPricelistComId\", \""
              + data[0].getField("tarifa_compra") + "\"),");
          resultado.append("new Array(\"inpmPricelistId\", \"" + data[0].getField("tarifa")
              + "\"),");
          resultado.append("new Array(\"inpmPricelistVersionId\", \""
              + data[0].getField("version_tarifa") + "\"),");
          resultado.append("new Array(\"inpldtPuertoTerminalId\", \""
              + data[0].getField("nom_puerto_org") + "\"),");
          resultado.append("new Array(\"inpldtCod\", \"" + data[0].getField("cod") + "\"),");
          resultado.append("new Array(\"inpcostosLocales\", \""
              + data[0].getField("costos_locales") + "\"),");
          resultado.append("new Array(\"inpagenteId\", \"" + data[0].getField("agente") + "\"),");
          resultado.append("new Array(\"inptarifaVenta\", \"" + data[0].getField("venta") + "\"),");
          resultado.append("new Array(\"inptarifaNegociada\", \"" + data[0].getField("negociado")
              + "\")");
          resultado.append("new Array(\"inpcostosLocalesEur\", \""
              + data[0].getField("costos_locales_eur") + "\"),");
          resultado.append("new Array(\"inptarifaVentaEur\", \"" + data[0].getField("venta_eur")
              + "\"),");
          resultado.append("new Array(\"inptarifaNegociadaEur\", \""
              + data[0].getField("negociado_eur") + "\")");
        } else {
          resultado.append("null");
        }

        resultado.append(");");
      }
    } else {
      String message = "La Tarifa de la cotización seleccionada ha caducado, por favor cambie la Cotización."
          + "<br>";
      resultado.append(" new Array('MESSAGE', \"" + message + "\"));");
    }

    xmlDocument.setParameter("array", resultado.toString());
    xmlDocument.setParameter("frameName", "appFrame");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();

  }

}
