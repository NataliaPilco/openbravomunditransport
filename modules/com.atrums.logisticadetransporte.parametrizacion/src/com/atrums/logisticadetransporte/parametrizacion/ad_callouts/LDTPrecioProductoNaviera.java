package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class LDTPrecioProductoNaviera extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    if (vars.commandIn("DEFAULT")) {
      String strChanged = vars.getStringParameter("inpLastFieldChanged");
      if (log4j.isDebugEnabled())
        log4j.debug("CHANGED: " + strChanged);

      String strProducto = vars.getStringParameter("inpmProductId");
      if (strProducto == "") {
        strProducto = vars.getStringParameter("inpmProductCoId");
      }
      String strIncoterms = vars.getStringParameter("inpcIncotermsId");
      String strPricelist = vars.getStringParameter("inpmPricelistId");
      String strCotNavieraId = vars.getStringParameter("inpldtCotizacionNavieraId");
      String strOrgId = vars.getStringParameter("inpadOrgId");
      String strClientId = vars.getStringParameter("inpadClientId");
      String strContratoId = vars.getStringParameter("inpldtContratoPuertoId");
      String strPaisId = vars.getStringParameter("inpcCountryId");

      try {
        printPage(response, vars, strProducto, strIncoterms, strPricelist, strOrgId, strClientId,
            strContratoId, strPaisId, strCotNavieraId);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strProducto,
      String strIncoterms, String strPricelist, String strOrgId, String strClientId,
      String strContratoId, String strPaisId, String strCotNavieraId) throws IOException,
      ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: dataSheet");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "com/atrums/logisticadetransporte/parametrizacion/ad_callouts/CallOut").createXmlDocument();

    LDTPrecioProductoData[] data = null;
    LDTPrecioProductoChinaData[] datas = null;

    if (!strContratoId.equals(null) && !strContratoId.equals("")) {
      datas = LDTPrecioProductoChinaData.selectPrecio(this, strProducto, strPricelist,
          strContratoId);
    } else {
      data = LDTPrecioProductoData.selectPrecio(this, strProducto, strIncoterms, strPricelist,
          strCotNavieraId);
    }

    StringBuffer resultado = new StringBuffer();
    resultado.append("var calloutName='LDTPrecioProductoNaviera';\n\n");

    if (!strContratoId.equals(null) && !strContratoId.equals("")) {
      if (datas == null || datas.length == 0) {
        resultado.append("var respuesta = new Array(new Array(\"inpprecioUnit\", null),");
        resultado.append("new Array(\"inpprecioNegociado\", null),");
        resultado.append("new Array(\"inpmPricelistVersionId\", null),");
        resultado.append("new Array(\"inpldtMinimo\", null),");
        resultado.append("new Array(\"inpmWarehouseId\", null));");
      } else {
        if (datas != null && datas.length > 0) {
          resultado.append("var respuesta = new Array(");
          resultado
              .append("new Array(\"inpprecioUnit\", \"" + datas[0].getField("PRECIO") + "\"),");
          resultado.append("new Array(\"inpprecioNegociado\", \"" + datas[0].getField("PRECIO")
              + "\"),");
          resultado.append("new Array(\"inpmPricelistVersionId\", \""
              + datas[0].getField("VERSION_TARIFA") + "\"),");
          resultado.append("new Array(\"inpldtMinimo\", \"" + datas[0].getField("MINIMO") + "\"),");
          resultado.append("new Array(\"inpmWarehouseId\", \"" + datas[0].getField("WAREHOUSE")
              + "\")");
        } else {
          resultado.append("null");
        }
        resultado.append(");");
      }
    } else {
      if (data == null || data.length == 0) {
        resultado.append("var respuesta = new Array(new Array(\"inpprecioUnit\", null),");
        resultado.append("new Array(\"inpprecioNegociado\", null),");
        resultado.append("new Array(\"inpmPricelistVersionId\", null),");
        resultado.append("new Array(\"inpldtMinimo\", null),");
        resultado.append("new Array(\"inpmWarehouseId\", null));");
      } else {
        if (data != null && data.length > 0) {
          resultado.append("var respuesta = new Array(");
          resultado.append("new Array(\"inpprecioUnit\", \"" + data[0].getField("PRECIO") + "\"),");
          resultado.append("new Array(\"inpprecioNegociado\", \"" + data[0].getField("PRECIO")
              + "\"),");
          resultado.append("new Array(\"inpmPricelistVersionId\", \""
              + data[0].getField("VERSION_TARIFA") + "\"),");
          resultado.append("new Array(\"inpldtMinimo\", \"" + data[0].getField("MINIMO") + "\"),");
          resultado.append("new Array(\"inpmWarehouseId\", \"" + data[0].getField("WAREHOUSE")
              + "\")");
        } else {
          resultado.append("null");
        }
        resultado.append(");");
      }
    }

    xmlDocument.setParameter("array", resultado.toString());
    xmlDocument.setParameter("frameName", "appFrame");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

}
