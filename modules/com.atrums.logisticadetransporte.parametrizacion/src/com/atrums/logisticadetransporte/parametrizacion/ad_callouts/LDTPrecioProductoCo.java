package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class LDTPrecioProductoCo extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    if (vars.commandIn("DEFAULT")) {
      String strChanged = vars.getStringParameter("inpLastFieldChanged");
      if (log4j.isDebugEnabled())
        log4j.debug("CHANGED: " + strChanged);

      String strProducto = vars.getStringParameter("inpmProductCoId");
      String strOrgId = vars.getStringParameter("inpadOrgId");
      String strClientId = vars.getStringParameter("inpadClientId");
      String strTarifaCo = vars.getStringParameter("inpmPricelistCoId");
      String strVersionCo = vars.getStringParameter("inpmPricelistVersionCoId");
      try {
        printPage(response, vars, strProducto, strOrgId, strClientId, strTarifaCo, strVersionCo);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strProducto,
      String strOrgId, String strClientId, String strTarifaCo, String strVersionCo)
      throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: dataSheet");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "com/atrums/logisticadetransporte/parametrizacion/ad_callouts/CallOut").createXmlDocument();

    LDTPrecioProductoCoData[] dataMin = LDTPrecioProductoCoData.selectMinimo(this, strProducto);

    // String strPricelistVersion = LDTVTarifaCoData.selectVersionTarifa(this, strTarifaCo);

    LDTPrecioProductoCoData[] data = LDTPrecioProductoCoData.selectPrecio(this, strProducto,
        strVersionCo);

    StringBuffer resultado = new StringBuffer();
    resultado.append("var calloutName='LDTPrecioProductoCo';\n\n");

    if ((data != null && data.length > 0) || (dataMin != null && dataMin.length > 0)) {
      resultado.append("var respuesta = new Array(");
    }

    if (data != null && data.length > 0) {
      resultado.append("new Array(\"inpprecioUnit\", \"" + data[0].getField("PRECIO") + "\"),");
      resultado
          .append("new Array(\"inpprecioNegociado\", \"" + data[0].getField("PRECIO") + "\"),");
      resultado.append("new Array(\"inpmPricelistVersionId\", \"" + strVersionCo + "\"),");
    } else {
      resultado.append("null");
    }

    if (dataMin != null && dataMin.length > 0) {
      resultado.append("new Array(\"inpldtMinimo\", \"" + dataMin[0].getField("PRECIO") + "\")");
    } else {
      resultado.append("null");
    }
    if ((data != null && data.length > 0) || (dataMin != null && dataMin.length > 0)) {
      resultado.append(");");
    }

    xmlDocument.setParameter("array", resultado.toString());
    xmlDocument.setParameter("frameName", "appFrame");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();

  }
}