package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.xmlEngine.XmlDocument;

public class LDTLiquidacion extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    if (vars.commandIn("DEFAULT")) {
      String strChanged = vars.getStringParameter("inpLastFieldChanged");
      if (log4j.isDebugEnabled())
        log4j.debug("CHANGED: " + strChanged);

      int valor;
      String strLdtLiquidacion = vars.getStringParameter("inpldtLiquidacionCabeceraId");
      String strCondicion = vars.getStringParameter("inpcondicion");
      String strOrgId = vars.getStringParameter("inpadOrgId");
      BigDecimal isd = BigDecimal.ZERO;

      DecimalFormat numberFormat = Utility.getFormat(vars, "priceRelation");

      BigDecimal cod = new BigDecimal(vars.getStringParameter("inpcod", "0").replace(
          String.valueOf(numberFormat.getDecimalFormatSymbols().getMonetaryDecimalSeparator()), ""));
      BigDecimal otros = new BigDecimal(vars.getStringParameter("inpotros1", "0").replace(
          String.valueOf(numberFormat.getDecimalFormatSymbols().getMonetaryDecimalSeparator()), ""));
      BigDecimal notaCredito = new BigDecimal(vars.getStringParameter("inpncAgente", "0").replace(
          String.valueOf(numberFormat.getDecimalFormatSymbols().getMonetaryDecimalSeparator()), ""));
      BigDecimal notaDebito = new BigDecimal(vars.getStringParameter("inpndAgente", "0").replace(
          String.valueOf(numberFormat.getDecimalFormatSymbols().getMonetaryDecimalSeparator()), ""));

      valor = notaDebito.compareTo(BigDecimal.ZERO);
      if (valor == 1)
        isd = notaDebito.multiply(new BigDecimal(0.05));

      try {
        printPage(response, vars, strLdtLiquidacion, strCondicion, strOrgId, notaDebito,
            notaCredito, isd, cod, otros);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars,
      String strLdtLiquidacion, String strCondicion, String strOrgId, BigDecimal notaDebito,
      BigDecimal notaCredito, BigDecimal isd, BigDecimal cod, BigDecimal otros) throws IOException,
      ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: dataSheet");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "com/atrums/logisticadetransporte/parametrizacion/ad_callouts/CallOut").createXmlDocument();

    String data;
    BigDecimal utilidad;
    // int valor;

    if (strCondicion.equals("FCL")) {
      data = LDTLiquidacionData.calculaLiquidacionFCL(this, strLdtLiquidacion);
      utilidad = new BigDecimal(data);
      // utilidad = (utilidad.subtract(cod)).subtract(otros);
    } else {
      data = LDTLiquidacionData.calculaLiquidacionLCL(this, strLdtLiquidacion);
      utilidad = new BigDecimal(data);
      // utilidad = (utilidad.subtract(cod)).subtract(otros);
    }

    StringBuffer resultado = new StringBuffer();
    resultado.append("var calloutName='LDTLiquidacion';\n\n");

    if (data != null && data.length() > 0) {
      resultado.append("var respuesta = new Array(");
      resultado.append("new Array(\"inputilidad\", \"" + utilidad + "\")");
    }
    ;

    /*
     * valor = isd.compareTo(BigDecimal.ZERO); if (valor == 1) {
     * resultado.append(",new Array(\"inpisd\", \"" + isd + "\")"); }
     */

    resultado.append(");");

    xmlDocument.setParameter("array", resultado.toString());
    xmlDocument.setParameter("frameName", "appFrame");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

}
