package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class LDTVTarifaCompra extends HttpSecureAppServlet {
	private static final long serialVersionUID = 1L;

	public void init(ServletConfig config) {
		super.init(config);
		boolHist = false;
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		VariablesSecureApp vars = new VariablesSecureApp(request);
		if (vars.commandIn("DEFAULT")) {
			String strChanged = vars.getStringParameter("inpLastFieldChanged");
			if (log4j.isDebugEnabled())
				log4j.debug("CHANGED: " + strChanged);

			String strMPricelist = vars.getStringParameter("inpmPricelistComId");
			String strOrgId = vars.getStringParameter("inpadOrgId");
			try {
				printPage(response, vars, strMPricelist, strOrgId);
			} catch (ServletException ex) {
				pageErrorCallOut(response);
			}
		} else
			pageError(response);
	}

	private void printPage(HttpServletResponse response,
			VariablesSecureApp vars, String strMPricelist, String strOrgId)
			throws IOException, ServletException {
		if (log4j.isDebugEnabled())
			log4j.debug("Output: dataSheet");
		XmlDocument xmlDocument = xmlEngine
				.readXmlTemplate("com/atrums/logisticadetransporte/parametrizacion/ad_callouts/CallOut")
				.createXmlDocument();

		String strTarifaCompra = LDTVTarifaCompraData.selectVersionTarifa(this, strMPricelist);
			
		StringBuffer resultado = new StringBuffer();
		resultado.append("var calloutName='LDTVTarifaCompra';\n\n");
			
		if (strTarifaCompra == null || strTarifaCompra.length() == 0){
			resultado.append("var respuesta = new Array(new Array(\"inpmPricelistVersionComId\", null));");
			}
		else {
			if (strTarifaCompra != null && strTarifaCompra.length() > 0){
				resultado.append("var respuesta = new Array(");
				resultado.append("new Array(\"inpmPricelistVersionComId\", \"" + strTarifaCompra + "\")");
			}else{
				resultado.append("null");
			}		
		    resultado.append(");");
		}
		
		xmlDocument.setParameter("array", resultado.toString());
		xmlDocument.setParameter("frameName", "appFrame");
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.println(xmlDocument.print());
		out.close();
	}

}
