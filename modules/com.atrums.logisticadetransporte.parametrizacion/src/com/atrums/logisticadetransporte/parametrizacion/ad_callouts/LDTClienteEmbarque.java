package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class LDTClienteEmbarque extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    if (vars.commandIn("DEFAULT")) {
      String strChanged = vars.getStringParameter("inpLastFieldChanged");
      if (log4j.isDebugEnabled())
        log4j.debug("CHANGED: " + strChanged);

      String strRoutingOrder = vars.getStringParameter("inpemLdtRoutingOrderId");
      String strOrgId = vars.getStringParameter("inpadOrgId");
      try {
        printPage(response, vars, strRoutingOrder, strOrgId);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars,
      String strRoutingOrder, String strOrgId) throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: dataSheet");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "com/atrums/logisticadetransporte/parametrizacion/ad_callouts/CallOut").createXmlDocument();

    LDTFleteHblData[] dataF = LDTFleteHblData.selectFlete(this, strRoutingOrder);

    LDTClienteEmbarqueData[] data = LDTClienteEmbarqueData.selectClienteEmbarque(this,
        strRoutingOrder);

    StringBuffer resultado = new StringBuffer();
    resultado.append("var calloutName='LDTClienteEmbarque';\n\n");

    if (dataF == null || dataF.length == 0) {
      resultado.append("var respuesta = new Array(new Array(\"inpcBpartnerId\", null),");
      resultado.append("new Array(\"inpcBpartnerLocationId\", null));");
    } else {
      if (dataF != null && dataF.length > 0) {
        resultado.append("var respuesta = new Array(");
        resultado.append("new Array(\"inpcBpartnerId\", \"" + data[0].getField("cliente") + "\"),");
        resultado.append("new Array(\"inpcBpartnerLocationId\", \""
            + data[0].getField("dir_cliente") + "\")");
      } else {
        resultado.append("null");
      }

      resultado.append(");");
    }

    xmlDocument.setParameter("array", resultado.toString());
    xmlDocument.setParameter("frameName", "appFrame");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

}
