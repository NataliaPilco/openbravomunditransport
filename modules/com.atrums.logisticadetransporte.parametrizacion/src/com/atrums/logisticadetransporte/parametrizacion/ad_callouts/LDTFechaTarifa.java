package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class LDTFechaTarifa extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    if (vars.commandIn("DEFAULT")) {
      String strChanged = vars.getStringParameter("inpLastFieldChanged");
      if (log4j.isDebugEnabled())
        log4j.debug("CHANGED: " + strChanged);

      String strOrigenId = vars.getStringParameter("inppuertoTerminalOrigenId");
      String strDestinoId = vars.getStringParameter("inppuertoTerminalDestinoId");
      String strTransportistaId = vars.getStringParameter("inpcBpartnerId");
      String strContratoPuertoId = vars.getStringParameter("inpldtContratoPuertoId");
      String strVersionTarifaId = vars.getStringParameter("inpmVersionTarifaId");

      try {
        printPage(response, vars, strOrigenId, strDestinoId, strTransportistaId,
            strContratoPuertoId, strVersionTarifaId);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strOrigenId,
      String strDestinoId, String strTransportistaId, String strContratoPuertoId,
      String strVersionTarifaId) throws IOException, ServletException {
    String strFechaTarifa = "";
    String strTarifaID = "";
    String strVersionID = "";
    String strContratoID = "";
    String strDiasLibres = "";
    String strMoneda = "";

    if (log4j.isDebugEnabled())
      log4j.debug("Output: dataSheet");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "com/atrums/logisticadetransporte/parametrizacion/ad_callouts/CallOut").createXmlDocument();

    StringBuffer resultado = new StringBuffer();
    resultado.append("var calloutName='LDTFechaTarifa';\n\n");

    LDTFechaTarifaData data[];

    if (strVersionTarifaId.equals(null) || strVersionTarifaId.equals("")) {
      data = LDTFechaTarifaData.selectFechaTarifa(this, strOrigenId, strDestinoId,
          strTransportistaId);
    } else {
      /*
       * data = LDTFechaTarifaData.selectFechaTarifaContrato(this, strOrigenId, strDestinoId,
       * strTransportistaId, strContratoPuertoId);
       */
      data = LDTFechaTarifaData.selectFechaContrato(this, strVersionTarifaId);
    }

    if (data.length == 0 || data.equals(null)) {
      resultado
          .append("var respuesta = new Array( new Array('MESSAGE', \"No existe una Tarifa o Version de Tarifa para el Origen, Destino y Transportista seleccionado\"));");
    } else {
      strFechaTarifa = data[0].fechacaduca;
      strTarifaID = data[0].mPricelistId;
      strVersionID = data[0].versionTarifa;
      strContratoID = data[0].emLdtContratoPuertoId;
      strDiasLibres = data[0].diasLibres;
      strMoneda = data[0].moneda;

      if (strFechaTarifa == null || strFechaTarifa.length() == 0) {
        resultado.append("var respuesta = new Array(new Array(\"inpfechaValidez\", null), ");
        resultado.append("new Array(\"inpldtDiasLibres\", null),");
        resultado.append("new Array(\"inpcCurrencyId\", null),");
        resultado.append("new Array(\"inpmPricelistId\", null),");
        resultado.append("new Array(\"inpmPricelistVersionId\", null),");
        if (strContratoID.toString().equals("")) {
          resultado.append("new Array(\"inpldtContratoPuertoId\", null)");
        } else {
          resultado.append("new Array(\"inpldtContratoPuertoId\", '" + strContratoID + "')");
        }
        resultado.append(");");
      } else if (strFechaTarifa != null && strFechaTarifa.length() > 0) {
        resultado.append("var respuesta = new Array(new Array(\"inpfechaValidez\", '"
            + strFechaTarifa + "'), ");
        resultado.append("new Array(\"inpldtDiasLibres\", '" + strDiasLibres + "'),");
        resultado.append("new Array(\"inpcCurrencyId\", '" + strMoneda + "'),");
        resultado.append("new Array(\"inpmPricelistId\", '" + strTarifaID + "'),");
        resultado.append("new Array(\"inpmPricelistVersionId\", '" + strVersionID + "'),");
        if (strContratoID.toString().equals("")) {
          resultado.append("new Array(\"inpldtContratoPuertoId\", null)");
        } else {
          resultado.append("new Array(\"inpldtContratoPuertoId\", '" + strContratoID + "')");
        }
        resultado.append(");");
      }

    }
    xmlDocument.setParameter("array", resultado.toString());
    xmlDocument.setParameter("frameName", "appFrame");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

}
