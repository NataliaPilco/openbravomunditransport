package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class LDTFleteCostoLcl extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    if (vars.commandIn("DEFAULT")) {
      String strChanged = vars.getStringParameter("inpLastFieldChanged");
      if (log4j.isDebugEnabled())
        log4j.debug("CHANGED: " + strChanged);

      String strLdtMbl = vars.getStringParameter("inpldtMblId");
      String strOrgId = vars.getStringParameter("inpadOrgId");
      try {
        printPage(response, vars, strLdtMbl, strOrgId);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strLdtMbl,
      String strOrgId) throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: dataSheet");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "com/atrums/logisticadetransporte/parametrizacion/ad_callouts/CallOut").createXmlDocument();

    LDTFleteCostoLclData[] data = LDTFleteCostoLclData.selectFleteCosto(this, strLdtMbl);

    StringBuffer resultado = new StringBuffer();
    resultado.append("var calloutName='LDTFleteCostoLcl';\n\n");

    if (data == null || data.length == 0) {
      resultado.append("var respuesta = new Array(new Array(\"inptotalFleteCosto\", 0),");
      resultado.append("new Array(\"inpfleteCostoDiv\",0),");
      resultado.append("new Array(\"inpnumDetalle\",0));");
    } else {
      if (data != null && data.length > 0) {
        resultado.append("var respuesta = new Array(");
        resultado.append("new Array(\"inptotalFleteCosto\", \"" + data[0].getField("FLETE_COSTO")
            + "\"),");
        resultado.append("new Array(\"inpfleteCostoDiv\", \"" + data[0].getField("DIV") + "\"),");
        resultado.append("new Array(\"inpnumDetalle\", \"" + data[0].getField("NUM_DET") + "\")");
      } else {
        resultado.append("null");
      }
      resultado.append(");");
    }

    xmlDocument.setParameter("array", resultado.toString());
    xmlDocument.setParameter("frameName", "appFrame");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();

  }

}
