package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import javax.servlet.ServletException;

import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.erpCommon.ad_callouts.SimpleCallout;
import org.openbravo.erpCommon.utility.Utility;

public class LDTCalcularTarifaProductos extends SimpleCallout {

  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;

  @Override
  protected void execute(CalloutInfo info) throws ServletException {
    VariablesSecureApp vars = info.vars;

    BigDecimal total = BigDecimal.ZERO;
    BigDecimal totalNeg = BigDecimal.ZERO;

    DecimalFormat numberFormat = Utility.getFormat(vars, "priceRelation");

    BigDecimal cantidad = new BigDecimal(vars.getStringParameter("inpcantidad", "0").replace(
        String.valueOf(numberFormat.getDecimalFormatSymbols().getMonetaryDecimalSeparator()), ""));

    BigDecimal precioUnitario = new BigDecimal(vars.getStringParameter("inpprecioUnit", "0")
        .replace(
            String.valueOf(numberFormat.getDecimalFormatSymbols().getMonetaryDecimalSeparator()),
            ""));
    BigDecimal precioNegociado = new BigDecimal(vars.getStringParameter("inpprecioNegociado", "0")
        .replace(
            String.valueOf(numberFormat.getDecimalFormatSymbols().getMonetaryDecimalSeparator()),
            ""));

    BigDecimal minimo = new BigDecimal(vars.getStringParameter("inpldtMinimo", "0").replace(
        String.valueOf(numberFormat.getDecimalFormatSymbols().getMonetaryDecimalSeparator()), ""));

    BigDecimal valor = cantidad.multiply(precioNegociado);
    total = cantidad.multiply(precioUnitario);

    if (valor.compareTo(minimo) == 1) {
      totalNeg = valor;
    } else {
      totalNeg = minimo;
    }

    try {
      info.addResult("inptotalOpcion", total);
      info.addResult("inptotalNegociado", totalNeg);
    } catch (Exception e) {
      log4j.info("No default info for the selected payment method");
    }
  }
}
