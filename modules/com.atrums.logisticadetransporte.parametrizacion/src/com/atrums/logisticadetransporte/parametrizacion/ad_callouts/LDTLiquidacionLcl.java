package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import javax.servlet.ServletException;

import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.erpCommon.ad_callouts.SimpleCallout;
import org.openbravo.erpCommon.utility.Utility;

public class LDTLiquidacionLcl extends SimpleCallout {

  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;

  @Override
  protected void execute(CalloutInfo info) throws ServletException {
    VariablesSecureApp vars = info.vars;

    BigDecimal total = BigDecimal.ZERO;

    DecimalFormat numberFormat = Utility.getFormat(vars, "priceRelation");

    BigDecimal notaDebito = new BigDecimal(vars.getStringParameter("inpdebitoPro", "0").replace(
        String.valueOf(numberFormat.getDecimalFormatSymbols().getMonetaryDecimalSeparator()), ""));
    BigDecimal notaCredito = new BigDecimal(vars.getStringParameter("inpcreditoPro", "0").replace(
        String.valueOf(numberFormat.getDecimalFormatSymbols().getMonetaryDecimalSeparator()), ""));
    BigDecimal resultadoBl = new BigDecimal(vars.getStringParameter("inptotal2", "0").replace(
        String.valueOf(numberFormat.getDecimalFormatSymbols().getMonetaryDecimalSeparator()), ""));

    total = (resultadoBl.add(notaCredito)).subtract(notaDebito);

    try {
      info.addResult("inptotal2", total);
    } catch (Exception e) {
      log4j.info("No default info for the selected payment method");
    }
  }

}
