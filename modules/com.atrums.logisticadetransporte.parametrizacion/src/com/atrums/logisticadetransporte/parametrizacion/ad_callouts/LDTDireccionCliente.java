package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.utils.FormatUtilities;
import org.openbravo.xmlEngine.XmlDocument;

public class LDTDireccionCliente extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    if (vars.commandIn("DEFAULT")) {
      String strChanged = vars.getStringParameter("inpLastFieldChanged");
      if (log4j.isDebugEnabled())
        log4j.debug("CHANGED: " + strChanged);

      String strBPartner = vars.getStringParameter("inpcBpartnerId");
      String strOrgId = vars.getStringParameter("inpadOrgId");
      try {

        printPage(response, vars, strBPartner, strOrgId);

      } catch (ServletException ex) {
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strBPartner,
      String strOrgId) throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: dataSheet");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "com/atrums/logisticadetransporte/parametrizacion/ad_callouts/CallOut").createXmlDocument();

    LDTDireccionClienteData[] data = LDTDireccionClienteData.obtenerDireccionCliente(this,
        strBPartner);

    StringBuffer resultado = new StringBuffer();
    resultado.append("var calloutName='LDTDireccionCliente';\n\n");
    if (data == null || data.length == 0) {
      resultado.append("var respuesta = new Array(new Array(\"inpcBpartnerLocationId\", null)");
      resultado.append("new Array(\"inpvendedorId\", null),");
      resultado.append("new Array(\"inpservicioClienteId\", null));");
    } else {
      resultado.append("var respuesta = new Array(new Array(\"inpcBpartnerLocationId\", ");

      if (data != null && data.length > 0) {
        resultado.append("new Array(");
        for (int i = 0; i < data.length; i++) {
          resultado.append("new Array(\"" + data[i].getField("id") + "\", \""
              + FormatUtilities.replaceJS(data[i].getField("name")) + "\", \""
              + (data[i].getField("id").equalsIgnoreCase("") ? "true" : "false") + "\")");
          if (i < data.length - 1) {
            resultado.append(",\n");
          }
        }
        resultado.append(")");
      } else {
        resultado.append("null");
      }
      resultado.append("),");
      resultado.append("new Array(\"inpvendedorId\", \"" + data[0].getField("VENDEDOR") + "\"),");
      resultado.append("new Array(\"inpservicioClienteId\", \"" + data[0].getField("SV_CLIENTE")
          + "\")");
      resultado.append(");");

    }

    xmlDocument.setParameter("array", resultado.toString());
    xmlDocument.setParameter("frameName", "appFrame");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

}
