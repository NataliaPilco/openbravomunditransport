package com.atrums.logisticadetransporte.parametrizacion.erpCommon.ad_actionButton;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.session.OBPropertiesProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.erpCommon.ad_actionButton.ActionButtonDefaultData;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.xmlEngine.XmlDocument;

public class LDTOpcionCarta extends HttpSecureAppServlet {

  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strProcessId = vars.getStringParameter("inpProcessId");
      String strWindow = vars.getStringParameter("inpwindowId");
      String strKey = vars.getStringParameter("inpldtHblId");
      String strLdtFh = vars.getStringParameter("inplftFh");
      String strMessage = "";

      printPage(response, vars, strKey, strWindow, strProcessId, strMessage, strLdtFh, true);
    } else if (vars.commandIn("GENERATE")) {
      String strldtHblid = vars.getStringParameter("inpldtHblid");
      String strLdtFh = vars.getStringParameter("inplftFh");
      String strResponsable = vars.getStringParameter("inpResponsable");
      String strDate = vars.getStringParameter("inpDateFrom");
      String strldtCartaId = vars.getStringParameter("inpCartaID");

      printPageDataPDF(request, response, vars, strldtHblid, strResponsable, strDate,
          strldtCartaId, strLdtFh);
    }
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strKey,
      String windowId, String strProcessId, String strMessage, String strLdtFh, boolean isDefault)
      throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: Button create file msg:" + strMessage);

    ActionButtonDefaultData[] data = null;
    String strHelp = "", strDescription = "";

    if (vars.getLanguage().equals("en_US"))
      data = ActionButtonDefaultData.select(this, strProcessId);
    else
      data = ActionButtonDefaultData.selectLanguage(this, vars.getLanguage(), strProcessId);

    if (data != null && data.length != 0) {
      strDescription = data[0].description;
      strHelp = data[0].help;
    }
    String[] discard = { "" };
    if (strHelp.equals(""))
      discard[0] = new String("helpDiscard");
    XmlDocument xmlDocument = xmlEngine
        .readXmlTemplate(
            "com/atrums/logisticadetransporte/parametrizacion/erpCommon/ad_actionButton/LDTOpcionCarta")
        .createXmlDocument();
    xmlDocument.setParameter("key", strKey);
    xmlDocument.setParameter("lftFh", strLdtFh);
    xmlDocument.setParameter("window", windowId);
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\r\n");
    xmlDocument.setParameter("theme", vars.getTheme());
    xmlDocument.setParameter("description", strDescription);
    xmlDocument.setParameter("help", strHelp);

    String dateFormat = OBPropertiesProvider.getInstance().getOpenbravoProperties()
        .getProperty("dateFormat.java");
    // SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    xmlDocument.setParameter("dateFrom", sdf.format(new Date()));
    xmlDocument.setParameter("dateFromdisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateFromsaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));

    xmlDocument.setData("reportLdt_Carta_ID", "liststructure", LDTCartaData.select(this,
        Utility.getContext(this, vars, "#AccessibleOrgTree", "GeneralAccountingReports"),
        Utility.getContext(this, vars, "#User_Client", "GeneralAccountingReports")));

    if (isDefault) {
      xmlDocument.setParameter("messageType", "");
      xmlDocument.setParameter("messageTitle", "");
      xmlDocument.setParameter("messageMessage", "");
    } else {
      OBError myMessage = new OBError();
      myMessage.setTitle("");
      if (log4j.isDebugEnabled())
        log4j.debug("Generar Cartas - before setMessage");
      if (strMessage == null || strMessage.equals(""))
        myMessage.setType("Success");
      else
        myMessage.setType("Error");
      if (strMessage != null && !strMessage.equals("")) {
        myMessage.setMessage(strMessage);
      } else
        Utility.translateError(this, vars, vars.getLanguage(), "Success");
      if (log4j.isDebugEnabled())
        log4j.debug("Generar Cartas - Message Type: " + myMessage.getType());
      vars.setMessage("CrearAnexo", myMessage);
      if (log4j.isDebugEnabled())
        log4j.debug("Generar Cartas - after setMessage");
      if (myMessage != null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageDataPDF(HttpServletRequest request, HttpServletResponse response,
      VariablesSecureApp vars, String strldtHblid, String strResponsable, String strDate,
      String strldtCartaId, String strLdtFh) throws IOException, ServletException {

    String strMessage = "";
    String strMainReportName = "";

    HashMap<String, Object> parameters = new HashMap<String, Object>();

    if (log4j.isDebugEnabled())
      log4j.debug("Output: Reconciliation PDF report");

    try {
      if (strLdtFh.trim().equals("Y")) {
        strMainReportName = "@basedesign@/com/atrums/logisticadetransporte/parametrizacion/erpReports/Rpt_LDT_Cartas_FH.jrxml";
      } else {
        strMainReportName = "@basedesign@/com/atrums/logisticadetransporte/parametrizacion/erpReports/Rpt_LDT_Cartas.jrxml";
      }

      OBContext.setAdminMode(true);

      String dateFormat = OBPropertiesProvider.getInstance().getOpenbravoProperties()
          .getProperty("dateFormat.java");
      // SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
      SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

      // Parameters
      parameters.put("ldt_hbl", strldtHblid);
      parameters.put("ldt_carta", strldtCartaId);
      parameters.put("fecha", strDate);
      parameters.put("responsable", strResponsable);

      OBContext.setAdminMode(true);

    } catch (Exception e) {
      strMessage = e.getMessage();
    }

    if (!strMessage.equals("")) {
      printPage(response, vars, strldtHblid, "", "", strMessage, strLdtFh, false);
    } else {
      try {
        response.setContentType("text/html; charset=UTF-8");
        renderJR(vars, response, strMainReportName, "pdf", parameters, null, null);

      } catch (Exception e) {
        strMessage = e.getMessage();
        printPage(response, vars, strldtHblid, "", "", strMessage, strLdtFh, false);
      }
    }
  }
}