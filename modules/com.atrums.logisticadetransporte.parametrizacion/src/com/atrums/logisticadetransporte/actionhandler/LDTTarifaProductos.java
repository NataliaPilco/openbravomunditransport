/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2011-2012 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package com.atrums.logisticadetransporte.actionhandler;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Query;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.client.kernel.RequestContext;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.pricing.pricelist.ProductPrice;
import org.openbravo.service.db.DalConnectionProvider;
import org.openbravo.service.db.DbUtility;

import com.atrums.logisticadetransporte.parametrizacion.data.LDTNavieraProductos;
import com.atrums.logisticadetransporte.parametrizacion.data.ldtCotizacion;
import com.atrums.logisticadetransporte.parametrizacion.data.ldtCotizacionNaviera;

/**
 * 
 * @author atrums
 * 
 */
public class LDTTarifaProductos extends BaseProcessActionHandler {
  private static final Logger log = Logger.getLogger(LDTTarifaProductos.class);

  @Override
  protected JSONObject doExecute(Map<String, Object> parameters, String content) {
    JSONObject jsonRequest = null;
    OBContext.setAdminMode(true);

    try {
      jsonRequest = new JSONObject(content);
      JSONObject params = jsonRequest.getJSONObject("_params");

      JSONArray tableIds = params.getJSONArray("m_productprice_id");

      if (tableIds != null) {
        crearTarifa(jsonRequest, tableIds);
      }

    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();
      VariablesSecureApp vars = RequestContext.get().getVariablesSecureApp();
      log.error(e.getMessage(), e);

      try {
        jsonRequest = new JSONObject();
        Throwable ex = DbUtility.getUnderlyingSQLException(e);
        String message = OBMessageUtils.translateError(new DalConnectionProvider(), vars,
            vars.getLanguage(), ex.getMessage()).getMessage();
        JSONObject errorMessage = new JSONObject();
        errorMessage.put("severity", "error");
        errorMessage.put("text", message);
        jsonRequest.put("message", errorMessage);

      } catch (Exception e2) {
        log.error(e.getMessage(), e2);
        // do nothing, give up
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    return jsonRequest;
  }

  private static String idCotNavieraId(String strCotizacionId, String strPriceListVersionId) {
    String ldtCotNav = "";

    String hql = " select e.id " + "   from ldt_cotizacion_naviera e "
        + "  where e.lDTCotizacion.id = '" + strCotizacionId + "'   and e.priceListVersion.id = '"
        + strPriceListVersionId + "'";

    Query query = OBDal.getInstance().getSession().createQuery(hql);
    if (query.list().size() > 0) {
      ldtCotNav = query.list().get(0).toString();
    }
    return (ldtCotNav);
  }

  private static String productoNavieraId(String versionTarifaId, String strCotizacionNavieraId,
      String strProductId) {
    String ldtNavProd = "";

    String hql = " select e.id " + "   from ldt_naviera_productos e "
        + "  where e.lDTCotizacionNaviera.id = '" + strCotizacionNavieraId
        + "'   and e.priceListVersion.id = '" + versionTarifaId + "'   and e.product.id = '"
        + strProductId + "'";

    Query query = OBDal.getInstance().getSession().createQuery(hql);
    if (query.list().size() > 0) {
      ldtNavProd = query.list().get(0).toString();
    }
    return (ldtNavProd);
  }

  private static Long secuenciaLinea(String strCotizacionId) {
    Long line = new Long("0");

    String hql = " select coalesce(max(e.lineNo),0) " + "   from ldt_cotizacion_naviera e "
        + "  where e.lDTCotizacion.id = '" + strCotizacionId + "'";

    Query query = OBDal.getInstance().getSession().createQuery(hql);
    if (query.list().size() > 0) {
      line = (new Long(query.list().get(0).toString())) + 10;
    }
    return (line);
  }

  private void crearTarifa(JSONObject jsonRequest, JSONArray tableIds) throws JSONException,
      OBException {

    String adClientId = jsonRequest.getString("inpadClientId");
    String adOrgId = jsonRequest.getString("inpadOrgId");
    String ldtCotizacionId = jsonRequest.getString("inpldtCotizacionId");
    String cotizacionNavieraId = null;
    String productoNaviera = null;
    BigDecimal valor1 = null;
    BigDecimal valor2 = null;
    int resultado;

    ldtCotizacionNaviera newLdtCotizacionNaviera = null;
    LDTNavieraProductos newLDTNavieraProductos = null;
    ProductPrice productPriceId = null;

    for (int i = 0; i < tableIds.length(); i++) {
      productPriceId = OBDal.getInstance().get(ProductPrice.class, tableIds.getString(i));

      cotizacionNavieraId = idCotNavieraId(ldtCotizacionId, productPriceId.getPriceListVersion()
          .getId());

      Long lineNo = 10L;

      if (cotizacionNavieraId.equals("")) {
        // insertar cotizacion naviera
        newLdtCotizacionNaviera = OBProvider.getInstance().get(ldtCotizacionNaviera.class);

        newLdtCotizacionNaviera.setClient(OBDal.getInstance().get(Client.class, adClientId));
        newLdtCotizacionNaviera.setOrganization(OBDal.getInstance()
            .get(Organization.class, adOrgId));
        newLdtCotizacionNaviera.setLDTCotizacion(OBDal.getInstance().get(ldtCotizacion.class,
            ldtCotizacionId));

        newLdtCotizacionNaviera.setActive(true);
        lineNo = secuenciaLinea(ldtCotizacionId);
        newLdtCotizacionNaviera.setLineNo(lineNo);
        newLdtCotizacionNaviera.setNaviera(productPriceId.getPriceListVersion().getPriceList()
            .getLdtTransportista());
        newLdtCotizacionNaviera.setPuertoTerminalOrigen(productPriceId.getPriceListVersion()
            .getPriceList().getLdtPuertoOrigen());
        newLdtCotizacionNaviera.setPuertoTerminalDestino(productPriceId.getPriceListVersion()
            .getPriceList().getLdtPuertoDestino());
        newLdtCotizacionNaviera.setPricelist(productPriceId.getPriceListVersion().getPriceList()
            .getId());
        newLdtCotizacionNaviera.setFechaDeValidez(productPriceId.getPriceListVersion()
            .getLdtFechaCaduca());
        // newLdtCotizacionNaviera.setLdtDiasLibres(productPriceId.getPriceListVersion()
        // .getLdtDiasLibres());
        newLdtCotizacionNaviera.setLDTContratoPuerto(productPriceId.getPriceListVersion()
            .getLdtContratoPuerto());
        newLdtCotizacionNaviera.setCountry(productPriceId.getPriceListVersion().getPriceList()
            .getLdtPuertoOrigen().getCountry().getId());
        newLdtCotizacionNaviera.setPriceListVersion(productPriceId.getPriceListVersion());
        newLdtCotizacionNaviera.setVersionTarifa(productPriceId.getPriceListVersion());
        newLdtCotizacionNaviera.setCurrency(productPriceId.getPriceListVersion().getPriceList()
            .getCurrency());

        OBDal.getInstance().save(newLdtCotizacionNaviera);
        cotizacionNavieraId = newLdtCotizacionNaviera.getId();
        OBDal.getInstance().save(productPriceId);
        OBDal.getInstance().flush();
      }

      productoNaviera = productoNavieraId(productPriceId.getPriceListVersion().getId(),
          cotizacionNavieraId, productPriceId.getProduct().getId());

      if (productoNaviera.equals("")) {
        // insertar productos en cotizacion que ya existe
        newLDTNavieraProductos = OBProvider.getInstance().get(LDTNavieraProductos.class);

        newLDTNavieraProductos.setClient(OBDal.getInstance().get(Client.class, adClientId));
        newLDTNavieraProductos
            .setOrganization(OBDal.getInstance().get(Organization.class, adOrgId));
        newLDTNavieraProductos.setPriceListVersion(productPriceId.getPriceListVersion().getId());
        newLDTNavieraProductos.setProduct(productPriceId.getProduct());
        newLDTNavieraProductos.setCantidad(new BigDecimal("1"));
        newLDTNavieraProductos.setPrecioUnit(productPriceId.getListPrice().add(
            productPriceId.getLdtOpa()));
        newLDTNavieraProductos.setPrecioNegociado(productPriceId.getListPrice().add(
            productPriceId.getLdtOpa()));
        newLDTNavieraProductos.setTotalOpcion(productPriceId.getListPrice().add(
            productPriceId.getLdtOpa()));

        valor1 = new BigDecimal(productPriceId.getListPrice().add(productPriceId.getLdtOpa())
            .toString());

        if (productPriceId.getProduct().getLdtValorMinimo() != null) {
          valor2 = new BigDecimal(productPriceId.getProduct().getLdtValorMinimo().toString());
        } else {
          valor2 = new BigDecimal("0");
        }

        resultado = valor1.compareTo(valor2);

        if (resultado == 1) {
          newLDTNavieraProductos.setTotalNegociado(productPriceId.getListPrice().add(
              productPriceId.getLdtOpa()));
        } else {
          if (productPriceId.getProduct().getLdtValorMinimo() != null) {
            newLDTNavieraProductos.setTotalNegociado(productPriceId.getProduct()
                .getLdtValorMinimo());
          } else {
            newLDTNavieraProductos.setTotalNegociado(new BigDecimal("0"));
          }
        }

        newLDTNavieraProductos.setLDTCotizacionNaviera(OBDal.getInstance().get(
            ldtCotizacionNaviera.class, cotizacionNavieraId));
        if (productPriceId.getProduct().getLdtValorMinimo() != null) {
          newLDTNavieraProductos.setLdtMinimo(productPriceId.getProduct().getLdtValorMinimo());
        } else {
          newLDTNavieraProductos.setLdtMinimo(new BigDecimal("0"));
        }
        newLDTNavieraProductos.setCurrency(productPriceId.getPriceListVersion().getPriceList()
            .getCurrency());
        newLDTNavieraProductos.setTipoUnidad("BL");
        newLDTNavieraProductos.setWarehouse(OBDal.getInstance().get(Warehouse.class,
            "F0DF1B44AD334E67827222317B41C840"));

        OBDal.getInstance().save(newLDTNavieraProductos);
        OBDal.getInstance().save(productPriceId);
        OBDal.getInstance().flush();
      }

    }

  }

}
