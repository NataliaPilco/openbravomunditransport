package com.atrums.offline.feletronica.process;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.OrganizationInformation;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.shipping.ShippingCompany;

public class ATECOFF_GenerarGuiaDespachoXML {

  public static boolean generarFacturaXMLGre(ShipmentInOut spiDato, ConnectionProvider conn,
      String strUser, boolean enviarSRI, OBError msg) throws Exception {
    File flXml = null;
    Document docXML = null;
    Client cltDato = null;
    OutputFormat ofFormat = null;
    DocumentType dctDato = null;
    OrganizationInformation oriDato = null;
    BusinessPartner bspDato = null;
    ShippingCompany shcDato = null;
    Organization orgDato = null;
    ATECOFFGenerarXmlData[] axmlDirMatriz = null;
    ATECOFFGenerarXmlData[] axmlDirec = null;
    ATECOFFGenerarXmlData[] axmlEmail = null;

    Hashtable<String, String> hstClaveAcceso = new Hashtable<String, String>();

    try {

      flXml = File.createTempFile("documento.xml", null);
      flXml.deleteOnExit();

      docXML = DocumentHelper.createDocument();

      ofFormat = OutputFormat.createPrettyPrint();
      ofFormat.setEncoding("utf-8");
      ofFormat.setTrimText(true);

      final Element elmgre = docXML.addElement("guiaRemision");
      elmgre.addAttribute("id", "comprobante");
      elmgre.addAttribute("version", "1.0.0");

      final Element elminftri = elmgre.addElement("infoTributaria");

      if (spiDato.getClient() != null) {
        cltDato = OBDal.getInstance().get(Client.class, spiDato.getClient().getId());

        Date cldFechaIn = spiDato.getMovementDate();
        SimpleDateFormat sdfFormato = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat sdfFormatoClave = new SimpleDateFormat("ddMMyyyy");
        SimpleDateFormat sdfFormatoPerido = new SimpleDateFormat("MM/yyyy");

        dctDato = OBDal.getInstance().get(DocumentType.class, spiDato.getDocumentType().getId());

        oriDato = OBDal.getInstance().get(OrganizationInformation.class,
            spiDato.getOrganization().getId());

        shcDato = OBDal.getInstance().get(ShippingCompany.class,
            spiDato.getShippingCompany().getId());

        bspDato = OBDal.getInstance().get(BusinessPartner.class,
            shcDato.getBusinessPartner().getId());

        axmlDirMatriz = ATECOFFGenerarXmlData.methodSelDirMatriz(conn);

        String strDirMat = "";
        if (axmlDirMatriz != null && axmlDirMatriz.length == 1) {
          strDirMat = axmlDirMatriz[0].dato1;
        }

        axmlDirec = ATECOFFGenerarXmlData.methodSeleccionarDirec(conn,
            spiDato.getOrganization().getId());

        String strDir = "";
        if (axmlDirec != null && axmlDirec.length == 1) {
          strDir = axmlDirec[0].dato1;
        }

        orgDato = OBDal.getInstance().get(Organization.class, spiDato.getOrganization().getId());

        if (ATECOFF_Operacion_Auxiliares.generarCabecera(elminftri, cltDato.getAtecfeTipoambiente(),
            dctDato.getCoTipoComprobanteAutorizadorSRI().toString(), oriDato.getTaxID(),
            orgDato.getCoNroEstab(), orgDato.getCoPuntoEmision(), spiDato.getDocumentNo(),
            cltDato.getAtecfeCodinumerico(), cltDato.getAtecfeTipoemisi(),
            sdfFormatoClave.format(cldFechaIn), strDirMat, cltDato.getName(), cltDato.getName(),
            msg, hstClaveAcceso) && dctDato != null && oriDato != null && bspDato != null) {

          final Element elmcomgre = elmgre.addElement("infoGuiaRemision");

          if (!strDir.equals("")) {
            elmcomgre.addElement("dirEstablecimiento").addText(strDir);
          }

          if (!strDir.equals("")) {
            elmcomgre.addElement("dirPartida").addText(strDir);
          }

          if (bspDato.getName2() != null) {
            elmcomgre.addElement("razonSocialTransportista").addText(bspDato.getName2());
          } else if (bspDato.getName() != null) {
            elmcomgre.addElement("razonSocialTransportista").addText(bspDato.getName());
          } else {
            msg.setType("Error");
            msg.setMessage("Es necesaria la razón social del comprador");
            msg.setTitle("@Error@");
            return false;
          }

          if (bspDato.getCOTipoIdentificacion() != null) {
            String strTipoIden = bspDato.getCOTipoIdentificacion();
          }

          if (bspDato.getTaxID() != null) {
            elmcomgre.addElement("rucTransportista").addText(bspDato.getTaxID());
          } else {
            msg.setType("Error");
            msg.setMessage("Es necesaria la CI/RUC");
            msg.setTitle("@Error@");
            return false;
          }

        } else {
          return false;
        }
      }

      msg.setType("Error");
      msg.setMessage("No hay un tercero en la retención");
      msg.setTitle("@Error@");
      return false;
    } finally {
      flXml.delete();
      flXml = null;

      docXML.clearContent();
      docXML = null;
      ofFormat = null;

      dctDato = null;
      cltDato = null;
      oriDato = null;
      bspDato = null;
      shcDato = null;
      orgDato = null;

      axmlDirMatriz = null;
      axmlDirec = null;
      axmlEmail = null;

      hstClaveAcceso.clear();
    }
  }
}
