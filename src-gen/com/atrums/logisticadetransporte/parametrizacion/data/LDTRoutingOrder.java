/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2011 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.logisticadetransporte.parametrizacion.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Location;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.geography.Country;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.financialmgmt.payment.Incoterms;
import org.openbravo.model.pricing.pricelist.PriceList;
import org.openbravo.model.pricing.pricelist.PriceListVersion;
/**
 * Entity class for entity ldt_routing_order (stored in table ldt_routing_order).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class LDTRoutingOrder extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "ldt_routing_order";
    public static final String ENTITY_NAME = "ldt_routing_order";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_PROCESSED = "processed";
    public static final String PROPERTY_DOCUMENTTYPE = "documentType";
    public static final String PROPERTY_DOCUMENTNO = "documentNo";
    public static final String PROPERTY_LDTCOTIZACION = "lDTCotizacion";
    public static final String PROPERTY_FECHA = "fecha";
    public static final String PROPERTY_ESTADO = "estado";
    public static final String PROPERTY_BUSINESSPARTNER = "businessPartner";
    public static final String PROPERTY_BPARTNERRUC = "bpartnerRuc";
    public static final String PROPERTY_TELFSHIPPER = "telfShipper";
    public static final String PROPERTY_PARTNERADDRESS = "partnerAddress";
    public static final String PROPERTY_MAIL = "mail";
    public static final String PROPERTY_CLIENTE = "cliente";
    public static final String PROPERTY_CLIENTERUC = "clienteRuc";
    public static final String PROPERTY_CLIENTEDIRECCION = "clienteDireccion";
    public static final String PROPERTY_INCOTERMS = "incoterms";
    public static final String PROPERTY_MODALIDAD = "modalidad";
    public static final String PROPERTY_TIPO = "tipo";
    public static final String PROPERTY_AGENTE = "agente";
    public static final String PROPERTY_TARIFAVENTA = "tarifaVenta";
    public static final String PROPERTY_TARIFANEGOCIADA = "tarifaNegociada";
    public static final String PROPERTY_TARIFACOMPRA = "tarifaCompra";
    public static final String PROPERTY_COD = "cod";
    public static final String PROPERTY_OBSERVACION = "observacion";
    public static final String PROPERTY_VENDEDOR = "vendedor";
    public static final String PROPERTY_NUMEMBARQUE = "numembarque";
    public static final String PROPERTY_EMBARQUE = "embarque";
    public static final String PROPERTY_PRICELISTVERSION = "priceListVersion";
    public static final String PROPERTY_FECHAESTIMADA = "fechaEstimada";
    public static final String PROPERTY_PRICELISTVERSIONCOM = "pricelistVersionCom";
    public static final String PROPERTY_FINALIZAREMBARQUE = "finalizarEmbarque";
    public static final String PROPERTY_PRICELIST = "priceList";
    public static final String PROPERTY_PRICELISTCOM = "pricelistCom";
    public static final String PROPERTY_RUTA = "ruta";
    public static final String PROPERTY_USERCONTACT = "userContact";
    public static final String PROPERTY_COSTOSLOCALES = "costosLocales";
    public static final String PROPERTY_PUERTOTERMINALORIGEN = "puertoTerminalOrigen";
    public static final String PROPERTY_PUERTOTERMINALDESTINO = "puertoTerminalDestino";
    public static final String PROPERTY_LDTPUERTOTERMINAL = "lDTPuertoTerminal";
    public static final String PROPERTY_LDTCOD = "ldtCod";
    public static final String PROPERTY_LDTORDER = "ldtOrder";
    public static final String PROPERTY_LDTCOSTOAGENTE = "ldtCostoAgente";
    public static final String PROPERTY_LDTAVISORO = "lDTAvisoRo";
    public static final String PROPERTY_TOTALTVENTA = "totalTVenta";
    public static final String PROPERTY_TOTALNEGOCIADO = "totalNegociado";
    public static final String PROPERTY_BPARTNERMT = "bpartnerMt";
    public static final String PROPERTY_CONTADOR = "contador";
    public static final String PROPERTY_COSTOSLOCALESEUR = "costosLocalesEur";
    public static final String PROPERTY_COUNTRY = "country";
    public static final String PROPERTY_CURRENCY = "currency";
    public static final String PROPERTY_LDTCONTRATOPUERTO = "lDTContratoPuerto";
    public static final String PROPERTY_REFERENCIACANTIDAD = "referenciaCantidad";
    public static final String PROPERTY_TARIFANEGOCIADAEUR = "tarifaNegociadaEur";
    public static final String PROPERTY_TARIFAVENTAEUR = "tarifaVentaEur";
    public static final String PROPERTY_TOTALNEGOCIADOEUR = "totalNegociadoEur";
    public static final String PROPERTY_TOTALVENTAEUR = "totalVentaEur";
    public static final String PROPERTY_INVOICEEMLDTROAJUSTEIDLIST = "invoiceEmLdtRoAjusteIdList";
    public static final String PROPERTY_INVOICEEMLDTROUTINGORDERIDLIST = "invoiceEmLdtRoutingOrderIdList";
    public static final String PROPERTY_LDTCOTCOSTOSLOCALESLIST = "ldtCotCostosLocalesList";
    public static final String PROPERTY_LDTHBLLIST = "ldtHblList";
    public static final String PROPERTY_LDTINVEMBARQUEVLIST = "ldtInvEmbarqueVList";
    public static final String PROPERTY_LDTMBLLINEASLIST = "ldtMblLineasList";
    public static final String PROPERTY_LDTNAVIERAPRODUCTOSLIST = "ldtNavieraProductosList";
    public static final String PROPERTY_LDTPRELIQUIDACIONLIST = "ldtPreLiquidacionList";
    public static final String PROPERTY_LDTPRODUCTOSROLIST = "ldtProductosRoList";
    public static final String PROPERTY_LDTROSEGUIMIENTOLIST = "ldtRoSeguimientoList";

    public LDTRoutingOrder() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_PROCESSED, "CO");
        setDefaultValue(PROPERTY_ESTADO, "DR");
        setDefaultValue(PROPERTY_TARIFAVENTA, new BigDecimal(0.00));
        setDefaultValue(PROPERTY_TARIFANEGOCIADA, new BigDecimal(0.00));
        setDefaultValue(PROPERTY_TARIFACOMPRA, new BigDecimal(0.00));
        setDefaultValue(PROPERTY_EMBARQUE, false);
        setDefaultValue(PROPERTY_FINALIZAREMBARQUE, false);
        setDefaultValue(PROPERTY_LDTCOD, false);
        setDefaultValue(PROPERTY_LDTAVISORO, false);
        setDefaultValue(PROPERTY_COSTOSLOCALESEUR, new BigDecimal(0));
        setDefaultValue(PROPERTY_TARIFANEGOCIADAEUR, new BigDecimal(0));
        setDefaultValue(PROPERTY_TARIFAVENTAEUR, new BigDecimal(0));
        setDefaultValue(PROPERTY_TOTALNEGOCIADOEUR, new BigDecimal(0));
        setDefaultValue(PROPERTY_TOTALVENTAEUR, new BigDecimal(0));
        setDefaultValue(PROPERTY_INVOICEEMLDTROAJUSTEIDLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_INVOICEEMLDTROUTINGORDERIDLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTCOTCOSTOSLOCALESLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTHBLLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTINVEMBARQUEVLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTMBLLINEASLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTNAVIERAPRODUCTOSLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTPRELIQUIDACIONLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTPRODUCTOSROLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTROSEGUIMIENTOLIST, new ArrayList<Object>());
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public String getProcessed() {
        return (String) get(PROPERTY_PROCESSED);
    }

    public void setProcessed(String processed) {
        set(PROPERTY_PROCESSED, processed);
    }

    public DocumentType getDocumentType() {
        return (DocumentType) get(PROPERTY_DOCUMENTTYPE);
    }

    public void setDocumentType(DocumentType documentType) {
        set(PROPERTY_DOCUMENTTYPE, documentType);
    }

    public String getDocumentNo() {
        return (String) get(PROPERTY_DOCUMENTNO);
    }

    public void setDocumentNo(String documentNo) {
        set(PROPERTY_DOCUMENTNO, documentNo);
    }

    public ldtCotizacion getLDTCotizacion() {
        return (ldtCotizacion) get(PROPERTY_LDTCOTIZACION);
    }

    public void setLDTCotizacion(ldtCotizacion lDTCotizacion) {
        set(PROPERTY_LDTCOTIZACION, lDTCotizacion);
    }

    public Date getFecha() {
        return (Date) get(PROPERTY_FECHA);
    }

    public void setFecha(Date fecha) {
        set(PROPERTY_FECHA, fecha);
    }

    public String getEstado() {
        return (String) get(PROPERTY_ESTADO);
    }

    public void setEstado(String estado) {
        set(PROPERTY_ESTADO, estado);
    }

    public BusinessPartner getBusinessPartner() {
        return (BusinessPartner) get(PROPERTY_BUSINESSPARTNER);
    }

    public void setBusinessPartner(BusinessPartner businessPartner) {
        set(PROPERTY_BUSINESSPARTNER, businessPartner);
    }

    public String getBpartnerRuc() {
        return (String) get(PROPERTY_BPARTNERRUC);
    }

    public void setBpartnerRuc(String bpartnerRuc) {
        set(PROPERTY_BPARTNERRUC, bpartnerRuc);
    }

    public String getTelfShipper() {
        return (String) get(PROPERTY_TELFSHIPPER);
    }

    public void setTelfShipper(String telfShipper) {
        set(PROPERTY_TELFSHIPPER, telfShipper);
    }

    public String getPartnerAddress() {
        return (String) get(PROPERTY_PARTNERADDRESS);
    }

    public void setPartnerAddress(String partnerAddress) {
        set(PROPERTY_PARTNERADDRESS, partnerAddress);
    }

    public String getMail() {
        return (String) get(PROPERTY_MAIL);
    }

    public void setMail(String mail) {
        set(PROPERTY_MAIL, mail);
    }

    public BusinessPartner getCliente() {
        return (BusinessPartner) get(PROPERTY_CLIENTE);
    }

    public void setCliente(BusinessPartner cliente) {
        set(PROPERTY_CLIENTE, cliente);
    }

    public String getClienteRuc() {
        return (String) get(PROPERTY_CLIENTERUC);
    }

    public void setClienteRuc(String clienteRuc) {
        set(PROPERTY_CLIENTERUC, clienteRuc);
    }

    public Location getClienteDireccion() {
        return (Location) get(PROPERTY_CLIENTEDIRECCION);
    }

    public void setClienteDireccion(Location clienteDireccion) {
        set(PROPERTY_CLIENTEDIRECCION, clienteDireccion);
    }

    public Incoterms getIncoterms() {
        return (Incoterms) get(PROPERTY_INCOTERMS);
    }

    public void setIncoterms(Incoterms incoterms) {
        set(PROPERTY_INCOTERMS, incoterms);
    }

    public String getModalidad() {
        return (String) get(PROPERTY_MODALIDAD);
    }

    public void setModalidad(String modalidad) {
        set(PROPERTY_MODALIDAD, modalidad);
    }

    public String getTipo() {
        return (String) get(PROPERTY_TIPO);
    }

    public void setTipo(String tipo) {
        set(PROPERTY_TIPO, tipo);
    }

    public BusinessPartner getAgente() {
        return (BusinessPartner) get(PROPERTY_AGENTE);
    }

    public void setAgente(BusinessPartner agente) {
        set(PROPERTY_AGENTE, agente);
    }

    public BigDecimal getTarifaVenta() {
        return (BigDecimal) get(PROPERTY_TARIFAVENTA);
    }

    public void setTarifaVenta(BigDecimal tarifaVenta) {
        set(PROPERTY_TARIFAVENTA, tarifaVenta);
    }

    public BigDecimal getTarifaNegociada() {
        return (BigDecimal) get(PROPERTY_TARIFANEGOCIADA);
    }

    public void setTarifaNegociada(BigDecimal tarifaNegociada) {
        set(PROPERTY_TARIFANEGOCIADA, tarifaNegociada);
    }

    public BigDecimal getTarifaCompra() {
        return (BigDecimal) get(PROPERTY_TARIFACOMPRA);
    }

    public void setTarifaCompra(BigDecimal tarifaCompra) {
        set(PROPERTY_TARIFACOMPRA, tarifaCompra);
    }

    public BigDecimal getCod() {
        return (BigDecimal) get(PROPERTY_COD);
    }

    public void setCod(BigDecimal cod) {
        set(PROPERTY_COD, cod);
    }

    public String getObservacion() {
        return (String) get(PROPERTY_OBSERVACION);
    }

    public void setObservacion(String observacion) {
        set(PROPERTY_OBSERVACION, observacion);
    }

    public BusinessPartner getVendedor() {
        return (BusinessPartner) get(PROPERTY_VENDEDOR);
    }

    public void setVendedor(BusinessPartner vendedor) {
        set(PROPERTY_VENDEDOR, vendedor);
    }

    public String getNumembarque() {
        return (String) get(PROPERTY_NUMEMBARQUE);
    }

    public void setNumembarque(String numembarque) {
        set(PROPERTY_NUMEMBARQUE, numembarque);
    }

    public Boolean isEmbarque() {
        return (Boolean) get(PROPERTY_EMBARQUE);
    }

    public void setEmbarque(Boolean embarque) {
        set(PROPERTY_EMBARQUE, embarque);
    }

    public PriceListVersion getPriceListVersion() {
        return (PriceListVersion) get(PROPERTY_PRICELISTVERSION);
    }

    public void setPriceListVersion(PriceListVersion priceListVersion) {
        set(PROPERTY_PRICELISTVERSION, priceListVersion);
    }

    public Date getFechaEstimada() {
        return (Date) get(PROPERTY_FECHAESTIMADA);
    }

    public void setFechaEstimada(Date fechaEstimada) {
        set(PROPERTY_FECHAESTIMADA, fechaEstimada);
    }

    public String getPricelistVersionCom() {
        return (String) get(PROPERTY_PRICELISTVERSIONCOM);
    }

    public void setPricelistVersionCom(String pricelistVersionCom) {
        set(PROPERTY_PRICELISTVERSIONCOM, pricelistVersionCom);
    }

    public Boolean isFinalizarEmbarque() {
        return (Boolean) get(PROPERTY_FINALIZAREMBARQUE);
    }

    public void setFinalizarEmbarque(Boolean finalizarEmbarque) {
        set(PROPERTY_FINALIZAREMBARQUE, finalizarEmbarque);
    }

    public PriceList getPriceList() {
        return (PriceList) get(PROPERTY_PRICELIST);
    }

    public void setPriceList(PriceList priceList) {
        set(PROPERTY_PRICELIST, priceList);
    }

    public PriceList getPricelistCom() {
        return (PriceList) get(PROPERTY_PRICELISTCOM);
    }

    public void setPricelistCom(PriceList pricelistCom) {
        set(PROPERTY_PRICELISTCOM, pricelistCom);
    }

    public ldtRuta getRuta() {
        return (ldtRuta) get(PROPERTY_RUTA);
    }

    public void setRuta(ldtRuta ruta) {
        set(PROPERTY_RUTA, ruta);
    }

    public User getUserContact() {
        return (User) get(PROPERTY_USERCONTACT);
    }

    public void setUserContact(User userContact) {
        set(PROPERTY_USERCONTACT, userContact);
    }

    public BigDecimal getCostosLocales() {
        return (BigDecimal) get(PROPERTY_COSTOSLOCALES);
    }

    public void setCostosLocales(BigDecimal costosLocales) {
        set(PROPERTY_COSTOSLOCALES, costosLocales);
    }

    public ldtPuertoTerminal getPuertoTerminalOrigen() {
        return (ldtPuertoTerminal) get(PROPERTY_PUERTOTERMINALORIGEN);
    }

    public void setPuertoTerminalOrigen(ldtPuertoTerminal puertoTerminalOrigen) {
        set(PROPERTY_PUERTOTERMINALORIGEN, puertoTerminalOrigen);
    }

    public ldtPuertoTerminal getPuertoTerminalDestino() {
        return (ldtPuertoTerminal) get(PROPERTY_PUERTOTERMINALDESTINO);
    }

    public void setPuertoTerminalDestino(ldtPuertoTerminal puertoTerminalDestino) {
        set(PROPERTY_PUERTOTERMINALDESTINO, puertoTerminalDestino);
    }

    public String getLDTPuertoTerminal() {
        return (String) get(PROPERTY_LDTPUERTOTERMINAL);
    }

    public void setLDTPuertoTerminal(String lDTPuertoTerminal) {
        set(PROPERTY_LDTPUERTOTERMINAL, lDTPuertoTerminal);
    }

    public Boolean isLdtCod() {
        return (Boolean) get(PROPERTY_LDTCOD);
    }

    public void setLdtCod(Boolean ldtCod) {
        set(PROPERTY_LDTCOD, ldtCod);
    }

    public String getLdtOrder() {
        return (String) get(PROPERTY_LDTORDER);
    }

    public void setLdtOrder(String ldtOrder) {
        set(PROPERTY_LDTORDER, ldtOrder);
    }

    public BigDecimal getLdtCostoAgente() {
        return (BigDecimal) get(PROPERTY_LDTCOSTOAGENTE);
    }

    public void setLdtCostoAgente(BigDecimal ldtCostoAgente) {
        set(PROPERTY_LDTCOSTOAGENTE, ldtCostoAgente);
    }

    public Boolean isLDTAvisoRo() {
        return (Boolean) get(PROPERTY_LDTAVISORO);
    }

    public void setLDTAvisoRo(Boolean lDTAvisoRo) {
        set(PROPERTY_LDTAVISORO, lDTAvisoRo);
    }

    public BigDecimal getTotalTVenta() {
        return (BigDecimal) get(PROPERTY_TOTALTVENTA);
    }

    public void setTotalTVenta(BigDecimal totalTVenta) {
        set(PROPERTY_TOTALTVENTA, totalTVenta);
    }

    public BigDecimal getTotalNegociado() {
        return (BigDecimal) get(PROPERTY_TOTALNEGOCIADO);
    }

    public void setTotalNegociado(BigDecimal totalNegociado) {
        set(PROPERTY_TOTALNEGOCIADO, totalNegociado);
    }

    public BusinessPartner getBpartnerMt() {
        return (BusinessPartner) get(PROPERTY_BPARTNERMT);
    }

    public void setBpartnerMt(BusinessPartner bpartnerMt) {
        set(PROPERTY_BPARTNERMT, bpartnerMt);
    }

    public BigDecimal getContador() {
        return (BigDecimal) get(PROPERTY_CONTADOR);
    }

    public void setContador(BigDecimal contador) {
        set(PROPERTY_CONTADOR, contador);
    }

    public BigDecimal getCostosLocalesEur() {
        return (BigDecimal) get(PROPERTY_COSTOSLOCALESEUR);
    }

    public void setCostosLocalesEur(BigDecimal costosLocalesEur) {
        set(PROPERTY_COSTOSLOCALESEUR, costosLocalesEur);
    }

    public Country getCountry() {
        return (Country) get(PROPERTY_COUNTRY);
    }

    public void setCountry(Country country) {
        set(PROPERTY_COUNTRY, country);
    }

    public Currency getCurrency() {
        return (Currency) get(PROPERTY_CURRENCY);
    }

    public void setCurrency(Currency currency) {
        set(PROPERTY_CURRENCY, currency);
    }

    public ldtContratoPuerto getLDTContratoPuerto() {
        return (ldtContratoPuerto) get(PROPERTY_LDTCONTRATOPUERTO);
    }

    public void setLDTContratoPuerto(ldtContratoPuerto lDTContratoPuerto) {
        set(PROPERTY_LDTCONTRATOPUERTO, lDTContratoPuerto);
    }

    public BigDecimal getReferenciaCantidad() {
        return (BigDecimal) get(PROPERTY_REFERENCIACANTIDAD);
    }

    public void setReferenciaCantidad(BigDecimal referenciaCantidad) {
        set(PROPERTY_REFERENCIACANTIDAD, referenciaCantidad);
    }

    public BigDecimal getTarifaNegociadaEur() {
        return (BigDecimal) get(PROPERTY_TARIFANEGOCIADAEUR);
    }

    public void setTarifaNegociadaEur(BigDecimal tarifaNegociadaEur) {
        set(PROPERTY_TARIFANEGOCIADAEUR, tarifaNegociadaEur);
    }

    public BigDecimal getTarifaVentaEur() {
        return (BigDecimal) get(PROPERTY_TARIFAVENTAEUR);
    }

    public void setTarifaVentaEur(BigDecimal tarifaVentaEur) {
        set(PROPERTY_TARIFAVENTAEUR, tarifaVentaEur);
    }

    public BigDecimal getTotalNegociadoEur() {
        return (BigDecimal) get(PROPERTY_TOTALNEGOCIADOEUR);
    }

    public void setTotalNegociadoEur(BigDecimal totalNegociadoEur) {
        set(PROPERTY_TOTALNEGOCIADOEUR, totalNegociadoEur);
    }

    public BigDecimal getTotalVentaEur() {
        return (BigDecimal) get(PROPERTY_TOTALVENTAEUR);
    }

    public void setTotalVentaEur(BigDecimal totalVentaEur) {
        set(PROPERTY_TOTALVENTAEUR, totalVentaEur);
    }

    @SuppressWarnings("unchecked")
    public List<Invoice> getInvoiceEmLdtRoAjusteIdList() {
        return (List<Invoice>) get(PROPERTY_INVOICEEMLDTROAJUSTEIDLIST);
    }

    public void setInvoiceEmLdtRoAjusteIdList(List<Invoice> invoiceEmLdtRoAjusteIdList) {
        set(PROPERTY_INVOICEEMLDTROAJUSTEIDLIST, invoiceEmLdtRoAjusteIdList);
    }

    @SuppressWarnings("unchecked")
    public List<Invoice> getInvoiceEmLdtRoutingOrderIdList() {
        return (List<Invoice>) get(PROPERTY_INVOICEEMLDTROUTINGORDERIDLIST);
    }

    public void setInvoiceEmLdtRoutingOrderIdList(List<Invoice> invoiceEmLdtRoutingOrderIdList) {
        set(PROPERTY_INVOICEEMLDTROUTINGORDERIDLIST, invoiceEmLdtRoutingOrderIdList);
    }

    @SuppressWarnings("unchecked")
    public List<ldtCotCostosLocales> getLdtCotCostosLocalesList() {
        return (List<ldtCotCostosLocales>) get(PROPERTY_LDTCOTCOSTOSLOCALESLIST);
    }

    public void setLdtCotCostosLocalesList(List<ldtCotCostosLocales> ldtCotCostosLocalesList) {
        set(PROPERTY_LDTCOTCOSTOSLOCALESLIST, ldtCotCostosLocalesList);
    }

    @SuppressWarnings("unchecked")
    public List<LDTHbl> getLdtHblList() {
        return (List<LDTHbl>) get(PROPERTY_LDTHBLLIST);
    }

    public void setLdtHblList(List<LDTHbl> ldtHblList) {
        set(PROPERTY_LDTHBLLIST, ldtHblList);
    }

    @SuppressWarnings("unchecked")
    public List<LdtInvEmbarqueV> getLdtInvEmbarqueVList() {
        return (List<LdtInvEmbarqueV>) get(PROPERTY_LDTINVEMBARQUEVLIST);
    }

    public void setLdtInvEmbarqueVList(List<LdtInvEmbarqueV> ldtInvEmbarqueVList) {
        set(PROPERTY_LDTINVEMBARQUEVLIST, ldtInvEmbarqueVList);
    }

    @SuppressWarnings("unchecked")
    public List<LDTMblLineas> getLdtMblLineasList() {
        return (List<LDTMblLineas>) get(PROPERTY_LDTMBLLINEASLIST);
    }

    public void setLdtMblLineasList(List<LDTMblLineas> ldtMblLineasList) {
        set(PROPERTY_LDTMBLLINEASLIST, ldtMblLineasList);
    }

    @SuppressWarnings("unchecked")
    public List<LDTNavieraProductos> getLdtNavieraProductosList() {
        return (List<LDTNavieraProductos>) get(PROPERTY_LDTNAVIERAPRODUCTOSLIST);
    }

    public void setLdtNavieraProductosList(List<LDTNavieraProductos> ldtNavieraProductosList) {
        set(PROPERTY_LDTNAVIERAPRODUCTOSLIST, ldtNavieraProductosList);
    }

    @SuppressWarnings("unchecked")
    public List<ldt_pre_liquidacion> getLdtPreLiquidacionList() {
        return (List<ldt_pre_liquidacion>) get(PROPERTY_LDTPRELIQUIDACIONLIST);
    }

    public void setLdtPreLiquidacionList(List<ldt_pre_liquidacion> ldtPreLiquidacionList) {
        set(PROPERTY_LDTPRELIQUIDACIONLIST, ldtPreLiquidacionList);
    }

    @SuppressWarnings("unchecked")
    public List<LDTProductosRo> getLdtProductosRoList() {
        return (List<LDTProductosRo>) get(PROPERTY_LDTPRODUCTOSROLIST);
    }

    public void setLdtProductosRoList(List<LDTProductosRo> ldtProductosRoList) {
        set(PROPERTY_LDTPRODUCTOSROLIST, ldtProductosRoList);
    }

    @SuppressWarnings("unchecked")
    public List<LDTRoSeguimiento> getLdtRoSeguimientoList() {
        return (List<LDTRoSeguimiento>) get(PROPERTY_LDTROSEGUIMIENTOLIST);
    }

    public void setLdtRoSeguimientoList(List<LDTRoSeguimiento> ldtRoSeguimientoList) {
        set(PROPERTY_LDTROSEGUIMIENTOLIST, ldtRoSeguimientoList);
    }

}
