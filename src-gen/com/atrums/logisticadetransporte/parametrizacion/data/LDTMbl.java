/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2011 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.logisticadetransporte.parametrizacion.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceLine;
/**
 * Entity class for entity ldt_mbl (stored in table ldt_mbl).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class LDTMbl extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "ldt_mbl";
    public static final String ENTITY_NAME = "ldt_mbl";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_PROCESSED = "processed";
    public static final String PROPERTY_NUMMBL = "nUMMbl";
    public static final String PROPERTY_FECHA = "fecha";
    public static final String PROPERTY_ESTADO = "estado";
    public static final String PROPERTY_BUSINESSPARTNER = "businessPartner";
    public static final String PROPERTY_MANIFIESTO = "manifiesto";
    public static final String PROPERTY_BUQUEVUELOZARPE = "buqueVueloZarpe";
    public static final String PROPERTY_CONTAINER = "container";
    public static final String PROPERTY_PUERTOTERMINALORIGEN = "puertoTerminalOrigen";
    public static final String PROPERTY_PUERTOTERMINALDESTINO = "puertoTerminalDestino";
    public static final String PROPERTY_BODEGA = "bodega";
    public static final String PROPERTY_VALOR = "valor";
    public static final String PROPERTY_FACTURA = "factura";
    public static final String PROPERTY_INVOICE = "invoice";
    public static final String PROPERTY_HBL = "hBL";
    public static final String PROPERTY_BUQUEVUELOARRIBO = "buqueVueloArribo";
    public static final String PROPERTY_PESOTOTAL = "pesoTotal";
    public static final String PROPERTY_TOTALPIEZAS = "totalPiezas";
    public static final String PROPERTY_CONDICION = "condicion";
    public static final String PROPERTY_TOTALVOLUMEN = "totalVolumen";
    public static final String PROPERTY_SECUENCIALMBLMAWB = "secuencialMBLMAWB";
    public static final String PROPERTY_FECHAESTIMADAARRIBO = "fechaEstimadaArribo";
    public static final String PROPERTY_NOTAS = "notas";
    public static final String PROPERTY_INVOICEEMLDTMBLIDLIST = "invoiceEmLdtMblIdList";
    public static final String PROPERTY_INVOICELINEEMLDTMBLLIST = "invoiceLineEMLDTMBLList";
    public static final String PROPERTY_LDTFACMBLVLIST = "ldtFacMblVList";
    public static final String PROPERTY_LDTBUQUEMBLLIST = "ldtBuquemblList";
    public static final String PROPERTY_LDTDATOSARRIBOVLIST = "ldtDatosArriboVList";
    public static final String PROPERTY_LDTLIQUIDACIONCABECERALIST = "ldtLiquidacionCabeceraList";
    public static final String PROPERTY_LDTMBLLINEASLIST = "ldtMblLineasList";
    public static final String PROPERTY_LDTMBLSEGCONTENEDORLIST = "ldtMblsegcontenedorList";

    public LDTMbl() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_PROCESSED, "CO");
        setDefaultValue(PROPERTY_ESTADO, "DR");
        setDefaultValue(PROPERTY_VALOR, new BigDecimal(0));
        setDefaultValue(PROPERTY_FACTURA, false);
        setDefaultValue(PROPERTY_PESOTOTAL, new BigDecimal(0));
        setDefaultValue(PROPERTY_TOTALPIEZAS, new BigDecimal(0));
        setDefaultValue(PROPERTY_TOTALVOLUMEN, new BigDecimal(0));
        setDefaultValue(PROPERTY_INVOICEEMLDTMBLIDLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_INVOICELINEEMLDTMBLLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTFACMBLVLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTBUQUEMBLLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTDATOSARRIBOVLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTLIQUIDACIONCABECERALIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTMBLLINEASLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTMBLSEGCONTENEDORLIST, new ArrayList<Object>());
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public String getProcessed() {
        return (String) get(PROPERTY_PROCESSED);
    }

    public void setProcessed(String processed) {
        set(PROPERTY_PROCESSED, processed);
    }

    public String getNUMMbl() {
        return (String) get(PROPERTY_NUMMBL);
    }

    public void setNUMMbl(String nUMMbl) {
        set(PROPERTY_NUMMBL, nUMMbl);
    }

    public Date getFecha() {
        return (Date) get(PROPERTY_FECHA);
    }

    public void setFecha(Date fecha) {
        set(PROPERTY_FECHA, fecha);
    }

    public String getEstado() {
        return (String) get(PROPERTY_ESTADO);
    }

    public void setEstado(String estado) {
        set(PROPERTY_ESTADO, estado);
    }

    public BusinessPartner getBusinessPartner() {
        return (BusinessPartner) get(PROPERTY_BUSINESSPARTNER);
    }

    public void setBusinessPartner(BusinessPartner businessPartner) {
        set(PROPERTY_BUSINESSPARTNER, businessPartner);
    }

    public String getManifiesto() {
        return (String) get(PROPERTY_MANIFIESTO);
    }

    public void setManifiesto(String manifiesto) {
        set(PROPERTY_MANIFIESTO, manifiesto);
    }

    public String getBuqueVueloZarpe() {
        return (String) get(PROPERTY_BUQUEVUELOZARPE);
    }

    public void setBuqueVueloZarpe(String buqueVueloZarpe) {
        set(PROPERTY_BUQUEVUELOZARPE, buqueVueloZarpe);
    }

    public String getContainer() {
        return (String) get(PROPERTY_CONTAINER);
    }

    public void setContainer(String container) {
        set(PROPERTY_CONTAINER, container);
    }

    public ldtPuertoTerminal getPuertoTerminalOrigen() {
        return (ldtPuertoTerminal) get(PROPERTY_PUERTOTERMINALORIGEN);
    }

    public void setPuertoTerminalOrigen(ldtPuertoTerminal puertoTerminalOrigen) {
        set(PROPERTY_PUERTOTERMINALORIGEN, puertoTerminalOrigen);
    }

    public ldtPuertoTerminal getPuertoTerminalDestino() {
        return (ldtPuertoTerminal) get(PROPERTY_PUERTOTERMINALDESTINO);
    }

    public void setPuertoTerminalDestino(ldtPuertoTerminal puertoTerminalDestino) {
        set(PROPERTY_PUERTOTERMINALDESTINO, puertoTerminalDestino);
    }

    public String getBodega() {
        return (String) get(PROPERTY_BODEGA);
    }

    public void setBodega(String bodega) {
        set(PROPERTY_BODEGA, bodega);
    }

    public BigDecimal getValor() {
        return (BigDecimal) get(PROPERTY_VALOR);
    }

    public void setValor(BigDecimal valor) {
        set(PROPERTY_VALOR, valor);
    }

    public Boolean isFactura() {
        return (Boolean) get(PROPERTY_FACTURA);
    }

    public void setFactura(Boolean factura) {
        set(PROPERTY_FACTURA, factura);
    }

    public Invoice getInvoice() {
        return (Invoice) get(PROPERTY_INVOICE);
    }

    public void setInvoice(Invoice invoice) {
        set(PROPERTY_INVOICE, invoice);
    }

    public LDTHbl getHBL() {
        return (LDTHbl) get(PROPERTY_HBL);
    }

    public void setHBL(LDTHbl hBL) {
        set(PROPERTY_HBL, hBL);
    }

    public String getBuqueVueloArribo() {
        return (String) get(PROPERTY_BUQUEVUELOARRIBO);
    }

    public void setBuqueVueloArribo(String buqueVueloArribo) {
        set(PROPERTY_BUQUEVUELOARRIBO, buqueVueloArribo);
    }

    public BigDecimal getPesoTotal() {
        return (BigDecimal) get(PROPERTY_PESOTOTAL);
    }

    public void setPesoTotal(BigDecimal pesoTotal) {
        set(PROPERTY_PESOTOTAL, pesoTotal);
    }

    public BigDecimal getTotalPiezas() {
        return (BigDecimal) get(PROPERTY_TOTALPIEZAS);
    }

    public void setTotalPiezas(BigDecimal totalPiezas) {
        set(PROPERTY_TOTALPIEZAS, totalPiezas);
    }

    public String getCondicion() {
        return (String) get(PROPERTY_CONDICION);
    }

    public void setCondicion(String condicion) {
        set(PROPERTY_CONDICION, condicion);
    }

    public BigDecimal getTotalVolumen() {
        return (BigDecimal) get(PROPERTY_TOTALVOLUMEN);
    }

    public void setTotalVolumen(BigDecimal totalVolumen) {
        set(PROPERTY_TOTALVOLUMEN, totalVolumen);
    }

    public String getSecuencialMBLMAWB() {
        return (String) get(PROPERTY_SECUENCIALMBLMAWB);
    }

    public void setSecuencialMBLMAWB(String secuencialMBLMAWB) {
        set(PROPERTY_SECUENCIALMBLMAWB, secuencialMBLMAWB);
    }

    public Date getFechaEstimadaArribo() {
        return (Date) get(PROPERTY_FECHAESTIMADAARRIBO);
    }

    public void setFechaEstimadaArribo(Date fechaEstimadaArribo) {
        set(PROPERTY_FECHAESTIMADAARRIBO, fechaEstimadaArribo);
    }

    public String getNotas() {
        return (String) get(PROPERTY_NOTAS);
    }

    public void setNotas(String notas) {
        set(PROPERTY_NOTAS, notas);
    }

    @SuppressWarnings("unchecked")
    public List<Invoice> getInvoiceEmLdtMblIdList() {
        return (List<Invoice>) get(PROPERTY_INVOICEEMLDTMBLIDLIST);
    }

    public void setInvoiceEmLdtMblIdList(List<Invoice> invoiceEmLdtMblIdList) {
        set(PROPERTY_INVOICEEMLDTMBLIDLIST, invoiceEmLdtMblIdList);
    }

    @SuppressWarnings("unchecked")
    public List<InvoiceLine> getInvoiceLineEMLDTMBLList() {
        return (List<InvoiceLine>) get(PROPERTY_INVOICELINEEMLDTMBLLIST);
    }

    public void setInvoiceLineEMLDTMBLList(List<InvoiceLine> invoiceLineEMLDTMBLList) {
        set(PROPERTY_INVOICELINEEMLDTMBLLIST, invoiceLineEMLDTMBLList);
    }

    @SuppressWarnings("unchecked")
    public List<ldtFacMblV> getLdtFacMblVList() {
        return (List<ldtFacMblV>) get(PROPERTY_LDTFACMBLVLIST);
    }

    public void setLdtFacMblVList(List<ldtFacMblV> ldtFacMblVList) {
        set(PROPERTY_LDTFACMBLVLIST, ldtFacMblVList);
    }

    @SuppressWarnings("unchecked")
    public List<ldtBuqueMbl> getLdtBuquemblList() {
        return (List<ldtBuqueMbl>) get(PROPERTY_LDTBUQUEMBLLIST);
    }

    public void setLdtBuquemblList(List<ldtBuqueMbl> ldtBuquemblList) {
        set(PROPERTY_LDTBUQUEMBLLIST, ldtBuquemblList);
    }

    @SuppressWarnings("unchecked")
    public List<ldtDatosArriboV> getLdtDatosArriboVList() {
        return (List<ldtDatosArriboV>) get(PROPERTY_LDTDATOSARRIBOVLIST);
    }

    public void setLdtDatosArriboVList(List<ldtDatosArriboV> ldtDatosArriboVList) {
        set(PROPERTY_LDTDATOSARRIBOVLIST, ldtDatosArriboVList);
    }

    @SuppressWarnings("unchecked")
    public List<ldtLiquidacionCabecera> getLdtLiquidacionCabeceraList() {
        return (List<ldtLiquidacionCabecera>) get(PROPERTY_LDTLIQUIDACIONCABECERALIST);
    }

    public void setLdtLiquidacionCabeceraList(List<ldtLiquidacionCabecera> ldtLiquidacionCabeceraList) {
        set(PROPERTY_LDTLIQUIDACIONCABECERALIST, ldtLiquidacionCabeceraList);
    }

    @SuppressWarnings("unchecked")
    public List<LDTMblLineas> getLdtMblLineasList() {
        return (List<LDTMblLineas>) get(PROPERTY_LDTMBLLINEASLIST);
    }

    public void setLdtMblLineasList(List<LDTMblLineas> ldtMblLineasList) {
        set(PROPERTY_LDTMBLLINEASLIST, ldtMblLineasList);
    }

    @SuppressWarnings("unchecked")
    public List<ldtMblSegContenedor> getLdtMblsegcontenedorList() {
        return (List<ldtMblSegContenedor>) get(PROPERTY_LDTMBLSEGCONTENEDORLIST);
    }

    public void setLdtMblsegcontenedorList(List<ldtMblSegContenedor> ldtMblsegcontenedorList) {
        set(PROPERTY_LDTMBLSEGCONTENEDORLIST, ldtMblsegcontenedorList);
    }

}
