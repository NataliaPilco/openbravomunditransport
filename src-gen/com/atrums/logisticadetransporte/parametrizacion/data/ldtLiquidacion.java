/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2011 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.logisticadetransporte.parametrizacion.data;

import java.math.BigDecimal;
import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.Organization;
/**
 * Entity class for entity ldt_liquidacion (stored in table ldt_liquidacion).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class ldtLiquidacion extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "ldt_liquidacion";
    public static final String ENTITY_NAME = "ldt_liquidacion";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_LDTLIQUIDACIONCABECERA = "lDTLiquidacionCabecera";
    public static final String PROPERTY_BPARTNERCLIENTE = "bpartnerCliente";
    public static final String PROPERTY_ASESOR = "asesor";
    public static final String PROPERTY_CONTENEDOR = "contenedor";
    public static final String PROPERTY_METROSCUBICOS = "metrosCubicos";
    public static final String PROPERTY_NOHBL = "noHBL";
    public static final String PROPERTY_NUMEMBARQUE = "numembarque";
    public static final String PROPERTY_FACTHBL = "factHbl";
    public static final String PROPERTY_VALFACTFLETE = "vALFactFlete";
    public static final String PROPERTY_VALFACTCO = "vALFactCo";
    public static final String PROPERTY_FLETENAVIERA = "fleteNaviera";
    public static final String PROPERTY_THC = "thc";
    public static final String PROPERTY_LOCALESIVA = "localesIva";
    public static final String PROPERTY_OTROS1 = "otros1";
    public static final String PROPERTY_TOTAL1 = "total1";
    public static final String PROPERTY_FLETEMBL = "fleteMbl";
    public static final String PROPERTY_TOTAL2 = "total2";
    public static final String PROPERTY_ISD = "isd";
    public static final String PROPERTY_MDT = "mdt";
    public static final String PROPERTY_UTILIDAD = "utilidad";
    public static final String PROPERTY_BPARTNERAGENTE = "bpartnerAgente";
    public static final String PROPERTY_BPARTNERNAVIERA = "bpartnerNaviera";
    public static final String PROPERTY_NUMMBL = "nUMMbl";
    public static final String PROPERTY_KILOSCONT = "kilosCont";
    public static final String PROPERTY_FLETECOSTO = "fleteCosto";
    public static final String PROPERTY_FLETECOMERCIAL = "fleteComercial";
    public static final String PROPERTY_FLETENEGOCIADO = "fleteNegociado";
    public static final String PROPERTY_VALUECONT = "valueCont";
    public static final String PROPERTY_COSTOLOCALNAV = "costoLocalNav";
    public static final String PROPERTY_CAS = "cas";
    public static final String PROPERTY_COD = "cod";
    public static final String PROPERTY_AGENTE = "agente";
    public static final String PROPERTY_AGENTE40 = "agente40";
    public static final String PROPERTY_CONTENEDORES = "contenedores";
    public static final String PROPERTY_DEBITOPRO = "debitoPro";
    public static final String PROPERTY_CREDITOPRO = "creditoPro";
    public static final String PROPERTY_COSTOSORIGEN = "costosOrigen";
    public static final String PROPERTY_TXTDOCCOSTOSLOCALES = "tXTDocCostosLocales";
    public static final String PROPERTY_TXTDOCFLETNAVMBL = "tXTDocFletNavMbl";
    public static final String PROPERTY_TXTDOCCOSTOLOCALNAV = "tXTDocCostoLocalNav";
    public static final String PROPERTY_KILOSDIV = "kilosDiv";
    public static final String PROPERTY_METROSCUBICOSFINAL = "metrosCubicosFinal";
    public static final String PROPERTY_RECIBO = "recibo";
    public static final String PROPERTY_NOTACREDITO = "notaCredito";

    public ldtLiquidacion() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_METROSCUBICOS, new BigDecimal(0));
        setDefaultValue(PROPERTY_VALFACTFLETE, new BigDecimal(0));
        setDefaultValue(PROPERTY_VALFACTCO, new BigDecimal(0));
        setDefaultValue(PROPERTY_FLETENAVIERA, new BigDecimal(0));
        setDefaultValue(PROPERTY_THC, new BigDecimal(0));
        setDefaultValue(PROPERTY_LOCALESIVA, new BigDecimal(0));
        setDefaultValue(PROPERTY_OTROS1, new BigDecimal(0));
        setDefaultValue(PROPERTY_TOTAL1, new BigDecimal(0));
        setDefaultValue(PROPERTY_FLETEMBL, new BigDecimal(0));
        setDefaultValue(PROPERTY_TOTAL2, new BigDecimal(0));
        setDefaultValue(PROPERTY_ISD, new BigDecimal(0));
        setDefaultValue(PROPERTY_MDT, new BigDecimal(0));
        setDefaultValue(PROPERTY_UTILIDAD, new BigDecimal(0));
        setDefaultValue(PROPERTY_KILOSCONT, new BigDecimal(0));
        setDefaultValue(PROPERTY_FLETECOSTO, new BigDecimal(0));
        setDefaultValue(PROPERTY_FLETECOMERCIAL, new BigDecimal(0));
        setDefaultValue(PROPERTY_FLETENEGOCIADO, new BigDecimal(0));
        setDefaultValue(PROPERTY_COSTOLOCALNAV, new BigDecimal(0));
        setDefaultValue(PROPERTY_CAS, new BigDecimal(0));
        setDefaultValue(PROPERTY_COD, new BigDecimal(0));
        setDefaultValue(PROPERTY_AGENTE, new BigDecimal(0));
        setDefaultValue(PROPERTY_AGENTE40, new BigDecimal(0));
        setDefaultValue(PROPERTY_RECIBO, new BigDecimal(0));
        setDefaultValue(PROPERTY_NOTACREDITO, new BigDecimal(0));
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public ldtLiquidacionCabecera getLDTLiquidacionCabecera() {
        return (ldtLiquidacionCabecera) get(PROPERTY_LDTLIQUIDACIONCABECERA);
    }

    public void setLDTLiquidacionCabecera(ldtLiquidacionCabecera lDTLiquidacionCabecera) {
        set(PROPERTY_LDTLIQUIDACIONCABECERA, lDTLiquidacionCabecera);
    }

    public BusinessPartner getBpartnerCliente() {
        return (BusinessPartner) get(PROPERTY_BPARTNERCLIENTE);
    }

    public void setBpartnerCliente(BusinessPartner bpartnerCliente) {
        set(PROPERTY_BPARTNERCLIENTE, bpartnerCliente);
    }

    public String getAsesor() {
        return (String) get(PROPERTY_ASESOR);
    }

    public void setAsesor(String asesor) {
        set(PROPERTY_ASESOR, asesor);
    }

    public String getContenedor() {
        return (String) get(PROPERTY_CONTENEDOR);
    }

    public void setContenedor(String contenedor) {
        set(PROPERTY_CONTENEDOR, contenedor);
    }

    public BigDecimal getMetrosCubicos() {
        return (BigDecimal) get(PROPERTY_METROSCUBICOS);
    }

    public void setMetrosCubicos(BigDecimal metrosCubicos) {
        set(PROPERTY_METROSCUBICOS, metrosCubicos);
    }

    public String getNoHBL() {
        return (String) get(PROPERTY_NOHBL);
    }

    public void setNoHBL(String noHBL) {
        set(PROPERTY_NOHBL, noHBL);
    }

    public String getNumembarque() {
        return (String) get(PROPERTY_NUMEMBARQUE);
    }

    public void setNumembarque(String numembarque) {
        set(PROPERTY_NUMEMBARQUE, numembarque);
    }

    public String getFactHbl() {
        return (String) get(PROPERTY_FACTHBL);
    }

    public void setFactHbl(String factHbl) {
        set(PROPERTY_FACTHBL, factHbl);
    }

    public BigDecimal getVALFactFlete() {
        return (BigDecimal) get(PROPERTY_VALFACTFLETE);
    }

    public void setVALFactFlete(BigDecimal vALFactFlete) {
        set(PROPERTY_VALFACTFLETE, vALFactFlete);
    }

    public BigDecimal getVALFactCo() {
        return (BigDecimal) get(PROPERTY_VALFACTCO);
    }

    public void setVALFactCo(BigDecimal vALFactCo) {
        set(PROPERTY_VALFACTCO, vALFactCo);
    }

    public BigDecimal getFleteNaviera() {
        return (BigDecimal) get(PROPERTY_FLETENAVIERA);
    }

    public void setFleteNaviera(BigDecimal fleteNaviera) {
        set(PROPERTY_FLETENAVIERA, fleteNaviera);
    }

    public BigDecimal getThc() {
        return (BigDecimal) get(PROPERTY_THC);
    }

    public void setThc(BigDecimal thc) {
        set(PROPERTY_THC, thc);
    }

    public BigDecimal getLocalesIva() {
        return (BigDecimal) get(PROPERTY_LOCALESIVA);
    }

    public void setLocalesIva(BigDecimal localesIva) {
        set(PROPERTY_LOCALESIVA, localesIva);
    }

    public BigDecimal getOtros1() {
        return (BigDecimal) get(PROPERTY_OTROS1);
    }

    public void setOtros1(BigDecimal otros1) {
        set(PROPERTY_OTROS1, otros1);
    }

    public BigDecimal getTotal1() {
        return (BigDecimal) get(PROPERTY_TOTAL1);
    }

    public void setTotal1(BigDecimal total1) {
        set(PROPERTY_TOTAL1, total1);
    }

    public BigDecimal getFleteMbl() {
        return (BigDecimal) get(PROPERTY_FLETEMBL);
    }

    public void setFleteMbl(BigDecimal fleteMbl) {
        set(PROPERTY_FLETEMBL, fleteMbl);
    }

    public BigDecimal getTotal2() {
        return (BigDecimal) get(PROPERTY_TOTAL2);
    }

    public void setTotal2(BigDecimal total2) {
        set(PROPERTY_TOTAL2, total2);
    }

    public BigDecimal getIsd() {
        return (BigDecimal) get(PROPERTY_ISD);
    }

    public void setIsd(BigDecimal isd) {
        set(PROPERTY_ISD, isd);
    }

    public BigDecimal getMdt() {
        return (BigDecimal) get(PROPERTY_MDT);
    }

    public void setMdt(BigDecimal mdt) {
        set(PROPERTY_MDT, mdt);
    }

    public BigDecimal getUtilidad() {
        return (BigDecimal) get(PROPERTY_UTILIDAD);
    }

    public void setUtilidad(BigDecimal utilidad) {
        set(PROPERTY_UTILIDAD, utilidad);
    }

    public BusinessPartner getBpartnerAgente() {
        return (BusinessPartner) get(PROPERTY_BPARTNERAGENTE);
    }

    public void setBpartnerAgente(BusinessPartner bpartnerAgente) {
        set(PROPERTY_BPARTNERAGENTE, bpartnerAgente);
    }

    public BusinessPartner getBpartnerNaviera() {
        return (BusinessPartner) get(PROPERTY_BPARTNERNAVIERA);
    }

    public void setBpartnerNaviera(BusinessPartner bpartnerNaviera) {
        set(PROPERTY_BPARTNERNAVIERA, bpartnerNaviera);
    }

    public String getNUMMbl() {
        return (String) get(PROPERTY_NUMMBL);
    }

    public void setNUMMbl(String nUMMbl) {
        set(PROPERTY_NUMMBL, nUMMbl);
    }

    public BigDecimal getKilosCont() {
        return (BigDecimal) get(PROPERTY_KILOSCONT);
    }

    public void setKilosCont(BigDecimal kilosCont) {
        set(PROPERTY_KILOSCONT, kilosCont);
    }

    public BigDecimal getFleteCosto() {
        return (BigDecimal) get(PROPERTY_FLETECOSTO);
    }

    public void setFleteCosto(BigDecimal fleteCosto) {
        set(PROPERTY_FLETECOSTO, fleteCosto);
    }

    public BigDecimal getFleteComercial() {
        return (BigDecimal) get(PROPERTY_FLETECOMERCIAL);
    }

    public void setFleteComercial(BigDecimal fleteComercial) {
        set(PROPERTY_FLETECOMERCIAL, fleteComercial);
    }

    public BigDecimal getFleteNegociado() {
        return (BigDecimal) get(PROPERTY_FLETENEGOCIADO);
    }

    public void setFleteNegociado(BigDecimal fleteNegociado) {
        set(PROPERTY_FLETENEGOCIADO, fleteNegociado);
    }

    public String getValueCont() {
        return (String) get(PROPERTY_VALUECONT);
    }

    public void setValueCont(String valueCont) {
        set(PROPERTY_VALUECONT, valueCont);
    }

    public BigDecimal getCostoLocalNav() {
        return (BigDecimal) get(PROPERTY_COSTOLOCALNAV);
    }

    public void setCostoLocalNav(BigDecimal costoLocalNav) {
        set(PROPERTY_COSTOLOCALNAV, costoLocalNav);
    }

    public BigDecimal getCas() {
        return (BigDecimal) get(PROPERTY_CAS);
    }

    public void setCas(BigDecimal cas) {
        set(PROPERTY_CAS, cas);
    }

    public BigDecimal getCod() {
        return (BigDecimal) get(PROPERTY_COD);
    }

    public void setCod(BigDecimal cod) {
        set(PROPERTY_COD, cod);
    }

    public BigDecimal getAgente() {
        return (BigDecimal) get(PROPERTY_AGENTE);
    }

    public void setAgente(BigDecimal agente) {
        set(PROPERTY_AGENTE, agente);
    }

    public BigDecimal getAgente40() {
        return (BigDecimal) get(PROPERTY_AGENTE40);
    }

    public void setAgente40(BigDecimal agente40) {
        set(PROPERTY_AGENTE40, agente40);
    }

    public String getContenedores() {
        return (String) get(PROPERTY_CONTENEDORES);
    }

    public void setContenedores(String contenedores) {
        set(PROPERTY_CONTENEDORES, contenedores);
    }

    public BigDecimal getDebitoPro() {
        return (BigDecimal) get(PROPERTY_DEBITOPRO);
    }

    public void setDebitoPro(BigDecimal debitoPro) {
        set(PROPERTY_DEBITOPRO, debitoPro);
    }

    public BigDecimal getCreditoPro() {
        return (BigDecimal) get(PROPERTY_CREDITOPRO);
    }

    public void setCreditoPro(BigDecimal creditoPro) {
        set(PROPERTY_CREDITOPRO, creditoPro);
    }

    public BigDecimal getCostosOrigen() {
        return (BigDecimal) get(PROPERTY_COSTOSORIGEN);
    }

    public void setCostosOrigen(BigDecimal costosOrigen) {
        set(PROPERTY_COSTOSORIGEN, costosOrigen);
    }

    public String getTXTDocCostosLocales() {
        return (String) get(PROPERTY_TXTDOCCOSTOSLOCALES);
    }

    public void setTXTDocCostosLocales(String tXTDocCostosLocales) {
        set(PROPERTY_TXTDOCCOSTOSLOCALES, tXTDocCostosLocales);
    }

    public String getTXTDocFletNavMbl() {
        return (String) get(PROPERTY_TXTDOCFLETNAVMBL);
    }

    public void setTXTDocFletNavMbl(String tXTDocFletNavMbl) {
        set(PROPERTY_TXTDOCFLETNAVMBL, tXTDocFletNavMbl);
    }

    public String getTXTDocCostoLocalNav() {
        return (String) get(PROPERTY_TXTDOCCOSTOLOCALNAV);
    }

    public void setTXTDocCostoLocalNav(String tXTDocCostoLocalNav) {
        set(PROPERTY_TXTDOCCOSTOLOCALNAV, tXTDocCostoLocalNav);
    }

    public BigDecimal getKilosDiv() {
        return (BigDecimal) get(PROPERTY_KILOSDIV);
    }

    public void setKilosDiv(BigDecimal kilosDiv) {
        set(PROPERTY_KILOSDIV, kilosDiv);
    }

    public BigDecimal getMetrosCubicosFinal() {
        return (BigDecimal) get(PROPERTY_METROSCUBICOSFINAL);
    }

    public void setMetrosCubicosFinal(BigDecimal metrosCubicosFinal) {
        set(PROPERTY_METROSCUBICOSFINAL, metrosCubicosFinal);
    }

    public BigDecimal getRecibo() {
        return (BigDecimal) get(PROPERTY_RECIBO);
    }

    public void setRecibo(BigDecimal recibo) {
        set(PROPERTY_RECIBO, recibo);
    }

    public BigDecimal getNotaCredito() {
        return (BigDecimal) get(PROPERTY_NOTACREDITO);
    }

    public void setNotaCredito(BigDecimal notaCredito) {
        set(PROPERTY_NOTACREDITO, notaCredito);
    }

}
