/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2011 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.logisticadetransporte.parametrizacion.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.pricing.pricelist.PriceListVersion;
/**
 * Entity class for entity ldt_contrato_puerto (stored in table ldt_contrato_puerto).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class ldtContratoPuerto extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "ldt_contrato_puerto";
    public static final String ENTITY_NAME = "ldt_contrato_puerto";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_CODIGO = "codigo";
    public static final String PROPERTY_COMMERCIALNAME = "commercialName";
    public static final String PROPERTY_PRICINGPRICELISTVERSIONEMLDTCONTRATOPUERTOIDLIST = "pricingPriceListVersionEmLdtContratoPuertoIdList";
    public static final String PROPERTY_LDTCOTIZACIONNAVIERALIST = "ldtCotizacionNavieraList";
    public static final String PROPERTY_LDTPRICELISTCONTRATOVEMLDTCONTRATOPUERTOIDLIST = "ldtPricelistContratoVEmLdtContratoPuertoIdList";
    public static final String PROPERTY_LDTPRICELISTINCOTERMSVEMLDTCONTRATOPUERTOIDLIST = "ldtPricelistIncotermsVEmLdtContratoPuertoIdList";
    public static final String PROPERTY_LDTROUTINGORDERLIST = "ldtRoutingOrderList";

    public ldtContratoPuerto() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_PRICINGPRICELISTVERSIONEMLDTCONTRATOPUERTOIDLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTCOTIZACIONNAVIERALIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTPRICELISTCONTRATOVEMLDTCONTRATOPUERTOIDLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTPRICELISTINCOTERMSVEMLDTCONTRATOPUERTOIDLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTROUTINGORDERLIST, new ArrayList<Object>());
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public String getCodigo() {
        return (String) get(PROPERTY_CODIGO);
    }

    public void setCodigo(String codigo) {
        set(PROPERTY_CODIGO, codigo);
    }

    public String getCommercialName() {
        return (String) get(PROPERTY_COMMERCIALNAME);
    }

    public void setCommercialName(String commercialName) {
        set(PROPERTY_COMMERCIALNAME, commercialName);
    }

    @SuppressWarnings("unchecked")
    public List<PriceListVersion> getPricingPriceListVersionEmLdtContratoPuertoIdList() {
        return (List<PriceListVersion>) get(PROPERTY_PRICINGPRICELISTVERSIONEMLDTCONTRATOPUERTOIDLIST);
    }

    public void setPricingPriceListVersionEmLdtContratoPuertoIdList(List<PriceListVersion> pricingPriceListVersionEmLdtContratoPuertoIdList) {
        set(PROPERTY_PRICINGPRICELISTVERSIONEMLDTCONTRATOPUERTOIDLIST, pricingPriceListVersionEmLdtContratoPuertoIdList);
    }

    @SuppressWarnings("unchecked")
    public List<ldtCotizacionNaviera> getLdtCotizacionNavieraList() {
        return (List<ldtCotizacionNaviera>) get(PROPERTY_LDTCOTIZACIONNAVIERALIST);
    }

    public void setLdtCotizacionNavieraList(List<ldtCotizacionNaviera> ldtCotizacionNavieraList) {
        set(PROPERTY_LDTCOTIZACIONNAVIERALIST, ldtCotizacionNavieraList);
    }

    @SuppressWarnings("unchecked")
    public List<ldt_pricelist_contrato_v> getLdtPricelistContratoVEmLdtContratoPuertoIdList() {
        return (List<ldt_pricelist_contrato_v>) get(PROPERTY_LDTPRICELISTCONTRATOVEMLDTCONTRATOPUERTOIDLIST);
    }

    public void setLdtPricelistContratoVEmLdtContratoPuertoIdList(List<ldt_pricelist_contrato_v> ldtPricelistContratoVEmLdtContratoPuertoIdList) {
        set(PROPERTY_LDTPRICELISTCONTRATOVEMLDTCONTRATOPUERTOIDLIST, ldtPricelistContratoVEmLdtContratoPuertoIdList);
    }

    @SuppressWarnings("unchecked")
    public List<ldtPricelistIncotermsV> getLdtPricelistIncotermsVEmLdtContratoPuertoIdList() {
        return (List<ldtPricelistIncotermsV>) get(PROPERTY_LDTPRICELISTINCOTERMSVEMLDTCONTRATOPUERTOIDLIST);
    }

    public void setLdtPricelistIncotermsVEmLdtContratoPuertoIdList(List<ldtPricelistIncotermsV> ldtPricelistIncotermsVEmLdtContratoPuertoIdList) {
        set(PROPERTY_LDTPRICELISTINCOTERMSVEMLDTCONTRATOPUERTOIDLIST, ldtPricelistIncotermsVEmLdtContratoPuertoIdList);
    }

    @SuppressWarnings("unchecked")
    public List<LDTRoutingOrder> getLdtRoutingOrderList() {
        return (List<LDTRoutingOrder>) get(PROPERTY_LDTROUTINGORDERLIST);
    }

    public void setLdtRoutingOrderList(List<LDTRoutingOrder> ldtRoutingOrderList) {
        set(PROPERTY_LDTROUTINGORDERLIST, ldtRoutingOrderList);
    }

}
