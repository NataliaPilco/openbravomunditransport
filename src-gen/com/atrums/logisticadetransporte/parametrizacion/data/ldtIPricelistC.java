/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2011 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.logisticadetransporte.parametrizacion.data;

import java.math.BigDecimal;
import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.Organization;
/**
 * Entity class for entity ldt_i_pricelist_c (stored in table ldt_i_pricelist_c).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class ldtIPricelistC extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "ldt_i_pricelist_c";
    public static final String ENTITY_NAME = "ldt_i_pricelist_c";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_COMMERCIALNAME = "commercialName";
    public static final String PROPERTY_DESCRIPTION = "description";
    public static final String PROPERTY_CURRENCY = "currency";
    public static final String PROPERTY_LDTPUERTOORIGEN = "lDTPuertoOrigen";
    public static final String PROPERTY_LDTPUERTODESTINO = "lDTPuertoDestino";
    public static final String PROPERTY_LDTTRANSPORTISTA = "lDTTransportista";
    public static final String PROPERTY_NOMVERSION = "nOMVersion";
    public static final String PROPERTY_VALIDODESDE = "validoDesde";
    public static final String PROPERTY_FECHACADUCA = "fechaCaduca";
    public static final String PROPERTY_NOMBREPRODUCTO = "nombreProducto";
    public static final String PROPERTY_PRECIOESTANDAR = "precioEstandar";
    public static final String PROPERTY_PRECIOTARIFA = "precioTarifa";
    public static final String PROPERTY_OPA = "opa";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CONTRATO = "contrato";
    public static final String PROPERTY_DIASLIBRES = "diasLibres";
    public static final String PROPERTY_AGENTE = "agente";

    public ldtIPricelistC() {
        setDefaultValue(PROPERTY_ACTIVE, true);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public String getCommercialName() {
        return (String) get(PROPERTY_COMMERCIALNAME);
    }

    public void setCommercialName(String commercialName) {
        set(PROPERTY_COMMERCIALNAME, commercialName);
    }

    public String getDescription() {
        return (String) get(PROPERTY_DESCRIPTION);
    }

    public void setDescription(String description) {
        set(PROPERTY_DESCRIPTION, description);
    }

    public Currency getCurrency() {
        return (Currency) get(PROPERTY_CURRENCY);
    }

    public void setCurrency(Currency currency) {
        set(PROPERTY_CURRENCY, currency);
    }

    public ldtPuertoTerminal getLDTPuertoOrigen() {
        return (ldtPuertoTerminal) get(PROPERTY_LDTPUERTOORIGEN);
    }

    public void setLDTPuertoOrigen(ldtPuertoTerminal lDTPuertoOrigen) {
        set(PROPERTY_LDTPUERTOORIGEN, lDTPuertoOrigen);
    }

    public ldtPuertoTerminal getLDTPuertoDestino() {
        return (ldtPuertoTerminal) get(PROPERTY_LDTPUERTODESTINO);
    }

    public void setLDTPuertoDestino(ldtPuertoTerminal lDTPuertoDestino) {
        set(PROPERTY_LDTPUERTODESTINO, lDTPuertoDestino);
    }

    public String getLDTTransportista() {
        return (String) get(PROPERTY_LDTTRANSPORTISTA);
    }

    public void setLDTTransportista(String lDTTransportista) {
        set(PROPERTY_LDTTRANSPORTISTA, lDTTransportista);
    }

    public String getNOMVersion() {
        return (String) get(PROPERTY_NOMVERSION);
    }

    public void setNOMVersion(String nOMVersion) {
        set(PROPERTY_NOMVERSION, nOMVersion);
    }

    public Date getValidoDesde() {
        return (Date) get(PROPERTY_VALIDODESDE);
    }

    public void setValidoDesde(Date validoDesde) {
        set(PROPERTY_VALIDODESDE, validoDesde);
    }

    public Date getFechaCaduca() {
        return (Date) get(PROPERTY_FECHACADUCA);
    }

    public void setFechaCaduca(Date fechaCaduca) {
        set(PROPERTY_FECHACADUCA, fechaCaduca);
    }

    public String getNombreProducto() {
        return (String) get(PROPERTY_NOMBREPRODUCTO);
    }

    public void setNombreProducto(String nombreProducto) {
        set(PROPERTY_NOMBREPRODUCTO, nombreProducto);
    }

    public BigDecimal getPrecioEstandar() {
        return (BigDecimal) get(PROPERTY_PRECIOESTANDAR);
    }

    public void setPrecioEstandar(BigDecimal precioEstandar) {
        set(PROPERTY_PRECIOESTANDAR, precioEstandar);
    }

    public BigDecimal getPrecioTarifa() {
        return (BigDecimal) get(PROPERTY_PRECIOTARIFA);
    }

    public void setPrecioTarifa(BigDecimal precioTarifa) {
        set(PROPERTY_PRECIOTARIFA, precioTarifa);
    }

    public BigDecimal getOpa() {
        return (BigDecimal) get(PROPERTY_OPA);
    }

    public void setOpa(BigDecimal opa) {
        set(PROPERTY_OPA, opa);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public String getContrato() {
        return (String) get(PROPERTY_CONTRATO);
    }

    public void setContrato(String contrato) {
        set(PROPERTY_CONTRATO, contrato);
    }

    public BigDecimal getDiasLibres() {
        return (BigDecimal) get(PROPERTY_DIASLIBRES);
    }

    public void setDiasLibres(BigDecimal diasLibres) {
        set(PROPERTY_DIASLIBRES, diasLibres);
    }

    public BusinessPartner getAgente() {
        return (BusinessPartner) get(PROPERTY_AGENTE);
    }

    public void setAgente(BusinessPartner agente) {
        set(PROPERTY_AGENTE, agente);
    }

}
