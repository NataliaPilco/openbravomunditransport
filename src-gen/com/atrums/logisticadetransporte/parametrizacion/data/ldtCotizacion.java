/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2011 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.logisticadetransporte.parametrizacion.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Location;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.financialmgmt.payment.Incoterms;
/**
 * Entity class for entity ldt_cotizacion (stored in table ldt_cotizacion).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class ldtCotizacion extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "ldt_cotizacion";
    public static final String ENTITY_NAME = "ldt_cotizacion";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_PROCESSED = "processed";
    public static final String PROPERTY_DOCUMENTTYPE = "documentType";
    public static final String PROPERTY_DOCUMENTNO = "documentNo";
    public static final String PROPERTY_FECHA = "fecha";
    public static final String PROPERTY_CLIENTE = "cliente";
    public static final String PROPERTY_VENDEDOR = "vendedor";
    public static final String PROPERTY_SERVICIOCLIENTE = "servicioCliente";
    public static final String PROPERTY_PARTNERADDRESS = "partnerAddress";
    public static final String PROPERTY_DESCRIPCIN = "descripcin";
    public static final String PROPERTY_MODALIDAD = "modalidad";
    public static final String PROPERTY_TIPO = "tipo";
    public static final String PROPERTY_ESTADO = "estado";
    public static final String PROPERTY_INCOTERMS = "incoterms";
    public static final String PROPERTY_FRECUENCIA = "frecuencia";
    public static final String PROPERTY_OPCION = "opcion";
    public static final String PROPERTY_USERVENDEDOR = "userVendedor";
    public static final String PROPERTY_USERSCLIENTE = "userScliente";
    public static final String PROPERTY_GENERAROUTINGORDER = "generaRoutingOrder";
    public static final String PROPERTY_TARIFAPRODUCTOS = "tarifaProductos";
    public static final String PROPERTY_LDTCOTCOSTOSLOCALESLIST = "ldtCotCostosLocalesList";
    public static final String PROPERTY_LDTCOTIZACIONNAVIERALIST = "ldtCotizacionNavieraList";
    public static final String PROPERTY_LDTROUTINGORDERLIST = "ldtRoutingOrderList";

    public ldtCotizacion() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_PROCESSED, "CO");
        setDefaultValue(PROPERTY_ESTADO, "DR");
        setDefaultValue(PROPERTY_OPCION, false);
        setDefaultValue(PROPERTY_GENERAROUTINGORDER, false);
        setDefaultValue(PROPERTY_TARIFAPRODUCTOS, false);
        setDefaultValue(PROPERTY_LDTCOTCOSTOSLOCALESLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTCOTIZACIONNAVIERALIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTROUTINGORDERLIST, new ArrayList<Object>());
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public String getProcessed() {
        return (String) get(PROPERTY_PROCESSED);
    }

    public void setProcessed(String processed) {
        set(PROPERTY_PROCESSED, processed);
    }

    public DocumentType getDocumentType() {
        return (DocumentType) get(PROPERTY_DOCUMENTTYPE);
    }

    public void setDocumentType(DocumentType documentType) {
        set(PROPERTY_DOCUMENTTYPE, documentType);
    }

    public String getDocumentNo() {
        return (String) get(PROPERTY_DOCUMENTNO);
    }

    public void setDocumentNo(String documentNo) {
        set(PROPERTY_DOCUMENTNO, documentNo);
    }

    public Date getFecha() {
        return (Date) get(PROPERTY_FECHA);
    }

    public void setFecha(Date fecha) {
        set(PROPERTY_FECHA, fecha);
    }

    public BusinessPartner getCliente() {
        return (BusinessPartner) get(PROPERTY_CLIENTE);
    }

    public void setCliente(BusinessPartner cliente) {
        set(PROPERTY_CLIENTE, cliente);
    }

    public BusinessPartner getVendedor() {
        return (BusinessPartner) get(PROPERTY_VENDEDOR);
    }

    public void setVendedor(BusinessPartner vendedor) {
        set(PROPERTY_VENDEDOR, vendedor);
    }

    public BusinessPartner getServicioCliente() {
        return (BusinessPartner) get(PROPERTY_SERVICIOCLIENTE);
    }

    public void setServicioCliente(BusinessPartner servicioCliente) {
        set(PROPERTY_SERVICIOCLIENTE, servicioCliente);
    }

    public Location getPartnerAddress() {
        return (Location) get(PROPERTY_PARTNERADDRESS);
    }

    public void setPartnerAddress(Location partnerAddress) {
        set(PROPERTY_PARTNERADDRESS, partnerAddress);
    }

    public String getDescripcin() {
        return (String) get(PROPERTY_DESCRIPCIN);
    }

    public void setDescripcin(String descripcin) {
        set(PROPERTY_DESCRIPCIN, descripcin);
    }

    public String getModalidad() {
        return (String) get(PROPERTY_MODALIDAD);
    }

    public void setModalidad(String modalidad) {
        set(PROPERTY_MODALIDAD, modalidad);
    }

    public String getTipo() {
        return (String) get(PROPERTY_TIPO);
    }

    public void setTipo(String tipo) {
        set(PROPERTY_TIPO, tipo);
    }

    public String getEstado() {
        return (String) get(PROPERTY_ESTADO);
    }

    public void setEstado(String estado) {
        set(PROPERTY_ESTADO, estado);
    }

    public Incoterms getIncoterms() {
        return (Incoterms) get(PROPERTY_INCOTERMS);
    }

    public void setIncoterms(Incoterms incoterms) {
        set(PROPERTY_INCOTERMS, incoterms);
    }

    public String getFrecuencia() {
        return (String) get(PROPERTY_FRECUENCIA);
    }

    public void setFrecuencia(String frecuencia) {
        set(PROPERTY_FRECUENCIA, frecuencia);
    }

    public Boolean isOpcion() {
        return (Boolean) get(PROPERTY_OPCION);
    }

    public void setOpcion(Boolean opcion) {
        set(PROPERTY_OPCION, opcion);
    }

    public String getUserVendedor() {
        return (String) get(PROPERTY_USERVENDEDOR);
    }

    public void setUserVendedor(String userVendedor) {
        set(PROPERTY_USERVENDEDOR, userVendedor);
    }

    public String getUserScliente() {
        return (String) get(PROPERTY_USERSCLIENTE);
    }

    public void setUserScliente(String userScliente) {
        set(PROPERTY_USERSCLIENTE, userScliente);
    }

    public Boolean isGeneraRoutingOrder() {
        return (Boolean) get(PROPERTY_GENERAROUTINGORDER);
    }

    public void setGeneraRoutingOrder(Boolean generaRoutingOrder) {
        set(PROPERTY_GENERAROUTINGORDER, generaRoutingOrder);
    }

    public Boolean isTarifaProductos() {
        return (Boolean) get(PROPERTY_TARIFAPRODUCTOS);
    }

    public void setTarifaProductos(Boolean tarifaProductos) {
        set(PROPERTY_TARIFAPRODUCTOS, tarifaProductos);
    }

    @SuppressWarnings("unchecked")
    public List<ldtCotCostosLocales> getLdtCotCostosLocalesList() {
        return (List<ldtCotCostosLocales>) get(PROPERTY_LDTCOTCOSTOSLOCALESLIST);
    }

    public void setLdtCotCostosLocalesList(List<ldtCotCostosLocales> ldtCotCostosLocalesList) {
        set(PROPERTY_LDTCOTCOSTOSLOCALESLIST, ldtCotCostosLocalesList);
    }

    @SuppressWarnings("unchecked")
    public List<ldtCotizacionNaviera> getLdtCotizacionNavieraList() {
        return (List<ldtCotizacionNaviera>) get(PROPERTY_LDTCOTIZACIONNAVIERALIST);
    }

    public void setLdtCotizacionNavieraList(List<ldtCotizacionNaviera> ldtCotizacionNavieraList) {
        set(PROPERTY_LDTCOTIZACIONNAVIERALIST, ldtCotizacionNavieraList);
    }

    @SuppressWarnings("unchecked")
    public List<LDTRoutingOrder> getLdtRoutingOrderList() {
        return (List<LDTRoutingOrder>) get(PROPERTY_LDTROUTINGORDERLIST);
    }

    public void setLdtRoutingOrderList(List<LDTRoutingOrder> ldtRoutingOrderList) {
        set(PROPERTY_LDTROUTINGORDERLIST, ldtRoutingOrderList);
    }

}
