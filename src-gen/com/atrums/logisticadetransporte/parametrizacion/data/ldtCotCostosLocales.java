/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2011 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.logisticadetransporte.parametrizacion.data;

import java.math.BigDecimal;
import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.pricing.pricelist.PriceList;
import org.openbravo.model.pricing.pricelist.PriceListVersion;
/**
 * Entity class for entity ldt_cot_costos_locales (stored in table ldt_cot_costos_locales).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class ldtCotCostosLocales extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "ldt_cot_costos_locales";
    public static final String ENTITY_NAME = "ldt_cot_costos_locales";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_PRICELISTVERSION = "priceListVersion";
    public static final String PROPERTY_PRODUCT = "product";
    public static final String PROPERTY_CANTIDAD = "cantidad";
    public static final String PROPERTY_PRECIOUNIT = "precioUnit";
    public static final String PROPERTY_TOTALOPCION = "totalOpcion";
    public static final String PROPERTY_WAREHOUSE = "warehouse";
    public static final String PROPERTY_LDTCOTIZACION = "lDTCotizacion";
    public static final String PROPERTY_PRICELIST = "pricelist";
    public static final String PROPERTY_PRECIOSTD = "precioStd";
    public static final String PROPERTY_LDTTIPOUNIDAD = "lDTTipoUnidad";
    public static final String PROPERTY_LDTMINIMO = "ldtMinimo";
    public static final String PROPERTY_LDTADROL = "ldtAdRol";
    public static final String PROPERTY_LDTROUTINGORDER = "lDTRoutingOrder";
    public static final String PROPERTY_CURRENCY = "currency";
    public static final String PROPERTY_EMPLEADO = "empleado";

    public ldtCotCostosLocales() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_PRECIOUNIT, new BigDecimal(0));
        setDefaultValue(PROPERTY_TOTALOPCION, new BigDecimal(0.00));
        setDefaultValue(PROPERTY_PRECIOSTD, new BigDecimal(0));
        setDefaultValue(PROPERTY_LDTMINIMO, new BigDecimal(0));
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public PriceListVersion getPriceListVersion() {
        return (PriceListVersion) get(PROPERTY_PRICELISTVERSION);
    }

    public void setPriceListVersion(PriceListVersion priceListVersion) {
        set(PROPERTY_PRICELISTVERSION, priceListVersion);
    }

    public Product getProduct() {
        return (Product) get(PROPERTY_PRODUCT);
    }

    public void setProduct(Product product) {
        set(PROPERTY_PRODUCT, product);
    }

    public BigDecimal getCantidad() {
        return (BigDecimal) get(PROPERTY_CANTIDAD);
    }

    public void setCantidad(BigDecimal cantidad) {
        set(PROPERTY_CANTIDAD, cantidad);
    }

    public BigDecimal getPrecioUnit() {
        return (BigDecimal) get(PROPERTY_PRECIOUNIT);
    }

    public void setPrecioUnit(BigDecimal precioUnit) {
        set(PROPERTY_PRECIOUNIT, precioUnit);
    }

    public BigDecimal getTotalOpcion() {
        return (BigDecimal) get(PROPERTY_TOTALOPCION);
    }

    public void setTotalOpcion(BigDecimal totalOpcion) {
        set(PROPERTY_TOTALOPCION, totalOpcion);
    }

    public Warehouse getWarehouse() {
        return (Warehouse) get(PROPERTY_WAREHOUSE);
    }

    public void setWarehouse(Warehouse warehouse) {
        set(PROPERTY_WAREHOUSE, warehouse);
    }

    public ldtCotizacion getLDTCotizacion() {
        return (ldtCotizacion) get(PROPERTY_LDTCOTIZACION);
    }

    public void setLDTCotizacion(ldtCotizacion lDTCotizacion) {
        set(PROPERTY_LDTCOTIZACION, lDTCotizacion);
    }

    public PriceList getPricelist() {
        return (PriceList) get(PROPERTY_PRICELIST);
    }

    public void setPricelist(PriceList pricelist) {
        set(PROPERTY_PRICELIST, pricelist);
    }

    public BigDecimal getPrecioStd() {
        return (BigDecimal) get(PROPERTY_PRECIOSTD);
    }

    public void setPrecioStd(BigDecimal precioStd) {
        set(PROPERTY_PRECIOSTD, precioStd);
    }

    public String getLDTTipoUnidad() {
        return (String) get(PROPERTY_LDTTIPOUNIDAD);
    }

    public void setLDTTipoUnidad(String lDTTipoUnidad) {
        set(PROPERTY_LDTTIPOUNIDAD, lDTTipoUnidad);
    }

    public BigDecimal getLdtMinimo() {
        return (BigDecimal) get(PROPERTY_LDTMINIMO);
    }

    public void setLdtMinimo(BigDecimal ldtMinimo) {
        set(PROPERTY_LDTMINIMO, ldtMinimo);
    }

    public String getLdtAdRol() {
        return (String) get(PROPERTY_LDTADROL);
    }

    public void setLdtAdRol(String ldtAdRol) {
        set(PROPERTY_LDTADROL, ldtAdRol);
    }

    public LDTRoutingOrder getLDTRoutingOrder() {
        return (LDTRoutingOrder) get(PROPERTY_LDTROUTINGORDER);
    }

    public void setLDTRoutingOrder(LDTRoutingOrder lDTRoutingOrder) {
        set(PROPERTY_LDTROUTINGORDER, lDTRoutingOrder);
    }

    public Currency getCurrency() {
        return (Currency) get(PROPERTY_CURRENCY);
    }

    public void setCurrency(Currency currency) {
        set(PROPERTY_CURRENCY, currency);
    }

    public BusinessPartner getEmpleado() {
        return (BusinessPartner) get(PROPERTY_EMPLEADO);
    }

    public void setEmpleado(BusinessPartner empleado) {
        set(PROPERTY_EMPLEADO, empleado);
    }

}
