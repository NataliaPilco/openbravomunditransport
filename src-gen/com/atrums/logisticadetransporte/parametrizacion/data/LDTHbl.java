/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2011 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.logisticadetransporte.parametrizacion.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceLine;
/**
 * Entity class for entity ldt_hbl (stored in table ldt_hbl).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class LDTHbl extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "ldt_hbl";
    public static final String ENTITY_NAME = "ldt_hbl";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_PROCESSED = "processed";
    public static final String PROPERTY_NUMHBL = "nUMHbl";
    public static final String PROPERTY_LDTROUTINGORDER = "lDTRoutingOrder";
    public static final String PROPERTY_BUSINESSPARTNER = "businessPartner";
    public static final String PROPERTY_ESTADO = "estado";
    public static final String PROPERTY_CARTAS = "cartas";
    public static final String PROPERTY_VALORFLETE = "valorFlete";
    public static final String PROPERTY_AVISOARRIBO = "avisoarribo";
    public static final String PROPERTY_FACTURA = "factura";
    public static final String PROPERTY_INVOICE = "invoice";
    public static final String PROPERTY_SECUENCIALHBLHAWB = "secuencialHBLHAWB";
    public static final String PROPERTY_FLETEPP = "fletePp";
    public static final String PROPERTY_LFTFH = "lftFh";
    public static final String PROPERTY_CALCULAR = "calcular";
    public static final String PROPERTY_FLETEPPEUR = "fletePpEur";
    public static final String PROPERTY_VALORFLETEEUR = "valorFleteEur";
    public static final String PROPERTY_VALORTOTALUSD = "valorTotalUsd";
    public static final String PROPERTY_INVOICELINEEMLDTHBLIDLIST = "invoiceLineEmLdtHblIdList";
    public static final String PROPERTY_LDTFACMBLVLIST = "ldtFacMblVList";
    public static final String PROPERTY_LDTBOOKINGLIST = "ldtBookingList";
    public static final String PROPERTY_LDTHBLLINEALIST = "ldtHblLineaList";
    public static final String PROPERTY_LDTMBLLIST = "ldtMblList";
    public static final String PROPERTY_LDTMBLLINEASLIST = "ldtMblLineasList";
    public static final String PROPERTY_LDTMBLSEGCONTENEDORLIST = "ldtMblsegcontenedorList";

    public LDTHbl() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_PROCESSED, "CO");
        setDefaultValue(PROPERTY_ESTADO, "DR");
        setDefaultValue(PROPERTY_CARTAS, false);
        setDefaultValue(PROPERTY_VALORFLETE, new BigDecimal(0));
        setDefaultValue(PROPERTY_AVISOARRIBO, true);
        setDefaultValue(PROPERTY_FACTURA, false);
        setDefaultValue(PROPERTY_FLETEPP, new BigDecimal(0));
        setDefaultValue(PROPERTY_CALCULAR, false);
        setDefaultValue(PROPERTY_FLETEPPEUR, new BigDecimal(0));
        setDefaultValue(PROPERTY_VALORFLETEEUR, new BigDecimal(0));
        setDefaultValue(PROPERTY_VALORTOTALUSD, new BigDecimal(0));
        setDefaultValue(PROPERTY_INVOICELINEEMLDTHBLIDLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTFACMBLVLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTBOOKINGLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTHBLLINEALIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTMBLLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTMBLLINEASLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTMBLSEGCONTENEDORLIST, new ArrayList<Object>());
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public String getProcessed() {
        return (String) get(PROPERTY_PROCESSED);
    }

    public void setProcessed(String processed) {
        set(PROPERTY_PROCESSED, processed);
    }

    public String getNUMHbl() {
        return (String) get(PROPERTY_NUMHBL);
    }

    public void setNUMHbl(String nUMHbl) {
        set(PROPERTY_NUMHBL, nUMHbl);
    }

    public LDTRoutingOrder getLDTRoutingOrder() {
        return (LDTRoutingOrder) get(PROPERTY_LDTROUTINGORDER);
    }

    public void setLDTRoutingOrder(LDTRoutingOrder lDTRoutingOrder) {
        set(PROPERTY_LDTROUTINGORDER, lDTRoutingOrder);
    }

    public BusinessPartner getBusinessPartner() {
        return (BusinessPartner) get(PROPERTY_BUSINESSPARTNER);
    }

    public void setBusinessPartner(BusinessPartner businessPartner) {
        set(PROPERTY_BUSINESSPARTNER, businessPartner);
    }

    public String getEstado() {
        return (String) get(PROPERTY_ESTADO);
    }

    public void setEstado(String estado) {
        set(PROPERTY_ESTADO, estado);
    }

    public Boolean isCartas() {
        return (Boolean) get(PROPERTY_CARTAS);
    }

    public void setCartas(Boolean cartas) {
        set(PROPERTY_CARTAS, cartas);
    }

    public BigDecimal getValorFlete() {
        return (BigDecimal) get(PROPERTY_VALORFLETE);
    }

    public void setValorFlete(BigDecimal valorFlete) {
        set(PROPERTY_VALORFLETE, valorFlete);
    }

    public Boolean isAvisoarribo() {
        return (Boolean) get(PROPERTY_AVISOARRIBO);
    }

    public void setAvisoarribo(Boolean avisoarribo) {
        set(PROPERTY_AVISOARRIBO, avisoarribo);
    }

    public Boolean isFactura() {
        return (Boolean) get(PROPERTY_FACTURA);
    }

    public void setFactura(Boolean factura) {
        set(PROPERTY_FACTURA, factura);
    }

    public Invoice getInvoice() {
        return (Invoice) get(PROPERTY_INVOICE);
    }

    public void setInvoice(Invoice invoice) {
        set(PROPERTY_INVOICE, invoice);
    }

    public String getSecuencialHBLHAWB() {
        return (String) get(PROPERTY_SECUENCIALHBLHAWB);
    }

    public void setSecuencialHBLHAWB(String secuencialHBLHAWB) {
        set(PROPERTY_SECUENCIALHBLHAWB, secuencialHBLHAWB);
    }

    public BigDecimal getFletePp() {
        return (BigDecimal) get(PROPERTY_FLETEPP);
    }

    public void setFletePp(BigDecimal fletePp) {
        set(PROPERTY_FLETEPP, fletePp);
    }

    public Boolean isLftFh() {
        return (Boolean) get(PROPERTY_LFTFH);
    }

    public void setLftFh(Boolean lftFh) {
        set(PROPERTY_LFTFH, lftFh);
    }

    public Boolean isCalcular() {
        return (Boolean) get(PROPERTY_CALCULAR);
    }

    public void setCalcular(Boolean calcular) {
        set(PROPERTY_CALCULAR, calcular);
    }

    public BigDecimal getFletePpEur() {
        return (BigDecimal) get(PROPERTY_FLETEPPEUR);
    }

    public void setFletePpEur(BigDecimal fletePpEur) {
        set(PROPERTY_FLETEPPEUR, fletePpEur);
    }

    public BigDecimal getValorFleteEur() {
        return (BigDecimal) get(PROPERTY_VALORFLETEEUR);
    }

    public void setValorFleteEur(BigDecimal valorFleteEur) {
        set(PROPERTY_VALORFLETEEUR, valorFleteEur);
    }

    public BigDecimal getValorTotalUsd() {
        return (BigDecimal) get(PROPERTY_VALORTOTALUSD);
    }

    public void setValorTotalUsd(BigDecimal valorTotalUsd) {
        set(PROPERTY_VALORTOTALUSD, valorTotalUsd);
    }

    @SuppressWarnings("unchecked")
    public List<InvoiceLine> getInvoiceLineEmLdtHblIdList() {
        return (List<InvoiceLine>) get(PROPERTY_INVOICELINEEMLDTHBLIDLIST);
    }

    public void setInvoiceLineEmLdtHblIdList(List<InvoiceLine> invoiceLineEmLdtHblIdList) {
        set(PROPERTY_INVOICELINEEMLDTHBLIDLIST, invoiceLineEmLdtHblIdList);
    }

    @SuppressWarnings("unchecked")
    public List<ldtFacMblV> getLdtFacMblVList() {
        return (List<ldtFacMblV>) get(PROPERTY_LDTFACMBLVLIST);
    }

    public void setLdtFacMblVList(List<ldtFacMblV> ldtFacMblVList) {
        set(PROPERTY_LDTFACMBLVLIST, ldtFacMblVList);
    }

    @SuppressWarnings("unchecked")
    public List<ldtBooking> getLdtBookingList() {
        return (List<ldtBooking>) get(PROPERTY_LDTBOOKINGLIST);
    }

    public void setLdtBookingList(List<ldtBooking> ldtBookingList) {
        set(PROPERTY_LDTBOOKINGLIST, ldtBookingList);
    }

    @SuppressWarnings("unchecked")
    public List<LDTHblLinea> getLdtHblLineaList() {
        return (List<LDTHblLinea>) get(PROPERTY_LDTHBLLINEALIST);
    }

    public void setLdtHblLineaList(List<LDTHblLinea> ldtHblLineaList) {
        set(PROPERTY_LDTHBLLINEALIST, ldtHblLineaList);
    }

    @SuppressWarnings("unchecked")
    public List<LDTMbl> getLdtMblList() {
        return (List<LDTMbl>) get(PROPERTY_LDTMBLLIST);
    }

    public void setLdtMblList(List<LDTMbl> ldtMblList) {
        set(PROPERTY_LDTMBLLIST, ldtMblList);
    }

    @SuppressWarnings("unchecked")
    public List<LDTMblLineas> getLdtMblLineasList() {
        return (List<LDTMblLineas>) get(PROPERTY_LDTMBLLINEASLIST);
    }

    public void setLdtMblLineasList(List<LDTMblLineas> ldtMblLineasList) {
        set(PROPERTY_LDTMBLLINEASLIST, ldtMblLineasList);
    }

    @SuppressWarnings("unchecked")
    public List<ldtMblSegContenedor> getLdtMblsegcontenedorList() {
        return (List<ldtMblSegContenedor>) get(PROPERTY_LDTMBLSEGCONTENEDORLIST);
    }

    public void setLdtMblsegcontenedorList(List<ldtMblSegContenedor> ldtMblsegcontenedorList) {
        set(PROPERTY_LDTMBLSEGCONTENEDORLIST, ldtMblsegcontenedorList);
    }

}
