/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2011 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.logisticadetransporte.parametrizacion.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.geography.Country;
import org.openbravo.model.pricing.pricelist.PriceList;
/**
 * Entity class for entity ldt_puerto_terminal (stored in table ldt_puerto_terminal).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class ldtPuertoTerminal extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "ldt_puerto_terminal";
    public static final String ENTITY_NAME = "ldt_puerto_terminal";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_COUNTRY = "country";
    public static final String PROPERTY_CODIGO = "codigo";
    public static final String PROPERTY_NAME = "name";
    public static final String PROPERTY_AEROPUERTO = "aeropuerto";
    public static final String PROPERTY_PUERTO = "puerto";
    public static final String PROPERTY_BASEPORT = "basePort";
    public static final String PROPERTY_FEEDERPORT = "feederPort";
    public static final String PROPERTY_PRICINGPRICELISTEMLDTPUERTOORIGENIDLIST = "pricingPriceListEMLdtPuertoOrigenIDList";
    public static final String PROPERTY_PRICINGPRICELISTEMLDTPUERTODESTINOIDLIST = "pricingPriceListEMLdtPuertoDestinoIDList";
    public static final String PROPERTY_LDTCOTIZACIONNAVIERAPUERTOTERMINALORIGENLIST = "ldtCotizacionNavieraPuertoTerminalOrigenList";
    public static final String PROPERTY_LDTCOTIZACIONNAVIERAPUERTOTERMINALDESTINOLIST = "ldtCotizacionNavieraPuertoTerminalDestinoList";
    public static final String PROPERTY_LDTIPRICELISTLDTPUERTOORIGENIDLIST = "ldtIPricelistLDTPuertoOrigenIDList";
    public static final String PROPERTY_LDTIPRICELISTLDTPUERTODESTINOIDLIST = "ldtIPricelistLDTPuertoDestinoIDList";
    public static final String PROPERTY_LDTIPRICELISTCLDTPUERTOORIGENIDLIST = "ldtIPricelistCLDTPuertoOrigenIDList";
    public static final String PROPERTY_LDTIPRICELISTCLDTPUERTODESTINOIDLIST = "ldtIPricelistCLDTPuertoDestinoIDList";
    public static final String PROPERTY_LDTMBLPUERTOTERMINALORIGENLIST = "ldtMblPuertoTerminalOrigenList";
    public static final String PROPERTY_LDTMBLPUERTOTERMINALDESTINOLIST = "ldtMblPuertoTerminalDestinoList";
    public static final String PROPERTY_LDTNAVIERAPRODUCTOSLDTPUERTOORIGENIDLIST = "ldtNavieraProductosLDTPuertoOrigenIDList";
    public static final String PROPERTY_LDTPRICELISTCONTRATOVPUERTOTERMINALORIGENLIST = "ldtPricelistContratoVPuertoTerminalOrigenList";
    public static final String PROPERTY_LDTPRICELISTCONTRATOVPUERTOTERMINALDESTINOLIST = "ldtPricelistContratoVPuertoTerminalDestinoList";
    public static final String PROPERTY_LDTROUTINGORDERPUERTOTERMINALORIGENIDLIST = "ldtRoutingOrderPuertoTerminalOrigenIDList";
    public static final String PROPERTY_LDTROUTINGORDERPUERTOTERMINALDESTINOIDLIST = "ldtRoutingOrderPuertoTerminalDestinoIDList";
    public static final String PROPERTY_LDTRUTAPUERTOTERMINALORIGENLIST = "ldtRutaPuertoTerminalOrigenList";
    public static final String PROPERTY_LDTRUTAPUERTOTERMINALDESTINOLIST = "ldtRutaPuertoTerminalDestinoList";

    public ldtPuertoTerminal() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_AEROPUERTO, false);
        setDefaultValue(PROPERTY_PUERTO, false);
        setDefaultValue(PROPERTY_BASEPORT, false);
        setDefaultValue(PROPERTY_FEEDERPORT, false);
        setDefaultValue(PROPERTY_PRICINGPRICELISTEMLDTPUERTOORIGENIDLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_PRICINGPRICELISTEMLDTPUERTODESTINOIDLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTCOTIZACIONNAVIERAPUERTOTERMINALORIGENLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTCOTIZACIONNAVIERAPUERTOTERMINALDESTINOLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTIPRICELISTLDTPUERTOORIGENIDLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTIPRICELISTLDTPUERTODESTINOIDLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTIPRICELISTCLDTPUERTOORIGENIDLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTIPRICELISTCLDTPUERTODESTINOIDLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTMBLPUERTOTERMINALORIGENLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTMBLPUERTOTERMINALDESTINOLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTNAVIERAPRODUCTOSLDTPUERTOORIGENIDLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTPRICELISTCONTRATOVPUERTOTERMINALORIGENLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTPRICELISTCONTRATOVPUERTOTERMINALDESTINOLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTROUTINGORDERPUERTOTERMINALORIGENIDLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTROUTINGORDERPUERTOTERMINALDESTINOIDLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTRUTAPUERTOTERMINALORIGENLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTRUTAPUERTOTERMINALDESTINOLIST, new ArrayList<Object>());
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public Country getCountry() {
        return (Country) get(PROPERTY_COUNTRY);
    }

    public void setCountry(Country country) {
        set(PROPERTY_COUNTRY, country);
    }

    public String getCodigo() {
        return (String) get(PROPERTY_CODIGO);
    }

    public void setCodigo(String codigo) {
        set(PROPERTY_CODIGO, codigo);
    }

    public String getName() {
        return (String) get(PROPERTY_NAME);
    }

    public void setName(String name) {
        set(PROPERTY_NAME, name);
    }

    public Boolean isAeropuerto() {
        return (Boolean) get(PROPERTY_AEROPUERTO);
    }

    public void setAeropuerto(Boolean aeropuerto) {
        set(PROPERTY_AEROPUERTO, aeropuerto);
    }

    public Boolean isPuerto() {
        return (Boolean) get(PROPERTY_PUERTO);
    }

    public void setPuerto(Boolean puerto) {
        set(PROPERTY_PUERTO, puerto);
    }

    public Boolean isBasePort() {
        return (Boolean) get(PROPERTY_BASEPORT);
    }

    public void setBasePort(Boolean basePort) {
        set(PROPERTY_BASEPORT, basePort);
    }

    public Boolean isFeederPort() {
        return (Boolean) get(PROPERTY_FEEDERPORT);
    }

    public void setFeederPort(Boolean feederPort) {
        set(PROPERTY_FEEDERPORT, feederPort);
    }

    @SuppressWarnings("unchecked")
    public List<PriceList> getPricingPriceListEMLdtPuertoOrigenIDList() {
        return (List<PriceList>) get(PROPERTY_PRICINGPRICELISTEMLDTPUERTOORIGENIDLIST);
    }

    public void setPricingPriceListEMLdtPuertoOrigenIDList(List<PriceList> pricingPriceListEMLdtPuertoOrigenIDList) {
        set(PROPERTY_PRICINGPRICELISTEMLDTPUERTOORIGENIDLIST, pricingPriceListEMLdtPuertoOrigenIDList);
    }

    @SuppressWarnings("unchecked")
    public List<PriceList> getPricingPriceListEMLdtPuertoDestinoIDList() {
        return (List<PriceList>) get(PROPERTY_PRICINGPRICELISTEMLDTPUERTODESTINOIDLIST);
    }

    public void setPricingPriceListEMLdtPuertoDestinoIDList(List<PriceList> pricingPriceListEMLdtPuertoDestinoIDList) {
        set(PROPERTY_PRICINGPRICELISTEMLDTPUERTODESTINOIDLIST, pricingPriceListEMLdtPuertoDestinoIDList);
    }

    @SuppressWarnings("unchecked")
    public List<ldtCotizacionNaviera> getLdtCotizacionNavieraPuertoTerminalOrigenList() {
        return (List<ldtCotizacionNaviera>) get(PROPERTY_LDTCOTIZACIONNAVIERAPUERTOTERMINALORIGENLIST);
    }

    public void setLdtCotizacionNavieraPuertoTerminalOrigenList(List<ldtCotizacionNaviera> ldtCotizacionNavieraPuertoTerminalOrigenList) {
        set(PROPERTY_LDTCOTIZACIONNAVIERAPUERTOTERMINALORIGENLIST, ldtCotizacionNavieraPuertoTerminalOrigenList);
    }

    @SuppressWarnings("unchecked")
    public List<ldtCotizacionNaviera> getLdtCotizacionNavieraPuertoTerminalDestinoList() {
        return (List<ldtCotizacionNaviera>) get(PROPERTY_LDTCOTIZACIONNAVIERAPUERTOTERMINALDESTINOLIST);
    }

    public void setLdtCotizacionNavieraPuertoTerminalDestinoList(List<ldtCotizacionNaviera> ldtCotizacionNavieraPuertoTerminalDestinoList) {
        set(PROPERTY_LDTCOTIZACIONNAVIERAPUERTOTERMINALDESTINOLIST, ldtCotizacionNavieraPuertoTerminalDestinoList);
    }

    @SuppressWarnings("unchecked")
    public List<ldt_i_pricelist> getLdtIPricelistLDTPuertoOrigenIDList() {
        return (List<ldt_i_pricelist>) get(PROPERTY_LDTIPRICELISTLDTPUERTOORIGENIDLIST);
    }

    public void setLdtIPricelistLDTPuertoOrigenIDList(List<ldt_i_pricelist> ldtIPricelistLDTPuertoOrigenIDList) {
        set(PROPERTY_LDTIPRICELISTLDTPUERTOORIGENIDLIST, ldtIPricelistLDTPuertoOrigenIDList);
    }

    @SuppressWarnings("unchecked")
    public List<ldt_i_pricelist> getLdtIPricelistLDTPuertoDestinoIDList() {
        return (List<ldt_i_pricelist>) get(PROPERTY_LDTIPRICELISTLDTPUERTODESTINOIDLIST);
    }

    public void setLdtIPricelistLDTPuertoDestinoIDList(List<ldt_i_pricelist> ldtIPricelistLDTPuertoDestinoIDList) {
        set(PROPERTY_LDTIPRICELISTLDTPUERTODESTINOIDLIST, ldtIPricelistLDTPuertoDestinoIDList);
    }

    @SuppressWarnings("unchecked")
    public List<ldtIPricelistC> getLdtIPricelistCLDTPuertoOrigenIDList() {
        return (List<ldtIPricelistC>) get(PROPERTY_LDTIPRICELISTCLDTPUERTOORIGENIDLIST);
    }

    public void setLdtIPricelistCLDTPuertoOrigenIDList(List<ldtIPricelistC> ldtIPricelistCLDTPuertoOrigenIDList) {
        set(PROPERTY_LDTIPRICELISTCLDTPUERTOORIGENIDLIST, ldtIPricelistCLDTPuertoOrigenIDList);
    }

    @SuppressWarnings("unchecked")
    public List<ldtIPricelistC> getLdtIPricelistCLDTPuertoDestinoIDList() {
        return (List<ldtIPricelistC>) get(PROPERTY_LDTIPRICELISTCLDTPUERTODESTINOIDLIST);
    }

    public void setLdtIPricelistCLDTPuertoDestinoIDList(List<ldtIPricelistC> ldtIPricelistCLDTPuertoDestinoIDList) {
        set(PROPERTY_LDTIPRICELISTCLDTPUERTODESTINOIDLIST, ldtIPricelistCLDTPuertoDestinoIDList);
    }

    @SuppressWarnings("unchecked")
    public List<LDTMbl> getLdtMblPuertoTerminalOrigenList() {
        return (List<LDTMbl>) get(PROPERTY_LDTMBLPUERTOTERMINALORIGENLIST);
    }

    public void setLdtMblPuertoTerminalOrigenList(List<LDTMbl> ldtMblPuertoTerminalOrigenList) {
        set(PROPERTY_LDTMBLPUERTOTERMINALORIGENLIST, ldtMblPuertoTerminalOrigenList);
    }

    @SuppressWarnings("unchecked")
    public List<LDTMbl> getLdtMblPuertoTerminalDestinoList() {
        return (List<LDTMbl>) get(PROPERTY_LDTMBLPUERTOTERMINALDESTINOLIST);
    }

    public void setLdtMblPuertoTerminalDestinoList(List<LDTMbl> ldtMblPuertoTerminalDestinoList) {
        set(PROPERTY_LDTMBLPUERTOTERMINALDESTINOLIST, ldtMblPuertoTerminalDestinoList);
    }

    @SuppressWarnings("unchecked")
    public List<LDTNavieraProductos> getLdtNavieraProductosLDTPuertoOrigenIDList() {
        return (List<LDTNavieraProductos>) get(PROPERTY_LDTNAVIERAPRODUCTOSLDTPUERTOORIGENIDLIST);
    }

    public void setLdtNavieraProductosLDTPuertoOrigenIDList(List<LDTNavieraProductos> ldtNavieraProductosLDTPuertoOrigenIDList) {
        set(PROPERTY_LDTNAVIERAPRODUCTOSLDTPUERTOORIGENIDLIST, ldtNavieraProductosLDTPuertoOrigenIDList);
    }

    @SuppressWarnings("unchecked")
    public List<ldt_pricelist_contrato_v> getLdtPricelistContratoVPuertoTerminalOrigenList() {
        return (List<ldt_pricelist_contrato_v>) get(PROPERTY_LDTPRICELISTCONTRATOVPUERTOTERMINALORIGENLIST);
    }

    public void setLdtPricelistContratoVPuertoTerminalOrigenList(List<ldt_pricelist_contrato_v> ldtPricelistContratoVPuertoTerminalOrigenList) {
        set(PROPERTY_LDTPRICELISTCONTRATOVPUERTOTERMINALORIGENLIST, ldtPricelistContratoVPuertoTerminalOrigenList);
    }

    @SuppressWarnings("unchecked")
    public List<ldt_pricelist_contrato_v> getLdtPricelistContratoVPuertoTerminalDestinoList() {
        return (List<ldt_pricelist_contrato_v>) get(PROPERTY_LDTPRICELISTCONTRATOVPUERTOTERMINALDESTINOLIST);
    }

    public void setLdtPricelistContratoVPuertoTerminalDestinoList(List<ldt_pricelist_contrato_v> ldtPricelistContratoVPuertoTerminalDestinoList) {
        set(PROPERTY_LDTPRICELISTCONTRATOVPUERTOTERMINALDESTINOLIST, ldtPricelistContratoVPuertoTerminalDestinoList);
    }

    @SuppressWarnings("unchecked")
    public List<LDTRoutingOrder> getLdtRoutingOrderPuertoTerminalOrigenIDList() {
        return (List<LDTRoutingOrder>) get(PROPERTY_LDTROUTINGORDERPUERTOTERMINALORIGENIDLIST);
    }

    public void setLdtRoutingOrderPuertoTerminalOrigenIDList(List<LDTRoutingOrder> ldtRoutingOrderPuertoTerminalOrigenIDList) {
        set(PROPERTY_LDTROUTINGORDERPUERTOTERMINALORIGENIDLIST, ldtRoutingOrderPuertoTerminalOrigenIDList);
    }

    @SuppressWarnings("unchecked")
    public List<LDTRoutingOrder> getLdtRoutingOrderPuertoTerminalDestinoIDList() {
        return (List<LDTRoutingOrder>) get(PROPERTY_LDTROUTINGORDERPUERTOTERMINALDESTINOIDLIST);
    }

    public void setLdtRoutingOrderPuertoTerminalDestinoIDList(List<LDTRoutingOrder> ldtRoutingOrderPuertoTerminalDestinoIDList) {
        set(PROPERTY_LDTROUTINGORDERPUERTOTERMINALDESTINOIDLIST, ldtRoutingOrderPuertoTerminalDestinoIDList);
    }

    @SuppressWarnings("unchecked")
    public List<ldtRuta> getLdtRutaPuertoTerminalOrigenList() {
        return (List<ldtRuta>) get(PROPERTY_LDTRUTAPUERTOTERMINALORIGENLIST);
    }

    public void setLdtRutaPuertoTerminalOrigenList(List<ldtRuta> ldtRutaPuertoTerminalOrigenList) {
        set(PROPERTY_LDTRUTAPUERTOTERMINALORIGENLIST, ldtRutaPuertoTerminalOrigenList);
    }

    @SuppressWarnings("unchecked")
    public List<ldtRuta> getLdtRutaPuertoTerminalDestinoList() {
        return (List<ldtRuta>) get(PROPERTY_LDTRUTAPUERTOTERMINALDESTINOLIST);
    }

    public void setLdtRutaPuertoTerminalDestinoList(List<ldtRuta> ldtRutaPuertoTerminalDestinoList) {
        set(PROPERTY_LDTRUTAPUERTOTERMINALDESTINOLIST, ldtRutaPuertoTerminalDestinoList);
    }

}
