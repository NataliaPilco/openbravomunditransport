/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2011 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.logisticadetransporte.parametrizacion.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.pricing.pricelist.PriceListVersion;
/**
 * Entity class for entity ldt_cotizacion_naviera (stored in table ldt_cotizacion_naviera).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class ldtCotizacionNaviera extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "ldt_cotizacion_naviera";
    public static final String ENTITY_NAME = "ldt_cotizacion_naviera";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_LINENO = "lineNo";
    public static final String PROPERTY_NAVIERA = "naviera";
    public static final String PROPERTY_LDTCOTIZACION = "lDTCotizacion";
    public static final String PROPERTY_PUERTOTERMINALORIGEN = "puertoTerminalOrigen";
    public static final String PROPERTY_PUERTOTERMINALDESTINO = "puertoTerminalDestino";
    public static final String PROPERTY_TOTALOPCION = "totalOpcion";
    public static final String PROPERTY_TOTALCOSTOSLOCALES = "totalCostosLocales";
    public static final String PROPERTY_TOTAL = "total";
    public static final String PROPERTY_SELECCIONAROP = "seleccionarOp";
    public static final String PROPERTY_TOTALNEGOCIADO = "totalNegociado";
    public static final String PROPERTY_TOTALTVENTA = "totalTVenta";
    public static final String PROPERTY_PRICELIST = "pricelist";
    public static final String PROPERTY_FECHADEVALIDEZ = "fechaDeValidez";
    public static final String PROPERTY_LDTDIASLIBRES = "ldtDiasLibres";
    public static final String PROPERTY_LDTDIASTRANSITO = "ldtDiasTransito";
    public static final String PROPERTY_LDTCONTRATOPUERTO = "lDTContratoPuerto";
    public static final String PROPERTY_COUNTRY = "country";
    public static final String PROPERTY_TOTALNEGOCIADOEUR = "totalNegociadoEur";
    public static final String PROPERTY_TOTALVENTAEUR = "totalVentaEur";
    public static final String PROPERTY_TOTALEUR = "totalEur";
    public static final String PROPERTY_TOTALCOSTOSEUR = "totalCostosEur";
    public static final String PROPERTY_TOTALOPCIONEUR = "totalOpcionEur";
    public static final String PROPERTY_PRICELISTVERSION = "priceListVersion";
    public static final String PROPERTY_VERSIONTARIFA = "versionTarifa";
    public static final String PROPERTY_CURRENCY = "currency";
    public static final String PROPERTY_LDTNAVIERAPRODUCTOSLIST = "ldtNavieraProductosList";

    public ldtCotizacionNaviera() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_TOTALOPCION, new BigDecimal(0.00));
        setDefaultValue(PROPERTY_TOTALCOSTOSLOCALES, new BigDecimal(0.00));
        setDefaultValue(PROPERTY_TOTAL, new BigDecimal(0.00));
        setDefaultValue(PROPERTY_SELECCIONAROP, false);
        setDefaultValue(PROPERTY_TOTALNEGOCIADO, new BigDecimal(0.00));
        setDefaultValue(PROPERTY_TOTALTVENTA, new BigDecimal(0.00));
        setDefaultValue(PROPERTY_PRICELIST, "0");
        setDefaultValue(PROPERTY_TOTALNEGOCIADOEUR, new BigDecimal(0));
        setDefaultValue(PROPERTY_TOTALVENTAEUR, new BigDecimal(0));
        setDefaultValue(PROPERTY_TOTALEUR, new BigDecimal(0));
        setDefaultValue(PROPERTY_TOTALCOSTOSEUR, new BigDecimal(0));
        setDefaultValue(PROPERTY_LDTNAVIERAPRODUCTOSLIST, new ArrayList<Object>());
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public Long getLineNo() {
        return (Long) get(PROPERTY_LINENO);
    }

    public void setLineNo(Long lineNo) {
        set(PROPERTY_LINENO, lineNo);
    }

    public BusinessPartner getNaviera() {
        return (BusinessPartner) get(PROPERTY_NAVIERA);
    }

    public void setNaviera(BusinessPartner naviera) {
        set(PROPERTY_NAVIERA, naviera);
    }

    public ldtCotizacion getLDTCotizacion() {
        return (ldtCotizacion) get(PROPERTY_LDTCOTIZACION);
    }

    public void setLDTCotizacion(ldtCotizacion lDTCotizacion) {
        set(PROPERTY_LDTCOTIZACION, lDTCotizacion);
    }

    public ldtPuertoTerminal getPuertoTerminalOrigen() {
        return (ldtPuertoTerminal) get(PROPERTY_PUERTOTERMINALORIGEN);
    }

    public void setPuertoTerminalOrigen(ldtPuertoTerminal puertoTerminalOrigen) {
        set(PROPERTY_PUERTOTERMINALORIGEN, puertoTerminalOrigen);
    }

    public ldtPuertoTerminal getPuertoTerminalDestino() {
        return (ldtPuertoTerminal) get(PROPERTY_PUERTOTERMINALDESTINO);
    }

    public void setPuertoTerminalDestino(ldtPuertoTerminal puertoTerminalDestino) {
        set(PROPERTY_PUERTOTERMINALDESTINO, puertoTerminalDestino);
    }

    public BigDecimal getTotalOpcion() {
        return (BigDecimal) get(PROPERTY_TOTALOPCION);
    }

    public void setTotalOpcion(BigDecimal totalOpcion) {
        set(PROPERTY_TOTALOPCION, totalOpcion);
    }

    public BigDecimal getTotalCostosLocales() {
        return (BigDecimal) get(PROPERTY_TOTALCOSTOSLOCALES);
    }

    public void setTotalCostosLocales(BigDecimal totalCostosLocales) {
        set(PROPERTY_TOTALCOSTOSLOCALES, totalCostosLocales);
    }

    public BigDecimal getTotal() {
        return (BigDecimal) get(PROPERTY_TOTAL);
    }

    public void setTotal(BigDecimal total) {
        set(PROPERTY_TOTAL, total);
    }

    public Boolean isSeleccionarOp() {
        return (Boolean) get(PROPERTY_SELECCIONAROP);
    }

    public void setSeleccionarOp(Boolean seleccionarOp) {
        set(PROPERTY_SELECCIONAROP, seleccionarOp);
    }

    public BigDecimal getTotalNegociado() {
        return (BigDecimal) get(PROPERTY_TOTALNEGOCIADO);
    }

    public void setTotalNegociado(BigDecimal totalNegociado) {
        set(PROPERTY_TOTALNEGOCIADO, totalNegociado);
    }

    public BigDecimal getTotalTVenta() {
        return (BigDecimal) get(PROPERTY_TOTALTVENTA);
    }

    public void setTotalTVenta(BigDecimal totalTVenta) {
        set(PROPERTY_TOTALTVENTA, totalTVenta);
    }

    public String getPricelist() {
        return (String) get(PROPERTY_PRICELIST);
    }

    public void setPricelist(String pricelist) {
        set(PROPERTY_PRICELIST, pricelist);
    }

    public Date getFechaDeValidez() {
        return (Date) get(PROPERTY_FECHADEVALIDEZ);
    }

    public void setFechaDeValidez(Date fechaDeValidez) {
        set(PROPERTY_FECHADEVALIDEZ, fechaDeValidez);
    }

    public Long getLdtDiasLibres() {
        return (Long) get(PROPERTY_LDTDIASLIBRES);
    }

    public void setLdtDiasLibres(Long ldtDiasLibres) {
        set(PROPERTY_LDTDIASLIBRES, ldtDiasLibres);
    }

    public String getLdtDiasTransito() {
        return (String) get(PROPERTY_LDTDIASTRANSITO);
    }

    public void setLdtDiasTransito(String ldtDiasTransito) {
        set(PROPERTY_LDTDIASTRANSITO, ldtDiasTransito);
    }

    public ldtContratoPuerto getLDTContratoPuerto() {
        return (ldtContratoPuerto) get(PROPERTY_LDTCONTRATOPUERTO);
    }

    public void setLDTContratoPuerto(ldtContratoPuerto lDTContratoPuerto) {
        set(PROPERTY_LDTCONTRATOPUERTO, lDTContratoPuerto);
    }

    public String getCountry() {
        return (String) get(PROPERTY_COUNTRY);
    }

    public void setCountry(String country) {
        set(PROPERTY_COUNTRY, country);
    }

    public BigDecimal getTotalNegociadoEur() {
        return (BigDecimal) get(PROPERTY_TOTALNEGOCIADOEUR);
    }

    public void setTotalNegociadoEur(BigDecimal totalNegociadoEur) {
        set(PROPERTY_TOTALNEGOCIADOEUR, totalNegociadoEur);
    }

    public BigDecimal getTotalVentaEur() {
        return (BigDecimal) get(PROPERTY_TOTALVENTAEUR);
    }

    public void setTotalVentaEur(BigDecimal totalVentaEur) {
        set(PROPERTY_TOTALVENTAEUR, totalVentaEur);
    }

    public BigDecimal getTotalEur() {
        return (BigDecimal) get(PROPERTY_TOTALEUR);
    }

    public void setTotalEur(BigDecimal totalEur) {
        set(PROPERTY_TOTALEUR, totalEur);
    }

    public BigDecimal getTotalCostosEur() {
        return (BigDecimal) get(PROPERTY_TOTALCOSTOSEUR);
    }

    public void setTotalCostosEur(BigDecimal totalCostosEur) {
        set(PROPERTY_TOTALCOSTOSEUR, totalCostosEur);
    }

    public BigDecimal getTotalOpcionEur() {
        return (BigDecimal) get(PROPERTY_TOTALOPCIONEUR);
    }

    public void setTotalOpcionEur(BigDecimal totalOpcionEur) {
        set(PROPERTY_TOTALOPCIONEUR, totalOpcionEur);
    }

    public PriceListVersion getPriceListVersion() {
        return (PriceListVersion) get(PROPERTY_PRICELISTVERSION);
    }

    public void setPriceListVersion(PriceListVersion priceListVersion) {
        set(PROPERTY_PRICELISTVERSION, priceListVersion);
    }

    public PriceListVersion getVersionTarifa() {
        return (PriceListVersion) get(PROPERTY_VERSIONTARIFA);
    }

    public void setVersionTarifa(PriceListVersion versionTarifa) {
        set(PROPERTY_VERSIONTARIFA, versionTarifa);
    }

    public Currency getCurrency() {
        return (Currency) get(PROPERTY_CURRENCY);
    }

    public void setCurrency(Currency currency) {
        set(PROPERTY_CURRENCY, currency);
    }

    @SuppressWarnings("unchecked")
    public List<LDTNavieraProductos> getLdtNavieraProductosList() {
        return (List<LDTNavieraProductos>) get(PROPERTY_LDTNAVIERAPRODUCTOSLIST);
    }

    public void setLdtNavieraProductosList(List<LDTNavieraProductos> ldtNavieraProductosList) {
        set(PROPERTY_LDTNAVIERAPRODUCTOSLIST, ldtNavieraProductosList);
    }

}
