/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2011 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.logisticadetransporte.parametrizacion.data;

import java.math.BigDecimal;
import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.financialmgmt.payment.Incoterms;
import org.openbravo.model.pricing.pricelist.PriceList;
import org.openbravo.model.pricing.pricelist.PriceListVersion;
import org.openbravo.model.pricing.pricelist.ProductPrice;
/**
 * Entity class for entity ldt_pricelist_contrato_v (stored in table ldt_pricelist_contrato_v).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class ldt_pricelist_contrato_v extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "ldt_pricelist_contrato_v";
    public static final String ENTITY_NAME = "ldt_pricelist_contrato_v";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_PRODUCT = "product";
    public static final String PROPERTY_PRICELISTVERSION = "priceListVersion";
    public static final String PROPERTY_WAREHOUSE = "warehouse";
    public static final String PROPERTY_PRODUCTPRICE = "productPrice";
    public static final String PROPERTY_INCOTERMS = "incoterms";
    public static final String PROPERTY_PRICELIST = "priceList";
    public static final String PROPERTY_LISTPRICE = "listPrice";
    public static final String PROPERTY_STANDARDPRICE = "standardPrice";
    public static final String PROPERTY_PRICELIMIT = "priceLimit";
    public static final String PROPERTY_AEREO = "aereo";
    public static final String PROPERTY_MARITIMO = "maritimo";
    public static final String PROPERTY_LDTTIPOPRODUCTO = "ldtTipoProducto";
    public static final String PROPERTY_LDTVALORMINIMO = "ldtValorMinimo";
    public static final String PROPERTY_OPA = "opa";
    public static final String PROPERTY_LDTCONTRATOPUERTO = "ldtContratoPuerto";
    public static final String PROPERTY_CODIGO = "codigo";
    public static final String PROPERTY_LDTFECHACADUCA = "ldtFechaCaduca";
    public static final String PROPERTY_CURRENCY = "currency";
    public static final String PROPERTY_PUERTOTERMINALORIGEN = "puertoTerminalOrigen";
    public static final String PROPERTY_PUERTOTERMINALDESTINO = "puertoTerminalDestino";
    public static final String PROPERTY_TRANSPORTISTA = "transportista";
    public static final String PROPERTY_LDTDIASLIBRES = "ldtDiasLibres";
    public static final String PROPERTY_LDTAGENTE = "ldtAgente";
    public static final String PROPERTY_VALIDFROMDATE = "validFromDate";

    public ldt_pricelist_contrato_v() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_AEREO, false);
        setDefaultValue(PROPERTY_MARITIMO, false);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Product getProduct() {
        return (Product) get(PROPERTY_PRODUCT);
    }

    public void setProduct(Product product) {
        set(PROPERTY_PRODUCT, product);
    }

    public PriceListVersion getPriceListVersion() {
        return (PriceListVersion) get(PROPERTY_PRICELISTVERSION);
    }

    public void setPriceListVersion(PriceListVersion priceListVersion) {
        set(PROPERTY_PRICELISTVERSION, priceListVersion);
    }

    public Warehouse getWarehouse() {
        return (Warehouse) get(PROPERTY_WAREHOUSE);
    }

    public void setWarehouse(Warehouse warehouse) {
        set(PROPERTY_WAREHOUSE, warehouse);
    }

    public ProductPrice getProductPrice() {
        return (ProductPrice) get(PROPERTY_PRODUCTPRICE);
    }

    public void setProductPrice(ProductPrice productPrice) {
        set(PROPERTY_PRODUCTPRICE, productPrice);
    }

    public Incoterms getIncoterms() {
        return (Incoterms) get(PROPERTY_INCOTERMS);
    }

    public void setIncoterms(Incoterms incoterms) {
        set(PROPERTY_INCOTERMS, incoterms);
    }

    public PriceList getPriceList() {
        return (PriceList) get(PROPERTY_PRICELIST);
    }

    public void setPriceList(PriceList priceList) {
        set(PROPERTY_PRICELIST, priceList);
    }

    public BigDecimal getListPrice() {
        return (BigDecimal) get(PROPERTY_LISTPRICE);
    }

    public void setListPrice(BigDecimal listPrice) {
        set(PROPERTY_LISTPRICE, listPrice);
    }

    public BigDecimal getStandardPrice() {
        return (BigDecimal) get(PROPERTY_STANDARDPRICE);
    }

    public void setStandardPrice(BigDecimal standardPrice) {
        set(PROPERTY_STANDARDPRICE, standardPrice);
    }

    public BigDecimal getPriceLimit() {
        return (BigDecimal) get(PROPERTY_PRICELIMIT);
    }

    public void setPriceLimit(BigDecimal priceLimit) {
        set(PROPERTY_PRICELIMIT, priceLimit);
    }

    public Boolean isAereo() {
        return (Boolean) get(PROPERTY_AEREO);
    }

    public void setAereo(Boolean aereo) {
        set(PROPERTY_AEREO, aereo);
    }

    public Boolean isMaritimo() {
        return (Boolean) get(PROPERTY_MARITIMO);
    }

    public void setMaritimo(Boolean maritimo) {
        set(PROPERTY_MARITIMO, maritimo);
    }

    public String getLdtTipoProducto() {
        return (String) get(PROPERTY_LDTTIPOPRODUCTO);
    }

    public void setLdtTipoProducto(String ldtTipoProducto) {
        set(PROPERTY_LDTTIPOPRODUCTO, ldtTipoProducto);
    }

    public BigDecimal getLdtValorMinimo() {
        return (BigDecimal) get(PROPERTY_LDTVALORMINIMO);
    }

    public void setLdtValorMinimo(BigDecimal ldtValorMinimo) {
        set(PROPERTY_LDTVALORMINIMO, ldtValorMinimo);
    }

    public BigDecimal getOpa() {
        return (BigDecimal) get(PROPERTY_OPA);
    }

    public void setOpa(BigDecimal opa) {
        set(PROPERTY_OPA, opa);
    }

    public ldtContratoPuerto getLdtContratoPuerto() {
        return (ldtContratoPuerto) get(PROPERTY_LDTCONTRATOPUERTO);
    }

    public void setLdtContratoPuerto(ldtContratoPuerto ldtContratoPuerto) {
        set(PROPERTY_LDTCONTRATOPUERTO, ldtContratoPuerto);
    }

    public String getCodigo() {
        return (String) get(PROPERTY_CODIGO);
    }

    public void setCodigo(String codigo) {
        set(PROPERTY_CODIGO, codigo);
    }

    public Date getLdtFechaCaduca() {
        return (Date) get(PROPERTY_LDTFECHACADUCA);
    }

    public void setLdtFechaCaduca(Date ldtFechaCaduca) {
        set(PROPERTY_LDTFECHACADUCA, ldtFechaCaduca);
    }

    public Currency getCurrency() {
        return (Currency) get(PROPERTY_CURRENCY);
    }

    public void setCurrency(Currency currency) {
        set(PROPERTY_CURRENCY, currency);
    }

    public ldtPuertoTerminal getPuertoTerminalOrigen() {
        return (ldtPuertoTerminal) get(PROPERTY_PUERTOTERMINALORIGEN);
    }

    public void setPuertoTerminalOrigen(ldtPuertoTerminal puertoTerminalOrigen) {
        set(PROPERTY_PUERTOTERMINALORIGEN, puertoTerminalOrigen);
    }

    public ldtPuertoTerminal getPuertoTerminalDestino() {
        return (ldtPuertoTerminal) get(PROPERTY_PUERTOTERMINALDESTINO);
    }

    public void setPuertoTerminalDestino(ldtPuertoTerminal puertoTerminalDestino) {
        set(PROPERTY_PUERTOTERMINALDESTINO, puertoTerminalDestino);
    }

    public BusinessPartner getTransportista() {
        return (BusinessPartner) get(PROPERTY_TRANSPORTISTA);
    }

    public void setTransportista(BusinessPartner transportista) {
        set(PROPERTY_TRANSPORTISTA, transportista);
    }

    public BigDecimal getLdtDiasLibres() {
        return (BigDecimal) get(PROPERTY_LDTDIASLIBRES);
    }

    public void setLdtDiasLibres(BigDecimal ldtDiasLibres) {
        set(PROPERTY_LDTDIASLIBRES, ldtDiasLibres);
    }

    public BusinessPartner getLdtAgente() {
        return (BusinessPartner) get(PROPERTY_LDTAGENTE);
    }

    public void setLdtAgente(BusinessPartner ldtAgente) {
        set(PROPERTY_LDTAGENTE, ldtAgente);
    }

    public Date getValidFromDate() {
        return (Date) get(PROPERTY_VALIDFROMDATE);
    }

    public void setValidFromDate(Date validFromDate) {
        set(PROPERTY_VALIDFROMDATE, validFromDate);
    }

}
