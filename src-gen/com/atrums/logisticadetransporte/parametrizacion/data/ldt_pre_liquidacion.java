/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2011 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.logisticadetransporte.parametrizacion.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.Organization;
/**
 * Entity class for entity ldt_pre_liquidacion (stored in table ldt_pre_liquidacion).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class ldt_pre_liquidacion extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "ldt_pre_liquidacion";
    public static final String ENTITY_NAME = "ldt_pre_liquidacion";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_LDTROUTINGORDER = "lDTRoutingOrder";
    public static final String PROPERTY_VENDEDOR = "vendedor";
    public static final String PROPERTY_PRELIQUIDACION = "pRELiquidacion";
    public static final String PROPERTY_SERVICIOCLIENTE = "servicioCliente";
    public static final String PROPERTY_AGENTE = "agente";
    public static final String PROPERTY_RESULTADO = "resultado";
    public static final String PROPERTY_DOCUMENTSTATUS = "documentStatus";
    public static final String PROPERTY_LDTTIPO = "lDTTipo";
    public static final String PROPERTY_EMPLEADO = "empleado";
    public static final String PROPERTY_CANTIDAD = "cantidad";
    public static final String PROPERTY_PROCESAR = "procesar";
    public static final String PROPERTY_USERCONTACT = "userContact";
    public static final String PROPERTY_USERVENDEDOR = "userVendedor";
    public static final String PROPERTY_LDTPRECOSTOSORIGENLIST = "ldtPreCostosOrigenList";
    public static final String PROPERTY_LDTPREGASTOSLOCALESLIST = "ldtPreGastosLocalesList";
    public static final String PROPERTY_LDTPRELIQUIDACIONLINEASLIST = "ldtPreLiquidacionLineasList";
    public static final String PROPERTY_LDTPREOTROSCOSTOSLIST = "ldtPreOtrosCostosList";

    public ldt_pre_liquidacion() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_PRELIQUIDACION, false);
        setDefaultValue(PROPERTY_RESULTADO, new BigDecimal(0));
        setDefaultValue(PROPERTY_DOCUMENTSTATUS, "BR");
        setDefaultValue(PROPERTY_PROCESAR, "CO");
        setDefaultValue(PROPERTY_LDTPRECOSTOSORIGENLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTPREGASTOSLOCALESLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTPRELIQUIDACIONLINEASLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_LDTPREOTROSCOSTOSLIST, new ArrayList<Object>());
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public LDTRoutingOrder getLDTRoutingOrder() {
        return (LDTRoutingOrder) get(PROPERTY_LDTROUTINGORDER);
    }

    public void setLDTRoutingOrder(LDTRoutingOrder lDTRoutingOrder) {
        set(PROPERTY_LDTROUTINGORDER, lDTRoutingOrder);
    }

    public BusinessPartner getVendedor() {
        return (BusinessPartner) get(PROPERTY_VENDEDOR);
    }

    public void setVendedor(BusinessPartner vendedor) {
        set(PROPERTY_VENDEDOR, vendedor);
    }

    public Boolean isPRELiquidacion() {
        return (Boolean) get(PROPERTY_PRELIQUIDACION);
    }

    public void setPRELiquidacion(Boolean pRELiquidacion) {
        set(PROPERTY_PRELIQUIDACION, pRELiquidacion);
    }

    public BusinessPartner getServicioCliente() {
        return (BusinessPartner) get(PROPERTY_SERVICIOCLIENTE);
    }

    public void setServicioCliente(BusinessPartner servicioCliente) {
        set(PROPERTY_SERVICIOCLIENTE, servicioCliente);
    }

    public BusinessPartner getAgente() {
        return (BusinessPartner) get(PROPERTY_AGENTE);
    }

    public void setAgente(BusinessPartner agente) {
        set(PROPERTY_AGENTE, agente);
    }

    public BigDecimal getResultado() {
        return (BigDecimal) get(PROPERTY_RESULTADO);
    }

    public void setResultado(BigDecimal resultado) {
        set(PROPERTY_RESULTADO, resultado);
    }

    public String getDocumentStatus() {
        return (String) get(PROPERTY_DOCUMENTSTATUS);
    }

    public void setDocumentStatus(String documentStatus) {
        set(PROPERTY_DOCUMENTSTATUS, documentStatus);
    }

    public String getLDTTipo() {
        return (String) get(PROPERTY_LDTTIPO);
    }

    public void setLDTTipo(String lDTTipo) {
        set(PROPERTY_LDTTIPO, lDTTipo);
    }

    public BusinessPartner getEmpleado() {
        return (BusinessPartner) get(PROPERTY_EMPLEADO);
    }

    public void setEmpleado(BusinessPartner empleado) {
        set(PROPERTY_EMPLEADO, empleado);
    }

    public BigDecimal getCantidad() {
        return (BigDecimal) get(PROPERTY_CANTIDAD);
    }

    public void setCantidad(BigDecimal cantidad) {
        set(PROPERTY_CANTIDAD, cantidad);
    }

    public String getProcesar() {
        return (String) get(PROPERTY_PROCESAR);
    }

    public void setProcesar(String procesar) {
        set(PROPERTY_PROCESAR, procesar);
    }

    public User getUserContact() {
        return (User) get(PROPERTY_USERCONTACT);
    }

    public void setUserContact(User userContact) {
        set(PROPERTY_USERCONTACT, userContact);
    }

    public User getUserVendedor() {
        return (User) get(PROPERTY_USERVENDEDOR);
    }

    public void setUserVendedor(User userVendedor) {
        set(PROPERTY_USERVENDEDOR, userVendedor);
    }

    @SuppressWarnings("unchecked")
    public List<ldt_pre_costos_origen> getLdtPreCostosOrigenList() {
        return (List<ldt_pre_costos_origen>) get(PROPERTY_LDTPRECOSTOSORIGENLIST);
    }

    public void setLdtPreCostosOrigenList(List<ldt_pre_costos_origen> ldtPreCostosOrigenList) {
        set(PROPERTY_LDTPRECOSTOSORIGENLIST, ldtPreCostosOrigenList);
    }

    @SuppressWarnings("unchecked")
    public List<ldt_pre_gastos_locales> getLdtPreGastosLocalesList() {
        return (List<ldt_pre_gastos_locales>) get(PROPERTY_LDTPREGASTOSLOCALESLIST);
    }

    public void setLdtPreGastosLocalesList(List<ldt_pre_gastos_locales> ldtPreGastosLocalesList) {
        set(PROPERTY_LDTPREGASTOSLOCALESLIST, ldtPreGastosLocalesList);
    }

    @SuppressWarnings("unchecked")
    public List<ldt_pre_liquidacion_lineas> getLdtPreLiquidacionLineasList() {
        return (List<ldt_pre_liquidacion_lineas>) get(PROPERTY_LDTPRELIQUIDACIONLINEASLIST);
    }

    public void setLdtPreLiquidacionLineasList(List<ldt_pre_liquidacion_lineas> ldtPreLiquidacionLineasList) {
        set(PROPERTY_LDTPRELIQUIDACIONLINEASLIST, ldtPreLiquidacionLineasList);
    }

    @SuppressWarnings("unchecked")
    public List<ldt_pre_otros_costos> getLdtPreOtrosCostosList() {
        return (List<ldt_pre_otros_costos>) get(PROPERTY_LDTPREOTROSCOSTOSLIST);
    }

    public void setLdtPreOtrosCostosList(List<ldt_pre_otros_costos> ldtPreOtrosCostosList) {
        set(PROPERTY_LDTPREOTROSCOSTOSLIST, ldtPreOtrosCostosList);
    }

}
