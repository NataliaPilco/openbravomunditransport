//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.logisticadetransporte.parametrizacion.MDTLiquidaciones;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class LiquidacionesDFDA384C51D24338B9070979CDF44876Data implements FieldProvider {
static Logger log4j = Logger.getLogger(LiquidacionesDFDA384C51D24338B9070979CDF44876Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String condicion;
  public String condicionr;
  public String ldtMblId;
  public String adOrgId;
  public String adOrgIdr;
  public String ldtNdAgente;
  public String ldtNcAgente;
  public String datosLiq;
  public String generaLiq;
  public String processed;
  public String processedBtn;
  public String ldtLiquidacionCabeceraId;
  public String adClientId;
  public String docstatus;
  public String bloquear;
  public String cantidadCont;
  public String fleteCostoDiv;
  public String totalFleteCosto;
  public String isactive;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("condicion"))
      return condicion;
    else if (fieldName.equalsIgnoreCase("condicionr"))
      return condicionr;
    else if (fieldName.equalsIgnoreCase("ldt_mbl_id") || fieldName.equals("ldtMblId"))
      return ldtMblId;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("ldt_nd_agente") || fieldName.equals("ldtNdAgente"))
      return ldtNdAgente;
    else if (fieldName.equalsIgnoreCase("ldt_nc_agente") || fieldName.equals("ldtNcAgente"))
      return ldtNcAgente;
    else if (fieldName.equalsIgnoreCase("datos_liq") || fieldName.equals("datosLiq"))
      return datosLiq;
    else if (fieldName.equalsIgnoreCase("genera_liq") || fieldName.equals("generaLiq"))
      return generaLiq;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("processed_btn") || fieldName.equals("processedBtn"))
      return processedBtn;
    else if (fieldName.equalsIgnoreCase("ldt_liquidacion_cabecera_id") || fieldName.equals("ldtLiquidacionCabeceraId"))
      return ldtLiquidacionCabeceraId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("bloquear"))
      return bloquear;
    else if (fieldName.equalsIgnoreCase("cantidad_cont") || fieldName.equals("cantidadCont"))
      return cantidadCont;
    else if (fieldName.equalsIgnoreCase("flete_costo_div") || fieldName.equals("fleteCostoDiv"))
      return fleteCostoDiv;
    else if (fieldName.equalsIgnoreCase("total_flete_costo") || fieldName.equals("totalFleteCosto"))
      return totalFleteCosto;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static LiquidacionesDFDA384C51D24338B9070979CDF44876Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static LiquidacionesDFDA384C51D24338B9070979CDF44876Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(ldt_liquidacion_cabecera.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = ldt_liquidacion_cabecera.CreatedBy) as CreatedByR, " +
      "        to_char(ldt_liquidacion_cabecera.Updated, ?) as updated, " +
      "        to_char(ldt_liquidacion_cabecera.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        ldt_liquidacion_cabecera.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = ldt_liquidacion_cabecera.UpdatedBy) as UpdatedByR," +
      "        ldt_liquidacion_cabecera.condicion, " +
      "(CASE WHEN ldt_liquidacion_cabecera.condicion IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS condicionR, " +
      "ldt_liquidacion_cabecera.ldt_mbl_id, " +
      "ldt_liquidacion_cabecera.AD_Org_ID, " +
      "(CASE WHEN ldt_liquidacion_cabecera.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "ldt_liquidacion_cabecera.ldt_nd_agente, " +
      "ldt_liquidacion_cabecera.ldt_nc_agente, " +
      "ldt_liquidacion_cabecera.datos_liq, " +
      "ldt_liquidacion_cabecera.genera_liq, " +
      "ldt_liquidacion_cabecera.processed, " +
      "list2.name as processed_BTN, " +
      "ldt_liquidacion_cabecera.LDT_Liquidacion_Cabecera_ID, " +
      "ldt_liquidacion_cabecera.AD_Client_ID, " +
      "ldt_liquidacion_cabecera.docstatus, " +
      "COALESCE(ldt_liquidacion_cabecera.bloquear, 'N') AS bloquear, " +
      "ldt_liquidacion_cabecera.cantidad_cont, " +
      "ldt_liquidacion_cabecera.flete_costo_div, " +
      "ldt_liquidacion_cabecera.total_flete_costo, " +
      "COALESCE(ldt_liquidacion_cabecera.Isactive, 'N') AS Isactive, " +
      "        ? AS LANGUAGE " +
      "        FROM ldt_liquidacion_cabecera left join ad_ref_list_v list1 on (ldt_liquidacion_cabecera.condicion = list1.value and list1.ad_reference_id = '3F49F47A1C5A422FB113F8C41D747E74' and list1.ad_language = ?)  left join (select AD_Org_ID, Name from AD_Org) table1 on (ldt_liquidacion_cabecera.AD_Org_ID = table1.AD_Org_ID) left join ad_ref_list_v list2 on (list2.ad_reference_id = '1F931E9A5FAB445A8FD2F069ADD2DBE4' and list2.ad_language = ?  AND ldt_liquidacion_cabecera.processed = TO_CHAR(list2.value))" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND ldt_liquidacion_cabecera.LDT_Liquidacion_Cabecera_ID = ? " +
      "        AND ldt_liquidacion_cabecera.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND ldt_liquidacion_cabecera.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LiquidacionesDFDA384C51D24338B9070979CDF44876Data objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data = new LiquidacionesDFDA384C51D24338B9070979CDF44876Data();
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.created = UtilSql.getValue(result, "created");
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.updated = UtilSql.getValue(result, "updated");
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.condicion = UtilSql.getValue(result, "condicion");
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.condicionr = UtilSql.getValue(result, "condicionr");
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.ldtMblId = UtilSql.getValue(result, "ldt_mbl_id");
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.ldtNdAgente = UtilSql.getValue(result, "ldt_nd_agente");
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.ldtNcAgente = UtilSql.getValue(result, "ldt_nc_agente");
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.datosLiq = UtilSql.getValue(result, "datos_liq");
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.generaLiq = UtilSql.getValue(result, "genera_liq");
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.processed = UtilSql.getValue(result, "processed");
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.processedBtn = UtilSql.getValue(result, "processed_btn");
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.ldtLiquidacionCabeceraId = UtilSql.getValue(result, "ldt_liquidacion_cabecera_id");
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.docstatus = UtilSql.getValue(result, "docstatus");
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.bloquear = UtilSql.getValue(result, "bloquear");
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.cantidadCont = UtilSql.getValue(result, "cantidad_cont");
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.fleteCostoDiv = UtilSql.getValue(result, "flete_costo_div");
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.totalFleteCosto = UtilSql.getValue(result, "total_flete_costo");
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.isactive = UtilSql.getValue(result, "isactive");
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.language = UtilSql.getValue(result, "language");
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.adUserClient = "";
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.adOrgClient = "";
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.createdby = "";
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.trBgcolor = "";
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.totalCount = "";
        objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LiquidacionesDFDA384C51D24338B9070979CDF44876Data objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[] = new LiquidacionesDFDA384C51D24338B9070979CDF44876Data[vector.size()];
    vector.copyInto(objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data);
    return(objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data);
  }

/**
Create a registry
 */
  public static LiquidacionesDFDA384C51D24338B9070979CDF44876Data[] set(String isactive, String adOrgId, String generaLiq, String totalFleteCosto, String ldtMblId, String processed, String processedBtn, String updatedby, String updatedbyr, String ldtNdAgente, String fleteCostoDiv, String condicion, String createdby, String createdbyr, String cantidadCont, String bloquear, String adClientId, String datosLiq, String docstatus, String ldtLiquidacionCabeceraId, String ldtNcAgente)    throws ServletException {
    LiquidacionesDFDA384C51D24338B9070979CDF44876Data objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[] = new LiquidacionesDFDA384C51D24338B9070979CDF44876Data[1];
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0] = new LiquidacionesDFDA384C51D24338B9070979CDF44876Data();
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0].created = "";
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0].createdbyr = createdbyr;
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0].updated = "";
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0].updatedTimeStamp = "";
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0].updatedby = updatedby;
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0].updatedbyr = updatedbyr;
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0].condicion = condicion;
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0].condicionr = "";
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0].ldtMblId = ldtMblId;
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0].adOrgId = adOrgId;
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0].adOrgIdr = "";
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0].ldtNdAgente = ldtNdAgente;
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0].ldtNcAgente = ldtNcAgente;
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0].datosLiq = datosLiq;
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0].generaLiq = generaLiq;
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0].processed = processed;
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0].processedBtn = processedBtn;
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0].ldtLiquidacionCabeceraId = ldtLiquidacionCabeceraId;
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0].adClientId = adClientId;
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0].docstatus = docstatus;
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0].bloquear = bloquear;
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0].cantidadCont = cantidadCont;
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0].fleteCostoDiv = fleteCostoDiv;
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0].totalFleteCosto = totalFleteCosto;
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0].isactive = isactive;
    objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data[0].language = "";
    return objectLiquidacionesDFDA384C51D24338B9070979CDF44876Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef474C49D137784BD8B2B1A1CBBC04A92B_0(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef8E59C22FD0334042B7654F01FF637140_1(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE ldt_liquidacion_cabecera" +
      "        SET condicion = (?) , ldt_mbl_id = (?) , AD_Org_ID = (?) , ldt_nd_agente = TO_NUMBER(?) , ldt_nc_agente = TO_NUMBER(?) , datos_liq = (?) , genera_liq = (?) , processed = (?) , LDT_Liquidacion_Cabecera_ID = (?) , AD_Client_ID = (?) , docstatus = (?) , bloquear = (?) , cantidad_cont = TO_NUMBER(?) , flete_costo_div = TO_NUMBER(?) , total_flete_costo = TO_NUMBER(?) , Isactive = (?) , updated = now(), updatedby = ? " +
      "        WHERE ldt_liquidacion_cabecera.LDT_Liquidacion_Cabecera_ID = ? " +
      "        AND ldt_liquidacion_cabecera.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND ldt_liquidacion_cabecera.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, condicion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtMblId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtNdAgente);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtNcAgente);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, datosLiq);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generaLiq);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtLiquidacionCabeceraId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, bloquear);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantidadCont);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fleteCostoDiv);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalFleteCosto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtLiquidacionCabeceraId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO ldt_liquidacion_cabecera " +
      "        (condicion, ldt_mbl_id, AD_Org_ID, ldt_nd_agente, ldt_nc_agente, datos_liq, genera_liq, processed, LDT_Liquidacion_Cabecera_ID, AD_Client_ID, docstatus, bloquear, cantidad_cont, flete_costo_div, total_flete_costo, Isactive, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, condicion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtMblId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtNdAgente);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtNcAgente);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, datosLiq);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generaLiq);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtLiquidacionCabeceraId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, bloquear);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantidadCont);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fleteCostoDiv);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalFleteCosto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM ldt_liquidacion_cabecera" +
      "        WHERE ldt_liquidacion_cabecera.LDT_Liquidacion_Cabecera_ID = ? " +
      "        AND ldt_liquidacion_cabecera.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND ldt_liquidacion_cabecera.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM ldt_liquidacion_cabecera" +
      "         WHERE ldt_liquidacion_cabecera.LDT_Liquidacion_Cabecera_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM ldt_liquidacion_cabecera" +
      "         WHERE ldt_liquidacion_cabecera.LDT_Liquidacion_Cabecera_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
