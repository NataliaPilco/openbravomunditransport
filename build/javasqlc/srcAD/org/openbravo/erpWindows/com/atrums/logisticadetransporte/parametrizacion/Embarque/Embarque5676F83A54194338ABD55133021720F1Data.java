//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.logisticadetransporte.parametrizacion.Embarque;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class Embarque5676F83A54194338ABD55133021720F1Data implements FieldProvider {
static Logger log4j = Logger.getLogger(Embarque5676F83A54194338ABD55133021720F1Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String cDoctypeId;
  public String cDoctypeIdr;
  public String numembarque;
  public String documentno;
  public String ldtCotizacionId;
  public String fecha;
  public String estado;
  public String puertoTerminalOrigenId;
  public String puertoTerminalDestinoId;
  public String fechaEstimada;
  public String cBpartnerId;
  public String cBpartnerRuc;
  public String cBpartnerLocationId;
  public String telfShipper;
  public String adUserId;
  public String adUserIdr;
  public String mail;
  public String clienteId;
  public String clienteIdr;
  public String clienteRuc;
  public String clienteDireccionId;
  public String clienteDireccionIdr;
  public String cIncotermsId;
  public String cIncotermsIdr;
  public String modalidad;
  public String modalidadr;
  public String tipo;
  public String tipor;
  public String agenteId;
  public String tarifaVenta;
  public String tarifaNegociada;
  public String costosLocales;
  public String tarifaNegociadaEur;
  public String costosLocalesEur;
  public String tarifaCompra;
  public String cod;
  public String vendedorId;
  public String vendedorIdr;
  public String referenciaCantidad;
  public String observacion;
  public String esFinEmbarque;
  public String processed;
  public String processedBtn;
  public String tarifaVentaEur;
  public String mPricelistVersionId;
  public String adClientId;
  public String ldtRoutingOrderId;
  public String isactive;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("c_doctype_idr") || fieldName.equals("cDoctypeIdr"))
      return cDoctypeIdr;
    else if (fieldName.equalsIgnoreCase("numembarque"))
      return numembarque;
    else if (fieldName.equalsIgnoreCase("documentno"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("ldt_cotizacion_id") || fieldName.equals("ldtCotizacionId"))
      return ldtCotizacionId;
    else if (fieldName.equalsIgnoreCase("fecha"))
      return fecha;
    else if (fieldName.equalsIgnoreCase("estado"))
      return estado;
    else if (fieldName.equalsIgnoreCase("puerto_terminal_origen_id") || fieldName.equals("puertoTerminalOrigenId"))
      return puertoTerminalOrigenId;
    else if (fieldName.equalsIgnoreCase("puerto_terminal_destino_id") || fieldName.equals("puertoTerminalDestinoId"))
      return puertoTerminalDestinoId;
    else if (fieldName.equalsIgnoreCase("fecha_estimada") || fieldName.equals("fechaEstimada"))
      return fechaEstimada;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_ruc") || fieldName.equals("cBpartnerRuc"))
      return cBpartnerRuc;
    else if (fieldName.equalsIgnoreCase("c_bpartner_location_id") || fieldName.equals("cBpartnerLocationId"))
      return cBpartnerLocationId;
    else if (fieldName.equalsIgnoreCase("telf_shipper") || fieldName.equals("telfShipper"))
      return telfShipper;
    else if (fieldName.equalsIgnoreCase("ad_user_id") || fieldName.equals("adUserId"))
      return adUserId;
    else if (fieldName.equalsIgnoreCase("ad_user_idr") || fieldName.equals("adUserIdr"))
      return adUserIdr;
    else if (fieldName.equalsIgnoreCase("mail"))
      return mail;
    else if (fieldName.equalsIgnoreCase("cliente_id") || fieldName.equals("clienteId"))
      return clienteId;
    else if (fieldName.equalsIgnoreCase("cliente_idr") || fieldName.equals("clienteIdr"))
      return clienteIdr;
    else if (fieldName.equalsIgnoreCase("cliente_ruc") || fieldName.equals("clienteRuc"))
      return clienteRuc;
    else if (fieldName.equalsIgnoreCase("cliente_direccion_id") || fieldName.equals("clienteDireccionId"))
      return clienteDireccionId;
    else if (fieldName.equalsIgnoreCase("cliente_direccion_idr") || fieldName.equals("clienteDireccionIdr"))
      return clienteDireccionIdr;
    else if (fieldName.equalsIgnoreCase("c_incoterms_id") || fieldName.equals("cIncotermsId"))
      return cIncotermsId;
    else if (fieldName.equalsIgnoreCase("c_incoterms_idr") || fieldName.equals("cIncotermsIdr"))
      return cIncotermsIdr;
    else if (fieldName.equalsIgnoreCase("modalidad"))
      return modalidad;
    else if (fieldName.equalsIgnoreCase("modalidadr"))
      return modalidadr;
    else if (fieldName.equalsIgnoreCase("tipo"))
      return tipo;
    else if (fieldName.equalsIgnoreCase("tipor"))
      return tipor;
    else if (fieldName.equalsIgnoreCase("agente_id") || fieldName.equals("agenteId"))
      return agenteId;
    else if (fieldName.equalsIgnoreCase("tarifa_venta") || fieldName.equals("tarifaVenta"))
      return tarifaVenta;
    else if (fieldName.equalsIgnoreCase("tarifa_negociada") || fieldName.equals("tarifaNegociada"))
      return tarifaNegociada;
    else if (fieldName.equalsIgnoreCase("costos_locales") || fieldName.equals("costosLocales"))
      return costosLocales;
    else if (fieldName.equalsIgnoreCase("tarifa_negociada_eur") || fieldName.equals("tarifaNegociadaEur"))
      return tarifaNegociadaEur;
    else if (fieldName.equalsIgnoreCase("costos_locales_eur") || fieldName.equals("costosLocalesEur"))
      return costosLocalesEur;
    else if (fieldName.equalsIgnoreCase("tarifa_compra") || fieldName.equals("tarifaCompra"))
      return tarifaCompra;
    else if (fieldName.equalsIgnoreCase("cod"))
      return cod;
    else if (fieldName.equalsIgnoreCase("vendedor_id") || fieldName.equals("vendedorId"))
      return vendedorId;
    else if (fieldName.equalsIgnoreCase("vendedor_idr") || fieldName.equals("vendedorIdr"))
      return vendedorIdr;
    else if (fieldName.equalsIgnoreCase("referencia_cantidad") || fieldName.equals("referenciaCantidad"))
      return referenciaCantidad;
    else if (fieldName.equalsIgnoreCase("observacion"))
      return observacion;
    else if (fieldName.equalsIgnoreCase("es_fin_embarque") || fieldName.equals("esFinEmbarque"))
      return esFinEmbarque;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("processed_btn") || fieldName.equals("processedBtn"))
      return processedBtn;
    else if (fieldName.equalsIgnoreCase("tarifa_venta_eur") || fieldName.equals("tarifaVentaEur"))
      return tarifaVentaEur;
    else if (fieldName.equalsIgnoreCase("m_pricelist_version_id") || fieldName.equals("mPricelistVersionId"))
      return mPricelistVersionId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("ldt_routing_order_id") || fieldName.equals("ldtRoutingOrderId"))
      return ldtRoutingOrderId;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static Embarque5676F83A54194338ABD55133021720F1Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static Embarque5676F83A54194338ABD55133021720F1Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(ldt_routing_order.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = ldt_routing_order.CreatedBy) as CreatedByR, " +
      "        to_char(ldt_routing_order.Updated, ?) as updated, " +
      "        to_char(ldt_routing_order.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        ldt_routing_order.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = ldt_routing_order.UpdatedBy) as UpdatedByR," +
      "        ldt_routing_order.AD_Org_ID, " +
      "(CASE WHEN ldt_routing_order.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "ldt_routing_order.C_Doctype_ID, " +
      "(CASE WHEN ldt_routing_order.C_Doctype_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL2.Name IS NULL THEN TO_CHAR(table2.Name) ELSE TO_CHAR(tableTRL2.Name) END)), ''))),'') ) END) AS C_Doctype_IDR, " +
      "ldt_routing_order.Numembarque, " +
      "ldt_routing_order.Documentno, " +
      "ldt_routing_order.LDT_Cotizacion_ID, " +
      "ldt_routing_order.Fecha, " +
      "ldt_routing_order.Estado, " +
      "ldt_routing_order.Puerto_Terminal_Origen_ID, " +
      "ldt_routing_order.Puerto_Terminal_Destino_ID, " +
      "ldt_routing_order.Fecha_Estimada, " +
      "ldt_routing_order.C_Bpartner_ID, " +
      "ldt_routing_order.C_Bpartner_Ruc, " +
      "ldt_routing_order.C_Bpartner_Location_ID, " +
      "ldt_routing_order.Telf_Shipper, " +
      "ldt_routing_order.AD_User_ID, " +
      "(CASE WHEN ldt_routing_order.AD_User_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.Name), ''))),'') ) END) AS AD_User_IDR, " +
      "ldt_routing_order.Mail, " +
      "ldt_routing_order.Cliente_ID, " +
      "(CASE WHEN ldt_routing_order.Cliente_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name), ''))),'') ) END) AS Cliente_IDR, " +
      "ldt_routing_order.Cliente_Ruc, " +
      "ldt_routing_order.Cliente_Direccion_ID, " +
      "(CASE WHEN ldt_routing_order.Cliente_Direccion_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table5.Name), ''))),'') ) END) AS Cliente_Direccion_IDR, " +
      "ldt_routing_order.C_Incoterms_ID, " +
      "(CASE WHEN ldt_routing_order.C_Incoterms_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table6.Name), ''))),'') ) END) AS C_Incoterms_IDR, " +
      "ldt_routing_order.Modalidad, " +
      "(CASE WHEN ldt_routing_order.Modalidad IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS ModalidadR, " +
      "ldt_routing_order.Tipo, " +
      "(CASE WHEN ldt_routing_order.Tipo IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list2.name),'') ) END) AS TipoR, " +
      "ldt_routing_order.Agente_ID, " +
      "ldt_routing_order.Tarifa_Venta, " +
      "ldt_routing_order.Tarifa_Negociada, " +
      "ldt_routing_order.Costos_Locales, " +
      "ldt_routing_order.tarifa_negociada_eur, " +
      "ldt_routing_order.costos_locales_eur, " +
      "ldt_routing_order.Tarifa_Compra, " +
      "ldt_routing_order.Cod, " +
      "ldt_routing_order.Vendedor_ID, " +
      "(CASE WHEN ldt_routing_order.Vendedor_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table7.Name), ''))),'') ) END) AS Vendedor_IDR, " +
      "ldt_routing_order.referencia_cantidad, " +
      "ldt_routing_order.Observacion, " +
      "ldt_routing_order.ES_Fin_Embarque, " +
      "ldt_routing_order.Processed, " +
      "list3.name as Processed_BTN, " +
      "ldt_routing_order.tarifa_venta_eur, " +
      "ldt_routing_order.M_Pricelist_Version_ID, " +
      "ldt_routing_order.AD_Client_ID, " +
      "ldt_routing_order.LDT_Routing_Order_ID, " +
      "COALESCE(ldt_routing_order.Isactive, 'N') AS Isactive, " +
      "        ? AS LANGUAGE " +
      "        FROM ldt_routing_order left join (select AD_Org_ID, Name from AD_Org) table1 on (ldt_routing_order.AD_Org_ID = table1.AD_Org_ID) left join (select C_Doctype_ID, Name from C_Doctype) table2 on (ldt_routing_order.C_Doctype_ID = table2.C_Doctype_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL2 on (table2.C_DocType_ID = tableTRL2.C_DocType_ID and tableTRL2.AD_Language = ?)  left join (select AD_User_ID, Name from AD_User) table3 on (ldt_routing_order.AD_User_ID = table3.AD_User_ID) left join (select C_BPartner_ID, Name from C_BPartner) table4 on (ldt_routing_order.Cliente_ID = table4.C_BPartner_ID) left join (select C_BPartner_Location_ID, Name from C_BPartner_Location) table5 on (ldt_routing_order.Cliente_Direccion_ID =  table5.C_BPartner_Location_ID) left join (select C_Incoterms_ID, Name from C_Incoterms) table6 on (ldt_routing_order.C_Incoterms_ID = table6.C_Incoterms_ID) left join ad_ref_list_v list1 on (ldt_routing_order.Modalidad = list1.value and list1.ad_reference_id = '084746C4C4934924AABBD1EAB1DB9BF5' and list1.ad_language = ?)  left join ad_ref_list_v list2 on (ldt_routing_order.Tipo = list2.value and list2.ad_reference_id = '336BA96AA2694CB0BD2689AB13A88F03' and list2.ad_language = ?)  left join (select C_BPartner_ID, Name from C_BPartner) table7 on (ldt_routing_order.Vendedor_ID =  table7.C_BPartner_ID) left join ad_ref_list_v list3 on (list3.ad_reference_id = '1F931E9A5FAB445A8FD2F069ADD2DBE4' and list3.ad_language = ?  AND ldt_routing_order.Processed = TO_CHAR(list3.value))" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND ldt_routing_order.LDT_Routing_Order_ID = ? " +
      "        AND ldt_routing_order.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND ldt_routing_order.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Embarque5676F83A54194338ABD55133021720F1Data objectEmbarque5676F83A54194338ABD55133021720F1Data = new Embarque5676F83A54194338ABD55133021720F1Data();
        objectEmbarque5676F83A54194338ABD55133021720F1Data.created = UtilSql.getValue(result, "created");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.updated = UtilSql.getValue(result, "updated");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.cDoctypeIdr = UtilSql.getValue(result, "c_doctype_idr");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.numembarque = UtilSql.getValue(result, "numembarque");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.documentno = UtilSql.getValue(result, "documentno");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.ldtCotizacionId = UtilSql.getValue(result, "ldt_cotizacion_id");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.fecha = UtilSql.getDateValue(result, "fecha", "dd-MM-yyyy");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.estado = UtilSql.getValue(result, "estado");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.puertoTerminalOrigenId = UtilSql.getValue(result, "puerto_terminal_origen_id");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.puertoTerminalDestinoId = UtilSql.getValue(result, "puerto_terminal_destino_id");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.fechaEstimada = UtilSql.getDateValue(result, "fecha_estimada", "dd-MM-yyyy");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.cBpartnerRuc = UtilSql.getValue(result, "c_bpartner_ruc");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.cBpartnerLocationId = UtilSql.getValue(result, "c_bpartner_location_id");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.telfShipper = UtilSql.getValue(result, "telf_shipper");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.adUserId = UtilSql.getValue(result, "ad_user_id");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.adUserIdr = UtilSql.getValue(result, "ad_user_idr");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.mail = UtilSql.getValue(result, "mail");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.clienteId = UtilSql.getValue(result, "cliente_id");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.clienteIdr = UtilSql.getValue(result, "cliente_idr");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.clienteRuc = UtilSql.getValue(result, "cliente_ruc");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.clienteDireccionId = UtilSql.getValue(result, "cliente_direccion_id");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.clienteDireccionIdr = UtilSql.getValue(result, "cliente_direccion_idr");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.cIncotermsId = UtilSql.getValue(result, "c_incoterms_id");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.cIncotermsIdr = UtilSql.getValue(result, "c_incoterms_idr");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.modalidad = UtilSql.getValue(result, "modalidad");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.modalidadr = UtilSql.getValue(result, "modalidadr");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.tipo = UtilSql.getValue(result, "tipo");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.tipor = UtilSql.getValue(result, "tipor");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.agenteId = UtilSql.getValue(result, "agente_id");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.tarifaVenta = UtilSql.getValue(result, "tarifa_venta");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.tarifaNegociada = UtilSql.getValue(result, "tarifa_negociada");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.costosLocales = UtilSql.getValue(result, "costos_locales");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.tarifaNegociadaEur = UtilSql.getValue(result, "tarifa_negociada_eur");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.costosLocalesEur = UtilSql.getValue(result, "costos_locales_eur");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.tarifaCompra = UtilSql.getValue(result, "tarifa_compra");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.cod = UtilSql.getValue(result, "cod");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.vendedorId = UtilSql.getValue(result, "vendedor_id");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.vendedorIdr = UtilSql.getValue(result, "vendedor_idr");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.referenciaCantidad = UtilSql.getValue(result, "referencia_cantidad");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.observacion = UtilSql.getValue(result, "observacion");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.esFinEmbarque = UtilSql.getValue(result, "es_fin_embarque");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.processed = UtilSql.getValue(result, "processed");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.processedBtn = UtilSql.getValue(result, "processed_btn");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.tarifaVentaEur = UtilSql.getValue(result, "tarifa_venta_eur");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.mPricelistVersionId = UtilSql.getValue(result, "m_pricelist_version_id");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.ldtRoutingOrderId = UtilSql.getValue(result, "ldt_routing_order_id");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.isactive = UtilSql.getValue(result, "isactive");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.language = UtilSql.getValue(result, "language");
        objectEmbarque5676F83A54194338ABD55133021720F1Data.adUserClient = "";
        objectEmbarque5676F83A54194338ABD55133021720F1Data.adOrgClient = "";
        objectEmbarque5676F83A54194338ABD55133021720F1Data.createdby = "";
        objectEmbarque5676F83A54194338ABD55133021720F1Data.trBgcolor = "";
        objectEmbarque5676F83A54194338ABD55133021720F1Data.totalCount = "";
        objectEmbarque5676F83A54194338ABD55133021720F1Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectEmbarque5676F83A54194338ABD55133021720F1Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Embarque5676F83A54194338ABD55133021720F1Data objectEmbarque5676F83A54194338ABD55133021720F1Data[] = new Embarque5676F83A54194338ABD55133021720F1Data[vector.size()];
    vector.copyInto(objectEmbarque5676F83A54194338ABD55133021720F1Data);
    return(objectEmbarque5676F83A54194338ABD55133021720F1Data);
  }

/**
Create a registry
 */
  public static Embarque5676F83A54194338ABD55133021720F1Data[] set(String cBpartnerRuc, String mail, String fechaEstimada, String tarifaCompra, String cBpartnerLocationId, String esFinEmbarque, String clienteId, String clienteIdr, String ldtRoutingOrderId, String tipo, String adClientId, String cIncotermsId, String tarifaNegociadaEur, String updatedby, String updatedbyr, String tarifaVenta, String tarifaVentaEur, String processed, String processedBtn, String vendedorId, String telfShipper, String adOrgId, String adUserId, String puertoTerminalDestinoId, String modalidad, String mPricelistVersionId, String observacion, String ldtCotizacionId, String estado, String costosLocales, String costosLocalesEur, String clienteDireccionId, String documentno, String createdby, String createdbyr, String referenciaCantidad, String numembarque, String cod, String clienteRuc, String fecha, String isactive, String tarifaNegociada, String cDoctypeId, String puertoTerminalOrigenId, String cBpartnerId, String agenteId)    throws ServletException {
    Embarque5676F83A54194338ABD55133021720F1Data objectEmbarque5676F83A54194338ABD55133021720F1Data[] = new Embarque5676F83A54194338ABD55133021720F1Data[1];
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0] = new Embarque5676F83A54194338ABD55133021720F1Data();
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].created = "";
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].createdbyr = createdbyr;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].updated = "";
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].updatedTimeStamp = "";
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].updatedby = updatedby;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].updatedbyr = updatedbyr;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].adOrgId = adOrgId;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].adOrgIdr = "";
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].cDoctypeId = cDoctypeId;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].cDoctypeIdr = "";
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].numembarque = numembarque;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].documentno = documentno;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].ldtCotizacionId = ldtCotizacionId;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].fecha = fecha;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].estado = estado;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].puertoTerminalOrigenId = puertoTerminalOrigenId;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].puertoTerminalDestinoId = puertoTerminalDestinoId;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].fechaEstimada = fechaEstimada;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].cBpartnerId = cBpartnerId;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].cBpartnerRuc = cBpartnerRuc;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].cBpartnerLocationId = cBpartnerLocationId;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].telfShipper = telfShipper;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].adUserId = adUserId;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].adUserIdr = "";
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].mail = mail;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].clienteId = clienteId;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].clienteIdr = clienteIdr;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].clienteRuc = clienteRuc;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].clienteDireccionId = clienteDireccionId;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].clienteDireccionIdr = "";
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].cIncotermsId = cIncotermsId;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].cIncotermsIdr = "";
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].modalidad = modalidad;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].modalidadr = "";
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].tipo = tipo;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].tipor = "";
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].agenteId = agenteId;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].tarifaVenta = tarifaVenta;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].tarifaNegociada = tarifaNegociada;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].costosLocales = costosLocales;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].tarifaNegociadaEur = tarifaNegociadaEur;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].costosLocalesEur = costosLocalesEur;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].tarifaCompra = tarifaCompra;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].cod = cod;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].vendedorId = vendedorId;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].vendedorIdr = "";
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].referenciaCantidad = referenciaCantidad;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].observacion = observacion;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].esFinEmbarque = esFinEmbarque;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].processed = processed;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].processedBtn = processedBtn;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].tarifaVentaEur = tarifaVentaEur;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].mPricelistVersionId = mPricelistVersionId;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].adClientId = adClientId;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].ldtRoutingOrderId = ldtRoutingOrderId;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].isactive = isactive;
    objectEmbarque5676F83A54194338ABD55133021720F1Data[0].language = "";
    return objectEmbarque5676F83A54194338ABD55133021720F1Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef288AAF7724AB4659AC3EA3CAB946388E_0(ConnectionProvider connectionProvider, String Cliente_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Cliente_ID FROM C_BPartner left join (select C_BPartner_ID, Name from C_BPartner) table2 on (C_BPartner.C_BPartner_ID = table2.C_BPartner_ID) WHERE C_BPartner.isActive='Y' AND C_BPartner.C_BPartner_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, Cliente_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "cliente_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef3C3EE0E1BB8041268CB1EF103CC6E8D0_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefA68E17A6693C4D78B7972B1419D3E24E_2(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE ldt_routing_order" +
      "        SET AD_Org_ID = (?) , C_Doctype_ID = (?) , Numembarque = (?) , Documentno = (?) , LDT_Cotizacion_ID = (?) , Fecha = TO_DATE(?) , Estado = (?) , Puerto_Terminal_Origen_ID = (?) , Puerto_Terminal_Destino_ID = (?) , Fecha_Estimada = TO_DATE(?) , C_Bpartner_ID = (?) , C_Bpartner_Ruc = (?) , C_Bpartner_Location_ID = (?) , Telf_Shipper = (?) , AD_User_ID = (?) , Mail = (?) , Cliente_ID = (?) , Cliente_Ruc = (?) , Cliente_Direccion_ID = (?) , C_Incoterms_ID = (?) , Modalidad = (?) , Tipo = (?) , Agente_ID = (?) , Tarifa_Venta = TO_NUMBER(?) , Tarifa_Negociada = TO_NUMBER(?) , Costos_Locales = TO_NUMBER(?) , tarifa_negociada_eur = TO_NUMBER(?) , costos_locales_eur = TO_NUMBER(?) , Tarifa_Compra = TO_NUMBER(?) , Cod = TO_NUMBER(?) , Vendedor_ID = (?) , referencia_cantidad = TO_NUMBER(?) , Observacion = (?) , ES_Fin_Embarque = (?) , Processed = (?) , tarifa_venta_eur = TO_NUMBER(?) , M_Pricelist_Version_ID = (?) , AD_Client_ID = (?) , LDT_Routing_Order_ID = (?) , Isactive = (?) , updated = now(), updatedby = ? " +
      "        WHERE ldt_routing_order.LDT_Routing_Order_ID = ? " +
      "        AND ldt_routing_order.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND ldt_routing_order.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numembarque);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtCotizacionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecha);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, puertoTerminalOrigenId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, puertoTerminalDestinoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaEstimada);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerRuc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, telfShipper);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mail);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clienteId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clienteRuc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clienteDireccionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cIncotermsId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, modalidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agenteId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tarifaVenta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tarifaNegociada);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, costosLocales);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tarifaNegociadaEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, costosLocalesEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tarifaCompra);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cod);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vendedorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, referenciaCantidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, observacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, esFinEmbarque);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tarifaVentaEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistVersionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO ldt_routing_order " +
      "        (AD_Org_ID, C_Doctype_ID, Numembarque, Documentno, LDT_Cotizacion_ID, Fecha, Estado, Puerto_Terminal_Origen_ID, Puerto_Terminal_Destino_ID, Fecha_Estimada, C_Bpartner_ID, C_Bpartner_Ruc, C_Bpartner_Location_ID, Telf_Shipper, AD_User_ID, Mail, Cliente_ID, Cliente_Ruc, Cliente_Direccion_ID, C_Incoterms_ID, Modalidad, Tipo, Agente_ID, Tarifa_Venta, Tarifa_Negociada, Costos_Locales, tarifa_negociada_eur, costos_locales_eur, Tarifa_Compra, Cod, Vendedor_ID, referencia_cantidad, Observacion, ES_Fin_Embarque, Processed, tarifa_venta_eur, M_Pricelist_Version_ID, AD_Client_ID, LDT_Routing_Order_ID, Isactive, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), TO_DATE(?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), TO_NUMBER(?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numembarque);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtCotizacionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecha);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, puertoTerminalOrigenId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, puertoTerminalDestinoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaEstimada);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerRuc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, telfShipper);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mail);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clienteId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clienteRuc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clienteDireccionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cIncotermsId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, modalidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agenteId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tarifaVenta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tarifaNegociada);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, costosLocales);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tarifaNegociadaEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, costosLocalesEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tarifaCompra);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cod);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vendedorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, referenciaCantidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, observacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, esFinEmbarque);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tarifaVentaEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistVersionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM ldt_routing_order" +
      "        WHERE ldt_routing_order.LDT_Routing_Order_ID = ? " +
      "        AND ldt_routing_order.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND ldt_routing_order.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM ldt_routing_order" +
      "         WHERE ldt_routing_order.LDT_Routing_Order_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM ldt_routing_order" +
      "         WHERE ldt_routing_order.LDT_Routing_Order_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
