//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.logisticadetransporte.parametrizacion.Cotizacion;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData implements FieldProvider {
static Logger log4j = Logger.getLogger(MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String puertoTerminalOrigenId;
  public String puertoTerminalDestinoId;
  public String cBpartnerId;
  public String cCurrencyId;
  public String cCurrencyIdr;
  public String mVersionTarifaId;
  public String ldtContratoPuertoId;
  public String ldtContratoPuertoIdr;
  public String grandTotal;
  public String totalCostosEur;
  public String totalNegociado;
  public String totalNegociadoEur;
  public String total;
  public String totalOpcion;
  public String totalEur;
  public String seleccionarOp;
  public String fechaValidez;
  public String ldtDiasTransito;
  public String ldtDiasLibres;
  public String isactive;
  public String mPricelistId;
  public String mPricelistVersionId;
  public String line;
  public String adOrgId;
  public String ldtCotizacionId;
  public String adClientId;
  public String ldtCotizacionNavieraId;
  public String cCountryId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("puerto_terminal_origen_id") || fieldName.equals("puertoTerminalOrigenId"))
      return puertoTerminalOrigenId;
    else if (fieldName.equalsIgnoreCase("puerto_terminal_destino_id") || fieldName.equals("puertoTerminalDestinoId"))
      return puertoTerminalDestinoId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("c_currency_id") || fieldName.equals("cCurrencyId"))
      return cCurrencyId;
    else if (fieldName.equalsIgnoreCase("c_currency_idr") || fieldName.equals("cCurrencyIdr"))
      return cCurrencyIdr;
    else if (fieldName.equalsIgnoreCase("m_version_tarifa_id") || fieldName.equals("mVersionTarifaId"))
      return mVersionTarifaId;
    else if (fieldName.equalsIgnoreCase("ldt_contrato_puerto_id") || fieldName.equals("ldtContratoPuertoId"))
      return ldtContratoPuertoId;
    else if (fieldName.equalsIgnoreCase("ldt_contrato_puerto_idr") || fieldName.equals("ldtContratoPuertoIdr"))
      return ldtContratoPuertoIdr;
    else if (fieldName.equalsIgnoreCase("grand_total") || fieldName.equals("grandTotal"))
      return grandTotal;
    else if (fieldName.equalsIgnoreCase("total_costos_eur") || fieldName.equals("totalCostosEur"))
      return totalCostosEur;
    else if (fieldName.equalsIgnoreCase("total_negociado") || fieldName.equals("totalNegociado"))
      return totalNegociado;
    else if (fieldName.equalsIgnoreCase("total_negociado_eur") || fieldName.equals("totalNegociadoEur"))
      return totalNegociadoEur;
    else if (fieldName.equalsIgnoreCase("total"))
      return total;
    else if (fieldName.equalsIgnoreCase("total_opcion") || fieldName.equals("totalOpcion"))
      return totalOpcion;
    else if (fieldName.equalsIgnoreCase("total_eur") || fieldName.equals("totalEur"))
      return totalEur;
    else if (fieldName.equalsIgnoreCase("seleccionar_op") || fieldName.equals("seleccionarOp"))
      return seleccionarOp;
    else if (fieldName.equalsIgnoreCase("fecha_validez") || fieldName.equals("fechaValidez"))
      return fechaValidez;
    else if (fieldName.equalsIgnoreCase("ldt_dias_transito") || fieldName.equals("ldtDiasTransito"))
      return ldtDiasTransito;
    else if (fieldName.equalsIgnoreCase("ldt_dias_libres") || fieldName.equals("ldtDiasLibres"))
      return ldtDiasLibres;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("m_pricelist_id") || fieldName.equals("mPricelistId"))
      return mPricelistId;
    else if (fieldName.equalsIgnoreCase("m_pricelist_version_id") || fieldName.equals("mPricelistVersionId"))
      return mPricelistVersionId;
    else if (fieldName.equalsIgnoreCase("line"))
      return line;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ldt_cotizacion_id") || fieldName.equals("ldtCotizacionId"))
      return ldtCotizacionId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("ldt_cotizacion_naviera_id") || fieldName.equals("ldtCotizacionNavieraId"))
      return ldtCotizacionNavieraId;
    else if (fieldName.equalsIgnoreCase("c_country_id") || fieldName.equals("cCountryId"))
      return cCountryId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String ldtCotizacionId, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, ldtCotizacionId, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String ldtCotizacionId, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(ldt_cotizacion_naviera.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = ldt_cotizacion_naviera.CreatedBy) as CreatedByR, " +
      "        to_char(ldt_cotizacion_naviera.Updated, ?) as updated, " +
      "        to_char(ldt_cotizacion_naviera.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        ldt_cotizacion_naviera.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = ldt_cotizacion_naviera.UpdatedBy) as UpdatedByR," +
      "        ldt_cotizacion_naviera.Puerto_Terminal_Origen_ID, " +
      "ldt_cotizacion_naviera.Puerto_Terminal_Destino_ID, " +
      "ldt_cotizacion_naviera.C_Bpartner_ID, " +
      "ldt_cotizacion_naviera.C_Currency_ID, " +
      "(CASE WHEN ldt_cotizacion_naviera.C_Currency_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.ISO_Code), ''))),'') ) END) AS C_Currency_IDR, " +
      "ldt_cotizacion_naviera.M_Version_Tarifa_ID, " +
      "ldt_cotizacion_naviera.ldt_contrato_puerto_id, " +
      "(CASE WHEN ldt_cotizacion_naviera.ldt_contrato_puerto_id IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Codigo), ''))),'') ) END) AS ldt_contrato_puerto_idR, " +
      "ldt_cotizacion_naviera.Grand_Total, " +
      "ldt_cotizacion_naviera.total_costos_eur, " +
      "ldt_cotizacion_naviera.Total_Negociado, " +
      "ldt_cotizacion_naviera.total_negociado_eur, " +
      "ldt_cotizacion_naviera.Total, " +
      "ldt_cotizacion_naviera.Total_Opcion, " +
      "ldt_cotizacion_naviera.total_eur, " +
      "ldt_cotizacion_naviera.Seleccionar_Op, " +
      "ldt_cotizacion_naviera.Fecha_Validez, " +
      "ldt_cotizacion_naviera.ldt_dias_transito, " +
      "ldt_cotizacion_naviera.ldt_dias_libres, " +
      "COALESCE(ldt_cotizacion_naviera.Isactive, 'N') AS Isactive, " +
      "ldt_cotizacion_naviera.M_Pricelist_ID, " +
      "ldt_cotizacion_naviera.m_pricelist_version_id, " +
      "ldt_cotizacion_naviera.Line, " +
      "ldt_cotizacion_naviera.AD_Org_ID, " +
      "ldt_cotizacion_naviera.LDT_Cotizacion_ID, " +
      "ldt_cotizacion_naviera.AD_Client_ID, " +
      "ldt_cotizacion_naviera.LDT_Cotizacion_Naviera_ID, " +
      "ldt_cotizacion_naviera.c_country_id, " +
      "        ? AS LANGUAGE " +
      "        FROM ldt_cotizacion_naviera left join (select C_Currency_ID, ISO_Code from C_Currency) table1 on (ldt_cotizacion_naviera.C_Currency_ID = table1.C_Currency_ID) left join (select LDT_Contrato_Puerto_ID, Codigo from ldt_contrato_puerto) table2 on (ldt_cotizacion_naviera.ldt_contrato_puerto_id =  table2.LDT_Contrato_Puerto_ID)" +
      "        WHERE 2=2 " +
      "        AND 1=1 ";
    strSql = strSql + ((ldtCotizacionId==null || ldtCotizacionId.equals(""))?"":"  AND ldt_cotizacion_naviera.LDT_Cotizacion_ID = ?  ");
    strSql = strSql + 
      "        AND ldt_cotizacion_naviera.LDT_Cotizacion_Naviera_ID = ? " +
      "        AND ldt_cotizacion_naviera.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND ldt_cotizacion_naviera.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      if (ldtCotizacionId != null && !(ldtCotizacionId.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtCotizacionId);
      }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData = new MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData();
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.created = UtilSql.getValue(result, "created");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.updated = UtilSql.getValue(result, "updated");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.updatedby = UtilSql.getValue(result, "updatedby");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.puertoTerminalOrigenId = UtilSql.getValue(result, "puerto_terminal_origen_id");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.puertoTerminalDestinoId = UtilSql.getValue(result, "puerto_terminal_destino_id");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.cCurrencyId = UtilSql.getValue(result, "c_currency_id");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.cCurrencyIdr = UtilSql.getValue(result, "c_currency_idr");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.mVersionTarifaId = UtilSql.getValue(result, "m_version_tarifa_id");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.ldtContratoPuertoId = UtilSql.getValue(result, "ldt_contrato_puerto_id");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.ldtContratoPuertoIdr = UtilSql.getValue(result, "ldt_contrato_puerto_idr");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.grandTotal = UtilSql.getValue(result, "grand_total");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.totalCostosEur = UtilSql.getValue(result, "total_costos_eur");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.totalNegociado = UtilSql.getValue(result, "total_negociado");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.totalNegociadoEur = UtilSql.getValue(result, "total_negociado_eur");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.total = UtilSql.getValue(result, "total");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.totalOpcion = UtilSql.getValue(result, "total_opcion");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.totalEur = UtilSql.getValue(result, "total_eur");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.seleccionarOp = UtilSql.getValue(result, "seleccionar_op");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.fechaValidez = UtilSql.getDateValue(result, "fecha_validez", "dd-MM-yyyy");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.ldtDiasTransito = UtilSql.getValue(result, "ldt_dias_transito");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.ldtDiasLibres = UtilSql.getValue(result, "ldt_dias_libres");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.isactive = UtilSql.getValue(result, "isactive");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.mPricelistId = UtilSql.getValue(result, "m_pricelist_id");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.mPricelistVersionId = UtilSql.getValue(result, "m_pricelist_version_id");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.line = UtilSql.getValue(result, "line");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.ldtCotizacionId = UtilSql.getValue(result, "ldt_cotizacion_id");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.ldtCotizacionNavieraId = UtilSql.getValue(result, "ldt_cotizacion_naviera_id");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.cCountryId = UtilSql.getValue(result, "c_country_id");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.language = UtilSql.getValue(result, "language");
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.adUserClient = "";
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.adOrgClient = "";
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.createdby = "";
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.trBgcolor = "";
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.totalCount = "";
        objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[] = new MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[vector.size()];
    vector.copyInto(objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData);
    return(objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData);
  }

/**
Create a registry
 */
  public static MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[] set(String ldtCotizacionId, String ldtDiasTransito, String puertoTerminalOrigenId, String fechaValidez, String totalOpcion, String cCountryId, String total, String cBpartnerId, String mPricelistVersionId, String ldtContratoPuertoId, String isactive, String updatedby, String updatedbyr, String grandTotal, String mVersionTarifaId, String totalNegociado, String line, String mPricelistId, String cCurrencyId, String adClientId, String puertoTerminalDestinoId, String createdby, String createdbyr, String adOrgId, String totalCostosEur, String seleccionarOp, String ldtCotizacionNavieraId, String totalEur, String ldtDiasLibres, String totalNegociadoEur)    throws ServletException {
    MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[] = new MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[1];
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0] = new MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData();
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].created = "";
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].createdbyr = createdbyr;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].updated = "";
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].updatedTimeStamp = "";
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].updatedby = updatedby;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].updatedbyr = updatedbyr;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].puertoTerminalOrigenId = puertoTerminalOrigenId;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].puertoTerminalDestinoId = puertoTerminalDestinoId;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].cBpartnerId = cBpartnerId;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].cCurrencyId = cCurrencyId;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].cCurrencyIdr = "";
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].mVersionTarifaId = mVersionTarifaId;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].ldtContratoPuertoId = ldtContratoPuertoId;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].ldtContratoPuertoIdr = "";
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].grandTotal = grandTotal;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].totalCostosEur = totalCostosEur;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].totalNegociado = totalNegociado;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].totalNegociadoEur = totalNegociadoEur;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].total = total;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].totalOpcion = totalOpcion;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].totalEur = totalEur;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].seleccionarOp = seleccionarOp;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].fechaValidez = fechaValidez;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].ldtDiasTransito = ldtDiasTransito;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].ldtDiasLibres = ldtDiasLibres;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].isactive = isactive;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].mPricelistId = mPricelistId;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].mPricelistVersionId = mPricelistVersionId;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].line = line;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].adOrgId = adOrgId;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].ldtCotizacionId = ldtCotizacionId;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].adClientId = adClientId;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].ldtCotizacionNavieraId = ldtCotizacionNavieraId;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].cCountryId = cCountryId;
    objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0].language = "";
    return objectMediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef7939AB039A104E06A45430FA7C5425D4_0(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef9F4F2DA3436A4125B6793D632DE0052E(ConnectionProvider connectionProvider, String LDT_COTIZACION_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT COALESCE(MAX(LINE),0)+10 AS DefaultValue FROM LDT_COTIZACION_NAVIERA WHERE LDT_COTIZACION_ID=? ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, LDT_COTIZACION_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefCE837539DA6F4C7BB58AAFB177F8AD2D_1(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
return the parent ID
 */
  public static String selectParentID(ConnectionProvider connectionProvider, String key)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT ldt_cotizacion_naviera.LDT_Cotizacion_ID AS NAME" +
      "        FROM ldt_cotizacion_naviera" +
      "        WHERE ldt_cotizacion_naviera.LDT_Cotizacion_Naviera_ID = ?";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParent(ConnectionProvider connectionProvider, String ldtCotizacionId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table2.Name), '')) || ' - ' || TO_CHAR(COALESCE(TO_CHAR(table1.Documentno), ''))) AS NAME FROM ldt_cotizacion left join (select LDT_Cotizacion_ID, C_Bpartner_ID, Documentno from LDT_Cotizacion) table1 on (ldt_cotizacion.LDT_Cotizacion_ID = table1.LDT_Cotizacion_ID) left join (select C_BPartner_ID, Name from C_BPartner) table2 on (table1.C_Bpartner_ID = table2.C_BPartner_ID) WHERE ldt_cotizacion.LDT_Cotizacion_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtCotizacionId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParentTrl(ConnectionProvider connectionProvider, String ldtCotizacionId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table2.Name), '')) || ' - ' || TO_CHAR(COALESCE(TO_CHAR(table1.Documentno), ''))) AS NAME FROM ldt_cotizacion left join (select LDT_Cotizacion_ID, C_Bpartner_ID, Documentno from LDT_Cotizacion) table1 on (ldt_cotizacion.LDT_Cotizacion_ID = table1.LDT_Cotizacion_ID) left join (select C_BPartner_ID, Name from C_BPartner) table2 on (table1.C_Bpartner_ID = table2.C_BPartner_ID) WHERE ldt_cotizacion.LDT_Cotizacion_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtCotizacionId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE ldt_cotizacion_naviera" +
      "        SET Puerto_Terminal_Origen_ID = (?) , Puerto_Terminal_Destino_ID = (?) , C_Bpartner_ID = (?) , C_Currency_ID = (?) , M_Version_Tarifa_ID = (?) , ldt_contrato_puerto_id = (?) , Grand_Total = TO_NUMBER(?) , total_costos_eur = TO_NUMBER(?) , Total_Negociado = TO_NUMBER(?) , total_negociado_eur = TO_NUMBER(?) , Total = TO_NUMBER(?) , Total_Opcion = TO_NUMBER(?) , total_eur = TO_NUMBER(?) , Seleccionar_Op = (?) , Fecha_Validez = TO_DATE(?) , ldt_dias_transito = (?) , ldt_dias_libres = TO_NUMBER(?) , Isactive = (?) , M_Pricelist_ID = (?) , m_pricelist_version_id = (?) , Line = TO_NUMBER(?) , AD_Org_ID = (?) , LDT_Cotizacion_ID = (?) , AD_Client_ID = (?) , LDT_Cotizacion_Naviera_ID = (?) , c_country_id = (?) , updated = now(), updatedby = ? " +
      "        WHERE ldt_cotizacion_naviera.LDT_Cotizacion_Naviera_ID = ? " +
      "                 AND ldt_cotizacion_naviera.LDT_Cotizacion_ID = ? " +
      "        AND ldt_cotizacion_naviera.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND ldt_cotizacion_naviera.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, puertoTerminalOrigenId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, puertoTerminalDestinoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mVersionTarifaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtContratoPuertoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, grandTotal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalCostosEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalNegociado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalNegociadoEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, total);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalOpcion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, seleccionarOp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaValidez);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtDiasTransito);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtDiasLibres);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistVersionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, line);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtCotizacionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtCotizacionNavieraId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCountryId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtCotizacionNavieraId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtCotizacionId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO ldt_cotizacion_naviera " +
      "        (Puerto_Terminal_Origen_ID, Puerto_Terminal_Destino_ID, C_Bpartner_ID, C_Currency_ID, M_Version_Tarifa_ID, ldt_contrato_puerto_id, Grand_Total, total_costos_eur, Total_Negociado, total_negociado_eur, Total, Total_Opcion, total_eur, Seleccionar_Op, Fecha_Validez, ldt_dias_transito, ldt_dias_libres, Isactive, M_Pricelist_ID, m_pricelist_version_id, Line, AD_Org_ID, LDT_Cotizacion_ID, AD_Client_ID, LDT_Cotizacion_Naviera_ID, c_country_id, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), TO_DATE(?), (?), TO_NUMBER(?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, puertoTerminalOrigenId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, puertoTerminalDestinoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mVersionTarifaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtContratoPuertoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, grandTotal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalCostosEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalNegociado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalNegociadoEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, total);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalOpcion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, seleccionarOp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaValidez);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtDiasTransito);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtDiasLibres);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistVersionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, line);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtCotizacionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtCotizacionNavieraId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCountryId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String ldtCotizacionId, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM ldt_cotizacion_naviera" +
      "        WHERE ldt_cotizacion_naviera.LDT_Cotizacion_Naviera_ID = ? " +
      "                 AND ldt_cotizacion_naviera.LDT_Cotizacion_ID = ? " +
      "        AND ldt_cotizacion_naviera.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND ldt_cotizacion_naviera.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtCotizacionId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM ldt_cotizacion_naviera" +
      "         WHERE ldt_cotizacion_naviera.LDT_Cotizacion_Naviera_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM ldt_cotizacion_naviera" +
      "         WHERE ldt_cotizacion_naviera.LDT_Cotizacion_Naviera_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
