//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.logisticadetransporte.parametrizacion.FreeHand;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class FreeHand873BB22017634E47876049F4C5489617Data implements FieldProvider {
static Logger log4j = Logger.getLogger(FreeHand873BB22017634E47876049F4C5489617Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String cDoctypeId;
  public String cDoctypeIdr;
  public String numembarque;
  public String fecha;
  public String estado;
  public String puertoTerminalOrigenId;
  public String puertoTerminalDestinoId;
  public String cBpartnerMtId;
  public String ldtContratoPuertoId;
  public String fechaEstimada;
  public String vendedorId;
  public String vendedorIdr;
  public String mPricelistVersionId;
  public String mPricelistVersionIdr;
  public String cBpartnerId;
  public String cBpartnerRuc;
  public String cBpartnerLocationId;
  public String telfShipper;
  public String adUserId;
  public String adUserIdr;
  public String mail;
  public String clienteId;
  public String clienteIdr;
  public String clienteRuc;
  public String clienteDireccionId;
  public String clienteDireccionIdr;
  public String modalidad;
  public String modalidadr;
  public String tipo;
  public String tipor;
  public String cIncotermsId;
  public String cIncotermsIdr;
  public String agenteId;
  public String costosLocales;
  public String costosLocalesEur;
  public String totalNegociado;
  public String totalNegociadoEur;
  public String cod;
  public String observacion;
  public String embarque;
  public String esFinEmbarque;
  public String isactive;
  public String adClientId;
  public String mPricelistId;
  public String ldtRoutingOrderId;
  public String cCountryId;
  public String tarifaVenta;
  public String documentno;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("c_doctype_idr") || fieldName.equals("cDoctypeIdr"))
      return cDoctypeIdr;
    else if (fieldName.equalsIgnoreCase("numembarque"))
      return numembarque;
    else if (fieldName.equalsIgnoreCase("fecha"))
      return fecha;
    else if (fieldName.equalsIgnoreCase("estado"))
      return estado;
    else if (fieldName.equalsIgnoreCase("puerto_terminal_origen_id") || fieldName.equals("puertoTerminalOrigenId"))
      return puertoTerminalOrigenId;
    else if (fieldName.equalsIgnoreCase("puerto_terminal_destino_id") || fieldName.equals("puertoTerminalDestinoId"))
      return puertoTerminalDestinoId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_mt_id") || fieldName.equals("cBpartnerMtId"))
      return cBpartnerMtId;
    else if (fieldName.equalsIgnoreCase("ldt_contrato_puerto_id") || fieldName.equals("ldtContratoPuertoId"))
      return ldtContratoPuertoId;
    else if (fieldName.equalsIgnoreCase("fecha_estimada") || fieldName.equals("fechaEstimada"))
      return fechaEstimada;
    else if (fieldName.equalsIgnoreCase("vendedor_id") || fieldName.equals("vendedorId"))
      return vendedorId;
    else if (fieldName.equalsIgnoreCase("vendedor_idr") || fieldName.equals("vendedorIdr"))
      return vendedorIdr;
    else if (fieldName.equalsIgnoreCase("m_pricelist_version_id") || fieldName.equals("mPricelistVersionId"))
      return mPricelistVersionId;
    else if (fieldName.equalsIgnoreCase("m_pricelist_version_idr") || fieldName.equals("mPricelistVersionIdr"))
      return mPricelistVersionIdr;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_ruc") || fieldName.equals("cBpartnerRuc"))
      return cBpartnerRuc;
    else if (fieldName.equalsIgnoreCase("c_bpartner_location_id") || fieldName.equals("cBpartnerLocationId"))
      return cBpartnerLocationId;
    else if (fieldName.equalsIgnoreCase("telf_shipper") || fieldName.equals("telfShipper"))
      return telfShipper;
    else if (fieldName.equalsIgnoreCase("ad_user_id") || fieldName.equals("adUserId"))
      return adUserId;
    else if (fieldName.equalsIgnoreCase("ad_user_idr") || fieldName.equals("adUserIdr"))
      return adUserIdr;
    else if (fieldName.equalsIgnoreCase("mail"))
      return mail;
    else if (fieldName.equalsIgnoreCase("cliente_id") || fieldName.equals("clienteId"))
      return clienteId;
    else if (fieldName.equalsIgnoreCase("cliente_idr") || fieldName.equals("clienteIdr"))
      return clienteIdr;
    else if (fieldName.equalsIgnoreCase("cliente_ruc") || fieldName.equals("clienteRuc"))
      return clienteRuc;
    else if (fieldName.equalsIgnoreCase("cliente_direccion_id") || fieldName.equals("clienteDireccionId"))
      return clienteDireccionId;
    else if (fieldName.equalsIgnoreCase("cliente_direccion_idr") || fieldName.equals("clienteDireccionIdr"))
      return clienteDireccionIdr;
    else if (fieldName.equalsIgnoreCase("modalidad"))
      return modalidad;
    else if (fieldName.equalsIgnoreCase("modalidadr"))
      return modalidadr;
    else if (fieldName.equalsIgnoreCase("tipo"))
      return tipo;
    else if (fieldName.equalsIgnoreCase("tipor"))
      return tipor;
    else if (fieldName.equalsIgnoreCase("c_incoterms_id") || fieldName.equals("cIncotermsId"))
      return cIncotermsId;
    else if (fieldName.equalsIgnoreCase("c_incoterms_idr") || fieldName.equals("cIncotermsIdr"))
      return cIncotermsIdr;
    else if (fieldName.equalsIgnoreCase("agente_id") || fieldName.equals("agenteId"))
      return agenteId;
    else if (fieldName.equalsIgnoreCase("costos_locales") || fieldName.equals("costosLocales"))
      return costosLocales;
    else if (fieldName.equalsIgnoreCase("costos_locales_eur") || fieldName.equals("costosLocalesEur"))
      return costosLocalesEur;
    else if (fieldName.equalsIgnoreCase("total_negociado") || fieldName.equals("totalNegociado"))
      return totalNegociado;
    else if (fieldName.equalsIgnoreCase("total_negociado_eur") || fieldName.equals("totalNegociadoEur"))
      return totalNegociadoEur;
    else if (fieldName.equalsIgnoreCase("cod"))
      return cod;
    else if (fieldName.equalsIgnoreCase("observacion"))
      return observacion;
    else if (fieldName.equalsIgnoreCase("embarque"))
      return embarque;
    else if (fieldName.equalsIgnoreCase("es_fin_embarque") || fieldName.equals("esFinEmbarque"))
      return esFinEmbarque;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("m_pricelist_id") || fieldName.equals("mPricelistId"))
      return mPricelistId;
    else if (fieldName.equalsIgnoreCase("ldt_routing_order_id") || fieldName.equals("ldtRoutingOrderId"))
      return ldtRoutingOrderId;
    else if (fieldName.equalsIgnoreCase("c_country_id") || fieldName.equals("cCountryId"))
      return cCountryId;
    else if (fieldName.equalsIgnoreCase("tarifa_venta") || fieldName.equals("tarifaVenta"))
      return tarifaVenta;
    else if (fieldName.equalsIgnoreCase("documentno"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static FreeHand873BB22017634E47876049F4C5489617Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static FreeHand873BB22017634E47876049F4C5489617Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(ldt_routing_order.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = ldt_routing_order.CreatedBy) as CreatedByR, " +
      "        to_char(ldt_routing_order.Updated, ?) as updated, " +
      "        to_char(ldt_routing_order.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        ldt_routing_order.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = ldt_routing_order.UpdatedBy) as UpdatedByR," +
      "        ldt_routing_order.AD_Org_ID, " +
      "(CASE WHEN ldt_routing_order.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "ldt_routing_order.C_Doctype_ID, " +
      "(CASE WHEN ldt_routing_order.C_Doctype_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL2.Name IS NULL THEN TO_CHAR(table2.Name) ELSE TO_CHAR(tableTRL2.Name) END)), ''))),'') ) END) AS C_Doctype_IDR, " +
      "ldt_routing_order.Numembarque, " +
      "ldt_routing_order.Fecha, " +
      "ldt_routing_order.Estado, " +
      "ldt_routing_order.Puerto_Terminal_Origen_ID, " +
      "ldt_routing_order.Puerto_Terminal_Destino_ID, " +
      "ldt_routing_order.c_bpartner_mt_id, " +
      "ldt_routing_order.ldt_contrato_puerto_id, " +
      "ldt_routing_order.Fecha_Estimada, " +
      "ldt_routing_order.Vendedor_ID, " +
      "(CASE WHEN ldt_routing_order.Vendedor_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.Name), ''))),'') ) END) AS Vendedor_IDR, " +
      "ldt_routing_order.M_Pricelist_Version_ID, " +
      "(CASE WHEN ldt_routing_order.M_Pricelist_Version_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name), ''))),'') ) END) AS M_Pricelist_Version_IDR, " +
      "ldt_routing_order.C_Bpartner_ID, " +
      "ldt_routing_order.C_Bpartner_Ruc, " +
      "ldt_routing_order.C_Bpartner_Location_ID, " +
      "ldt_routing_order.Telf_Shipper, " +
      "ldt_routing_order.AD_User_ID, " +
      "(CASE WHEN ldt_routing_order.AD_User_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table5.Name), ''))),'') ) END) AS AD_User_IDR, " +
      "ldt_routing_order.Mail, " +
      "ldt_routing_order.Cliente_ID, " +
      "(CASE WHEN ldt_routing_order.Cliente_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table6.Name), ''))),'') ) END) AS Cliente_IDR, " +
      "ldt_routing_order.Cliente_Ruc, " +
      "ldt_routing_order.Cliente_Direccion_ID, " +
      "(CASE WHEN ldt_routing_order.Cliente_Direccion_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table7.Name), ''))),'') ) END) AS Cliente_Direccion_IDR, " +
      "ldt_routing_order.Modalidad, " +
      "(CASE WHEN ldt_routing_order.Modalidad IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS ModalidadR, " +
      "ldt_routing_order.Tipo, " +
      "(CASE WHEN ldt_routing_order.Tipo IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list2.name),'') ) END) AS TipoR, " +
      "ldt_routing_order.C_Incoterms_ID, " +
      "(CASE WHEN ldt_routing_order.C_Incoterms_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table8.Name), ''))),'') ) END) AS C_Incoterms_IDR, " +
      "ldt_routing_order.Agente_ID, " +
      "ldt_routing_order.Costos_Locales, " +
      "ldt_routing_order.costos_locales_eur, " +
      "ldt_routing_order.Total_Negociado, " +
      "ldt_routing_order.total_negociado_eur, " +
      "ldt_routing_order.Cod, " +
      "ldt_routing_order.Observacion, " +
      "ldt_routing_order.Embarque, " +
      "ldt_routing_order.ES_Fin_Embarque, " +
      "COALESCE(ldt_routing_order.Isactive, 'N') AS Isactive, " +
      "ldt_routing_order.AD_Client_ID, " +
      "ldt_routing_order.M_Pricelist_ID, " +
      "ldt_routing_order.LDT_Routing_Order_ID, " +
      "ldt_routing_order.c_country_id, " +
      "ldt_routing_order.Tarifa_Venta, " +
      "ldt_routing_order.Documentno, " +
      "        ? AS LANGUAGE " +
      "        FROM ldt_routing_order left join (select AD_Org_ID, Name from AD_Org) table1 on (ldt_routing_order.AD_Org_ID = table1.AD_Org_ID) left join (select C_Doctype_ID, Name from C_Doctype) table2 on (ldt_routing_order.C_Doctype_ID = table2.C_Doctype_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL2 on (table2.C_DocType_ID = tableTRL2.C_DocType_ID and tableTRL2.AD_Language = ?)  left join (select C_BPartner_ID, Name from C_BPartner) table3 on (ldt_routing_order.Vendedor_ID =  table3.C_BPartner_ID) left join (select M_PriceList_Version_ID, Name from M_PriceList_Version) table4 on (ldt_routing_order.M_Pricelist_Version_ID =  table4.M_PriceList_Version_ID) left join (select AD_User_ID, Name from AD_User) table5 on (ldt_routing_order.AD_User_ID = table5.AD_User_ID) left join (select C_BPartner_ID, Name from C_BPartner) table6 on (ldt_routing_order.Cliente_ID = table6.C_BPartner_ID) left join (select C_BPartner_Location_ID, Name from C_BPartner_Location) table7 on (ldt_routing_order.Cliente_Direccion_ID =  table7.C_BPartner_Location_ID) left join ad_ref_list_v list1 on (ldt_routing_order.Modalidad = list1.value and list1.ad_reference_id = '084746C4C4934924AABBD1EAB1DB9BF5' and list1.ad_language = ?)  left join ad_ref_list_v list2 on (ldt_routing_order.Tipo = list2.value and list2.ad_reference_id = '336BA96AA2694CB0BD2689AB13A88F03' and list2.ad_language = ?)  left join (select C_Incoterms_ID, Name from C_Incoterms) table8 on (ldt_routing_order.C_Incoterms_ID = table8.C_Incoterms_ID)" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND ldt_routing_order.LDT_Routing_Order_ID = ? " +
      "        AND ldt_routing_order.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND ldt_routing_order.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FreeHand873BB22017634E47876049F4C5489617Data objectFreeHand873BB22017634E47876049F4C5489617Data = new FreeHand873BB22017634E47876049F4C5489617Data();
        objectFreeHand873BB22017634E47876049F4C5489617Data.created = UtilSql.getValue(result, "created");
        objectFreeHand873BB22017634E47876049F4C5489617Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectFreeHand873BB22017634E47876049F4C5489617Data.updated = UtilSql.getValue(result, "updated");
        objectFreeHand873BB22017634E47876049F4C5489617Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectFreeHand873BB22017634E47876049F4C5489617Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectFreeHand873BB22017634E47876049F4C5489617Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectFreeHand873BB22017634E47876049F4C5489617Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectFreeHand873BB22017634E47876049F4C5489617Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectFreeHand873BB22017634E47876049F4C5489617Data.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectFreeHand873BB22017634E47876049F4C5489617Data.cDoctypeIdr = UtilSql.getValue(result, "c_doctype_idr");
        objectFreeHand873BB22017634E47876049F4C5489617Data.numembarque = UtilSql.getValue(result, "numembarque");
        objectFreeHand873BB22017634E47876049F4C5489617Data.fecha = UtilSql.getDateValue(result, "fecha", "dd-MM-yyyy");
        objectFreeHand873BB22017634E47876049F4C5489617Data.estado = UtilSql.getValue(result, "estado");
        objectFreeHand873BB22017634E47876049F4C5489617Data.puertoTerminalOrigenId = UtilSql.getValue(result, "puerto_terminal_origen_id");
        objectFreeHand873BB22017634E47876049F4C5489617Data.puertoTerminalDestinoId = UtilSql.getValue(result, "puerto_terminal_destino_id");
        objectFreeHand873BB22017634E47876049F4C5489617Data.cBpartnerMtId = UtilSql.getValue(result, "c_bpartner_mt_id");
        objectFreeHand873BB22017634E47876049F4C5489617Data.ldtContratoPuertoId = UtilSql.getValue(result, "ldt_contrato_puerto_id");
        objectFreeHand873BB22017634E47876049F4C5489617Data.fechaEstimada = UtilSql.getDateValue(result, "fecha_estimada", "dd-MM-yyyy");
        objectFreeHand873BB22017634E47876049F4C5489617Data.vendedorId = UtilSql.getValue(result, "vendedor_id");
        objectFreeHand873BB22017634E47876049F4C5489617Data.vendedorIdr = UtilSql.getValue(result, "vendedor_idr");
        objectFreeHand873BB22017634E47876049F4C5489617Data.mPricelistVersionId = UtilSql.getValue(result, "m_pricelist_version_id");
        objectFreeHand873BB22017634E47876049F4C5489617Data.mPricelistVersionIdr = UtilSql.getValue(result, "m_pricelist_version_idr");
        objectFreeHand873BB22017634E47876049F4C5489617Data.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectFreeHand873BB22017634E47876049F4C5489617Data.cBpartnerRuc = UtilSql.getValue(result, "c_bpartner_ruc");
        objectFreeHand873BB22017634E47876049F4C5489617Data.cBpartnerLocationId = UtilSql.getValue(result, "c_bpartner_location_id");
        objectFreeHand873BB22017634E47876049F4C5489617Data.telfShipper = UtilSql.getValue(result, "telf_shipper");
        objectFreeHand873BB22017634E47876049F4C5489617Data.adUserId = UtilSql.getValue(result, "ad_user_id");
        objectFreeHand873BB22017634E47876049F4C5489617Data.adUserIdr = UtilSql.getValue(result, "ad_user_idr");
        objectFreeHand873BB22017634E47876049F4C5489617Data.mail = UtilSql.getValue(result, "mail");
        objectFreeHand873BB22017634E47876049F4C5489617Data.clienteId = UtilSql.getValue(result, "cliente_id");
        objectFreeHand873BB22017634E47876049F4C5489617Data.clienteIdr = UtilSql.getValue(result, "cliente_idr");
        objectFreeHand873BB22017634E47876049F4C5489617Data.clienteRuc = UtilSql.getValue(result, "cliente_ruc");
        objectFreeHand873BB22017634E47876049F4C5489617Data.clienteDireccionId = UtilSql.getValue(result, "cliente_direccion_id");
        objectFreeHand873BB22017634E47876049F4C5489617Data.clienteDireccionIdr = UtilSql.getValue(result, "cliente_direccion_idr");
        objectFreeHand873BB22017634E47876049F4C5489617Data.modalidad = UtilSql.getValue(result, "modalidad");
        objectFreeHand873BB22017634E47876049F4C5489617Data.modalidadr = UtilSql.getValue(result, "modalidadr");
        objectFreeHand873BB22017634E47876049F4C5489617Data.tipo = UtilSql.getValue(result, "tipo");
        objectFreeHand873BB22017634E47876049F4C5489617Data.tipor = UtilSql.getValue(result, "tipor");
        objectFreeHand873BB22017634E47876049F4C5489617Data.cIncotermsId = UtilSql.getValue(result, "c_incoterms_id");
        objectFreeHand873BB22017634E47876049F4C5489617Data.cIncotermsIdr = UtilSql.getValue(result, "c_incoterms_idr");
        objectFreeHand873BB22017634E47876049F4C5489617Data.agenteId = UtilSql.getValue(result, "agente_id");
        objectFreeHand873BB22017634E47876049F4C5489617Data.costosLocales = UtilSql.getValue(result, "costos_locales");
        objectFreeHand873BB22017634E47876049F4C5489617Data.costosLocalesEur = UtilSql.getValue(result, "costos_locales_eur");
        objectFreeHand873BB22017634E47876049F4C5489617Data.totalNegociado = UtilSql.getValue(result, "total_negociado");
        objectFreeHand873BB22017634E47876049F4C5489617Data.totalNegociadoEur = UtilSql.getValue(result, "total_negociado_eur");
        objectFreeHand873BB22017634E47876049F4C5489617Data.cod = UtilSql.getValue(result, "cod");
        objectFreeHand873BB22017634E47876049F4C5489617Data.observacion = UtilSql.getValue(result, "observacion");
        objectFreeHand873BB22017634E47876049F4C5489617Data.embarque = UtilSql.getValue(result, "embarque");
        objectFreeHand873BB22017634E47876049F4C5489617Data.esFinEmbarque = UtilSql.getValue(result, "es_fin_embarque");
        objectFreeHand873BB22017634E47876049F4C5489617Data.isactive = UtilSql.getValue(result, "isactive");
        objectFreeHand873BB22017634E47876049F4C5489617Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectFreeHand873BB22017634E47876049F4C5489617Data.mPricelistId = UtilSql.getValue(result, "m_pricelist_id");
        objectFreeHand873BB22017634E47876049F4C5489617Data.ldtRoutingOrderId = UtilSql.getValue(result, "ldt_routing_order_id");
        objectFreeHand873BB22017634E47876049F4C5489617Data.cCountryId = UtilSql.getValue(result, "c_country_id");
        objectFreeHand873BB22017634E47876049F4C5489617Data.tarifaVenta = UtilSql.getValue(result, "tarifa_venta");
        objectFreeHand873BB22017634E47876049F4C5489617Data.documentno = UtilSql.getValue(result, "documentno");
        objectFreeHand873BB22017634E47876049F4C5489617Data.language = UtilSql.getValue(result, "language");
        objectFreeHand873BB22017634E47876049F4C5489617Data.adUserClient = "";
        objectFreeHand873BB22017634E47876049F4C5489617Data.adOrgClient = "";
        objectFreeHand873BB22017634E47876049F4C5489617Data.createdby = "";
        objectFreeHand873BB22017634E47876049F4C5489617Data.trBgcolor = "";
        objectFreeHand873BB22017634E47876049F4C5489617Data.totalCount = "";
        objectFreeHand873BB22017634E47876049F4C5489617Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFreeHand873BB22017634E47876049F4C5489617Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FreeHand873BB22017634E47876049F4C5489617Data objectFreeHand873BB22017634E47876049F4C5489617Data[] = new FreeHand873BB22017634E47876049F4C5489617Data[vector.size()];
    vector.copyInto(objectFreeHand873BB22017634E47876049F4C5489617Data);
    return(objectFreeHand873BB22017634E47876049F4C5489617Data);
  }

/**
Create a registry
 */
  public static FreeHand873BB22017634E47876049F4C5489617Data[] set(String cBpartnerRuc, String mail, String fechaEstimada, String cBpartnerLocationId, String esFinEmbarque, String clienteId, String clienteIdr, String ldtRoutingOrderId, String tipo, String adClientId, String embarque, String cIncotermsId, String updatedby, String updatedbyr, String tarifaVenta, String ldtContratoPuertoId, String vendedorId, String telfShipper, String adOrgId, String adUserId, String puertoTerminalDestinoId, String modalidad, String mPricelistVersionId, String observacion, String cBpartnerMtId, String estado, String costosLocales, String costosLocalesEur, String mPricelistId, String clienteDireccionId, String documentno, String cCountryId, String createdby, String createdbyr, String numembarque, String cod, String clienteRuc, String totalNegociadoEur, String fecha, String isactive, String totalNegociado, String cDoctypeId, String puertoTerminalOrigenId, String cBpartnerId, String agenteId)    throws ServletException {
    FreeHand873BB22017634E47876049F4C5489617Data objectFreeHand873BB22017634E47876049F4C5489617Data[] = new FreeHand873BB22017634E47876049F4C5489617Data[1];
    objectFreeHand873BB22017634E47876049F4C5489617Data[0] = new FreeHand873BB22017634E47876049F4C5489617Data();
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].created = "";
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].createdbyr = createdbyr;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].updated = "";
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].updatedTimeStamp = "";
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].updatedby = updatedby;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].updatedbyr = updatedbyr;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].adOrgId = adOrgId;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].adOrgIdr = "";
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].cDoctypeId = cDoctypeId;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].cDoctypeIdr = "";
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].numembarque = numembarque;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].fecha = fecha;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].estado = estado;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].puertoTerminalOrigenId = puertoTerminalOrigenId;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].puertoTerminalDestinoId = puertoTerminalDestinoId;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].cBpartnerMtId = cBpartnerMtId;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].ldtContratoPuertoId = ldtContratoPuertoId;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].fechaEstimada = fechaEstimada;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].vendedorId = vendedorId;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].vendedorIdr = "";
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].mPricelistVersionId = mPricelistVersionId;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].mPricelistVersionIdr = "";
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].cBpartnerId = cBpartnerId;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].cBpartnerRuc = cBpartnerRuc;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].cBpartnerLocationId = cBpartnerLocationId;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].telfShipper = telfShipper;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].adUserId = adUserId;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].adUserIdr = "";
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].mail = mail;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].clienteId = clienteId;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].clienteIdr = clienteIdr;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].clienteRuc = clienteRuc;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].clienteDireccionId = clienteDireccionId;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].clienteDireccionIdr = "";
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].modalidad = modalidad;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].modalidadr = "";
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].tipo = tipo;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].tipor = "";
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].cIncotermsId = cIncotermsId;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].cIncotermsIdr = "";
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].agenteId = agenteId;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].costosLocales = costosLocales;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].costosLocalesEur = costosLocalesEur;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].totalNegociado = totalNegociado;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].totalNegociadoEur = totalNegociadoEur;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].cod = cod;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].observacion = observacion;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].embarque = embarque;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].esFinEmbarque = esFinEmbarque;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].isactive = isactive;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].adClientId = adClientId;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].mPricelistId = mPricelistId;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].ldtRoutingOrderId = ldtRoutingOrderId;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].cCountryId = cCountryId;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].tarifaVenta = tarifaVenta;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].documentno = documentno;
    objectFreeHand873BB22017634E47876049F4C5489617Data[0].language = "";
    return objectFreeHand873BB22017634E47876049F4C5489617Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef288AAF7724AB4659AC3EA3CAB946388E_0(ConnectionProvider connectionProvider, String Cliente_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Cliente_ID FROM C_BPartner left join (select C_BPartner_ID, Name from C_BPartner) table2 on (C_BPartner.C_BPartner_ID = table2.C_BPartner_ID) WHERE C_BPartner.isActive='Y' AND C_BPartner.C_BPartner_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, Cliente_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "cliente_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef3C3EE0E1BB8041268CB1EF103CC6E8D0_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefA68E17A6693C4D78B7972B1419D3E24E_2(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE ldt_routing_order" +
      "        SET AD_Org_ID = (?) , C_Doctype_ID = (?) , Numembarque = (?) , Fecha = TO_DATE(?) , Estado = (?) , Puerto_Terminal_Origen_ID = (?) , Puerto_Terminal_Destino_ID = (?) , c_bpartner_mt_id = (?) , ldt_contrato_puerto_id = (?) , Fecha_Estimada = TO_DATE(?) , Vendedor_ID = (?) , M_Pricelist_Version_ID = (?) , C_Bpartner_ID = (?) , C_Bpartner_Ruc = (?) , C_Bpartner_Location_ID = (?) , Telf_Shipper = (?) , AD_User_ID = (?) , Mail = (?) , Cliente_ID = (?) , Cliente_Ruc = (?) , Cliente_Direccion_ID = (?) , Modalidad = (?) , Tipo = (?) , C_Incoterms_ID = (?) , Agente_ID = (?) , Costos_Locales = TO_NUMBER(?) , costos_locales_eur = TO_NUMBER(?) , Total_Negociado = TO_NUMBER(?) , total_negociado_eur = TO_NUMBER(?) , Cod = TO_NUMBER(?) , Observacion = (?) , Embarque = (?) , ES_Fin_Embarque = (?) , Isactive = (?) , AD_Client_ID = (?) , M_Pricelist_ID = (?) , LDT_Routing_Order_ID = (?) , c_country_id = (?) , Tarifa_Venta = TO_NUMBER(?) , Documentno = (?) , updated = now(), updatedby = ? " +
      "        WHERE ldt_routing_order.LDT_Routing_Order_ID = ? " +
      "        AND ldt_routing_order.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND ldt_routing_order.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numembarque);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecha);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, puertoTerminalOrigenId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, puertoTerminalDestinoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerMtId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtContratoPuertoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaEstimada);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vendedorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistVersionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerRuc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, telfShipper);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mail);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clienteId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clienteRuc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clienteDireccionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, modalidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cIncotermsId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agenteId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, costosLocales);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, costosLocalesEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalNegociado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalNegociadoEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cod);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, observacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, embarque);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, esFinEmbarque);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCountryId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tarifaVenta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO ldt_routing_order " +
      "        (AD_Org_ID, C_Doctype_ID, Numembarque, Fecha, Estado, Puerto_Terminal_Origen_ID, Puerto_Terminal_Destino_ID, c_bpartner_mt_id, ldt_contrato_puerto_id, Fecha_Estimada, Vendedor_ID, M_Pricelist_Version_ID, C_Bpartner_ID, C_Bpartner_Ruc, C_Bpartner_Location_ID, Telf_Shipper, AD_User_ID, Mail, Cliente_ID, Cliente_Ruc, Cliente_Direccion_ID, Modalidad, Tipo, C_Incoterms_ID, Agente_ID, Costos_Locales, costos_locales_eur, Total_Negociado, total_negociado_eur, Cod, Observacion, Embarque, ES_Fin_Embarque, Isactive, AD_Client_ID, M_Pricelist_ID, LDT_Routing_Order_ID, c_country_id, Tarifa_Venta, Documentno, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numembarque);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecha);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, puertoTerminalOrigenId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, puertoTerminalDestinoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerMtId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtContratoPuertoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaEstimada);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vendedorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistVersionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerRuc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, telfShipper);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mail);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clienteId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clienteRuc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clienteDireccionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, modalidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cIncotermsId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agenteId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, costosLocales);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, costosLocalesEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalNegociado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalNegociadoEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cod);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, observacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, embarque);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, esFinEmbarque);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCountryId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tarifaVenta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM ldt_routing_order" +
      "        WHERE ldt_routing_order.LDT_Routing_Order_ID = ? " +
      "        AND ldt_routing_order.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND ldt_routing_order.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM ldt_routing_order" +
      "         WHERE ldt_routing_order.LDT_Routing_Order_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM ldt_routing_order" +
      "         WHERE ldt_routing_order.LDT_Routing_Order_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
