//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.logisticadetransporte.parametrizacion.MDTPreLiquidacion;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class PreLiquidacionB43F509219314D47B91836B76F1B8091Data implements FieldProvider {
static Logger log4j = Logger.getLogger(PreLiquidacionB43F509219314D47B91836B76F1B8091Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String adUserId;
  public String servicioClienteId;
  public String servicioClienteIdr;
  public String vendedorId;
  public String vendedorIdr;
  public String ldtRoutingOrderId;
  public String ldtTipo;
  public String ldtTipor;
  public String cBpartnerId;
  public String cBpartnerIdr;
  public String resultado;
  public String agenteId;
  public String docstatus;
  public String docstatusr;
  public String cantidad;
  public String procesar;
  public String procesarBtn;
  public String preLiquidacion;
  public String adUserVendedorId;
  public String isactive;
  public String adClientId;
  public String ldtPreLiquidacionId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("ad_user_id") || fieldName.equals("adUserId"))
      return adUserId;
    else if (fieldName.equalsIgnoreCase("servicio_cliente_id") || fieldName.equals("servicioClienteId"))
      return servicioClienteId;
    else if (fieldName.equalsIgnoreCase("servicio_cliente_idr") || fieldName.equals("servicioClienteIdr"))
      return servicioClienteIdr;
    else if (fieldName.equalsIgnoreCase("vendedor_id") || fieldName.equals("vendedorId"))
      return vendedorId;
    else if (fieldName.equalsIgnoreCase("vendedor_idr") || fieldName.equals("vendedorIdr"))
      return vendedorIdr;
    else if (fieldName.equalsIgnoreCase("ldt_routing_order_id") || fieldName.equals("ldtRoutingOrderId"))
      return ldtRoutingOrderId;
    else if (fieldName.equalsIgnoreCase("ldt_tipo") || fieldName.equals("ldtTipo"))
      return ldtTipo;
    else if (fieldName.equalsIgnoreCase("ldt_tipor") || fieldName.equals("ldtTipor"))
      return ldtTipor;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_idr") || fieldName.equals("cBpartnerIdr"))
      return cBpartnerIdr;
    else if (fieldName.equalsIgnoreCase("resultado"))
      return resultado;
    else if (fieldName.equalsIgnoreCase("agente_id") || fieldName.equals("agenteId"))
      return agenteId;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("docstatusr"))
      return docstatusr;
    else if (fieldName.equalsIgnoreCase("cantidad"))
      return cantidad;
    else if (fieldName.equalsIgnoreCase("procesar"))
      return procesar;
    else if (fieldName.equalsIgnoreCase("procesar_btn") || fieldName.equals("procesarBtn"))
      return procesarBtn;
    else if (fieldName.equalsIgnoreCase("pre_liquidacion") || fieldName.equals("preLiquidacion"))
      return preLiquidacion;
    else if (fieldName.equalsIgnoreCase("ad_user_vendedor_id") || fieldName.equals("adUserVendedorId"))
      return adUserVendedorId;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("ldt_pre_liquidacion_id") || fieldName.equals("ldtPreLiquidacionId"))
      return ldtPreLiquidacionId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static PreLiquidacionB43F509219314D47B91836B76F1B8091Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static PreLiquidacionB43F509219314D47B91836B76F1B8091Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(ldt_pre_liquidacion.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = ldt_pre_liquidacion.CreatedBy) as CreatedByR, " +
      "        to_char(ldt_pre_liquidacion.Updated, ?) as updated, " +
      "        to_char(ldt_pre_liquidacion.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        ldt_pre_liquidacion.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = ldt_pre_liquidacion.UpdatedBy) as UpdatedByR," +
      "        ldt_pre_liquidacion.AD_Org_ID, " +
      "(CASE WHEN ldt_pre_liquidacion.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "ldt_pre_liquidacion.AD_User_ID, " +
      "ldt_pre_liquidacion.Servicio_Cliente_ID, " +
      "(CASE WHEN ldt_pre_liquidacion.Servicio_Cliente_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))),'') ) END) AS Servicio_Cliente_IDR, " +
      "ldt_pre_liquidacion.Vendedor_ID, " +
      "(CASE WHEN ldt_pre_liquidacion.Vendedor_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.Name), ''))),'') ) END) AS Vendedor_IDR, " +
      "ldt_pre_liquidacion.LDT_Routing_Order_ID, " +
      "ldt_pre_liquidacion.LDT_Tipo, " +
      "(CASE WHEN ldt_pre_liquidacion.LDT_Tipo IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS LDT_TipoR, " +
      "ldt_pre_liquidacion.C_Bpartner_ID, " +
      "(CASE WHEN ldt_pre_liquidacion.C_Bpartner_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name), ''))),'') ) END) AS C_Bpartner_IDR, " +
      "ldt_pre_liquidacion.Resultado, " +
      "ldt_pre_liquidacion.Agente_ID, " +
      "ldt_pre_liquidacion.Docstatus, " +
      "(CASE WHEN ldt_pre_liquidacion.Docstatus IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list2.name),'') ) END) AS DocstatusR, " +
      "ldt_pre_liquidacion.Cantidad, " +
      "ldt_pre_liquidacion.procesar, " +
      "list3.name as procesar_BTN, " +
      "ldt_pre_liquidacion.PRE_Liquidacion, " +
      "ldt_pre_liquidacion.AD_User_Vendedor_ID, " +
      "COALESCE(ldt_pre_liquidacion.Isactive, 'N') AS Isactive, " +
      "ldt_pre_liquidacion.AD_Client_ID, " +
      "ldt_pre_liquidacion.LDT_Pre_Liquidacion_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM ldt_pre_liquidacion left join (select AD_Org_ID, Name from AD_Org) table1 on (ldt_pre_liquidacion.AD_Org_ID = table1.AD_Org_ID) left join (select C_BPartner_ID, Name from C_BPartner) table2 on (ldt_pre_liquidacion.Servicio_Cliente_ID =  table2.C_BPartner_ID) left join (select C_BPartner_ID, Name from C_BPartner) table3 on (ldt_pre_liquidacion.Vendedor_ID =  table3.C_BPartner_ID) left join ad_ref_list_v list1 on (ldt_pre_liquidacion.LDT_Tipo = list1.value and list1.ad_reference_id = '336BA96AA2694CB0BD2689AB13A88F03' and list1.ad_language = ?)  left join (select C_BPartner_ID, Name from C_BPartner) table4 on (ldt_pre_liquidacion.C_Bpartner_ID = table4.C_BPartner_ID) left join ad_ref_list_v list2 on (ldt_pre_liquidacion.Docstatus = list2.value and list2.ad_reference_id = '81620756ABEB429EB58A014C9F3ABB3E' and list2.ad_language = ?)  left join ad_ref_list_v list3 on (list3.ad_reference_id = '2B056D3A40B349EBAD59FB72E029656F' and list3.ad_language = ?  AND ldt_pre_liquidacion.procesar = TO_CHAR(list3.value))" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND ldt_pre_liquidacion.LDT_Pre_Liquidacion_ID = ? " +
      "        AND ldt_pre_liquidacion.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND ldt_pre_liquidacion.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        PreLiquidacionB43F509219314D47B91836B76F1B8091Data objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data = new PreLiquidacionB43F509219314D47B91836B76F1B8091Data();
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.created = UtilSql.getValue(result, "created");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.updated = UtilSql.getValue(result, "updated");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.adUserId = UtilSql.getValue(result, "ad_user_id");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.servicioClienteId = UtilSql.getValue(result, "servicio_cliente_id");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.servicioClienteIdr = UtilSql.getValue(result, "servicio_cliente_idr");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.vendedorId = UtilSql.getValue(result, "vendedor_id");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.vendedorIdr = UtilSql.getValue(result, "vendedor_idr");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.ldtRoutingOrderId = UtilSql.getValue(result, "ldt_routing_order_id");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.ldtTipo = UtilSql.getValue(result, "ldt_tipo");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.ldtTipor = UtilSql.getValue(result, "ldt_tipor");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.cBpartnerIdr = UtilSql.getValue(result, "c_bpartner_idr");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.resultado = UtilSql.getValue(result, "resultado");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.agenteId = UtilSql.getValue(result, "agente_id");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.docstatus = UtilSql.getValue(result, "docstatus");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.docstatusr = UtilSql.getValue(result, "docstatusr");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.cantidad = UtilSql.getValue(result, "cantidad");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.procesar = UtilSql.getValue(result, "procesar");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.procesarBtn = UtilSql.getValue(result, "procesar_btn");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.preLiquidacion = UtilSql.getValue(result, "pre_liquidacion");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.adUserVendedorId = UtilSql.getValue(result, "ad_user_vendedor_id");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.isactive = UtilSql.getValue(result, "isactive");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.ldtPreLiquidacionId = UtilSql.getValue(result, "ldt_pre_liquidacion_id");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.language = UtilSql.getValue(result, "language");
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.adUserClient = "";
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.adOrgClient = "";
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.createdby = "";
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.trBgcolor = "";
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.totalCount = "";
        objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    PreLiquidacionB43F509219314D47B91836B76F1B8091Data objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[] = new PreLiquidacionB43F509219314D47B91836B76F1B8091Data[vector.size()];
    vector.copyInto(objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data);
    return(objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data);
  }

/**
Create a registry
 */
  public static PreLiquidacionB43F509219314D47B91836B76F1B8091Data[] set(String procesar, String procesarBtn, String resultado, String ldtRoutingOrderId, String cBpartnerId, String cBpartnerIdr, String agenteId, String servicioClienteId, String docstatus, String vendedorId, String createdby, String createdbyr, String adOrgId, String updatedby, String updatedbyr, String cantidad, String adUserId, String adClientId, String adUserVendedorId, String preLiquidacion, String isactive, String ldtPreLiquidacionId, String ldtTipo)    throws ServletException {
    PreLiquidacionB43F509219314D47B91836B76F1B8091Data objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[] = new PreLiquidacionB43F509219314D47B91836B76F1B8091Data[1];
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0] = new PreLiquidacionB43F509219314D47B91836B76F1B8091Data();
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].created = "";
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].createdbyr = createdbyr;
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].updated = "";
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].updatedTimeStamp = "";
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].updatedby = updatedby;
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].updatedbyr = updatedbyr;
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].adOrgId = adOrgId;
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].adOrgIdr = "";
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].adUserId = adUserId;
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].servicioClienteId = servicioClienteId;
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].servicioClienteIdr = "";
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].vendedorId = vendedorId;
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].vendedorIdr = "";
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].ldtRoutingOrderId = ldtRoutingOrderId;
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].ldtTipo = ldtTipo;
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].ldtTipor = "";
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].cBpartnerId = cBpartnerId;
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].cBpartnerIdr = cBpartnerIdr;
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].resultado = resultado;
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].agenteId = agenteId;
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].docstatus = docstatus;
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].docstatusr = "";
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].cantidad = cantidad;
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].procesar = procesar;
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].procesarBtn = procesarBtn;
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].preLiquidacion = preLiquidacion;
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].adUserVendedorId = adUserVendedorId;
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].isactive = isactive;
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].adClientId = adClientId;
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].ldtPreLiquidacionId = ldtPreLiquidacionId;
    objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data[0].language = "";
    return objectPreLiquidacionB43F509219314D47B91836B76F1B8091Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef17A2CA4B13AA4C6AADE8D91571E2C74F_0(ConnectionProvider connectionProvider, String C_Bpartner_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as C_Bpartner_ID FROM C_BPartner left join (select C_BPartner_ID, Name from C_BPartner) table2 on (C_BPartner.C_BPartner_ID = table2.C_BPartner_ID) WHERE C_BPartner.isActive='Y' AND C_BPartner.C_BPartner_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Bpartner_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_bpartner_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef2BBC62038FCF4A5DBEE6CF7356E72C56(ConnectionProvider connectionProvider, String AD_Role_Id, String Ad_User_Id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (case when to_char(?) = 'CDF58A89919F4911B50845CF34429C77' then C_BPARTNER_ID else  '1' end) AS DefaultValue FROM AD_USER WHERE AD_USER_ID = to_char(?) ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_Role_Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, Ad_User_Id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef407220E71CF44A22B97E6968BB053C5A(ConnectionProvider connectionProvider, String AD_Role_Id, String Ad_User_Id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (case when to_char(?) = '87EE22FCCC234FD2B0F79535F695D3B7' then C_BPARTNER_ID else  '1' end) AS DefaultValue FROM AD_USER WHERE AD_USER_ID = to_char(?) ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_Role_Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, Ad_User_Id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef47F27CAFC3124DFCB9719234C31B0696_1(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef4F370AE6B7FA4E069CB976287C9E5BEF_2(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE ldt_pre_liquidacion" +
      "        SET AD_Org_ID = (?) , AD_User_ID = (?) , Servicio_Cliente_ID = (?) , Vendedor_ID = (?) , LDT_Routing_Order_ID = (?) , LDT_Tipo = (?) , C_Bpartner_ID = (?) , Resultado = TO_NUMBER(?) , Agente_ID = (?) , Docstatus = (?) , Cantidad = TO_NUMBER(?) , procesar = (?) , PRE_Liquidacion = (?) , AD_User_Vendedor_ID = (?) , Isactive = (?) , AD_Client_ID = (?) , LDT_Pre_Liquidacion_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE ldt_pre_liquidacion.LDT_Pre_Liquidacion_ID = ? " +
      "        AND ldt_pre_liquidacion.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND ldt_pre_liquidacion.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, servicioClienteId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vendedorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtTipo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, resultado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agenteId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, preLiquidacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserVendedorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtPreLiquidacionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtPreLiquidacionId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO ldt_pre_liquidacion " +
      "        (AD_Org_ID, AD_User_ID, Servicio_Cliente_ID, Vendedor_ID, LDT_Routing_Order_ID, LDT_Tipo, C_Bpartner_ID, Resultado, Agente_ID, Docstatus, Cantidad, procesar, PRE_Liquidacion, AD_User_Vendedor_ID, Isactive, AD_Client_ID, LDT_Pre_Liquidacion_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, servicioClienteId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vendedorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtTipo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, resultado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agenteId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, preLiquidacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserVendedorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtPreLiquidacionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM ldt_pre_liquidacion" +
      "        WHERE ldt_pre_liquidacion.LDT_Pre_Liquidacion_ID = ? " +
      "        AND ldt_pre_liquidacion.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND ldt_pre_liquidacion.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM ldt_pre_liquidacion" +
      "         WHERE ldt_pre_liquidacion.LDT_Pre_Liquidacion_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM ldt_pre_liquidacion" +
      "         WHERE ldt_pre_liquidacion.LDT_Pre_Liquidacion_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
