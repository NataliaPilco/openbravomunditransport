//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.nomina.UtilidadesEmpleado;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class Utilidades780D8D8AED8D4D05B740E02687292A1CData implements FieldProvider {
static Logger log4j = Logger.getLogger(Utilidades780D8D8AED8D4D05B740E02687292A1CData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String isactive;
  public String anio;
  public String anior;
  public String total;
  public String name;
  public String adClientId;
  public String noUtilidadId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("anio"))
      return anio;
    else if (fieldName.equalsIgnoreCase("anior"))
      return anior;
    else if (fieldName.equalsIgnoreCase("total"))
      return total;
    else if (fieldName.equalsIgnoreCase("name"))
      return name;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("no_utilidad_id") || fieldName.equals("noUtilidadId"))
      return noUtilidadId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static Utilidades780D8D8AED8D4D05B740E02687292A1CData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static Utilidades780D8D8AED8D4D05B740E02687292A1CData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(no_utilidad.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_utilidad.CreatedBy) as CreatedByR, " +
      "        to_char(no_utilidad.Updated, ?) as updated, " +
      "        to_char(no_utilidad.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        no_utilidad.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_utilidad.UpdatedBy) as UpdatedByR," +
      "        no_utilidad.AD_Org_ID, " +
      "COALESCE(no_utilidad.Isactive, 'N') AS Isactive, " +
      "no_utilidad.Anio, " +
      "(CASE WHEN no_utilidad.Anio IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS AnioR, " +
      "no_utilidad.Total, " +
      "no_utilidad.Name, " +
      "no_utilidad.AD_Client_ID, " +
      "no_utilidad.NO_Utilidad_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM no_utilidad left join ad_ref_list_v list1 on (no_utilidad.Anio = list1.value and list1.ad_reference_id = 'DBE0CE97929A4EB3A2093E35875EA82B' and list1.ad_language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND no_utilidad.NO_Utilidad_ID = ? " +
      "        AND no_utilidad.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND no_utilidad.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Utilidades780D8D8AED8D4D05B740E02687292A1CData objectUtilidades780D8D8AED8D4D05B740E02687292A1CData = new Utilidades780D8D8AED8D4D05B740E02687292A1CData();
        objectUtilidades780D8D8AED8D4D05B740E02687292A1CData.created = UtilSql.getValue(result, "created");
        objectUtilidades780D8D8AED8D4D05B740E02687292A1CData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectUtilidades780D8D8AED8D4D05B740E02687292A1CData.updated = UtilSql.getValue(result, "updated");
        objectUtilidades780D8D8AED8D4D05B740E02687292A1CData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectUtilidades780D8D8AED8D4D05B740E02687292A1CData.updatedby = UtilSql.getValue(result, "updatedby");
        objectUtilidades780D8D8AED8D4D05B740E02687292A1CData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectUtilidades780D8D8AED8D4D05B740E02687292A1CData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectUtilidades780D8D8AED8D4D05B740E02687292A1CData.isactive = UtilSql.getValue(result, "isactive");
        objectUtilidades780D8D8AED8D4D05B740E02687292A1CData.anio = UtilSql.getValue(result, "anio");
        objectUtilidades780D8D8AED8D4D05B740E02687292A1CData.anior = UtilSql.getValue(result, "anior");
        objectUtilidades780D8D8AED8D4D05B740E02687292A1CData.total = UtilSql.getValue(result, "total");
        objectUtilidades780D8D8AED8D4D05B740E02687292A1CData.name = UtilSql.getValue(result, "name");
        objectUtilidades780D8D8AED8D4D05B740E02687292A1CData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectUtilidades780D8D8AED8D4D05B740E02687292A1CData.noUtilidadId = UtilSql.getValue(result, "no_utilidad_id");
        objectUtilidades780D8D8AED8D4D05B740E02687292A1CData.language = UtilSql.getValue(result, "language");
        objectUtilidades780D8D8AED8D4D05B740E02687292A1CData.adUserClient = "";
        objectUtilidades780D8D8AED8D4D05B740E02687292A1CData.adOrgClient = "";
        objectUtilidades780D8D8AED8D4D05B740E02687292A1CData.createdby = "";
        objectUtilidades780D8D8AED8D4D05B740E02687292A1CData.trBgcolor = "";
        objectUtilidades780D8D8AED8D4D05B740E02687292A1CData.totalCount = "";
        objectUtilidades780D8D8AED8D4D05B740E02687292A1CData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectUtilidades780D8D8AED8D4D05B740E02687292A1CData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Utilidades780D8D8AED8D4D05B740E02687292A1CData objectUtilidades780D8D8AED8D4D05B740E02687292A1CData[] = new Utilidades780D8D8AED8D4D05B740E02687292A1CData[vector.size()];
    vector.copyInto(objectUtilidades780D8D8AED8D4D05B740E02687292A1CData);
    return(objectUtilidades780D8D8AED8D4D05B740E02687292A1CData);
  }

/**
Create a registry
 */
  public static Utilidades780D8D8AED8D4D05B740E02687292A1CData[] set(String noUtilidadId, String updatedby, String updatedbyr, String adClientId, String adOrgId, String anio, String createdby, String createdbyr, String isactive, String name, String total)    throws ServletException {
    Utilidades780D8D8AED8D4D05B740E02687292A1CData objectUtilidades780D8D8AED8D4D05B740E02687292A1CData[] = new Utilidades780D8D8AED8D4D05B740E02687292A1CData[1];
    objectUtilidades780D8D8AED8D4D05B740E02687292A1CData[0] = new Utilidades780D8D8AED8D4D05B740E02687292A1CData();
    objectUtilidades780D8D8AED8D4D05B740E02687292A1CData[0].created = "";
    objectUtilidades780D8D8AED8D4D05B740E02687292A1CData[0].createdbyr = createdbyr;
    objectUtilidades780D8D8AED8D4D05B740E02687292A1CData[0].updated = "";
    objectUtilidades780D8D8AED8D4D05B740E02687292A1CData[0].updatedTimeStamp = "";
    objectUtilidades780D8D8AED8D4D05B740E02687292A1CData[0].updatedby = updatedby;
    objectUtilidades780D8D8AED8D4D05B740E02687292A1CData[0].updatedbyr = updatedbyr;
    objectUtilidades780D8D8AED8D4D05B740E02687292A1CData[0].adOrgId = adOrgId;
    objectUtilidades780D8D8AED8D4D05B740E02687292A1CData[0].isactive = isactive;
    objectUtilidades780D8D8AED8D4D05B740E02687292A1CData[0].anio = anio;
    objectUtilidades780D8D8AED8D4D05B740E02687292A1CData[0].anior = "";
    objectUtilidades780D8D8AED8D4D05B740E02687292A1CData[0].total = total;
    objectUtilidades780D8D8AED8D4D05B740E02687292A1CData[0].name = name;
    objectUtilidades780D8D8AED8D4D05B740E02687292A1CData[0].adClientId = adClientId;
    objectUtilidades780D8D8AED8D4D05B740E02687292A1CData[0].noUtilidadId = noUtilidadId;
    objectUtilidades780D8D8AED8D4D05B740E02687292A1CData[0].language = "";
    return objectUtilidades780D8D8AED8D4D05B740E02687292A1CData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef41FA4CAA5F3749428134BEB0717085F5_0(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefA2A7CA8ABC5E4D708A561F56AE24DE46_1(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE no_utilidad" +
      "        SET AD_Org_ID = (?) , Isactive = (?) , Anio = (?) , Total = TO_NUMBER(?) , Name = (?) , AD_Client_ID = (?) , NO_Utilidad_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE no_utilidad.NO_Utilidad_ID = ? " +
      "        AND no_utilidad.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_utilidad.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, total);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, name);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noUtilidadId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noUtilidadId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO no_utilidad " +
      "        (AD_Org_ID, Isactive, Anio, Total, Name, AD_Client_ID, NO_Utilidad_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), TO_NUMBER(?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, total);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, name);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noUtilidadId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM no_utilidad" +
      "        WHERE no_utilidad.NO_Utilidad_ID = ? " +
      "        AND no_utilidad.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_utilidad.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM no_utilidad" +
      "         WHERE no_utilidad.NO_Utilidad_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM no_utilidad" +
      "         WHERE no_utilidad.NO_Utilidad_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
