//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.logisticadetransporte.parametrizacion.Cotizacion;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data implements FieldProvider {
static Logger log4j = Logger.getLogger(Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String cDoctypeId;
  public String cDoctypeIdr;
  public String documentno;
  public String fecha;
  public String cBpartnerId;
  public String cBpartnerLocationId;
  public String cBpartnerLocationIdr;
  public String vendedorId;
  public String vendedorIdr;
  public String servicioClienteId;
  public String servicioClienteIdr;
  public String descripcion;
  public String modalidad;
  public String modalidadr;
  public String tipo;
  public String tipor;
  public String frecuencia;
  public String frecuenciar;
  public String processed;
  public String processedBtn;
  public String cIncotermsId;
  public String cIncotermsIdr;
  public String isactive;
  public String btnGeneraro;
  public String tarifaProductos;
  public String opcion;
  public String adUserVendedorId;
  public String adUserSclienteId;
  public String ldtCotizacionId;
  public String adClientId;
  public String estado;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("c_doctype_idr") || fieldName.equals("cDoctypeIdr"))
      return cDoctypeIdr;
    else if (fieldName.equalsIgnoreCase("documentno"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("fecha"))
      return fecha;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_location_id") || fieldName.equals("cBpartnerLocationId"))
      return cBpartnerLocationId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_location_idr") || fieldName.equals("cBpartnerLocationIdr"))
      return cBpartnerLocationIdr;
    else if (fieldName.equalsIgnoreCase("vendedor_id") || fieldName.equals("vendedorId"))
      return vendedorId;
    else if (fieldName.equalsIgnoreCase("vendedor_idr") || fieldName.equals("vendedorIdr"))
      return vendedorIdr;
    else if (fieldName.equalsIgnoreCase("servicio_cliente_id") || fieldName.equals("servicioClienteId"))
      return servicioClienteId;
    else if (fieldName.equalsIgnoreCase("servicio_cliente_idr") || fieldName.equals("servicioClienteIdr"))
      return servicioClienteIdr;
    else if (fieldName.equalsIgnoreCase("descripcion"))
      return descripcion;
    else if (fieldName.equalsIgnoreCase("modalidad"))
      return modalidad;
    else if (fieldName.equalsIgnoreCase("modalidadr"))
      return modalidadr;
    else if (fieldName.equalsIgnoreCase("tipo"))
      return tipo;
    else if (fieldName.equalsIgnoreCase("tipor"))
      return tipor;
    else if (fieldName.equalsIgnoreCase("frecuencia"))
      return frecuencia;
    else if (fieldName.equalsIgnoreCase("frecuenciar"))
      return frecuenciar;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("processed_btn") || fieldName.equals("processedBtn"))
      return processedBtn;
    else if (fieldName.equalsIgnoreCase("c_incoterms_id") || fieldName.equals("cIncotermsId"))
      return cIncotermsId;
    else if (fieldName.equalsIgnoreCase("c_incoterms_idr") || fieldName.equals("cIncotermsIdr"))
      return cIncotermsIdr;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("btn_generaro") || fieldName.equals("btnGeneraro"))
      return btnGeneraro;
    else if (fieldName.equalsIgnoreCase("tarifa_productos") || fieldName.equals("tarifaProductos"))
      return tarifaProductos;
    else if (fieldName.equalsIgnoreCase("opcion"))
      return opcion;
    else if (fieldName.equalsIgnoreCase("ad_user_vendedor_id") || fieldName.equals("adUserVendedorId"))
      return adUserVendedorId;
    else if (fieldName.equalsIgnoreCase("ad_user_scliente_id") || fieldName.equals("adUserSclienteId"))
      return adUserSclienteId;
    else if (fieldName.equalsIgnoreCase("ldt_cotizacion_id") || fieldName.equals("ldtCotizacionId"))
      return ldtCotizacionId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("estado"))
      return estado;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(ldt_cotizacion.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = ldt_cotizacion.CreatedBy) as CreatedByR, " +
      "        to_char(ldt_cotizacion.Updated, ?) as updated, " +
      "        to_char(ldt_cotizacion.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        ldt_cotizacion.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = ldt_cotizacion.UpdatedBy) as UpdatedByR," +
      "        ldt_cotizacion.AD_Org_ID, " +
      "(CASE WHEN ldt_cotizacion.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "ldt_cotizacion.C_Doctype_ID, " +
      "(CASE WHEN ldt_cotizacion.C_Doctype_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL2.Name IS NULL THEN TO_CHAR(table2.Name) ELSE TO_CHAR(tableTRL2.Name) END)), ''))),'') ) END) AS C_Doctype_IDR, " +
      "ldt_cotizacion.Documentno, " +
      "ldt_cotizacion.Fecha, " +
      "ldt_cotizacion.C_Bpartner_ID, " +
      "ldt_cotizacion.C_Bpartner_Location_ID, " +
      "(CASE WHEN ldt_cotizacion.C_Bpartner_Location_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.Name), ''))),'') ) END) AS C_Bpartner_Location_IDR, " +
      "ldt_cotizacion.Vendedor_ID, " +
      "(CASE WHEN ldt_cotizacion.Vendedor_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name), ''))),'') ) END) AS Vendedor_IDR, " +
      "ldt_cotizacion.Servicio_Cliente_ID, " +
      "(CASE WHEN ldt_cotizacion.Servicio_Cliente_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table5.Name), ''))),'') ) END) AS Servicio_Cliente_IDR, " +
      "ldt_cotizacion.Descripcion, " +
      "ldt_cotizacion.Modalidad, " +
      "(CASE WHEN ldt_cotizacion.Modalidad IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS ModalidadR, " +
      "ldt_cotizacion.Tipo, " +
      "(CASE WHEN ldt_cotizacion.Tipo IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list2.name),'') ) END) AS TipoR, " +
      "ldt_cotizacion.frecuencia, " +
      "(CASE WHEN ldt_cotizacion.frecuencia IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list3.name),'') ) END) AS frecuenciaR, " +
      "ldt_cotizacion.Processed, " +
      "list4.name as Processed_BTN, " +
      "ldt_cotizacion.C_Incoterms_ID, " +
      "(CASE WHEN ldt_cotizacion.C_Incoterms_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table6.Name), ''))),'') ) END) AS C_Incoterms_IDR, " +
      "COALESCE(ldt_cotizacion.Isactive, 'N') AS Isactive, " +
      "ldt_cotizacion.BTN_Generaro, " +
      "ldt_cotizacion.Tarifa_Productos, " +
      "COALESCE(ldt_cotizacion.opcion, 'N') AS opcion, " +
      "ldt_cotizacion.ad_user_vendedor_id, " +
      "ldt_cotizacion.ad_user_scliente_id, " +
      "ldt_cotizacion.LDT_Cotizacion_ID, " +
      "ldt_cotizacion.AD_Client_ID, " +
      "ldt_cotizacion.Estado, " +
      "        ? AS LANGUAGE " +
      "        FROM ldt_cotizacion left join (select AD_Org_ID, Name from AD_Org) table1 on (ldt_cotizacion.AD_Org_ID = table1.AD_Org_ID) left join (select C_DocType_ID, Name from C_DocType) table2 on (ldt_cotizacion.C_Doctype_ID =  table2.C_DocType_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL2 on (table2.C_DocType_ID = tableTRL2.C_DocType_ID and tableTRL2.AD_Language = ?)  left join (select C_Bpartner_Location_ID, Name from C_Bpartner_Location) table3 on (ldt_cotizacion.C_Bpartner_Location_ID = table3.C_Bpartner_Location_ID) left join (select C_BPartner_ID, Name from C_BPartner) table4 on (ldt_cotizacion.Vendedor_ID =  table4.C_BPartner_ID) left join (select C_BPartner_ID, Name from C_BPartner) table5 on (ldt_cotizacion.Servicio_Cliente_ID =  table5.C_BPartner_ID) left join ad_ref_list_v list1 on (ldt_cotizacion.Modalidad = list1.value and list1.ad_reference_id = '084746C4C4934924AABBD1EAB1DB9BF5' and list1.ad_language = ?)  left join ad_ref_list_v list2 on (ldt_cotizacion.Tipo = list2.value and list2.ad_reference_id = '336BA96AA2694CB0BD2689AB13A88F03' and list2.ad_language = ?)  left join ad_ref_list_v list3 on (ldt_cotizacion.frecuencia = list3.value and list3.ad_reference_id = '78F4041249B043B194408A0829BA35EC' and list3.ad_language = ?)  left join ad_ref_list_v list4 on (list4.ad_reference_id = '1F931E9A5FAB445A8FD2F069ADD2DBE4' and list4.ad_language = ?  AND ldt_cotizacion.Processed = TO_CHAR(list4.value)) left join (select C_Incoterms_ID, Name from C_Incoterms) table6 on (ldt_cotizacion.C_Incoterms_ID = table6.C_Incoterms_ID)" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND ldt_cotizacion.LDT_Cotizacion_ID = ? " +
      "        AND ldt_cotizacion.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND ldt_cotizacion.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data = new Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data();
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.created = UtilSql.getValue(result, "created");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.updated = UtilSql.getValue(result, "updated");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.cDoctypeIdr = UtilSql.getValue(result, "c_doctype_idr");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.documentno = UtilSql.getValue(result, "documentno");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.fecha = UtilSql.getDateValue(result, "fecha", "dd-MM-yyyy");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.cBpartnerLocationId = UtilSql.getValue(result, "c_bpartner_location_id");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.cBpartnerLocationIdr = UtilSql.getValue(result, "c_bpartner_location_idr");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.vendedorId = UtilSql.getValue(result, "vendedor_id");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.vendedorIdr = UtilSql.getValue(result, "vendedor_idr");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.servicioClienteId = UtilSql.getValue(result, "servicio_cliente_id");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.servicioClienteIdr = UtilSql.getValue(result, "servicio_cliente_idr");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.descripcion = UtilSql.getValue(result, "descripcion");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.modalidad = UtilSql.getValue(result, "modalidad");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.modalidadr = UtilSql.getValue(result, "modalidadr");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.tipo = UtilSql.getValue(result, "tipo");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.tipor = UtilSql.getValue(result, "tipor");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.frecuencia = UtilSql.getValue(result, "frecuencia");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.frecuenciar = UtilSql.getValue(result, "frecuenciar");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.processed = UtilSql.getValue(result, "processed");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.processedBtn = UtilSql.getValue(result, "processed_btn");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.cIncotermsId = UtilSql.getValue(result, "c_incoterms_id");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.cIncotermsIdr = UtilSql.getValue(result, "c_incoterms_idr");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.isactive = UtilSql.getValue(result, "isactive");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.btnGeneraro = UtilSql.getValue(result, "btn_generaro");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.tarifaProductos = UtilSql.getValue(result, "tarifa_productos");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.opcion = UtilSql.getValue(result, "opcion");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.adUserVendedorId = UtilSql.getValue(result, "ad_user_vendedor_id");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.adUserSclienteId = UtilSql.getValue(result, "ad_user_scliente_id");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.ldtCotizacionId = UtilSql.getValue(result, "ldt_cotizacion_id");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.estado = UtilSql.getValue(result, "estado");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.language = UtilSql.getValue(result, "language");
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.adUserClient = "";
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.adOrgClient = "";
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.createdby = "";
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.trBgcolor = "";
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.totalCount = "";
        objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[] = new Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[vector.size()];
    vector.copyInto(objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data);
    return(objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data);
  }

/**
Create a registry
 */
  public static Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[] set(String tarifaProductos, String opcion, String descripcion, String ldtCotizacionId, String tipo, String modalidad, String createdby, String createdbyr, String estado, String cIncotermsId, String adUserVendedorId, String adUserSclienteId, String fecha, String isactive, String frecuencia, String servicioClienteId, String cBpartnerLocationId, String processed, String processedBtn, String adOrgId, String updatedby, String updatedbyr, String vendedorId, String documentno, String cDoctypeId, String cBpartnerId, String btnGeneraro, String adClientId)    throws ServletException {
    Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[] = new Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[1];
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0] = new Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data();
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].created = "";
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].createdbyr = createdbyr;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].updated = "";
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].updatedTimeStamp = "";
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].updatedby = updatedby;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].updatedbyr = updatedbyr;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].adOrgId = adOrgId;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].adOrgIdr = "";
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].cDoctypeId = cDoctypeId;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].cDoctypeIdr = "";
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].documentno = documentno;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].fecha = fecha;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].cBpartnerId = cBpartnerId;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].cBpartnerLocationId = cBpartnerLocationId;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].cBpartnerLocationIdr = "";
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].vendedorId = vendedorId;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].vendedorIdr = "";
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].servicioClienteId = servicioClienteId;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].servicioClienteIdr = "";
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].descripcion = descripcion;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].modalidad = modalidad;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].modalidadr = "";
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].tipo = tipo;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].tipor = "";
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].frecuencia = frecuencia;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].frecuenciar = "";
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].processed = processed;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].processedBtn = processedBtn;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].cIncotermsId = cIncotermsId;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].cIncotermsIdr = "";
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].isactive = isactive;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].btnGeneraro = btnGeneraro;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].tarifaProductos = tarifaProductos;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].opcion = opcion;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].adUserVendedorId = adUserVendedorId;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].adUserSclienteId = adUserSclienteId;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].ldtCotizacionId = ldtCotizacionId;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].adClientId = adClientId;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].estado = estado;
    objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0].language = "";
    return objectCotizacion221A13EAEB064B3387B2BB9203B1E3F3Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef2AAFF521796A41ADA2CF0765D4495FF0_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefA27D34A306B043B8B2498F79549DF109_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE ldt_cotizacion" +
      "        SET AD_Org_ID = (?) , C_Doctype_ID = (?) , Documentno = (?) , Fecha = TO_DATE(?) , C_Bpartner_ID = (?) , C_Bpartner_Location_ID = (?) , Vendedor_ID = (?) , Servicio_Cliente_ID = (?) , Descripcion = (?) , Modalidad = (?) , Tipo = (?) , frecuencia = (?) , Processed = (?) , C_Incoterms_ID = (?) , Isactive = (?) , BTN_Generaro = (?) , Tarifa_Productos = (?) , opcion = (?) , ad_user_vendedor_id = (?) , ad_user_scliente_id = (?) , LDT_Cotizacion_ID = (?) , AD_Client_ID = (?) , Estado = (?) , updated = now(), updatedby = ? " +
      "        WHERE ldt_cotizacion.LDT_Cotizacion_ID = ? " +
      "        AND ldt_cotizacion.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND ldt_cotizacion.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecha);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vendedorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, servicioClienteId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descripcion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, modalidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, frecuencia);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cIncotermsId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, btnGeneraro);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tarifaProductos);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, opcion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserVendedorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserSclienteId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtCotizacionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtCotizacionId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO ldt_cotizacion " +
      "        (AD_Org_ID, C_Doctype_ID, Documentno, Fecha, C_Bpartner_ID, C_Bpartner_Location_ID, Vendedor_ID, Servicio_Cliente_ID, Descripcion, Modalidad, Tipo, frecuencia, Processed, C_Incoterms_ID, Isactive, BTN_Generaro, Tarifa_Productos, opcion, ad_user_vendedor_id, ad_user_scliente_id, LDT_Cotizacion_ID, AD_Client_ID, Estado, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecha);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vendedorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, servicioClienteId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descripcion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, modalidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, frecuencia);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cIncotermsId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, btnGeneraro);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tarifaProductos);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, opcion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserVendedorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserSclienteId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtCotizacionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM ldt_cotizacion" +
      "        WHERE ldt_cotizacion.LDT_Cotizacion_ID = ? " +
      "        AND ldt_cotizacion.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND ldt_cotizacion.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM ldt_cotizacion" +
      "         WHERE ldt_cotizacion.LDT_Cotizacion_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM ldt_cotizacion" +
      "         WHERE ldt_cotizacion.LDT_Cotizacion_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
