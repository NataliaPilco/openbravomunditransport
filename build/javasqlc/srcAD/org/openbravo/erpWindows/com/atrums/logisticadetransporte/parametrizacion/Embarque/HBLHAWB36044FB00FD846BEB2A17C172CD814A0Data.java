//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.logisticadetransporte.parametrizacion.Embarque;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class HBLHAWB36044FB00FD846BEB2A17C172CD814A0Data implements FieldProvider {
static Logger log4j = Logger.getLogger(HBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String numHbl;
  public String ldtRoutingOrderId;
  public String secuencialhblHawb;
  public String cBpartnerId;
  public String cBpartnerIdr;
  public String isactive;
  public String valorFlete;
  public String fletePp;
  public String valorFleteEur;
  public String fletePpEur;
  public String cartas;
  public String factura;
  public String avisoarribo;
  public String calcular;
  public String valorTotalUsd;
  public String ldtHblId;
  public String processed;
  public String estado;
  public String adOrgId;
  public String adClientId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("num_hbl") || fieldName.equals("numHbl"))
      return numHbl;
    else if (fieldName.equalsIgnoreCase("ldt_routing_order_id") || fieldName.equals("ldtRoutingOrderId"))
      return ldtRoutingOrderId;
    else if (fieldName.equalsIgnoreCase("secuencialhbl_hawb") || fieldName.equals("secuencialhblHawb"))
      return secuencialhblHawb;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_idr") || fieldName.equals("cBpartnerIdr"))
      return cBpartnerIdr;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("valor_flete") || fieldName.equals("valorFlete"))
      return valorFlete;
    else if (fieldName.equalsIgnoreCase("flete_pp") || fieldName.equals("fletePp"))
      return fletePp;
    else if (fieldName.equalsIgnoreCase("valor_flete_eur") || fieldName.equals("valorFleteEur"))
      return valorFleteEur;
    else if (fieldName.equalsIgnoreCase("flete_pp_eur") || fieldName.equals("fletePpEur"))
      return fletePpEur;
    else if (fieldName.equalsIgnoreCase("cartas"))
      return cartas;
    else if (fieldName.equalsIgnoreCase("factura"))
      return factura;
    else if (fieldName.equalsIgnoreCase("avisoarribo"))
      return avisoarribo;
    else if (fieldName.equalsIgnoreCase("calcular"))
      return calcular;
    else if (fieldName.equalsIgnoreCase("valor_total_usd") || fieldName.equals("valorTotalUsd"))
      return valorTotalUsd;
    else if (fieldName.equalsIgnoreCase("ldt_hbl_id") || fieldName.equals("ldtHblId"))
      return ldtHblId;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("estado"))
      return estado;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static HBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String ldtRoutingOrderId, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, ldtRoutingOrderId, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static HBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String ldtRoutingOrderId, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(ldt_hbl.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = ldt_hbl.CreatedBy) as CreatedByR, " +
      "        to_char(ldt_hbl.Updated, ?) as updated, " +
      "        to_char(ldt_hbl.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        ldt_hbl.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = ldt_hbl.UpdatedBy) as UpdatedByR," +
      "        ldt_hbl.NUM_Hbl, " +
      "ldt_hbl.LDT_Routing_Order_ID, " +
      "ldt_hbl.Secuencialhbl_Hawb, " +
      "ldt_hbl.C_Bpartner_ID, " +
      "(CASE WHEN ldt_hbl.C_Bpartner_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS C_Bpartner_IDR, " +
      "COALESCE(ldt_hbl.Isactive, 'N') AS Isactive, " +
      "ldt_hbl.Valor_Flete, " +
      "ldt_hbl.flete_pp, " +
      "ldt_hbl.valor_flete_eur, " +
      "ldt_hbl.flete_pp_eur, " +
      "ldt_hbl.Cartas, " +
      "ldt_hbl.Factura, " +
      "ldt_hbl.Avisoarribo, " +
      "COALESCE(ldt_hbl.calcular, 'N') AS calcular, " +
      "ldt_hbl.valor_total_usd, " +
      "ldt_hbl.LDT_Hbl_ID, " +
      "ldt_hbl.Processed, " +
      "ldt_hbl.Estado, " +
      "ldt_hbl.AD_Org_ID, " +
      "ldt_hbl.AD_Client_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM ldt_hbl left join (select C_BPartner_ID, Name from C_BPartner) table1 on (ldt_hbl.C_Bpartner_ID = table1.C_BPartner_ID)" +
      "        WHERE 2=2 " +
      "        AND 1=1 ";
    strSql = strSql + ((ldtRoutingOrderId==null || ldtRoutingOrderId.equals(""))?"":"  AND ldt_hbl.LDT_Routing_Order_ID = ?  ");
    strSql = strSql + 
      "        AND ldt_hbl.LDT_Hbl_ID = ? " +
      "        AND ldt_hbl.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND ldt_hbl.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      if (ldtRoutingOrderId != null && !(ldtRoutingOrderId.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);
      }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        HBLHAWB36044FB00FD846BEB2A17C172CD814A0Data objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data = new HBLHAWB36044FB00FD846BEB2A17C172CD814A0Data();
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.created = UtilSql.getValue(result, "created");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.updated = UtilSql.getValue(result, "updated");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.numHbl = UtilSql.getValue(result, "num_hbl");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.ldtRoutingOrderId = UtilSql.getValue(result, "ldt_routing_order_id");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.secuencialhblHawb = UtilSql.getValue(result, "secuencialhbl_hawb");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.cBpartnerIdr = UtilSql.getValue(result, "c_bpartner_idr");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.isactive = UtilSql.getValue(result, "isactive");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.valorFlete = UtilSql.getValue(result, "valor_flete");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.fletePp = UtilSql.getValue(result, "flete_pp");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.valorFleteEur = UtilSql.getValue(result, "valor_flete_eur");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.fletePpEur = UtilSql.getValue(result, "flete_pp_eur");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.cartas = UtilSql.getValue(result, "cartas");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.factura = UtilSql.getValue(result, "factura");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.avisoarribo = UtilSql.getValue(result, "avisoarribo");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.calcular = UtilSql.getValue(result, "calcular");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.valorTotalUsd = UtilSql.getValue(result, "valor_total_usd");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.ldtHblId = UtilSql.getValue(result, "ldt_hbl_id");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.processed = UtilSql.getValue(result, "processed");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.estado = UtilSql.getValue(result, "estado");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.language = UtilSql.getValue(result, "language");
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.adUserClient = "";
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.adOrgClient = "";
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.createdby = "";
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.trBgcolor = "";
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.totalCount = "";
        objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    HBLHAWB36044FB00FD846BEB2A17C172CD814A0Data objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[] = new HBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[vector.size()];
    vector.copyInto(objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data);
    return(objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data);
  }

/**
Create a registry
 */
  public static HBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[] set(String ldtRoutingOrderId, String calcular, String secuencialhblHawb, String adOrgId, String cartas, String processed, String valorTotalUsd, String estado, String valorFleteEur, String valorFlete, String cBpartnerId, String cBpartnerIdr, String fletePpEur, String fletePp, String factura, String ldtHblId, String numHbl, String updatedby, String updatedbyr, String adClientId, String isactive, String createdby, String createdbyr, String avisoarribo)    throws ServletException {
    HBLHAWB36044FB00FD846BEB2A17C172CD814A0Data objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[] = new HBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[1];
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0] = new HBLHAWB36044FB00FD846BEB2A17C172CD814A0Data();
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].created = "";
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].createdbyr = createdbyr;
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].updated = "";
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].updatedTimeStamp = "";
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].updatedby = updatedby;
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].updatedbyr = updatedbyr;
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].numHbl = numHbl;
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].ldtRoutingOrderId = ldtRoutingOrderId;
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].secuencialhblHawb = secuencialhblHawb;
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].cBpartnerId = cBpartnerId;
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].cBpartnerIdr = cBpartnerIdr;
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].isactive = isactive;
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].valorFlete = valorFlete;
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].fletePp = fletePp;
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].valorFleteEur = valorFleteEur;
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].fletePpEur = fletePpEur;
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].cartas = cartas;
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].factura = factura;
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].avisoarribo = avisoarribo;
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].calcular = calcular;
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].valorTotalUsd = valorTotalUsd;
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].ldtHblId = ldtHblId;
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].processed = processed;
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].estado = estado;
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].adOrgId = adOrgId;
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].adClientId = adClientId;
    objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data[0].language = "";
    return objectHBLHAWB36044FB00FD846BEB2A17C172CD814A0Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef7F025BCB6D1343EEB2624BB5DFBA7625_0(ConnectionProvider connectionProvider, String C_Bpartner_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as C_Bpartner_ID FROM C_BPartner left join (select C_BPartner_ID, Name from C_BPartner) table2 on (C_BPartner.C_BPartner_ID = table2.C_BPartner_ID) WHERE C_BPartner.isActive='Y' AND C_BPartner.C_BPartner_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Bpartner_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_bpartner_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefCEA41398F6F34FCAB387780EDC6B8E94_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefF4819B0004B141B3888DFF590C15FAEC_2(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
return the parent ID
 */
  public static String selectParentID(ConnectionProvider connectionProvider, String key)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT ldt_hbl.LDT_Routing_Order_ID AS NAME" +
      "        FROM ldt_hbl" +
      "        WHERE ldt_hbl.LDT_Hbl_ID = ?";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParent(ConnectionProvider connectionProvider, String ldtRoutingOrderId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.Documentno), ''))) AS NAME FROM ldt_routing_order left join (select ldt_routing_order_ID, Documentno from ldt_routing_order) table1 on (ldt_routing_order.LDT_Routing_Order_ID = table1.ldt_routing_order_ID) WHERE ldt_routing_order.LDT_Routing_Order_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParentTrl(ConnectionProvider connectionProvider, String ldtRoutingOrderId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.Documentno), ''))) AS NAME FROM ldt_routing_order left join (select ldt_routing_order_ID, Documentno from ldt_routing_order) table1 on (ldt_routing_order.LDT_Routing_Order_ID = table1.ldt_routing_order_ID) WHERE ldt_routing_order.LDT_Routing_Order_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE ldt_hbl" +
      "        SET NUM_Hbl = (?) , LDT_Routing_Order_ID = (?) , Secuencialhbl_Hawb = (?) , C_Bpartner_ID = (?) , Isactive = (?) , Valor_Flete = TO_NUMBER(?) , flete_pp = TO_NUMBER(?) , valor_flete_eur = TO_NUMBER(?) , flete_pp_eur = TO_NUMBER(?) , Cartas = (?) , Factura = (?) , Avisoarribo = (?) , calcular = (?) , valor_total_usd = TO_NUMBER(?) , LDT_Hbl_ID = (?) , Processed = (?) , Estado = (?) , AD_Org_ID = (?) , AD_Client_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE ldt_hbl.LDT_Hbl_ID = ? " +
      "                 AND ldt_hbl.LDT_Routing_Order_ID = ? " +
      "        AND ldt_hbl.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND ldt_hbl.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numHbl);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, secuencialhblHawb);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valorFlete);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fletePp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valorFleteEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fletePpEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cartas);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, factura);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, avisoarribo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, calcular);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valorTotalUsd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtHblId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtHblId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO ldt_hbl " +
      "        (NUM_Hbl, LDT_Routing_Order_ID, Secuencialhbl_Hawb, C_Bpartner_ID, Isactive, Valor_Flete, flete_pp, valor_flete_eur, flete_pp_eur, Cartas, Factura, Avisoarribo, calcular, valor_total_usd, LDT_Hbl_ID, Processed, Estado, AD_Org_ID, AD_Client_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numHbl);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, secuencialhblHawb);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valorFlete);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fletePp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valorFleteEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fletePpEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cartas);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, factura);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, avisoarribo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, calcular);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valorTotalUsd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtHblId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String ldtRoutingOrderId, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM ldt_hbl" +
      "        WHERE ldt_hbl.LDT_Hbl_ID = ? " +
      "                 AND ldt_hbl.LDT_Routing_Order_ID = ? " +
      "        AND ldt_hbl.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND ldt_hbl.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM ldt_hbl" +
      "         WHERE ldt_hbl.LDT_Hbl_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM ldt_hbl" +
      "         WHERE ldt_hbl.LDT_Hbl_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
