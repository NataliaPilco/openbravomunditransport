//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.logisticadetransporte.parametrizacion.RoutingOrder;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class RoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData implements FieldProvider {
static Logger log4j = Logger.getLogger(RoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String cDoctypeId;
  public String cDoctypeIdr;
  public String documentno;
  public String ldtCotizacionId;
  public String fecha;
  public String puertoTerminalOrigenId;
  public String puertoTerminalDestinoId;
  public String estado;
  public String numembarque;
  public String vendedorId;
  public String vendedorIdr;
  public String isactive;
  public String cBpartnerId;
  public String cBpartnerRuc;
  public String cBpartnerLocationId;
  public String telfShipper;
  public String adUserId;
  public String adUserIdr;
  public String mail;
  public String clienteId;
  public String clienteIdr;
  public String processed;
  public String processedBtn;
  public String clienteRuc;
  public String clienteDireccionId;
  public String clienteDireccionIdr;
  public String modalidad;
  public String modalidadr;
  public String tipo;
  public String tipor;
  public String cod;
  public String ldtOrder;
  public String agenteId;
  public String ldtCostoAgente;
  public String cCurrencyId;
  public String cCurrencyIdr;
  public String cIncotermsId;
  public String cIncotermsIdr;
  public String tarifaVenta;
  public String tarifaNegociada;
  public String costosLocales;
  public String tarifaVentaEur;
  public String tarifaNegociadaEur;
  public String costosLocalesEur;
  public String mPricelistComId;
  public String mPricelistComIdr;
  public String tarifaCompra;
  public String embarque;
  public String observacion;
  public String ldtAvisoRo;
  public String esFinEmbarque;
  public String mPricelistVersionId;
  public String mPricelistVersionComId;
  public String mPricelistId;
  public String ldtCod;
  public String adClientId;
  public String ldtRoutingOrderId;
  public String ldtPuertoTerminalId;
  public String fechaEstimada;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("c_doctype_idr") || fieldName.equals("cDoctypeIdr"))
      return cDoctypeIdr;
    else if (fieldName.equalsIgnoreCase("documentno"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("ldt_cotizacion_id") || fieldName.equals("ldtCotizacionId"))
      return ldtCotizacionId;
    else if (fieldName.equalsIgnoreCase("fecha"))
      return fecha;
    else if (fieldName.equalsIgnoreCase("puerto_terminal_origen_id") || fieldName.equals("puertoTerminalOrigenId"))
      return puertoTerminalOrigenId;
    else if (fieldName.equalsIgnoreCase("puerto_terminal_destino_id") || fieldName.equals("puertoTerminalDestinoId"))
      return puertoTerminalDestinoId;
    else if (fieldName.equalsIgnoreCase("estado"))
      return estado;
    else if (fieldName.equalsIgnoreCase("numembarque"))
      return numembarque;
    else if (fieldName.equalsIgnoreCase("vendedor_id") || fieldName.equals("vendedorId"))
      return vendedorId;
    else if (fieldName.equalsIgnoreCase("vendedor_idr") || fieldName.equals("vendedorIdr"))
      return vendedorIdr;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_ruc") || fieldName.equals("cBpartnerRuc"))
      return cBpartnerRuc;
    else if (fieldName.equalsIgnoreCase("c_bpartner_location_id") || fieldName.equals("cBpartnerLocationId"))
      return cBpartnerLocationId;
    else if (fieldName.equalsIgnoreCase("telf_shipper") || fieldName.equals("telfShipper"))
      return telfShipper;
    else if (fieldName.equalsIgnoreCase("ad_user_id") || fieldName.equals("adUserId"))
      return adUserId;
    else if (fieldName.equalsIgnoreCase("ad_user_idr") || fieldName.equals("adUserIdr"))
      return adUserIdr;
    else if (fieldName.equalsIgnoreCase("mail"))
      return mail;
    else if (fieldName.equalsIgnoreCase("cliente_id") || fieldName.equals("clienteId"))
      return clienteId;
    else if (fieldName.equalsIgnoreCase("cliente_idr") || fieldName.equals("clienteIdr"))
      return clienteIdr;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("processed_btn") || fieldName.equals("processedBtn"))
      return processedBtn;
    else if (fieldName.equalsIgnoreCase("cliente_ruc") || fieldName.equals("clienteRuc"))
      return clienteRuc;
    else if (fieldName.equalsIgnoreCase("cliente_direccion_id") || fieldName.equals("clienteDireccionId"))
      return clienteDireccionId;
    else if (fieldName.equalsIgnoreCase("cliente_direccion_idr") || fieldName.equals("clienteDireccionIdr"))
      return clienteDireccionIdr;
    else if (fieldName.equalsIgnoreCase("modalidad"))
      return modalidad;
    else if (fieldName.equalsIgnoreCase("modalidadr"))
      return modalidadr;
    else if (fieldName.equalsIgnoreCase("tipo"))
      return tipo;
    else if (fieldName.equalsIgnoreCase("tipor"))
      return tipor;
    else if (fieldName.equalsIgnoreCase("cod"))
      return cod;
    else if (fieldName.equalsIgnoreCase("ldt_order") || fieldName.equals("ldtOrder"))
      return ldtOrder;
    else if (fieldName.equalsIgnoreCase("agente_id") || fieldName.equals("agenteId"))
      return agenteId;
    else if (fieldName.equalsIgnoreCase("ldt_costo_agente") || fieldName.equals("ldtCostoAgente"))
      return ldtCostoAgente;
    else if (fieldName.equalsIgnoreCase("c_currency_id") || fieldName.equals("cCurrencyId"))
      return cCurrencyId;
    else if (fieldName.equalsIgnoreCase("c_currency_idr") || fieldName.equals("cCurrencyIdr"))
      return cCurrencyIdr;
    else if (fieldName.equalsIgnoreCase("c_incoterms_id") || fieldName.equals("cIncotermsId"))
      return cIncotermsId;
    else if (fieldName.equalsIgnoreCase("c_incoterms_idr") || fieldName.equals("cIncotermsIdr"))
      return cIncotermsIdr;
    else if (fieldName.equalsIgnoreCase("tarifa_venta") || fieldName.equals("tarifaVenta"))
      return tarifaVenta;
    else if (fieldName.equalsIgnoreCase("tarifa_negociada") || fieldName.equals("tarifaNegociada"))
      return tarifaNegociada;
    else if (fieldName.equalsIgnoreCase("costos_locales") || fieldName.equals("costosLocales"))
      return costosLocales;
    else if (fieldName.equalsIgnoreCase("tarifa_venta_eur") || fieldName.equals("tarifaVentaEur"))
      return tarifaVentaEur;
    else if (fieldName.equalsIgnoreCase("tarifa_negociada_eur") || fieldName.equals("tarifaNegociadaEur"))
      return tarifaNegociadaEur;
    else if (fieldName.equalsIgnoreCase("costos_locales_eur") || fieldName.equals("costosLocalesEur"))
      return costosLocalesEur;
    else if (fieldName.equalsIgnoreCase("m_pricelist_com_id") || fieldName.equals("mPricelistComId"))
      return mPricelistComId;
    else if (fieldName.equalsIgnoreCase("m_pricelist_com_idr") || fieldName.equals("mPricelistComIdr"))
      return mPricelistComIdr;
    else if (fieldName.equalsIgnoreCase("tarifa_compra") || fieldName.equals("tarifaCompra"))
      return tarifaCompra;
    else if (fieldName.equalsIgnoreCase("embarque"))
      return embarque;
    else if (fieldName.equalsIgnoreCase("observacion"))
      return observacion;
    else if (fieldName.equalsIgnoreCase("ldt_aviso_ro") || fieldName.equals("ldtAvisoRo"))
      return ldtAvisoRo;
    else if (fieldName.equalsIgnoreCase("es_fin_embarque") || fieldName.equals("esFinEmbarque"))
      return esFinEmbarque;
    else if (fieldName.equalsIgnoreCase("m_pricelist_version_id") || fieldName.equals("mPricelistVersionId"))
      return mPricelistVersionId;
    else if (fieldName.equalsIgnoreCase("m_pricelist_version_com_id") || fieldName.equals("mPricelistVersionComId"))
      return mPricelistVersionComId;
    else if (fieldName.equalsIgnoreCase("m_pricelist_id") || fieldName.equals("mPricelistId"))
      return mPricelistId;
    else if (fieldName.equalsIgnoreCase("ldt_cod") || fieldName.equals("ldtCod"))
      return ldtCod;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("ldt_routing_order_id") || fieldName.equals("ldtRoutingOrderId"))
      return ldtRoutingOrderId;
    else if (fieldName.equalsIgnoreCase("ldt_puerto_terminal_id") || fieldName.equals("ldtPuertoTerminalId"))
      return ldtPuertoTerminalId;
    else if (fieldName.equalsIgnoreCase("fecha_estimada") || fieldName.equals("fechaEstimada"))
      return fechaEstimada;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static RoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static RoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(ldt_routing_order.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = ldt_routing_order.CreatedBy) as CreatedByR, " +
      "        to_char(ldt_routing_order.Updated, ?) as updated, " +
      "        to_char(ldt_routing_order.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        ldt_routing_order.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = ldt_routing_order.UpdatedBy) as UpdatedByR," +
      "        ldt_routing_order.AD_Org_ID, " +
      "(CASE WHEN ldt_routing_order.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "ldt_routing_order.C_Doctype_ID, " +
      "(CASE WHEN ldt_routing_order.C_Doctype_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL2.Name IS NULL THEN TO_CHAR(table2.Name) ELSE TO_CHAR(tableTRL2.Name) END)), ''))),'') ) END) AS C_Doctype_IDR, " +
      "ldt_routing_order.Documentno, " +
      "ldt_routing_order.LDT_Cotizacion_ID, " +
      "ldt_routing_order.Fecha, " +
      "ldt_routing_order.Puerto_Terminal_Origen_ID, " +
      "ldt_routing_order.Puerto_Terminal_Destino_ID, " +
      "ldt_routing_order.Estado, " +
      "ldt_routing_order.Numembarque, " +
      "ldt_routing_order.Vendedor_ID, " +
      "(CASE WHEN ldt_routing_order.Vendedor_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.Name), ''))),'') ) END) AS Vendedor_IDR, " +
      "COALESCE(ldt_routing_order.Isactive, 'N') AS Isactive, " +
      "ldt_routing_order.C_Bpartner_ID, " +
      "ldt_routing_order.C_Bpartner_Ruc, " +
      "ldt_routing_order.C_Bpartner_Location_ID, " +
      "ldt_routing_order.Telf_Shipper, " +
      "ldt_routing_order.AD_User_ID, " +
      "(CASE WHEN ldt_routing_order.AD_User_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name), ''))),'') ) END) AS AD_User_IDR, " +
      "ldt_routing_order.Mail, " +
      "ldt_routing_order.Cliente_ID, " +
      "(CASE WHEN ldt_routing_order.Cliente_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table5.Name), ''))),'') ) END) AS Cliente_IDR, " +
      "ldt_routing_order.Processed, " +
      "list1.name as Processed_BTN, " +
      "ldt_routing_order.Cliente_Ruc, " +
      "ldt_routing_order.Cliente_Direccion_ID, " +
      "(CASE WHEN ldt_routing_order.Cliente_Direccion_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table6.Name), ''))),'') ) END) AS Cliente_Direccion_IDR, " +
      "ldt_routing_order.Modalidad, " +
      "(CASE WHEN ldt_routing_order.Modalidad IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list2.name),'') ) END) AS ModalidadR, " +
      "ldt_routing_order.Tipo, " +
      "(CASE WHEN ldt_routing_order.Tipo IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list3.name),'') ) END) AS TipoR, " +
      "ldt_routing_order.Cod, " +
      "ldt_routing_order.ldt_order, " +
      "ldt_routing_order.Agente_ID, " +
      "ldt_routing_order.ldt_costo_agente, " +
      "ldt_routing_order.c_currency_id, " +
      "(CASE WHEN ldt_routing_order.c_currency_id IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table7.ISO_Code), ''))),'') ) END) AS c_currency_idR, " +
      "ldt_routing_order.C_Incoterms_ID, " +
      "(CASE WHEN ldt_routing_order.C_Incoterms_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table8.Name), ''))),'') ) END) AS C_Incoterms_IDR, " +
      "ldt_routing_order.Tarifa_Venta, " +
      "ldt_routing_order.Tarifa_Negociada, " +
      "ldt_routing_order.Costos_Locales, " +
      "ldt_routing_order.tarifa_venta_eur, " +
      "ldt_routing_order.tarifa_negociada_eur, " +
      "ldt_routing_order.costos_locales_eur, " +
      "ldt_routing_order.M_Pricelist_Com_ID, " +
      "(CASE WHEN ldt_routing_order.M_Pricelist_Com_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table9.Name), ''))),'') ) END) AS M_Pricelist_Com_IDR, " +
      "ldt_routing_order.Tarifa_Compra, " +
      "ldt_routing_order.Embarque, " +
      "ldt_routing_order.Observacion, " +
      "ldt_routing_order.LDT_Aviso_Ro, " +
      "ldt_routing_order.ES_Fin_Embarque, " +
      "ldt_routing_order.M_Pricelist_Version_ID, " +
      "ldt_routing_order.M_Pricelist_Version_Com_ID, " +
      "ldt_routing_order.M_Pricelist_ID, " +
      "COALESCE(ldt_routing_order.ldt_cod, 'N') AS ldt_cod, " +
      "ldt_routing_order.AD_Client_ID, " +
      "ldt_routing_order.LDT_Routing_Order_ID, " +
      "ldt_routing_order.ldt_puerto_terminal_id, " +
      "ldt_routing_order.Fecha_Estimada, " +
      "        ? AS LANGUAGE " +
      "        FROM ldt_routing_order left join (select AD_Org_ID, Name from AD_Org) table1 on (ldt_routing_order.AD_Org_ID = table1.AD_Org_ID) left join (select C_Doctype_ID, Name from C_Doctype) table2 on (ldt_routing_order.C_Doctype_ID = table2.C_Doctype_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL2 on (table2.C_DocType_ID = tableTRL2.C_DocType_ID and tableTRL2.AD_Language = ?)  left join (select C_BPartner_ID, Name from C_BPartner) table3 on (ldt_routing_order.Vendedor_ID =  table3.C_BPartner_ID) left join (select AD_User_ID, Name from AD_User) table4 on (ldt_routing_order.AD_User_ID = table4.AD_User_ID) left join (select C_BPartner_ID, Name from C_BPartner) table5 on (ldt_routing_order.Cliente_ID = table5.C_BPartner_ID) left join ad_ref_list_v list1 on (list1.ad_reference_id = '1F931E9A5FAB445A8FD2F069ADD2DBE4' and list1.ad_language = ?  AND ldt_routing_order.Processed = TO_CHAR(list1.value)) left join (select C_BPartner_Location_ID, Name from C_BPartner_Location) table6 on (ldt_routing_order.Cliente_Direccion_ID =  table6.C_BPartner_Location_ID) left join ad_ref_list_v list2 on (ldt_routing_order.Modalidad = list2.value and list2.ad_reference_id = '084746C4C4934924AABBD1EAB1DB9BF5' and list2.ad_language = ?)  left join ad_ref_list_v list3 on (ldt_routing_order.Tipo = list3.value and list3.ad_reference_id = '336BA96AA2694CB0BD2689AB13A88F03' and list3.ad_language = ?)  left join (select c_currency_id, ISO_Code from c_currency) table7 on (ldt_routing_order.c_currency_id = table7.c_currency_id) left join (select C_Incoterms_ID, Name from C_Incoterms) table8 on (ldt_routing_order.C_Incoterms_ID = table8.C_Incoterms_ID) left join (select M_PriceList_ID, Name from M_PriceList) table9 on (ldt_routing_order.M_Pricelist_Com_ID =  table9.M_PriceList_ID)" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND ldt_routing_order.LDT_Routing_Order_ID = ? " +
      "        AND ldt_routing_order.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND ldt_routing_order.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        RoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData = new RoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData();
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.created = UtilSql.getValue(result, "created");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.updated = UtilSql.getValue(result, "updated");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.updatedby = UtilSql.getValue(result, "updatedby");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.cDoctypeIdr = UtilSql.getValue(result, "c_doctype_idr");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.documentno = UtilSql.getValue(result, "documentno");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.ldtCotizacionId = UtilSql.getValue(result, "ldt_cotizacion_id");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.fecha = UtilSql.getDateValue(result, "fecha", "dd-MM-yyyy");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.puertoTerminalOrigenId = UtilSql.getValue(result, "puerto_terminal_origen_id");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.puertoTerminalDestinoId = UtilSql.getValue(result, "puerto_terminal_destino_id");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.estado = UtilSql.getValue(result, "estado");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.numembarque = UtilSql.getValue(result, "numembarque");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.vendedorId = UtilSql.getValue(result, "vendedor_id");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.vendedorIdr = UtilSql.getValue(result, "vendedor_idr");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.isactive = UtilSql.getValue(result, "isactive");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.cBpartnerRuc = UtilSql.getValue(result, "c_bpartner_ruc");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.cBpartnerLocationId = UtilSql.getValue(result, "c_bpartner_location_id");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.telfShipper = UtilSql.getValue(result, "telf_shipper");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.adUserId = UtilSql.getValue(result, "ad_user_id");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.adUserIdr = UtilSql.getValue(result, "ad_user_idr");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.mail = UtilSql.getValue(result, "mail");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.clienteId = UtilSql.getValue(result, "cliente_id");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.clienteIdr = UtilSql.getValue(result, "cliente_idr");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.processed = UtilSql.getValue(result, "processed");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.processedBtn = UtilSql.getValue(result, "processed_btn");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.clienteRuc = UtilSql.getValue(result, "cliente_ruc");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.clienteDireccionId = UtilSql.getValue(result, "cliente_direccion_id");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.clienteDireccionIdr = UtilSql.getValue(result, "cliente_direccion_idr");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.modalidad = UtilSql.getValue(result, "modalidad");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.modalidadr = UtilSql.getValue(result, "modalidadr");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.tipo = UtilSql.getValue(result, "tipo");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.tipor = UtilSql.getValue(result, "tipor");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.cod = UtilSql.getValue(result, "cod");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.ldtOrder = UtilSql.getValue(result, "ldt_order");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.agenteId = UtilSql.getValue(result, "agente_id");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.ldtCostoAgente = UtilSql.getValue(result, "ldt_costo_agente");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.cCurrencyId = UtilSql.getValue(result, "c_currency_id");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.cCurrencyIdr = UtilSql.getValue(result, "c_currency_idr");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.cIncotermsId = UtilSql.getValue(result, "c_incoterms_id");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.cIncotermsIdr = UtilSql.getValue(result, "c_incoterms_idr");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.tarifaVenta = UtilSql.getValue(result, "tarifa_venta");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.tarifaNegociada = UtilSql.getValue(result, "tarifa_negociada");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.costosLocales = UtilSql.getValue(result, "costos_locales");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.tarifaVentaEur = UtilSql.getValue(result, "tarifa_venta_eur");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.tarifaNegociadaEur = UtilSql.getValue(result, "tarifa_negociada_eur");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.costosLocalesEur = UtilSql.getValue(result, "costos_locales_eur");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.mPricelistComId = UtilSql.getValue(result, "m_pricelist_com_id");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.mPricelistComIdr = UtilSql.getValue(result, "m_pricelist_com_idr");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.tarifaCompra = UtilSql.getValue(result, "tarifa_compra");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.embarque = UtilSql.getValue(result, "embarque");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.observacion = UtilSql.getValue(result, "observacion");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.ldtAvisoRo = UtilSql.getValue(result, "ldt_aviso_ro");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.esFinEmbarque = UtilSql.getValue(result, "es_fin_embarque");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.mPricelistVersionId = UtilSql.getValue(result, "m_pricelist_version_id");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.mPricelistVersionComId = UtilSql.getValue(result, "m_pricelist_version_com_id");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.mPricelistId = UtilSql.getValue(result, "m_pricelist_id");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.ldtCod = UtilSql.getValue(result, "ldt_cod");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.ldtRoutingOrderId = UtilSql.getValue(result, "ldt_routing_order_id");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.ldtPuertoTerminalId = UtilSql.getValue(result, "ldt_puerto_terminal_id");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.fechaEstimada = UtilSql.getDateValue(result, "fecha_estimada", "dd-MM-yyyy");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.language = UtilSql.getValue(result, "language");
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.adUserClient = "";
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.adOrgClient = "";
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.createdby = "";
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.trBgcolor = "";
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.totalCount = "";
        objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    RoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[] = new RoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[vector.size()];
    vector.copyInto(objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData);
    return(objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData);
  }

/**
Create a registry
 */
  public static RoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[] set(String cBpartnerRuc, String mail, String fechaEstimada, String tarifaCompra, String cBpartnerLocationId, String esFinEmbarque, String clienteId, String clienteIdr, String cCurrencyId, String ldtRoutingOrderId, String mPricelistComId, String tipo, String adClientId, String embarque, String cIncotermsId, String tarifaNegociadaEur, String updatedby, String updatedbyr, String tarifaVenta, String tarifaVentaEur, String processed, String processedBtn, String vendedorId, String telfShipper, String adOrgId, String ldtAvisoRo, String adUserId, String puertoTerminalDestinoId, String modalidad, String mPricelistVersionId, String observacion, String ldtCotizacionId, String estado, String costosLocales, String costosLocalesEur, String mPricelistId, String clienteDireccionId, String documentno, String ldtCostoAgente, String createdby, String createdbyr, String numembarque, String cod, String clienteRuc, String ldtCod, String mPricelistVersionComId, String fecha, String isactive, String tarifaNegociada, String ldtOrder, String cDoctypeId, String puertoTerminalOrigenId, String ldtPuertoTerminalId, String cBpartnerId, String agenteId)    throws ServletException {
    RoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[] = new RoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[1];
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0] = new RoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData();
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].created = "";
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].createdbyr = createdbyr;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].updated = "";
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].updatedTimeStamp = "";
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].updatedby = updatedby;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].updatedbyr = updatedbyr;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].adOrgId = adOrgId;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].adOrgIdr = "";
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].cDoctypeId = cDoctypeId;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].cDoctypeIdr = "";
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].documentno = documentno;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].ldtCotizacionId = ldtCotizacionId;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].fecha = fecha;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].puertoTerminalOrigenId = puertoTerminalOrigenId;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].puertoTerminalDestinoId = puertoTerminalDestinoId;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].estado = estado;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].numembarque = numembarque;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].vendedorId = vendedorId;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].vendedorIdr = "";
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].isactive = isactive;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].cBpartnerId = cBpartnerId;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].cBpartnerRuc = cBpartnerRuc;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].cBpartnerLocationId = cBpartnerLocationId;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].telfShipper = telfShipper;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].adUserId = adUserId;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].adUserIdr = "";
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].mail = mail;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].clienteId = clienteId;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].clienteIdr = clienteIdr;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].processed = processed;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].processedBtn = processedBtn;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].clienteRuc = clienteRuc;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].clienteDireccionId = clienteDireccionId;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].clienteDireccionIdr = "";
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].modalidad = modalidad;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].modalidadr = "";
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].tipo = tipo;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].tipor = "";
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].cod = cod;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].ldtOrder = ldtOrder;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].agenteId = agenteId;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].ldtCostoAgente = ldtCostoAgente;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].cCurrencyId = cCurrencyId;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].cCurrencyIdr = "";
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].cIncotermsId = cIncotermsId;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].cIncotermsIdr = "";
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].tarifaVenta = tarifaVenta;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].tarifaNegociada = tarifaNegociada;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].costosLocales = costosLocales;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].tarifaVentaEur = tarifaVentaEur;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].tarifaNegociadaEur = tarifaNegociadaEur;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].costosLocalesEur = costosLocalesEur;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].mPricelistComId = mPricelistComId;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].mPricelistComIdr = "";
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].tarifaCompra = tarifaCompra;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].embarque = embarque;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].observacion = observacion;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].ldtAvisoRo = ldtAvisoRo;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].esFinEmbarque = esFinEmbarque;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].mPricelistVersionId = mPricelistVersionId;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].mPricelistVersionComId = mPricelistVersionComId;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].mPricelistId = mPricelistId;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].ldtCod = ldtCod;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].adClientId = adClientId;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].ldtRoutingOrderId = ldtRoutingOrderId;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].ldtPuertoTerminalId = ldtPuertoTerminalId;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].fechaEstimada = fechaEstimada;
    objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData[0].language = "";
    return objectRoutingOrderC90A0D7617094F6BA5BBE30FA7E4D88AData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef288AAF7724AB4659AC3EA3CAB946388E_0(ConnectionProvider connectionProvider, String Cliente_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Cliente_ID FROM C_BPartner left join (select C_BPartner_ID, Name from C_BPartner) table2 on (C_BPartner.C_BPartner_ID = table2.C_BPartner_ID) WHERE C_BPartner.isActive='Y' AND C_BPartner.C_BPartner_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, Cliente_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "cliente_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef3C3EE0E1BB8041268CB1EF103CC6E8D0_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefA68E17A6693C4D78B7972B1419D3E24E_2(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE ldt_routing_order" +
      "        SET AD_Org_ID = (?) , C_Doctype_ID = (?) , Documentno = (?) , LDT_Cotizacion_ID = (?) , Fecha = TO_DATE(?) , Puerto_Terminal_Origen_ID = (?) , Puerto_Terminal_Destino_ID = (?) , Estado = (?) , Numembarque = (?) , Vendedor_ID = (?) , Isactive = (?) , C_Bpartner_ID = (?) , C_Bpartner_Ruc = (?) , C_Bpartner_Location_ID = (?) , Telf_Shipper = (?) , AD_User_ID = (?) , Mail = (?) , Cliente_ID = (?) , Processed = (?) , Cliente_Ruc = (?) , Cliente_Direccion_ID = (?) , Modalidad = (?) , Tipo = (?) , Cod = TO_NUMBER(?) , ldt_order = (?) , Agente_ID = (?) , ldt_costo_agente = TO_NUMBER(?) , c_currency_id = (?) , C_Incoterms_ID = (?) , Tarifa_Venta = TO_NUMBER(?) , Tarifa_Negociada = TO_NUMBER(?) , Costos_Locales = TO_NUMBER(?) , tarifa_venta_eur = TO_NUMBER(?) , tarifa_negociada_eur = TO_NUMBER(?) , costos_locales_eur = TO_NUMBER(?) , M_Pricelist_Com_ID = (?) , Tarifa_Compra = TO_NUMBER(?) , Embarque = (?) , Observacion = (?) , LDT_Aviso_Ro = (?) , ES_Fin_Embarque = (?) , M_Pricelist_Version_ID = (?) , M_Pricelist_Version_Com_ID = (?) , M_Pricelist_ID = (?) , ldt_cod = (?) , AD_Client_ID = (?) , LDT_Routing_Order_ID = (?) , ldt_puerto_terminal_id = (?) , Fecha_Estimada = TO_DATE(?) , updated = now(), updatedby = ? " +
      "        WHERE ldt_routing_order.LDT_Routing_Order_ID = ? " +
      "        AND ldt_routing_order.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND ldt_routing_order.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtCotizacionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecha);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, puertoTerminalOrigenId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, puertoTerminalDestinoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numembarque);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vendedorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerRuc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, telfShipper);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mail);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clienteId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clienteRuc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clienteDireccionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, modalidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cod);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtOrder);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agenteId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtCostoAgente);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cIncotermsId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tarifaVenta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tarifaNegociada);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, costosLocales);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tarifaVentaEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tarifaNegociadaEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, costosLocalesEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistComId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tarifaCompra);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, embarque);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, observacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtAvisoRo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, esFinEmbarque);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistVersionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistVersionComId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtCod);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtPuertoTerminalId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaEstimada);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO ldt_routing_order " +
      "        (AD_Org_ID, C_Doctype_ID, Documentno, LDT_Cotizacion_ID, Fecha, Puerto_Terminal_Origen_ID, Puerto_Terminal_Destino_ID, Estado, Numembarque, Vendedor_ID, Isactive, C_Bpartner_ID, C_Bpartner_Ruc, C_Bpartner_Location_ID, Telf_Shipper, AD_User_ID, Mail, Cliente_ID, Processed, Cliente_Ruc, Cliente_Direccion_ID, Modalidad, Tipo, Cod, ldt_order, Agente_ID, ldt_costo_agente, c_currency_id, C_Incoterms_ID, Tarifa_Venta, Tarifa_Negociada, Costos_Locales, tarifa_venta_eur, tarifa_negociada_eur, costos_locales_eur, M_Pricelist_Com_ID, Tarifa_Compra, Embarque, Observacion, LDT_Aviso_Ro, ES_Fin_Embarque, M_Pricelist_Version_ID, M_Pricelist_Version_Com_ID, M_Pricelist_ID, ldt_cod, AD_Client_ID, LDT_Routing_Order_ID, ldt_puerto_terminal_id, Fecha_Estimada, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), TO_NUMBER(?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_DATE(?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtCotizacionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecha);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, puertoTerminalOrigenId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, puertoTerminalDestinoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numembarque);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vendedorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerRuc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, telfShipper);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mail);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clienteId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clienteRuc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clienteDireccionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, modalidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cod);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtOrder);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agenteId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtCostoAgente);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cIncotermsId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tarifaVenta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tarifaNegociada);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, costosLocales);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tarifaVentaEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tarifaNegociadaEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, costosLocalesEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistComId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tarifaCompra);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, embarque);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, observacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtAvisoRo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, esFinEmbarque);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistVersionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistVersionComId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtCod);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtPuertoTerminalId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaEstimada);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM ldt_routing_order" +
      "        WHERE ldt_routing_order.LDT_Routing_Order_ID = ? " +
      "        AND ldt_routing_order.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND ldt_routing_order.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM ldt_routing_order" +
      "         WHERE ldt_routing_order.LDT_Routing_Order_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM ldt_routing_order" +
      "         WHERE ldt_routing_order.LDT_Routing_Order_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
