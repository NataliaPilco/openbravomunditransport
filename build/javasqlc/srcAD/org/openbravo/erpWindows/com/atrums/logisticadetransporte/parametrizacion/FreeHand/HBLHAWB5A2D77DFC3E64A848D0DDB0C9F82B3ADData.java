//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.logisticadetransporte.parametrizacion.FreeHand;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData implements FieldProvider {
static Logger log4j = Logger.getLogger(HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String numHbl;
  public String ldtRoutingOrderId;
  public String secuencialhblHawb;
  public String cBpartnerId;
  public String cBpartnerIdr;
  public String isactive;
  public String valorFlete;
  public String fletePp;
  public String valorFleteEur;
  public String fletePpEur;
  public String calcular;
  public String valorTotalUsd;
  public String factura;
  public String cartas;
  public String avisoarribo;
  public String ldtHblId;
  public String adClientId;
  public String adOrgId;
  public String cInvoiceId;
  public String lftFh;
  public String estado;
  public String processed;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("num_hbl") || fieldName.equals("numHbl"))
      return numHbl;
    else if (fieldName.equalsIgnoreCase("ldt_routing_order_id") || fieldName.equals("ldtRoutingOrderId"))
      return ldtRoutingOrderId;
    else if (fieldName.equalsIgnoreCase("secuencialhbl_hawb") || fieldName.equals("secuencialhblHawb"))
      return secuencialhblHawb;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_idr") || fieldName.equals("cBpartnerIdr"))
      return cBpartnerIdr;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("valor_flete") || fieldName.equals("valorFlete"))
      return valorFlete;
    else if (fieldName.equalsIgnoreCase("flete_pp") || fieldName.equals("fletePp"))
      return fletePp;
    else if (fieldName.equalsIgnoreCase("valor_flete_eur") || fieldName.equals("valorFleteEur"))
      return valorFleteEur;
    else if (fieldName.equalsIgnoreCase("flete_pp_eur") || fieldName.equals("fletePpEur"))
      return fletePpEur;
    else if (fieldName.equalsIgnoreCase("calcular"))
      return calcular;
    else if (fieldName.equalsIgnoreCase("valor_total_usd") || fieldName.equals("valorTotalUsd"))
      return valorTotalUsd;
    else if (fieldName.equalsIgnoreCase("factura"))
      return factura;
    else if (fieldName.equalsIgnoreCase("cartas"))
      return cartas;
    else if (fieldName.equalsIgnoreCase("avisoarribo"))
      return avisoarribo;
    else if (fieldName.equalsIgnoreCase("ldt_hbl_id") || fieldName.equals("ldtHblId"))
      return ldtHblId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("c_invoice_id") || fieldName.equals("cInvoiceId"))
      return cInvoiceId;
    else if (fieldName.equalsIgnoreCase("lft_fh") || fieldName.equals("lftFh"))
      return lftFh;
    else if (fieldName.equalsIgnoreCase("estado"))
      return estado;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String ldtRoutingOrderId, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, ldtRoutingOrderId, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String ldtRoutingOrderId, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(ldt_hbl.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = ldt_hbl.CreatedBy) as CreatedByR, " +
      "        to_char(ldt_hbl.Updated, ?) as updated, " +
      "        to_char(ldt_hbl.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        ldt_hbl.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = ldt_hbl.UpdatedBy) as UpdatedByR," +
      "        ldt_hbl.NUM_Hbl, " +
      "ldt_hbl.LDT_Routing_Order_ID, " +
      "ldt_hbl.Secuencialhbl_Hawb, " +
      "ldt_hbl.C_Bpartner_ID, " +
      "(CASE WHEN ldt_hbl.C_Bpartner_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS C_Bpartner_IDR, " +
      "COALESCE(ldt_hbl.Isactive, 'N') AS Isactive, " +
      "ldt_hbl.Valor_Flete, " +
      "ldt_hbl.flete_pp, " +
      "ldt_hbl.valor_flete_eur, " +
      "ldt_hbl.flete_pp_eur, " +
      "COALESCE(ldt_hbl.calcular, 'N') AS calcular, " +
      "ldt_hbl.valor_total_usd, " +
      "ldt_hbl.Factura, " +
      "ldt_hbl.Cartas, " +
      "ldt_hbl.Avisoarribo, " +
      "ldt_hbl.LDT_Hbl_ID, " +
      "ldt_hbl.AD_Client_ID, " +
      "ldt_hbl.AD_Org_ID, " +
      "ldt_hbl.C_Invoice_ID, " +
      "COALESCE(ldt_hbl.LFT_Fh, 'N') AS LFT_Fh, " +
      "ldt_hbl.Estado, " +
      "ldt_hbl.Processed, " +
      "        ? AS LANGUAGE " +
      "        FROM ldt_hbl left join (select C_BPartner_ID, Name from C_BPartner) table1 on (ldt_hbl.C_Bpartner_ID = table1.C_BPartner_ID)" +
      "        WHERE 2=2 " +
      "        AND 1=1 ";
    strSql = strSql + ((ldtRoutingOrderId==null || ldtRoutingOrderId.equals(""))?"":"  AND ldt_hbl.LDT_Routing_Order_ID = ?  ");
    strSql = strSql + 
      "        AND ldt_hbl.LDT_Hbl_ID = ? " +
      "        AND ldt_hbl.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND ldt_hbl.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      if (ldtRoutingOrderId != null && !(ldtRoutingOrderId.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);
      }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData = new HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData();
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.created = UtilSql.getValue(result, "created");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.updated = UtilSql.getValue(result, "updated");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.updatedby = UtilSql.getValue(result, "updatedby");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.numHbl = UtilSql.getValue(result, "num_hbl");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.ldtRoutingOrderId = UtilSql.getValue(result, "ldt_routing_order_id");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.secuencialhblHawb = UtilSql.getValue(result, "secuencialhbl_hawb");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.cBpartnerIdr = UtilSql.getValue(result, "c_bpartner_idr");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.isactive = UtilSql.getValue(result, "isactive");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.valorFlete = UtilSql.getValue(result, "valor_flete");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.fletePp = UtilSql.getValue(result, "flete_pp");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.valorFleteEur = UtilSql.getValue(result, "valor_flete_eur");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.fletePpEur = UtilSql.getValue(result, "flete_pp_eur");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.calcular = UtilSql.getValue(result, "calcular");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.valorTotalUsd = UtilSql.getValue(result, "valor_total_usd");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.factura = UtilSql.getValue(result, "factura");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.cartas = UtilSql.getValue(result, "cartas");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.avisoarribo = UtilSql.getValue(result, "avisoarribo");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.ldtHblId = UtilSql.getValue(result, "ldt_hbl_id");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.cInvoiceId = UtilSql.getValue(result, "c_invoice_id");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.lftFh = UtilSql.getValue(result, "lft_fh");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.estado = UtilSql.getValue(result, "estado");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.processed = UtilSql.getValue(result, "processed");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.language = UtilSql.getValue(result, "language");
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.adUserClient = "";
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.adOrgClient = "";
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.createdby = "";
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.trBgcolor = "";
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.totalCount = "";
        objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[] = new HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[vector.size()];
    vector.copyInto(objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData);
    return(objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData);
  }

/**
Create a registry
 */
  public static HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[] set(String ldtRoutingOrderId, String calcular, String secuencialhblHawb, String adOrgId, String cartas, String processed, String valorTotalUsd, String estado, String valorFleteEur, String valorFlete, String cBpartnerId, String cBpartnerIdr, String fletePpEur, String fletePp, String cInvoiceId, String lftFh, String factura, String ldtHblId, String numHbl, String updatedby, String updatedbyr, String adClientId, String isactive, String createdby, String createdbyr, String avisoarribo)    throws ServletException {
    HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[] = new HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[1];
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0] = new HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData();
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].created = "";
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].createdbyr = createdbyr;
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].updated = "";
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].updatedTimeStamp = "";
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].updatedby = updatedby;
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].updatedbyr = updatedbyr;
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].numHbl = numHbl;
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].ldtRoutingOrderId = ldtRoutingOrderId;
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].secuencialhblHawb = secuencialhblHawb;
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].cBpartnerId = cBpartnerId;
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].cBpartnerIdr = cBpartnerIdr;
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].isactive = isactive;
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].valorFlete = valorFlete;
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].fletePp = fletePp;
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].valorFleteEur = valorFleteEur;
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].fletePpEur = fletePpEur;
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].calcular = calcular;
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].valorTotalUsd = valorTotalUsd;
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].factura = factura;
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].cartas = cartas;
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].avisoarribo = avisoarribo;
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].ldtHblId = ldtHblId;
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].adClientId = adClientId;
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].adOrgId = adOrgId;
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].cInvoiceId = cInvoiceId;
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].lftFh = lftFh;
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].estado = estado;
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].processed = processed;
    objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0].language = "";
    return objectHBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef7F025BCB6D1343EEB2624BB5DFBA7625_0(ConnectionProvider connectionProvider, String C_Bpartner_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as C_Bpartner_ID FROM C_BPartner left join (select C_BPartner_ID, Name from C_BPartner) table2 on (C_BPartner.C_BPartner_ID = table2.C_BPartner_ID) WHERE C_BPartner.isActive='Y' AND C_BPartner.C_BPartner_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Bpartner_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_bpartner_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefAB621508B70E4E3ABB779427C57F9CEB(ConnectionProvider connectionProvider, String LDT_Routing_Order_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (CASE WHEN LDT_COTIZACION_ID IS NULL THEN 'Y' ELSE 'N' END) as defaultValue  FROM LDT_ROUTING_ORDER WHERE LDT_ROUTING_ORDER_ID =? ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, LDT_Routing_Order_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefCEA41398F6F34FCAB387780EDC6B8E94_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefF4819B0004B141B3888DFF590C15FAEC_2(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
return the parent ID
 */
  public static String selectParentID(ConnectionProvider connectionProvider, String key)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT ldt_hbl.LDT_Routing_Order_ID AS NAME" +
      "        FROM ldt_hbl" +
      "        WHERE ldt_hbl.LDT_Hbl_ID = ?";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParent(ConnectionProvider connectionProvider, String ldtRoutingOrderId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.Documentno), ''))) AS NAME FROM ldt_routing_order left join (select ldt_routing_order_ID, Documentno from ldt_routing_order) table1 on (ldt_routing_order.LDT_Routing_Order_ID = table1.ldt_routing_order_ID) WHERE ldt_routing_order.LDT_Routing_Order_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParentTrl(ConnectionProvider connectionProvider, String ldtRoutingOrderId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.Documentno), ''))) AS NAME FROM ldt_routing_order left join (select ldt_routing_order_ID, Documentno from ldt_routing_order) table1 on (ldt_routing_order.LDT_Routing_Order_ID = table1.ldt_routing_order_ID) WHERE ldt_routing_order.LDT_Routing_Order_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE ldt_hbl" +
      "        SET NUM_Hbl = (?) , LDT_Routing_Order_ID = (?) , Secuencialhbl_Hawb = (?) , C_Bpartner_ID = (?) , Isactive = (?) , Valor_Flete = TO_NUMBER(?) , flete_pp = TO_NUMBER(?) , valor_flete_eur = TO_NUMBER(?) , flete_pp_eur = TO_NUMBER(?) , calcular = (?) , valor_total_usd = TO_NUMBER(?) , Factura = (?) , Cartas = (?) , Avisoarribo = (?) , LDT_Hbl_ID = (?) , AD_Client_ID = (?) , AD_Org_ID = (?) , C_Invoice_ID = (?) , LFT_Fh = (?) , Estado = (?) , Processed = (?) , updated = now(), updatedby = ? " +
      "        WHERE ldt_hbl.LDT_Hbl_ID = ? " +
      "                 AND ldt_hbl.LDT_Routing_Order_ID = ? " +
      "        AND ldt_hbl.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND ldt_hbl.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numHbl);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, secuencialhblHawb);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valorFlete);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fletePp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valorFleteEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fletePpEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, calcular);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valorTotalUsd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, factura);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cartas);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, avisoarribo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtHblId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, lftFh);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtHblId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO ldt_hbl " +
      "        (NUM_Hbl, LDT_Routing_Order_ID, Secuencialhbl_Hawb, C_Bpartner_ID, Isactive, Valor_Flete, flete_pp, valor_flete_eur, flete_pp_eur, calcular, valor_total_usd, Factura, Cartas, Avisoarribo, LDT_Hbl_ID, AD_Client_ID, AD_Org_ID, C_Invoice_ID, LFT_Fh, Estado, Processed, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numHbl);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, secuencialhblHawb);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valorFlete);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fletePp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valorFleteEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fletePpEur);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, calcular);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valorTotalUsd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, factura);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cartas);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, avisoarribo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtHblId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, lftFh);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String ldtRoutingOrderId, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM ldt_hbl" +
      "        WHERE ldt_hbl.LDT_Hbl_ID = ? " +
      "                 AND ldt_hbl.LDT_Routing_Order_ID = ? " +
      "        AND ldt_hbl.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND ldt_hbl.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM ldt_hbl" +
      "         WHERE ldt_hbl.LDT_Hbl_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM ldt_hbl" +
      "         WHERE ldt_hbl.LDT_Hbl_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
