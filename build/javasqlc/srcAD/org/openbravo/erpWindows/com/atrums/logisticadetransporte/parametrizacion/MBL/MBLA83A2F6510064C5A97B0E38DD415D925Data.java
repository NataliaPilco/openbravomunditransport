//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.logisticadetransporte.parametrizacion.MBL;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class MBLA83A2F6510064C5A97B0E38DD415D925Data implements FieldProvider {
static Logger log4j = Logger.getLogger(MBLA83A2F6510064C5A97B0E38DD415D925Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String numMbl;
  public String secuencialmblMawb;
  public String fecha;
  public String condicion;
  public String condicionr;
  public String ldtHblId;
  public String puertoTerminalOrigenId;
  public String puertoTerminalDestinoId;
  public String cBpartnerId;
  public String processed;
  public String factura;
  public String buqueVuelo;
  public String pesototal;
  public String total;
  public String manifiesto;
  public String container;
  public String bodega;
  public String totalvolumen;
  public String valor;
  public String cInvoiceId;
  public String buqueVueloArribo;
  public String isactive;
  public String fechaEstimadaArribo;
  public String notas;
  public String estado;
  public String ldtMblId;
  public String adClientId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("num_mbl") || fieldName.equals("numMbl"))
      return numMbl;
    else if (fieldName.equalsIgnoreCase("secuencialmbl_mawb") || fieldName.equals("secuencialmblMawb"))
      return secuencialmblMawb;
    else if (fieldName.equalsIgnoreCase("fecha"))
      return fecha;
    else if (fieldName.equalsIgnoreCase("condicion"))
      return condicion;
    else if (fieldName.equalsIgnoreCase("condicionr"))
      return condicionr;
    else if (fieldName.equalsIgnoreCase("ldt_hbl_id") || fieldName.equals("ldtHblId"))
      return ldtHblId;
    else if (fieldName.equalsIgnoreCase("puerto_terminal_origen_id") || fieldName.equals("puertoTerminalOrigenId"))
      return puertoTerminalOrigenId;
    else if (fieldName.equalsIgnoreCase("puerto_terminal_destino_id") || fieldName.equals("puertoTerminalDestinoId"))
      return puertoTerminalDestinoId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("factura"))
      return factura;
    else if (fieldName.equalsIgnoreCase("buque_vuelo") || fieldName.equals("buqueVuelo"))
      return buqueVuelo;
    else if (fieldName.equalsIgnoreCase("pesototal"))
      return pesototal;
    else if (fieldName.equalsIgnoreCase("total"))
      return total;
    else if (fieldName.equalsIgnoreCase("manifiesto"))
      return manifiesto;
    else if (fieldName.equalsIgnoreCase("container"))
      return container;
    else if (fieldName.equalsIgnoreCase("bodega"))
      return bodega;
    else if (fieldName.equalsIgnoreCase("totalvolumen"))
      return totalvolumen;
    else if (fieldName.equalsIgnoreCase("valor"))
      return valor;
    else if (fieldName.equalsIgnoreCase("c_invoice_id") || fieldName.equals("cInvoiceId"))
      return cInvoiceId;
    else if (fieldName.equalsIgnoreCase("buque_vuelo_arribo") || fieldName.equals("buqueVueloArribo"))
      return buqueVueloArribo;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("fecha_estimada_arribo") || fieldName.equals("fechaEstimadaArribo"))
      return fechaEstimadaArribo;
    else if (fieldName.equalsIgnoreCase("notas"))
      return notas;
    else if (fieldName.equalsIgnoreCase("estado"))
      return estado;
    else if (fieldName.equalsIgnoreCase("ldt_mbl_id") || fieldName.equals("ldtMblId"))
      return ldtMblId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static MBLA83A2F6510064C5A97B0E38DD415D925Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static MBLA83A2F6510064C5A97B0E38DD415D925Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(ldt_mbl.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = ldt_mbl.CreatedBy) as CreatedByR, " +
      "        to_char(ldt_mbl.Updated, ?) as updated, " +
      "        to_char(ldt_mbl.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        ldt_mbl.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = ldt_mbl.UpdatedBy) as UpdatedByR," +
      "        ldt_mbl.AD_Org_ID, " +
      "(CASE WHEN ldt_mbl.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "ldt_mbl.NUM_Mbl, " +
      "ldt_mbl.Secuencialmbl_Mawb, " +
      "ldt_mbl.Fecha, " +
      "ldt_mbl.Condicion, " +
      "(CASE WHEN ldt_mbl.Condicion IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS CondicionR, " +
      "ldt_mbl.LDT_Hbl_ID, " +
      "ldt_mbl.Puerto_Terminal_Origen_ID, " +
      "ldt_mbl.Puerto_Terminal_Destino_ID, " +
      "ldt_mbl.C_Bpartner_ID, " +
      "ldt_mbl.Processed, " +
      "ldt_mbl.Factura, " +
      "ldt_mbl.Buque_Vuelo, " +
      "ldt_mbl.Pesototal, " +
      "ldt_mbl.Total, " +
      "ldt_mbl.Manifiesto, " +
      "ldt_mbl.Container, " +
      "ldt_mbl.Bodega, " +
      "ldt_mbl.Totalvolumen, " +
      "ldt_mbl.Valor, " +
      "ldt_mbl.C_Invoice_ID, " +
      "ldt_mbl.Buque_Vuelo_Arribo, " +
      "COALESCE(ldt_mbl.Isactive, 'N') AS Isactive, " +
      "ldt_mbl.fecha_estimada_arribo, " +
      "ldt_mbl.notas, " +
      "ldt_mbl.Estado, " +
      "ldt_mbl.LDT_Mbl_ID, " +
      "ldt_mbl.AD_Client_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM ldt_mbl left join (select AD_Org_ID, Name from AD_Org) table1 on (ldt_mbl.AD_Org_ID = table1.AD_Org_ID) left join ad_ref_list_v list1 on (ldt_mbl.Condicion = list1.value and list1.ad_reference_id = '3F49F47A1C5A422FB113F8C41D747E74' and list1.ad_language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND ldt_mbl.LDT_Mbl_ID = ? " +
      "        AND ldt_mbl.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND ldt_mbl.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        MBLA83A2F6510064C5A97B0E38DD415D925Data objectMBLA83A2F6510064C5A97B0E38DD415D925Data = new MBLA83A2F6510064C5A97B0E38DD415D925Data();
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.created = UtilSql.getValue(result, "created");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.updated = UtilSql.getValue(result, "updated");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.numMbl = UtilSql.getValue(result, "num_mbl");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.secuencialmblMawb = UtilSql.getValue(result, "secuencialmbl_mawb");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.fecha = UtilSql.getDateValue(result, "fecha", "dd-MM-yyyy");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.condicion = UtilSql.getValue(result, "condicion");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.condicionr = UtilSql.getValue(result, "condicionr");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.ldtHblId = UtilSql.getValue(result, "ldt_hbl_id");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.puertoTerminalOrigenId = UtilSql.getValue(result, "puerto_terminal_origen_id");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.puertoTerminalDestinoId = UtilSql.getValue(result, "puerto_terminal_destino_id");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.processed = UtilSql.getValue(result, "processed");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.factura = UtilSql.getValue(result, "factura");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.buqueVuelo = UtilSql.getValue(result, "buque_vuelo");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.pesototal = UtilSql.getValue(result, "pesototal");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.total = UtilSql.getValue(result, "total");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.manifiesto = UtilSql.getValue(result, "manifiesto");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.container = UtilSql.getValue(result, "container");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.bodega = UtilSql.getValue(result, "bodega");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.totalvolumen = UtilSql.getValue(result, "totalvolumen");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.valor = UtilSql.getValue(result, "valor");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.cInvoiceId = UtilSql.getValue(result, "c_invoice_id");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.buqueVueloArribo = UtilSql.getValue(result, "buque_vuelo_arribo");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.isactive = UtilSql.getValue(result, "isactive");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.fechaEstimadaArribo = UtilSql.getDateValue(result, "fecha_estimada_arribo", "dd-MM-yyyy");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.notas = UtilSql.getValue(result, "notas");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.estado = UtilSql.getValue(result, "estado");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.ldtMblId = UtilSql.getValue(result, "ldt_mbl_id");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.language = UtilSql.getValue(result, "language");
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.adUserClient = "";
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.adOrgClient = "";
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.createdby = "";
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.trBgcolor = "";
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.totalCount = "";
        objectMBLA83A2F6510064C5A97B0E38DD415D925Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectMBLA83A2F6510064C5A97B0E38DD415D925Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    MBLA83A2F6510064C5A97B0E38DD415D925Data objectMBLA83A2F6510064C5A97B0E38DD415D925Data[] = new MBLA83A2F6510064C5A97B0E38DD415D925Data[vector.size()];
    vector.copyInto(objectMBLA83A2F6510064C5A97B0E38DD415D925Data);
    return(objectMBLA83A2F6510064C5A97B0E38DD415D925Data);
  }

/**
Create a registry
 */
  public static MBLA83A2F6510064C5A97B0E38DD415D925Data[] set(String adOrgId, String puertoTerminalDestinoId, String estado, String buqueVueloArribo, String totalvolumen, String numMbl, String buqueVuelo, String cBpartnerId, String manifiesto, String fechaEstimadaArribo, String container, String createdby, String createdbyr, String total, String secuencialmblMawb, String bodega, String valor, String fecha, String ldtHblId, String processed, String pesototal, String condicion, String notas, String factura, String isactive, String ldtMblId, String updatedby, String updatedbyr, String cInvoiceId, String puertoTerminalOrigenId, String adClientId)    throws ServletException {
    MBLA83A2F6510064C5A97B0E38DD415D925Data objectMBLA83A2F6510064C5A97B0E38DD415D925Data[] = new MBLA83A2F6510064C5A97B0E38DD415D925Data[1];
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0] = new MBLA83A2F6510064C5A97B0E38DD415D925Data();
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].created = "";
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].createdbyr = createdbyr;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].updated = "";
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].updatedTimeStamp = "";
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].updatedby = updatedby;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].updatedbyr = updatedbyr;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].adOrgId = adOrgId;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].adOrgIdr = "";
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].numMbl = numMbl;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].secuencialmblMawb = secuencialmblMawb;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].fecha = fecha;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].condicion = condicion;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].condicionr = "";
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].ldtHblId = ldtHblId;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].puertoTerminalOrigenId = puertoTerminalOrigenId;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].puertoTerminalDestinoId = puertoTerminalDestinoId;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].cBpartnerId = cBpartnerId;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].processed = processed;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].factura = factura;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].buqueVuelo = buqueVuelo;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].pesototal = pesototal;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].total = total;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].manifiesto = manifiesto;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].container = container;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].bodega = bodega;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].totalvolumen = totalvolumen;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].valor = valor;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].cInvoiceId = cInvoiceId;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].buqueVueloArribo = buqueVueloArribo;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].isactive = isactive;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].fechaEstimadaArribo = fechaEstimadaArribo;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].notas = notas;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].estado = estado;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].ldtMblId = ldtMblId;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].adClientId = adClientId;
    objectMBLA83A2F6510064C5A97B0E38DD415D925Data[0].language = "";
    return objectMBLA83A2F6510064C5A97B0E38DD415D925Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef7AE014C733A24CCF8DCB81D1E42E8FD1_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefC566100491F04E6183E50C6C72EFA24A_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE ldt_mbl" +
      "        SET AD_Org_ID = (?) , NUM_Mbl = (?) , Secuencialmbl_Mawb = (?) , Fecha = TO_DATE(?) , Condicion = (?) , LDT_Hbl_ID = (?) , Puerto_Terminal_Origen_ID = (?) , Puerto_Terminal_Destino_ID = (?) , C_Bpartner_ID = (?) , Processed = (?) , Factura = (?) , Buque_Vuelo = (?) , Pesototal = TO_NUMBER(?) , Total = TO_NUMBER(?) , Manifiesto = (?) , Container = (?) , Bodega = (?) , Totalvolumen = TO_NUMBER(?) , Valor = TO_NUMBER(?) , C_Invoice_ID = (?) , Buque_Vuelo_Arribo = (?) , Isactive = (?) , fecha_estimada_arribo = TO_DATE(?) , notas = (?) , Estado = (?) , LDT_Mbl_ID = (?) , AD_Client_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE ldt_mbl.LDT_Mbl_ID = ? " +
      "        AND ldt_mbl.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND ldt_mbl.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numMbl);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, secuencialmblMawb);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecha);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, condicion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtHblId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, puertoTerminalOrigenId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, puertoTerminalDestinoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, factura);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, buqueVuelo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pesototal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, total);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, manifiesto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, container);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, bodega);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalvolumen);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valor);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, buqueVueloArribo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaEstimadaArribo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, notas);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtMblId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtMblId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO ldt_mbl " +
      "        (AD_Org_ID, NUM_Mbl, Secuencialmbl_Mawb, Fecha, Condicion, LDT_Hbl_ID, Puerto_Terminal_Origen_ID, Puerto_Terminal_Destino_ID, C_Bpartner_ID, Processed, Factura, Buque_Vuelo, Pesototal, Total, Manifiesto, Container, Bodega, Totalvolumen, Valor, C_Invoice_ID, Buque_Vuelo_Arribo, Isactive, fecha_estimada_arribo, notas, Estado, LDT_Mbl_ID, AD_Client_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numMbl);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, secuencialmblMawb);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecha);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, condicion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtHblId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, puertoTerminalOrigenId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, puertoTerminalDestinoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, factura);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, buqueVuelo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pesototal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, total);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, manifiesto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, container);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, bodega);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalvolumen);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valor);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, buqueVueloArribo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaEstimadaArribo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, notas);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtMblId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM ldt_mbl" +
      "        WHERE ldt_mbl.LDT_Mbl_ID = ? " +
      "        AND ldt_mbl.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND ldt_mbl.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM ldt_mbl" +
      "         WHERE ldt_mbl.LDT_Mbl_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM ldt_mbl" +
      "         WHERE ldt_mbl.LDT_Mbl_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
