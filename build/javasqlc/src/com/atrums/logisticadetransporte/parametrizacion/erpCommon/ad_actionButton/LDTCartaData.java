//Sqlc generated V1.O00-1
package com.atrums.logisticadetransporte.parametrizacion.erpCommon.ad_actionButton;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class LDTCartaData implements FieldProvider {
static Logger log4j = Logger.getLogger(LDTCartaData.class);
  private String InitRecordNumber="0";
  public String id;
  public String name;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("id"))
      return id;
    else if (fieldName.equalsIgnoreCase("name"))
      return name;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static LDTCartaData[] select(ConnectionProvider connectionProvider, String srcAdOrgClient, String srcAdUserClient)    throws ServletException {
    return select(connectionProvider, srcAdOrgClient, srcAdUserClient, 0, 0);
  }

  public static LDTCartaData[] select(ConnectionProvider connectionProvider, String srcAdOrgClient, String srcAdUserClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "          SELECT ldt_cartas.ldt_cartas_id as id, " +
      "                 ((CASE ldt_cartas.isActive WHEN 'N' THEN '**' ELSE '' END) || ldt_cartas.nombre) as name " +
      "            FROM ldt_cartas" +
      "           WHERE ldt_cartas.AD_Org_ID IN(";
    strSql = strSql + ((srcAdOrgClient==null || srcAdOrgClient.equals(""))?"":srcAdOrgClient);
    strSql = strSql + 
      ") AND ldt_cartas.AD_Client_ID IN(";
    strSql = strSql + ((srcAdUserClient==null || srcAdUserClient.equals(""))?"":srcAdUserClient);
    strSql = strSql + 
      ")  " +
      "             AND ldt_cartas.isActive = 'Y'" +
      "          ORDER BY name";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);
      if (srcAdOrgClient != null && !(srcAdOrgClient.equals(""))) {
        }
      if (srcAdUserClient != null && !(srcAdUserClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LDTCartaData objectLDTCartaData = new LDTCartaData();
        objectLDTCartaData.id = UtilSql.getValue(result, "id");
        objectLDTCartaData.name = UtilSql.getValue(result, "name");
        objectLDTCartaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLDTCartaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LDTCartaData objectLDTCartaData[] = new LDTCartaData[vector.size()];
    vector.copyInto(objectLDTCartaData);
    return(objectLDTCartaData);
  }
}
