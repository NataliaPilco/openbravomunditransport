//Sqlc generated V1.O00-1
package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class LDTPrecioProductoData implements FieldProvider {
static Logger log4j = Logger.getLogger(LDTPrecioProductoData.class);
  private String InitRecordNumber="0";
  public String precio;
  public String versionTarifa;
  public String warehouse;
  public String minimo;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("precio"))
      return precio;
    else if (fieldName.equalsIgnoreCase("version_tarifa") || fieldName.equals("versionTarifa"))
      return versionTarifa;
    else if (fieldName.equalsIgnoreCase("warehouse"))
      return warehouse;
    else if (fieldName.equalsIgnoreCase("minimo"))
      return minimo;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static LDTPrecioProductoData[] selectPrecio(ConnectionProvider connectionProvider, String mProductId, String cIncotermsId, String mPricelistId, String ldtCotizacionNavieraId)    throws ServletException {
    return selectPrecio(connectionProvider, mProductId, cIncotermsId, mPricelistId, ldtCotizacionNavieraId, 0, 0);
  }

  public static LDTPrecioProductoData[] selectPrecio(ConnectionProvider connectionProvider, String mProductId, String cIncotermsId, String mPricelistId, String ldtCotizacionNavieraId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		 SELECT (COALESCE(PR.PRICELIST,0)+ COALESCE(PR.OPA,0)) AS PRECIO," +
      "		        PR.M_PRICELIST_VERSION_ID AS VERSION_TARIFA," +
      "		        PR.M_WAREHOUSE_ID AS WAREHOUSE," +
      "		        PR.EM_LDT_VALOR_MINIMO AS MINIMO" +
      "		   FROM LDT_PRICELIST_INCOTERMS_V  PR, M_WAREHOUSE W" +
      "		  WHERE PR.M_PRODUCT_ID    = ?" +
      "		    AND PR.C_INCOTERMS_ID  = ?" +
      "		    AND PR.M_PRICELIST_ID  = ?" +
      "		    AND PR.M_WAREHOUSE_ID   = W.M_WAREHOUSE_ID" +
      "		    AND PR.M_PRICELIST_VERSION_ID = (SELECT M_PRICELIST_VERSION_ID " +
      "		                                       FROM LDT_COTIZACION_NAVIERA " +
      "		                                      WHERE LDT_COTIZACION_NAVIERA_ID = ?)";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cIncotermsId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtCotizacionNavieraId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LDTPrecioProductoData objectLDTPrecioProductoData = new LDTPrecioProductoData();
        objectLDTPrecioProductoData.precio = UtilSql.getValue(result, "precio");
        objectLDTPrecioProductoData.versionTarifa = UtilSql.getValue(result, "version_tarifa");
        objectLDTPrecioProductoData.warehouse = UtilSql.getValue(result, "warehouse");
        objectLDTPrecioProductoData.minimo = UtilSql.getValue(result, "minimo");
        objectLDTPrecioProductoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLDTPrecioProductoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LDTPrecioProductoData objectLDTPrecioProductoData[] = new LDTPrecioProductoData[vector.size()];
    vector.copyInto(objectLDTPrecioProductoData);
    return(objectLDTPrecioProductoData);
  }
}
