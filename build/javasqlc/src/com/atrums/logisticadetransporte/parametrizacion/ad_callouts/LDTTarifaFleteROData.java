//Sqlc generated V1.O00-1
package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;

class LDTTarifaFleteROData implements FieldProvider {
static Logger log4j = Logger.getLogger(LDTTarifaFleteROData.class);
  private String InitRecordNumber="0";
  public String mPricelistId;
  public String fechacaduca;
  public String versionTarifa;
  public String emLdtContratoPuertoId;
  public String diasLibres;
  public String moneda;
  public String emLdtPuertoOrigenId;
  public String emLdtPuertoDestinoId;
  public String emLdtTransportistaId;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("m_pricelist_id") || fieldName.equals("mPricelistId"))
      return mPricelistId;
    else if (fieldName.equalsIgnoreCase("fechacaduca"))
      return fechacaduca;
    else if (fieldName.equalsIgnoreCase("version_tarifa") || fieldName.equals("versionTarifa"))
      return versionTarifa;
    else if (fieldName.equalsIgnoreCase("em_ldt_contrato_puerto_id") || fieldName.equals("emLdtContratoPuertoId"))
      return emLdtContratoPuertoId;
    else if (fieldName.equalsIgnoreCase("dias_libres") || fieldName.equals("diasLibres"))
      return diasLibres;
    else if (fieldName.equalsIgnoreCase("moneda"))
      return moneda;
    else if (fieldName.equalsIgnoreCase("em_ldt_puerto_origen_id") || fieldName.equals("emLdtPuertoOrigenId"))
      return emLdtPuertoOrigenId;
    else if (fieldName.equalsIgnoreCase("em_ldt_puerto_destino_id") || fieldName.equals("emLdtPuertoDestinoId"))
      return emLdtPuertoDestinoId;
    else if (fieldName.equalsIgnoreCase("em_ldt_transportista_id") || fieldName.equals("emLdtTransportistaId"))
      return emLdtTransportistaId;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static String seleccionarTarifa(ConnectionProvider connectionProvider, String inppuertoTerminalOrigenId, String inppuertoTerminalDestinoId, String inpcBpartnerMtId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		   SELECT T.m_pricelist_id, T.fechaCaduca, T.version_tarifa, '' as em_ldt_contrato_puerto_id," +
      "        T.dias_libres, T.moneda, T.em_ldt_puerto_origen_id, T.em_ldt_puerto_destino_id, T.em_ldt_transportista_id" +
      "        FROM (SELECT pl.m_pricelist_id," +
      "               pl.em_ldt_puerto_origen_id, " +
      "               pl.em_ldt_puerto_destino_id, " +
      "               pl.em_ldt_transportista_id," +
      "               to_char(plv.EM_LDT_FECHA_CADUCA,'dd-MM-yyyy') AS fechaCaduca," +
      "               plv.m_pricelist_version_id AS version_tarifa, " +
      "               plv.m_pricelist_version_id AS idv," +
      "               coalesce(plv.em_ldt_dias_libres,0) as dias_libres," +
      "               pl.c_currency_id as moneda" +
      "        FROM m_pricelist pl " +
      "             INNER JOIN m_pricelist_version plv ON (pl.m_pricelist_id = plv.m_pricelist_id AND (plv.em_ldt_contrato_puerto_id IS NULL OR plv.em_ldt_contrato_puerto_id = ''))" +
      "        WHERE pl.em_ldt_puerto_origen_id = ?" +
      "              AND pl.em_ldt_puerto_destino_id= ?" +
      "              AND pl.em_ldt_transportista_id= ?" +
      "              AND pl.issopricelist = 'Y') AS T" +
      "        WHERE T.idv =  (SELECT pl.m_pricelist_version_id" +
      "                        FROM m_pricelist_version pl " +
      "                        WHERE pl.m_pricelist_id = T.m_pricelist_id" +
      "                        AND pl.validfrom <= now()" +
      "                        AND (pl.em_ldt_contrato_puerto_id IS NULL OR pl.em_ldt_contrato_puerto_id = '')" +
      "                        ORDER BY pl.validfrom DESC ,pl.created DESC LIMIT 1) ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, inppuertoTerminalOrigenId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, inppuertoTerminalDestinoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, inpcBpartnerMtId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "m_pricelist_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
