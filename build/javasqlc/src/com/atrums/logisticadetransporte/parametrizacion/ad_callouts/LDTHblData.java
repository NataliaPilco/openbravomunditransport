//Sqlc generated V1.O00-1
package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class LDTHblData implements FieldProvider {
static Logger log4j = Logger.getLogger(LDTHblData.class);
  private String InitRecordNumber="0";
  public String idhbl;
  public String sucursal;
  public String activo;
  public String processed;
  public String numhbl;
  public String routingOrder;
  public String cliente;
  public String valorFlete;
  public String estado;
  public String cartas;
  public String numEmbarque;
  public String vendedor;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("idhbl"))
      return idhbl;
    else if (fieldName.equalsIgnoreCase("sucursal"))
      return sucursal;
    else if (fieldName.equalsIgnoreCase("activo"))
      return activo;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("numhbl"))
      return numhbl;
    else if (fieldName.equalsIgnoreCase("routing_order") || fieldName.equals("routingOrder"))
      return routingOrder;
    else if (fieldName.equalsIgnoreCase("cliente"))
      return cliente;
    else if (fieldName.equalsIgnoreCase("valor_flete") || fieldName.equals("valorFlete"))
      return valorFlete;
    else if (fieldName.equalsIgnoreCase("estado"))
      return estado;
    else if (fieldName.equalsIgnoreCase("cartas"))
      return cartas;
    else if (fieldName.equalsIgnoreCase("num_embarque") || fieldName.equals("numEmbarque"))
      return numEmbarque;
    else if (fieldName.equalsIgnoreCase("vendedor"))
      return vendedor;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static LDTHblData[] selectHbl(ConnectionProvider connectionProvider, String ldtHblId)    throws ServletException {
    return selectHbl(connectionProvider, ldtHblId, 0, 0);
  }

  public static LDTHblData[] selectHbl(ConnectionProvider connectionProvider, String ldtHblId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		 SELECT H.LDT_HBL_ID AS IDHBL," +
      "		        H.AD_ORG_ID AS SUCURSAL," +
      "		        H.ISACTIVE AS ACTIVO," +
      "		        H.PROCESSED AS PROCESSED," +
      "		        H.NUM_HBL AS NUMHBL," +
      "		        H.LDT_ROUTING_ORDER_ID AS ROUTING_ORDER," +
      "		        H.C_BPARTNER_ID AS CLIENTE," +
      "		        H.VALOR_FLETE AS VALOR_FLETE," +
      "		        H.ESTADO AS ESTADO," +
      "		        H.CARTAS AS CARTAS," +
      "                (SELECT RO.NUMEMBARQUE FROM LDT_ROUTING_ORDER RO WHERE RO.LDT_ROUTING_ORDER_ID = H.LDT_ROUTING_ORDER_ID) AS NUM_EMBARQUE," +
      "                (SELECT RO.vendedor_id FROM LDT_ROUTING_ORDER RO WHERE RO.LDT_ROUTING_ORDER_ID = H.LDT_ROUTING_ORDER_ID) AS vendedor" +
      "		   FROM LDT_HBL H" +
      "		  WHERE LDT_HBL_ID = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtHblId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LDTHblData objectLDTHblData = new LDTHblData();
        objectLDTHblData.idhbl = UtilSql.getValue(result, "idhbl");
        objectLDTHblData.sucursal = UtilSql.getValue(result, "sucursal");
        objectLDTHblData.activo = UtilSql.getValue(result, "activo");
        objectLDTHblData.processed = UtilSql.getValue(result, "processed");
        objectLDTHblData.numhbl = UtilSql.getValue(result, "numhbl");
        objectLDTHblData.routingOrder = UtilSql.getValue(result, "routing_order");
        objectLDTHblData.cliente = UtilSql.getValue(result, "cliente");
        objectLDTHblData.valorFlete = UtilSql.getValue(result, "valor_flete");
        objectLDTHblData.estado = UtilSql.getValue(result, "estado");
        objectLDTHblData.cartas = UtilSql.getValue(result, "cartas");
        objectLDTHblData.numEmbarque = UtilSql.getValue(result, "num_embarque");
        objectLDTHblData.vendedor = UtilSql.getValue(result, "vendedor");
        objectLDTHblData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLDTHblData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LDTHblData objectLDTHblData[] = new LDTHblData[vector.size()];
    vector.copyInto(objectLDTHblData);
    return(objectLDTHblData);
  }
}
