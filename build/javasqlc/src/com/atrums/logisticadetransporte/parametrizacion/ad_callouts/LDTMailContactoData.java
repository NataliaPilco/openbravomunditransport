//Sqlc generated V1.O00-1
package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class LDTMailContactoData implements FieldProvider {
static Logger log4j = Logger.getLogger(LDTMailContactoData.class);
  private String InitRecordNumber="0";
  public String email;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("email"))
      return email;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static LDTMailContactoData[] selectMail(ConnectionProvider connectionProvider, String adUserId)    throws ServletException {
    return selectMail(connectionProvider, adUserId, 0, 0);
  }

  public static LDTMailContactoData[] selectMail(ConnectionProvider connectionProvider, String adUserId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		 SELECT AD_USER.EMAIL" +
      "		   FROM AD_USER" +
      "		  WHERE AD_USER.AD_USER_ID = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LDTMailContactoData objectLDTMailContactoData = new LDTMailContactoData();
        objectLDTMailContactoData.email = UtilSql.getValue(result, "email");
        objectLDTMailContactoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLDTMailContactoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LDTMailContactoData objectLDTMailContactoData[] = new LDTMailContactoData[vector.size()];
    vector.copyInto(objectLDTMailContactoData);
    return(objectLDTMailContactoData);
  }
}
