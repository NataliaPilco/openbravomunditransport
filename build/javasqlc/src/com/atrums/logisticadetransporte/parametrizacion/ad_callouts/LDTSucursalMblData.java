//Sqlc generated V1.O00-1
package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class LDTSucursalMblData implements FieldProvider {
static Logger log4j = Logger.getLogger(LDTSucursalMblData.class);
  private String InitRecordNumber="0";
  public String sucursal;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("sucursal"))
      return sucursal;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static LDTSucursalMblData[] selectSucursal(ConnectionProvider connectionProvider, String ldtMblId)    throws ServletException {
    return selectSucursal(connectionProvider, ldtMblId, 0, 0);
  }

  public static LDTSucursalMblData[] selectSucursal(ConnectionProvider connectionProvider, String ldtMblId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		 SELECT M.AD_ORG_ID AS SUCURSAL" +
      "		   FROM LDT_MBL M" +
      "		  WHERE LDT_MBL_ID = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtMblId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LDTSucursalMblData objectLDTSucursalMblData = new LDTSucursalMblData();
        objectLDTSucursalMblData.sucursal = UtilSql.getValue(result, "sucursal");
        objectLDTSucursalMblData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLDTSucursalMblData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LDTSucursalMblData objectLDTSucursalMblData[] = new LDTSucursalMblData[vector.size()];
    vector.copyInto(objectLDTSucursalMblData);
    return(objectLDTSucursalMblData);
  }

  public static String cantidadContenedores(ConnectionProvider connectionProvider, String ldtMblId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "         SELECT COUNT(*) as cantidad" +
      "           FROM LDT_HBL_LINEA " +
      "          WHERE LDT_HBL_ID IN (SELECT LDT_HBL_ID " +
      "                                 FROM LDT_MBL_LINEAS " +
      "                                WHERE LDT_MBL_ID = ?)";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtMblId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "cantidad");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
