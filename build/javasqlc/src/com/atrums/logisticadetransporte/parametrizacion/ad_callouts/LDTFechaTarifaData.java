//Sqlc generated V1.O00-1
package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class LDTFechaTarifaData implements FieldProvider {
static Logger log4j = Logger.getLogger(LDTFechaTarifaData.class);
  private String InitRecordNumber="0";
  public String mPricelistId;
  public String fechacaduca;
  public String versionTarifa;
  public String emLdtContratoPuertoId;
  public String diasLibres;
  public String moneda;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("m_pricelist_id") || fieldName.equals("mPricelistId"))
      return mPricelistId;
    else if (fieldName.equalsIgnoreCase("fechacaduca"))
      return fechacaduca;
    else if (fieldName.equalsIgnoreCase("version_tarifa") || fieldName.equals("versionTarifa"))
      return versionTarifa;
    else if (fieldName.equalsIgnoreCase("em_ldt_contrato_puerto_id") || fieldName.equals("emLdtContratoPuertoId"))
      return emLdtContratoPuertoId;
    else if (fieldName.equalsIgnoreCase("dias_libres") || fieldName.equals("diasLibres"))
      return diasLibres;
    else if (fieldName.equalsIgnoreCase("moneda"))
      return moneda;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static LDTFechaTarifaData[] selectFechaTarifa(ConnectionProvider connectionProvider, String mPuertoOrigenId, String mPuertoDestinoId, String mTransportistaId)    throws ServletException {
    return selectFechaTarifa(connectionProvider, mPuertoOrigenId, mPuertoDestinoId, mTransportistaId, 0, 0);
  }

  public static LDTFechaTarifaData[] selectFechaTarifa(ConnectionProvider connectionProvider, String mPuertoOrigenId, String mPuertoDestinoId, String mTransportistaId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT T.m_pricelist_id, T.fechaCaduca, T.version_tarifa, '' as em_ldt_contrato_puerto_id," +
      "        T.dias_libres, T.moneda" +
      "        FROM (SELECT pl.m_pricelist_id," +
      "               to_char(plv.EM_LDT_FECHA_CADUCA,'dd-MM-yyyy') AS fechaCaduca," +
      "               plv.m_pricelist_version_id AS version_tarifa, " +
      "               plv.m_pricelist_version_id AS idv," +
      "               coalesce(plv.em_ldt_dias_libres,0) as dias_libres," +
      "               pl.c_currency_id as moneda" +
      "        FROM m_pricelist pl " +
      "             INNER JOIN m_pricelist_version plv ON (pl.m_pricelist_id = plv.m_pricelist_id AND (plv.em_ldt_contrato_puerto_id IS NULL OR plv.em_ldt_contrato_puerto_id = ''))" +
      "        WHERE pl.em_ldt_puerto_origen_id = ?" +
      "              AND pl.em_ldt_puerto_destino_id= ?" +
      "              AND pl.em_ldt_transportista_id= ?" +
      "              AND pl.issopricelist = 'Y') AS T" +
      "        WHERE T.idv =  (SELECT pl.m_pricelist_version_id" +
      "                        FROM m_pricelist_version pl " +
      "                        WHERE pl.m_pricelist_id = T.m_pricelist_id" +
      "                        AND pl.validfrom <= now()" +
      "                        AND (pl.em_ldt_contrato_puerto_id IS NULL OR pl.em_ldt_contrato_puerto_id = '')" +
      "                        ORDER BY pl.validfrom DESC ,pl.created DESC LIMIT 1) ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPuertoOrigenId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPuertoDestinoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mTransportistaId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LDTFechaTarifaData objectLDTFechaTarifaData = new LDTFechaTarifaData();
        objectLDTFechaTarifaData.mPricelistId = UtilSql.getValue(result, "m_pricelist_id");
        objectLDTFechaTarifaData.fechacaduca = UtilSql.getValue(result, "fechacaduca");
        objectLDTFechaTarifaData.versionTarifa = UtilSql.getValue(result, "version_tarifa");
        objectLDTFechaTarifaData.emLdtContratoPuertoId = UtilSql.getValue(result, "em_ldt_contrato_puerto_id");
        objectLDTFechaTarifaData.diasLibres = UtilSql.getValue(result, "dias_libres");
        objectLDTFechaTarifaData.moneda = UtilSql.getValue(result, "moneda");
        objectLDTFechaTarifaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLDTFechaTarifaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LDTFechaTarifaData objectLDTFechaTarifaData[] = new LDTFechaTarifaData[vector.size()];
    vector.copyInto(objectLDTFechaTarifaData);
    return(objectLDTFechaTarifaData);
  }

  public static LDTFechaTarifaData[] selectFechaTarifaContrato(ConnectionProvider connectionProvider, String mPuertoOrigenId, String mPuertoDestinoId, String mTransportistaId, String mEmLdtContratoPuertoId)    throws ServletException {
    return selectFechaTarifaContrato(connectionProvider, mPuertoOrigenId, mPuertoDestinoId, mTransportistaId, mEmLdtContratoPuertoId, 0, 0);
  }

  public static LDTFechaTarifaData[] selectFechaTarifaContrato(ConnectionProvider connectionProvider, String mPuertoOrigenId, String mPuertoDestinoId, String mTransportistaId, String mEmLdtContratoPuertoId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT T.m_pricelist_id, T.fechaCaduca, T.version_tarifa, '' as em_ldt_contrato_puerto_id," +
      "               T.dias_libres, T.moneda" +
      "        FROM (SELECT pl.m_pricelist_id," +
      "               to_char(plv.EM_LDT_FECHA_CADUCA,'dd-MM-yyyy') AS fechaCaduca," +
      "               plv.m_pricelist_version_id AS version_tarifa," +
      "               plv.m_pricelist_version_id AS idv," +
      "               coalesce(plv.em_ldt_dias_libres,0) as dias_libres," +
      "               pl.c_currency_id as moneda" +
      "        FROM m_pricelist pl" +
      "            INNER JOIN m_pricelist_version plv ON (pl.m_pricelist_id = plv.m_pricelist_id)" +
      "        WHERE pl.em_ldt_puerto_origen_id = ?" +
      "              AND pl.em_ldt_puerto_destino_id= ?" +
      "              AND pl.em_ldt_transportista_id= ?" +
      "              AND pl.issopricelist = 'Y'" +
      "              AND plv.em_ldt_contrato_puerto_id = ?) AS T" +
      "        WHERE T.idv = (SELECT pl.m_pricelist_version_id  " +
      "                        FROM m_pricelist_version pl " +
      "                        WHERE pl.m_pricelist_id = T.m_pricelist_id" +
      "                        AND pl.validfrom <= now()" +
      "                        AND pl.em_ldt_contrato_puerto_id = ?" +
      "                        ORDER BY pl.validfrom DESC ,pl.created DESC LIMIT 1)";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPuertoOrigenId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPuertoDestinoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mTransportistaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mEmLdtContratoPuertoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mEmLdtContratoPuertoId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LDTFechaTarifaData objectLDTFechaTarifaData = new LDTFechaTarifaData();
        objectLDTFechaTarifaData.mPricelistId = UtilSql.getValue(result, "m_pricelist_id");
        objectLDTFechaTarifaData.fechacaduca = UtilSql.getValue(result, "fechacaduca");
        objectLDTFechaTarifaData.versionTarifa = UtilSql.getValue(result, "version_tarifa");
        objectLDTFechaTarifaData.emLdtContratoPuertoId = UtilSql.getValue(result, "em_ldt_contrato_puerto_id");
        objectLDTFechaTarifaData.diasLibres = UtilSql.getValue(result, "dias_libres");
        objectLDTFechaTarifaData.moneda = UtilSql.getValue(result, "moneda");
        objectLDTFechaTarifaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLDTFechaTarifaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LDTFechaTarifaData objectLDTFechaTarifaData[] = new LDTFechaTarifaData[vector.size()];
    vector.copyInto(objectLDTFechaTarifaData);
    return(objectLDTFechaTarifaData);
  }

  public static LDTFechaTarifaData[] selectFechaContrato(ConnectionProvider connectionProvider, String mVersionTarifaId)    throws ServletException {
    return selectFechaContrato(connectionProvider, mVersionTarifaId, 0, 0);
  }

  public static LDTFechaTarifaData[] selectFechaContrato(ConnectionProvider connectionProvider, String mVersionTarifaId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT plv.m_pricelist_id, " +
      "               plv.em_ldt_fecha_caduca as fechaCaduca, " +
      "               plv.m_pricelist_version_id as version_tarifa," +
      "               plv.em_ldt_contrato_puerto_id as em_ldt_contrato_puerto_id," +
      "               coalesce(plv.em_ldt_dias_libres,0) as dias_libres," +
      "               (select pl.c_currency_id " +
      "                  from m_pricelist pl " +
      "                 where pl.m_pricelist_id = plv.m_pricelist_id) as moneda" +
      "          FROM m_pricelist_version plv" +
      "         WHERE plv.m_pricelist_version_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mVersionTarifaId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LDTFechaTarifaData objectLDTFechaTarifaData = new LDTFechaTarifaData();
        objectLDTFechaTarifaData.mPricelistId = UtilSql.getValue(result, "m_pricelist_id");
        objectLDTFechaTarifaData.fechacaduca = UtilSql.getDateValue(result, "fechacaduca", "dd-MM-yyyy");
        objectLDTFechaTarifaData.versionTarifa = UtilSql.getValue(result, "version_tarifa");
        objectLDTFechaTarifaData.emLdtContratoPuertoId = UtilSql.getValue(result, "em_ldt_contrato_puerto_id");
        objectLDTFechaTarifaData.diasLibres = UtilSql.getValue(result, "dias_libres");
        objectLDTFechaTarifaData.moneda = UtilSql.getValue(result, "moneda");
        objectLDTFechaTarifaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLDTFechaTarifaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LDTFechaTarifaData objectLDTFechaTarifaData[] = new LDTFechaTarifaData[vector.size()];
    vector.copyInto(objectLDTFechaTarifaData);
    return(objectLDTFechaTarifaData);
  }
}
