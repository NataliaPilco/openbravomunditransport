//Sqlc generated V1.O00-1
package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;

class LDTSecuencialRoData implements FieldProvider {
static Logger log4j = Logger.getLogger(LDTSecuencialRoData.class);
  private String InitRecordNumber="0";
  public String secuencial;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("secuencial"))
      return secuencial;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static String selectSecuencial(ConnectionProvider connectionProvider, String vendedorId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		 SELECT" +
      "       (CASE WHEN DOC IS NULL THEN" +
      "                SIG || '-' || '00001' ||'/RO'" +
      "            ELSE" +
      "                (CASE  WHEN CHAR_LENGTH(SIG) = 4 THEN " +
      "                    REPLACE((SIG || '-' || (SELECT TO_CHAR((TO_NUMBER((SUBSTRING(MAX(RO.DOCUMENTNO),6,6)),'99999') + 1), '09999') AS NUM FROM LDT_ROUTING_ORDER RO WHERE SUBSTRING(RO.DOCUMENTNO,0,5) = SIG AND RO.VENDEDOR_ID = ?) || '/RO'),' ', '')" +
      "                       WHEN CHAR_LENGTH(SIG) = 3 THEN " +
      "                    REPLACE((SIG || '-' || (SELECT TO_CHAR((TO_NUMBER((SUBSTRING(MAX(RO.DOCUMENTNO),5,6)),'99999') + 1), '09999') AS NUM FROM LDT_ROUTING_ORDER RO WHERE SUBSTRING(RO.DOCUMENTNO,0,4) = SIG AND RO.VENDEDOR_ID = ?) || '/RO'),' ', '')" +
      "                       WHEN CHAR_LENGTH(SIG) = 2 THEN" +
      "                    REPLACE((SIG || '-' || (SELECT TO_CHAR((TO_NUMBER((SUBSTRING(MAX(RO.DOCUMENTNO),4,6)),'99999') + 1), '09999') AS NUM FROM LDT_ROUTING_ORDER RO WHERE SUBSTRING(RO.DOCUMENTNO,0,3) = SIG AND RO.VENDEDOR_ID = ?) || '/RO'),' ', '')" +
      "                       WHEN CHAR_LENGTH(SIG) = 1 THEN" +
      "                    REPLACE((SIG || '-' || (SELECT TO_CHAR((TO_NUMBER((SUBSTRING(MAX(RO.DOCUMENTNO),3,6)),'99999') + 1), '09999') AS NUM FROM LDT_ROUTING_ORDER RO WHERE SUBSTRING(RO.DOCUMENTNO,0,2) = SIG AND RO.VENDEDOR_ID = ?) || '/RO'),' ', '')" +
      "                 END)" +
      "         END) AS SECUENCIAL" +
      "        FROM" +
      "        (" +
      "              SELECT SIG," +
      "                     (CASE  WHEN CHAR_LENGTH(SIG) = 4 THEN " +
      "                                 (SELECT MAX(RO.DOCUMENTNO) FROM LDT_ROUTING_ORDER RO WHERE SUBSTRING(RO.DOCUMENTNO,0,5) = SIG AND RO.VENDEDOR_ID = ?)" +
      "                            WHEN CHAR_LENGTH(SIG) = 3 THEN " +
      "                                 (SELECT MAX(RO.DOCUMENTNO) FROM LDT_ROUTING_ORDER RO WHERE SUBSTRING(RO.DOCUMENTNO,0,4) = SIG AND RO.VENDEDOR_ID = ?)" +
      "                            WHEN CHAR_LENGTH(SIG) = 2 THEN" +
      "                                 (SELECT MAX(RO.DOCUMENTNO) FROM LDT_ROUTING_ORDER RO WHERE SUBSTRING(RO.DOCUMENTNO,0,3) = SIG AND RO.VENDEDOR_ID = ?)" +
      "                            WHEN CHAR_LENGTH(SIG) = 1 THEN" +
      "                                 (SELECT MAX(RO.DOCUMENTNO) FROM LDT_ROUTING_ORDER RO WHERE SUBSTRING(RO.DOCUMENTNO,0,2) = SIG AND RO.VENDEDOR_ID = ?)" +
      "                       END" +
      "                      ) AS DOC" +
      "              FROM" +
      "                    (" +
      "                        SELECT BP.EM_LDT_SIGLAS AS SIG" +
      "                          FROM C_BPARTNER BP" +
      "                         WHERE BP.C_BPARTNER_ID  = ?  " +
      "                    ) AS SC" +
      "        ) AS SEC";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vendedorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vendedorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vendedorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vendedorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vendedorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vendedorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vendedorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vendedorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vendedorId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "secuencial");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
