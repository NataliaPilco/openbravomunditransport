//Sqlc generated V1.O00-1
package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class LDTPrecioProductoCoData implements FieldProvider {
static Logger log4j = Logger.getLogger(LDTPrecioProductoCoData.class);
  private String InitRecordNumber="0";
  public String precio;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("precio"))
      return precio;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static LDTPrecioProductoCoData[] selectMinimo(ConnectionProvider connectionProvider, String mProductCoId)    throws ServletException {
    return selectMinimo(connectionProvider, mProductCoId, 0, 0);
  }

  public static LDTPrecioProductoCoData[] selectMinimo(ConnectionProvider connectionProvider, String mProductCoId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		 SELECT PR.EM_LDT_VALOR_MINIMO AS PRECIO" +
      "		   FROM M_PRODUCT  PR" +
      "		  WHERE PR.M_PRODUCT_ID    = ?		    ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductCoId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LDTPrecioProductoCoData objectLDTPrecioProductoCoData = new LDTPrecioProductoCoData();
        objectLDTPrecioProductoCoData.precio = UtilSql.getValue(result, "precio");
        objectLDTPrecioProductoCoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLDTPrecioProductoCoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LDTPrecioProductoCoData objectLDTPrecioProductoCoData[] = new LDTPrecioProductoCoData[vector.size()];
    vector.copyInto(objectLDTPrecioProductoCoData);
    return(objectLDTPrecioProductoCoData);
  }

  public static LDTPrecioProductoCoData[] selectPrecio(ConnectionProvider connectionProvider, String mProductCoId, String mPricelistVersionId)    throws ServletException {
    return selectPrecio(connectionProvider, mProductCoId, mPricelistVersionId, 0, 0);
  }

  public static LDTPrecioProductoCoData[] selectPrecio(ConnectionProvider connectionProvider, String mProductCoId, String mPricelistVersionId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "         SELECT COALESCE(MP.PRICELIST,0) AS PRECIO" +
      "           FROM M_PRODUCT_PRICE_WAREHOUSE_V MP," +
      "                m_productprice pp, " +
      "                m_pricelist_version pv" +
      "          WHERE pp.m_productprice_id = mp.m_productprice_id" +
      "            AND pp.m_pricelist_version_id = pv.m_pricelist_version_id" +
      "            AND MP.M_PRODUCT_ID    = ?" +
      "            AND pv.m_pricelist_version_id  = ?       ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductCoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistVersionId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LDTPrecioProductoCoData objectLDTPrecioProductoCoData = new LDTPrecioProductoCoData();
        objectLDTPrecioProductoCoData.precio = UtilSql.getValue(result, "precio");
        objectLDTPrecioProductoCoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLDTPrecioProductoCoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LDTPrecioProductoCoData objectLDTPrecioProductoCoData[] = new LDTPrecioProductoCoData[vector.size()];
    vector.copyInto(objectLDTPrecioProductoCoData);
    return(objectLDTPrecioProductoCoData);
  }
}
