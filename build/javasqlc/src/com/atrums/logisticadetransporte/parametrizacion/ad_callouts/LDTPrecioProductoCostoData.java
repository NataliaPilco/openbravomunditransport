//Sqlc generated V1.O00-1
package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class LDTPrecioProductoCostoData implements FieldProvider {
static Logger log4j = Logger.getLogger(LDTPrecioProductoCostoData.class);
  private String InitRecordNumber="0";
  public String precio;
  public String precioEst;
  public String idprecio;
  public String warehouse;
  public String minimo;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("precio"))
      return precio;
    else if (fieldName.equalsIgnoreCase("precio_est") || fieldName.equals("precioEst"))
      return precioEst;
    else if (fieldName.equalsIgnoreCase("idprecio"))
      return idprecio;
    else if (fieldName.equalsIgnoreCase("warehouse"))
      return warehouse;
    else if (fieldName.equalsIgnoreCase("minimo"))
      return minimo;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static LDTPrecioProductoCostoData[] selectPrecio(ConnectionProvider connectionProvider, String mProductId, String mPricelistVersionId)    throws ServletException {
    return selectPrecio(connectionProvider, mProductId, mPricelistVersionId, 0, 0);
  }

  public static LDTPrecioProductoCostoData[] selectPrecio(ConnectionProvider connectionProvider, String mProductId, String mPricelistVersionId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "         SELECT COALESCE(MP.PRICELIST,0) AS PRECIO, " +
      "                COALESCE(MP.PRICESTD,0) AS PRECIO_EST," +
      "                MP.M_PRODUCTPRICE_ID AS IDPRECIO," +
      "                MP.M_WAREHOUSE_ID AS WAREHOUSE," +
      "                (SELECT P.EM_LDT_VALOR_MINIMO FROM M_PRODUCT P WHERE P.M_PRODUCT_ID = MP.M_PRODUCT_ID) AS MINIMO" +
      "           FROM M_PRODUCT_PRICE_WAREHOUSE_V MP," +
      "                m_productprice pp, " +
      "                m_pricelist_version pv" +
      "          WHERE pp.m_productprice_id = mp.m_productprice_id" +
      "            AND pp.m_pricelist_version_id = pv.m_pricelist_version_id" +
      "            AND MP.M_PRODUCT_ID    = ?" +
      "            AND pv.m_pricelist_version_id  = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistVersionId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LDTPrecioProductoCostoData objectLDTPrecioProductoCostoData = new LDTPrecioProductoCostoData();
        objectLDTPrecioProductoCostoData.precio = UtilSql.getValue(result, "precio");
        objectLDTPrecioProductoCostoData.precioEst = UtilSql.getValue(result, "precio_est");
        objectLDTPrecioProductoCostoData.idprecio = UtilSql.getValue(result, "idprecio");
        objectLDTPrecioProductoCostoData.warehouse = UtilSql.getValue(result, "warehouse");
        objectLDTPrecioProductoCostoData.minimo = UtilSql.getValue(result, "minimo");
        objectLDTPrecioProductoCostoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLDTPrecioProductoCostoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LDTPrecioProductoCostoData objectLDTPrecioProductoCostoData[] = new LDTPrecioProductoCostoData[vector.size()];
    vector.copyInto(objectLDTPrecioProductoCostoData);
    return(objectLDTPrecioProductoCostoData);
  }
}
