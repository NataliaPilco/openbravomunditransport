//Sqlc generated V1.O00-1
package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class LDTCotizacionData implements FieldProvider {
static Logger log4j = Logger.getLogger(LDTCotizacionData.class);
  private String InitRecordNumber="0";
  public String vendedor;
  public String numcot;
  public String fecha;
  public String cliente;
  public String dircliente;
  public String servicioCliente;
  public String descripcion;
  public String modalidad;
  public String tipo;
  public String pruetoorigen;
  public String pruetodestino;
  public String incoterms;
  public String naviera;
  public String tarifa;
  public String versionTarifa;
  public String nomPuertoOrg;
  public String venta;
  public String negociado;
  public String costosLocales;
  public String totalVenta;
  public String totalNegociado;
  public String esFechas;
  public String tarifaCompra;
  public String agente;
  public String cod;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("vendedor"))
      return vendedor;
    else if (fieldName.equalsIgnoreCase("numcot"))
      return numcot;
    else if (fieldName.equalsIgnoreCase("fecha"))
      return fecha;
    else if (fieldName.equalsIgnoreCase("cliente"))
      return cliente;
    else if (fieldName.equalsIgnoreCase("dircliente"))
      return dircliente;
    else if (fieldName.equalsIgnoreCase("servicio_cliente") || fieldName.equals("servicioCliente"))
      return servicioCliente;
    else if (fieldName.equalsIgnoreCase("descripcion"))
      return descripcion;
    else if (fieldName.equalsIgnoreCase("modalidad"))
      return modalidad;
    else if (fieldName.equalsIgnoreCase("tipo"))
      return tipo;
    else if (fieldName.equalsIgnoreCase("pruetoorigen"))
      return pruetoorigen;
    else if (fieldName.equalsIgnoreCase("pruetodestino"))
      return pruetodestino;
    else if (fieldName.equalsIgnoreCase("incoterms"))
      return incoterms;
    else if (fieldName.equalsIgnoreCase("naviera"))
      return naviera;
    else if (fieldName.equalsIgnoreCase("tarifa"))
      return tarifa;
    else if (fieldName.equalsIgnoreCase("version_tarifa") || fieldName.equals("versionTarifa"))
      return versionTarifa;
    else if (fieldName.equalsIgnoreCase("nom_puerto_org") || fieldName.equals("nomPuertoOrg"))
      return nomPuertoOrg;
    else if (fieldName.equalsIgnoreCase("venta"))
      return venta;
    else if (fieldName.equalsIgnoreCase("negociado"))
      return negociado;
    else if (fieldName.equalsIgnoreCase("costos_locales") || fieldName.equals("costosLocales"))
      return costosLocales;
    else if (fieldName.equalsIgnoreCase("total_venta") || fieldName.equals("totalVenta"))
      return totalVenta;
    else if (fieldName.equalsIgnoreCase("total_negociado") || fieldName.equals("totalNegociado"))
      return totalNegociado;
    else if (fieldName.equalsIgnoreCase("es_fechas") || fieldName.equals("esFechas"))
      return esFechas;
    else if (fieldName.equalsIgnoreCase("tarifa_compra") || fieldName.equals("tarifaCompra"))
      return tarifaCompra;
    else if (fieldName.equalsIgnoreCase("agente"))
      return agente;
    else if (fieldName.equalsIgnoreCase("cod"))
      return cod;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static LDTCotizacionData[] selectCotizacion(ConnectionProvider connectionProvider, String ldtCotizacionId)    throws ServletException {
    return selectCotizacion(connectionProvider, ldtCotizacionId, 0, 0);
  }

  public static LDTCotizacionData[] selectCotizacion(ConnectionProvider connectionProvider, String ldtCotizacionId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "                select vendedor, numcot, fecha, cliente, dircliente, servicio_cliente, descripcion," +
      "                        modalidad, tipo, pruetoorigen, pruetodestino, incoterms, naviera, tarifa, version_tarifa," +
      "                        nom_puerto_org,venta, negociado, costos_locales, total_venta, total_negociado," +
      "                        ldt_verifica_fechas_version(tarifa, version_tarifa) as es_fechas, tarifa_compra," +
      "                        agente, cod" +
      "                 from(" +
      "                 select c.vendedor_id as vendedor," +
      "                        c.documentno as numcot," +
      "                        c.fecha as fecha," +
      "                        c.c_bpartner_id as cliente," +
      "                        c.c_bpartner_location_id as dircliente," +
      "                        c.servicio_cliente_id as servicio_cliente," +
      "                        c.descripcion as descripcion," +
      "                        c.modalidad as modalidad," +
      "                        c.tipo as tipo," +
      "                        cn.puerto_terminal_origen_id as pruetoorigen," +
      "                        cn.puerto_terminal_destino_id as pruetodestino," +
      "                        c.c_incoterms_id as incoterms," +
      "                        cn.c_bpartner_id as naviera," +
      "                        cn.m_pricelist_id as tarifa," +
      "                        (select np.m_pricelist_version_id " +
      "                           from ldt_naviera_productos np " +
      "                          where np.ldt_cotizacion_naviera_id = cn.ldt_cotizacion_naviera_id " +
      "                          limit 1) as version_tarifa," +
      "                        (select UPPER(split_part(pt.name2,'-',2))||','|| UPPER(split_part(pt.name2,'-',1)) ||','|| UPPER(split_part(pt.name2,'-',3)) from ldt_puerto_terminal pt where pt.ldt_puerto_terminal_id = cn.puerto_terminal_origen_id) as nom_puerto_org," +
      "                        cn.total_opcion as venta," +
      "                        cn.total_negociado as negociado," +
      "                        cn.grand_total as costos_locales," +
      "                        cn.total_venta as total_venta," +
      "                        cn.total as total_negociado," +
      "                        cn.total_opcion_eur as venta_eur," +
      "                        cn.total_negociado_eur as negociado_eur," +
      "                        cn.total_costos_eur as costos_locales_eur," +
      "                        cn.total_venta_eur as total_venta_eur," +
      "                        cn.total_eur as total_negociado_eur," +
      "                        (select pl.m_pricelist_id" +
      "                           from m_pricelist pl " +
      "                          where pl.em_ldt_puerto_origen_id= cn.puerto_terminal_origen_id" +
      "                            and pl.em_ldt_puerto_destino_id = cn.puerto_terminal_destino_id" +
      "                            and pl.em_ldt_transportista_id = cn.c_bpartner_id" +
      "                            and pl.issopricelist = 'N') as tarifa_compra," +
      "                         (select np.c_bpartner_co_id from ldt_naviera_productos np where np.ldt_cotizacion_naviera_id = cn.ldt_cotizacion_naviera_id and np.c_bpartner_co_id is not null limit 1) as agente," +
      "                         (select bp.em_ldt_cod from c_bpartner bp where bp.c_bpartner_id = c.c_bpartner_id) as cod" +
      "                   from ldt_cotizacion c, ldt_cotizacion_naviera cn" +
      "                  where cn.ldt_cotizacion_id = c.ldt_cotizacion_id" +
      "                    and cn.isactive = 'Y'" +
      "                    and c.ldt_cotizacion_id = ?" +
      "        ) AS COTIZACION";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtCotizacionId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LDTCotizacionData objectLDTCotizacionData = new LDTCotizacionData();
        objectLDTCotizacionData.vendedor = UtilSql.getValue(result, "vendedor");
        objectLDTCotizacionData.numcot = UtilSql.getValue(result, "numcot");
        objectLDTCotizacionData.fecha = UtilSql.getDateValue(result, "fecha", "dd-MM-yyyy");
        objectLDTCotizacionData.cliente = UtilSql.getValue(result, "cliente");
        objectLDTCotizacionData.dircliente = UtilSql.getValue(result, "dircliente");
        objectLDTCotizacionData.servicioCliente = UtilSql.getValue(result, "servicio_cliente");
        objectLDTCotizacionData.descripcion = UtilSql.getValue(result, "descripcion");
        objectLDTCotizacionData.modalidad = UtilSql.getValue(result, "modalidad");
        objectLDTCotizacionData.tipo = UtilSql.getValue(result, "tipo");
        objectLDTCotizacionData.pruetoorigen = UtilSql.getValue(result, "pruetoorigen");
        objectLDTCotizacionData.pruetodestino = UtilSql.getValue(result, "pruetodestino");
        objectLDTCotizacionData.incoterms = UtilSql.getValue(result, "incoterms");
        objectLDTCotizacionData.naviera = UtilSql.getValue(result, "naviera");
        objectLDTCotizacionData.tarifa = UtilSql.getValue(result, "tarifa");
        objectLDTCotizacionData.versionTarifa = UtilSql.getValue(result, "version_tarifa");
        objectLDTCotizacionData.nomPuertoOrg = UtilSql.getValue(result, "nom_puerto_org");
        objectLDTCotizacionData.venta = UtilSql.getValue(result, "venta");
        objectLDTCotizacionData.negociado = UtilSql.getValue(result, "negociado");
        objectLDTCotizacionData.costosLocales = UtilSql.getValue(result, "costos_locales");
        objectLDTCotizacionData.totalVenta = UtilSql.getValue(result, "total_venta");
        objectLDTCotizacionData.totalNegociado = UtilSql.getValue(result, "total_negociado");
        objectLDTCotizacionData.esFechas = UtilSql.getValue(result, "es_fechas");
        objectLDTCotizacionData.tarifaCompra = UtilSql.getValue(result, "tarifa_compra");
        objectLDTCotizacionData.agente = UtilSql.getValue(result, "agente");
        objectLDTCotizacionData.cod = UtilSql.getValue(result, "cod");
        objectLDTCotizacionData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLDTCotizacionData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LDTCotizacionData objectLDTCotizacionData[] = new LDTCotizacionData[vector.size()];
    vector.copyInto(objectLDTCotizacionData);
    return(objectLDTCotizacionData);
  }
}
