//Sqlc generated V1.O00-1
package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class LDTFleteHblData implements FieldProvider {
static Logger log4j = Logger.getLogger(LDTFleteHblData.class);
  private String InitRecordNumber="0";
  public String flete;
  public String fleteEur;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("flete"))
      return flete;
    else if (fieldName.equalsIgnoreCase("flete_eur") || fieldName.equals("fleteEur"))
      return fleteEur;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static LDTFleteHblData[] selectFlete(ConnectionProvider connectionProvider, String ldtRoutingOrderId)    throws ServletException {
    return selectFlete(connectionProvider, ldtRoutingOrderId, 0, 0);
  }

  public static LDTFleteHblData[] selectFlete(ConnectionProvider connectionProvider, String ldtRoutingOrderId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		 SELECT (CASE WHEN RO.LDT_COTIZACION_ID IS NOT NULL THEN " +
      "             (SELECT SUM(CASE WHEN NP.TOTAL_NEGOCIADO > COALESCE(NP.LDT_MINIMO,0)" +
      "                                                          THEN NP.TOTAL_NEGOCIADO" +
      "                                                          ELSE NP.LDT_MINIMO" +
      "                                                     END)" +
      "                                            FROM LDT_NAVIERA_PRODUCTOS NP, LDT_COTIZACION_NAVIERA CN, LDT_COTIZACION C" +
      "                                           WHERE C.LDT_COTIZACION_ID = RO.LDT_COTIZACION_ID" +
      "                                             AND CN.LDT_COTIZACION_ID = C.LDT_COTIZACION_ID" +
      "                                             AND NP.LDT_COTIZACION_NAVIERA_ID = CN.LDT_COTIZACION_NAVIERA_ID" +
      "                                             AND CN.ISACTIVE = 'Y'" +
      "                                             AND (NP.C_CURRENCY_ID = '100' OR NP.C_CURRENCY_ID IS NULL))" +
      "         ELSE" +
      "             (SELECT SUM(CASE WHEN NP.TOTAL_NEGOCIADO > COALESCE(NP.LDT_MINIMO,0)" +
      "                                                          THEN NP.TOTAL_NEGOCIADO" +
      "                                                          ELSE NP.LDT_MINIMO" +
      "                                                     END)" +
      "                                            FROM LDT_NAVIERA_PRODUCTOS NP" +
      "                                           WHERE NP.LDT_ROUTING_ORDER_ID = RO.LDT_ROUTING_ORDER_ID" +
      "                                             AND (NP.C_CURRENCY_ID = '100' OR NP.C_CURRENCY_ID IS NULL))" +
      "         END" +
      "         ) AS FLETE," +
      "         (CASE WHEN RO.LDT_COTIZACION_ID IS NOT NULL THEN " +
      "             (SELECT SUM(CASE WHEN NP.TOTAL_NEGOCIADO > COALESCE(NP.LDT_MINIMO,0)" +
      "                                                          THEN NP.TOTAL_NEGOCIADO" +
      "                                                          ELSE NP.LDT_MINIMO" +
      "                                                     END)" +
      "                                            FROM LDT_NAVIERA_PRODUCTOS NP, LDT_COTIZACION_NAVIERA CN, LDT_COTIZACION C" +
      "                                           WHERE C.LDT_COTIZACION_ID = RO.LDT_COTIZACION_ID" +
      "                                             AND CN.LDT_COTIZACION_ID = C.LDT_COTIZACION_ID" +
      "                                             AND NP.LDT_COTIZACION_NAVIERA_ID = CN.LDT_COTIZACION_NAVIERA_ID" +
      "                                             AND CN.ISACTIVE = 'Y'" +
      "                                             AND NP.C_CURRENCY_ID = '102')" +
      "            ELSE" +
      "                (SELECT SUM(CASE WHEN NP.TOTAL_NEGOCIADO > COALESCE(NP.LDT_MINIMO,0)" +
      "                                                              THEN NP.TOTAL_NEGOCIADO" +
      "                                                              ELSE NP.LDT_MINIMO" +
      "                                                         END)" +
      "                                                FROM LDT_NAVIERA_PRODUCTOS NP" +
      "                                               WHERE NP.LDT_ROUTING_ORDER_ID = RO.LDT_ROUTING_ORDER_ID" +
      "                                                 AND NP.C_CURRENCY_ID = '102')" +
      "            END" +
      "           ) AS FLETE_EUR   " +
      "          FROM LDT_ROUTING_ORDER RO" +
      "         WHERE RO.LDT_ROUTING_ORDER_ID = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LDTFleteHblData objectLDTFleteHblData = new LDTFleteHblData();
        objectLDTFleteHblData.flete = UtilSql.getValue(result, "flete");
        objectLDTFleteHblData.fleteEur = UtilSql.getValue(result, "flete_eur");
        objectLDTFleteHblData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLDTFleteHblData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LDTFleteHblData objectLDTFleteHblData[] = new LDTFleteHblData[vector.size()];
    vector.copyInto(objectLDTFleteHblData);
    return(objectLDTFleteHblData);
  }
}
