//Sqlc generated V1.O00-1
package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class LDTValorHandlingFeeData implements FieldProvider {
static Logger log4j = Logger.getLogger(LDTValorHandlingFeeData.class);
  private String InitRecordNumber="0";
  public String valor;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("valor"))
      return valor;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static LDTValorHandlingFeeData[] selectValor(ConnectionProvider connectionProvider, String ldtpreLiquidacionId)    throws ServletException {
    return selectValor(connectionProvider, ldtpreLiquidacionId, 0, 0);
  }

  public static LDTValorHandlingFeeData[] selectValor(ConnectionProvider connectionProvider, String ldtpreLiquidacionId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "         SELECT " +
      "              (CASE WHEN ldt_tipo = 'FCL' AND C_BPARTNER_ID NOT IN (SELECT C_BPARTNER_ID FROM C_BPARTNER WHERE UPPER(NAME) LIKE '%GAVA%') THEN" +
      "                        (COALESCE(cantidad,0) * 50)" +
      "                    WHEN ldt_tipo = 'FCL'  AND C_BPARTNER_ID IN (SELECT C_BPARTNER_ID FROM C_BPARTNER WHERE UPPER(NAME) LIKE '%GAVA%') THEN" +
      "                        (CASE WHEN COALESCE(cantidad,0) > 1 THEN" +
      "                              100 + ((COALESCE(cantidad,0) - 1)*50)" +
      "                         ELSE" +
      "                              100" +
      "                         END)" +
      "               ELSE  " +
      "                   0" +
      "               END) AS VALOR    " +
      "         FROM LDT_PRE_LIQUIDACION " +
      "        WHERE LDT_PRE_LIQUIDACION_ID  = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtpreLiquidacionId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LDTValorHandlingFeeData objectLDTValorHandlingFeeData = new LDTValorHandlingFeeData();
        objectLDTValorHandlingFeeData.valor = UtilSql.getValue(result, "valor");
        objectLDTValorHandlingFeeData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLDTValorHandlingFeeData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LDTValorHandlingFeeData objectLDTValorHandlingFeeData[] = new LDTValorHandlingFeeData[vector.size()];
    vector.copyInto(objectLDTValorHandlingFeeData);
    return(objectLDTValorHandlingFeeData);
  }
}
