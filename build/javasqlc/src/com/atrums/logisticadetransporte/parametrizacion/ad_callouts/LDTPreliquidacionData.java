//Sqlc generated V1.O00-1
package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class LDTPreliquidacionData implements FieldProvider {
static Logger log4j = Logger.getLogger(LDTPreliquidacionData.class);
  private String InitRecordNumber="0";
  public String agenteId;
  public String vendedorId;
  public String servicioClienteId;
  public String clienteId;
  public String tipo;
  public String cantidad;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("agente_id") || fieldName.equals("agenteId"))
      return agenteId;
    else if (fieldName.equalsIgnoreCase("vendedor_id") || fieldName.equals("vendedorId"))
      return vendedorId;
    else if (fieldName.equalsIgnoreCase("servicio_cliente_id") || fieldName.equals("servicioClienteId"))
      return servicioClienteId;
    else if (fieldName.equalsIgnoreCase("cliente_id") || fieldName.equals("clienteId"))
      return clienteId;
    else if (fieldName.equalsIgnoreCase("tipo"))
      return tipo;
    else if (fieldName.equalsIgnoreCase("cantidad"))
      return cantidad;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static LDTPreliquidacionData[] selectInformacion(ConnectionProvider connectionProvider, String ldtRoutingOrderId)    throws ServletException {
    return selectInformacion(connectionProvider, ldtRoutingOrderId, 0, 0);
  }

  public static LDTPreliquidacionData[] selectInformacion(ConnectionProvider connectionProvider, String ldtRoutingOrderId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "         SELECT RO.AGENTE_ID," +
      "               RO.VENDEDOR_ID," +
      "               C.SERVICIO_CLIENTE_ID," +
      "               RO.CLIENTE_ID," +
      "               RO.TIPO," +
      "               (SELECT SUM(NP.CANTIDAD) " +
      "                  FROM LDT_NAVIERA_PRODUCTOS NP" +
      "                 WHERE NP.LDT_COTIZACION_NAVIERA_ID = CN.LDT_COTIZACION_NAVIERA_ID" +
      "                   AND NP.M_PRODUCT_ID IS NOT NULL) AS CANTIDAD" +
      "          FROM LDT_ROUTING_ORDER RO, LDT_COTIZACION C, LDT_COTIZACION_NAVIERA CN" +
      "         WHERE RO.LDT_COTIZACION_ID = C.LDT_COTIZACION_ID" +
      "           AND C.LDT_COTIZACION_ID = CN.LDT_COTIZACION_ID" +
      "           AND CN.ISACTIVE = 'Y'" +
      "           AND RO.LDT_ROUTING_ORDER_ID = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtRoutingOrderId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LDTPreliquidacionData objectLDTPreliquidacionData = new LDTPreliquidacionData();
        objectLDTPreliquidacionData.agenteId = UtilSql.getValue(result, "agente_id");
        objectLDTPreliquidacionData.vendedorId = UtilSql.getValue(result, "vendedor_id");
        objectLDTPreliquidacionData.servicioClienteId = UtilSql.getValue(result, "servicio_cliente_id");
        objectLDTPreliquidacionData.clienteId = UtilSql.getValue(result, "cliente_id");
        objectLDTPreliquidacionData.tipo = UtilSql.getValue(result, "tipo");
        objectLDTPreliquidacionData.cantidad = UtilSql.getValue(result, "cantidad");
        objectLDTPreliquidacionData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLDTPreliquidacionData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LDTPreliquidacionData objectLDTPreliquidacionData[] = new LDTPreliquidacionData[vector.size()];
    vector.copyInto(objectLDTPreliquidacionData);
    return(objectLDTPreliquidacionData);
  }
}
