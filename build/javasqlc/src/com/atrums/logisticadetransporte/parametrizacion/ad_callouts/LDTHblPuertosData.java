//Sqlc generated V1.O00-1
package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class LDTHblPuertosData implements FieldProvider {
static Logger log4j = Logger.getLogger(LDTHblPuertosData.class);
  private String InitRecordNumber="0";
  public String ptoOrigen;
  public String ptoDestino;
  public String medio;
  public String buqueNave;
  public String fechaEstimadaArribo;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("pto_origen") || fieldName.equals("ptoOrigen"))
      return ptoOrigen;
    else if (fieldName.equalsIgnoreCase("pto_destino") || fieldName.equals("ptoDestino"))
      return ptoDestino;
    else if (fieldName.equalsIgnoreCase("medio"))
      return medio;
    else if (fieldName.equalsIgnoreCase("buque_nave") || fieldName.equals("buqueNave"))
      return buqueNave;
    else if (fieldName.equalsIgnoreCase("fecha_estimada_arribo") || fieldName.equals("fechaEstimadaArribo"))
      return fechaEstimadaArribo;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static LDTHblPuertosData[] selectHblPuertos(ConnectionProvider connectionProvider, String ldtHblId)    throws ServletException {
    return selectHblPuertos(connectionProvider, ldtHblId, 0, 0);
  }

  public static LDTHblPuertosData[] selectHblPuertos(ConnectionProvider connectionProvider, String ldtHblId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		 SELECT RO.PUERTO_TERMINAL_ORIGEN_ID AS PTO_ORIGEN," +
      "                RO.PUERTO_TERMINAL_DESTINO_ID AS PTO_DESTINO," +
      "                (CASE WHEN RO.LDT_COTIZACION_ID IS NOT NULL THEN" +
      "                        (SELECT CN.C_BPARTNER_ID " +
      "                           FROM LDT_COTIZACION_NAVIERA CN, LDT_COTIZACION C " +
      "                          WHERE CN.LDT_COTIZACION_ID = C.LDT_COTIZACION_ID" +
      "                            AND C.LDT_COTIZACION_ID = RO.LDT_COTIZACION_ID" +
      "                            AND CN.ISACTIVE = 'Y'" +
      "                        )" +
      "                    ELSE" +
      "                         RO.C_BPARTNER_MT_ID" +
      "                    END) AS MEDIO," +
      "                RS.BUQUE_NAVE," +
      "                RS.FECHA_ESTIMADA_ARRIBO" +
      "           FROM LDT_HBL H" +
      "           LEFT JOIN LDT_ROUTING_ORDER RO ON (RO.LDT_ROUTING_ORDER_ID = H.LDT_ROUTING_ORDER_ID)" +
      "           LEFT JOIN LDT_RO_SEGUIMIENTO RS ON (RO.LDT_ROUTING_ORDER_ID = RS.LDT_ROUTING_ORDER_ID )" +
      "          WHERE H.LDT_HBL_ID = ?" +
      "          ORDER BY RS.LINE DESC LIMIT 1";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtHblId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LDTHblPuertosData objectLDTHblPuertosData = new LDTHblPuertosData();
        objectLDTHblPuertosData.ptoOrigen = UtilSql.getValue(result, "pto_origen");
        objectLDTHblPuertosData.ptoDestino = UtilSql.getValue(result, "pto_destino");
        objectLDTHblPuertosData.medio = UtilSql.getValue(result, "medio");
        objectLDTHblPuertosData.buqueNave = UtilSql.getValue(result, "buque_nave");
        objectLDTHblPuertosData.fechaEstimadaArribo = UtilSql.getDateValue(result, "fecha_estimada_arribo", "dd-MM-yyyy");
        objectLDTHblPuertosData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLDTHblPuertosData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LDTHblPuertosData objectLDTHblPuertosData[] = new LDTHblPuertosData[vector.size()];
    vector.copyInto(objectLDTHblPuertosData);
    return(objectLDTHblPuertosData);
  }
}
