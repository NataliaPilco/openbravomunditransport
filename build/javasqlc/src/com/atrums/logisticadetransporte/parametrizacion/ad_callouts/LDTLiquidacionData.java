//Sqlc generated V1.O00-1
package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;

class LDTLiquidacionData implements FieldProvider {
static Logger log4j = Logger.getLogger(LDTLiquidacionData.class);
  private String InitRecordNumber="0";
  public String utilidad;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("utilidad"))
      return utilidad;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static String calculaLiquidacionLCL(ConnectionProvider connectionProvider, String ldtLiquidacionCabeceraId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		 SELECT (SUM(COALESCE(VAL_FACT_FLETE,0)) + " +
      "                SUM(COALESCE(CO_LOCALES_IVA,0)) -" +
      "                SUM(COALESCE(FLETE_MBL,0)) -" +
      "                SUM(COALESCE(COSTO_LOCAL_NAV,0))-" +
      "                SUM(COALESCE(ND_AGENTE,0))+" +
      "                SUM(COALESCE(NC_AGENTE,0))-" +
      "                SUM(COALESCE(ISD,0))-" +
      "                SUM(COALESCE(CAS,0))-" +
      "                SUM(COALESCE(MDT,0))-" +
      "                SUM(COALESCE(COD,0))-" +
      "                SUM(COALESCE(OTROS1,0))-" +
      "                SUM(COALESCE(TOTAL1,0))) AS UTILIDAD" +
      "          FROM LDT_LIQUIDACION LI" +
      "         WHERE LI.LDT_LIQUIDACION_CABECERA_ID = ?";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtLiquidacionCabeceraId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "utilidad");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String calculaLiquidacionFCL(ConnectionProvider connectionProvider, String ldtLiquidacionCabeceraId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "         SELECT (VAL_FACT_FLETE + " +
      "                 CO_LOCALES_IVA -" +
      "                 FLETE_MBL -" +
      "                 COSTO_LOCAL_NAV -" +
      "                 ND_AGENTE +" +
      "                 NC_AGENTE -" +
      "                 ISD -" +
      "                 CAS -" +
      "                 MDT -" +
      "                 COD -" +
      "                 OTROS1 -" +
      "                 TOTAL1) AS UTILIDAD" +
      "           FROM LDT_LIQUIDACION LI" +
      "          WHERE LI.LDT_LIQUIDACION_CABECERA_ID = ?";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtLiquidacionCabeceraId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "utilidad");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
