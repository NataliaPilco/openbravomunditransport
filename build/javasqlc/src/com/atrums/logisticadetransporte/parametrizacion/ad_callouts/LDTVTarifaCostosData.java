//Sqlc generated V1.O00-1
package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;

class LDTVTarifaCostosData implements FieldProvider {
static Logger log4j = Logger.getLogger(LDTVTarifaCostosData.class);
  private String InitRecordNumber="0";
  public String mPricelistVersionId;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("m_pricelist_version_id") || fieldName.equals("mPricelistVersionId"))
      return mPricelistVersionId;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static String selectVersionTarifa(ConnectionProvider connectionProvider, String mPricelistComId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		  SELECT MP.M_PRICELIST_VERSION_ID " +
      "			FROM M_PRICELIST_VERSION MP" +
      "			WHERE MP.VALIDFROM =  (SELECT MAX(PL.VALIDFROM)" +
      "			                        FROM M_PRICELIST_VERSION PL" +
      "			                       WHERE PL.M_PRICELIST_ID = ?" +
      "			                         AND PL.VALIDFROM <= NOW()" +
      "			                       LIMIT 1)" +
      "			 AND MP.M_PRICELIST_ID = ?" +
      "			LIMIT 1";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistComId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistComId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "m_pricelist_version_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
