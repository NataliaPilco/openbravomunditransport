//Sqlc generated V1.O00-1
package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class LDTShipperClienteData implements FieldProvider {
static Logger log4j = Logger.getLogger(LDTShipperClienteData.class);
  private String InitRecordNumber="0";
  public String cBpartnerLocationId;
  public String name;
  public String phone;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("c_bpartner_location_id") || fieldName.equals("cBpartnerLocationId"))
      return cBpartnerLocationId;
    else if (fieldName.equalsIgnoreCase("name"))
      return name;
    else if (fieldName.equalsIgnoreCase("phone"))
      return phone;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static LDTShipperClienteData[] selectDireccion(ConnectionProvider connectionProvider, String cBpartnerId)    throws ServletException {
    return selectDireccion(connectionProvider, cBpartnerId, 0, 0);
  }

  public static LDTShipperClienteData[] selectDireccion(ConnectionProvider connectionProvider, String cBpartnerId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "         SELECT C_BPARTNER_LOCATION.C_BPARTNER_LOCATION_ID,  C_BPARTNER_LOCATION.NAME, C_BPARTNER_LOCATION.PHONE" +
      "		   FROM C_BPARTNER_LOCATION" +
      "		  WHERE C_BPARTNER_LOCATION.C_BPARTNER_ID = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LDTShipperClienteData objectLDTShipperClienteData = new LDTShipperClienteData();
        objectLDTShipperClienteData.cBpartnerLocationId = UtilSql.getValue(result, "c_bpartner_location_id");
        objectLDTShipperClienteData.name = UtilSql.getValue(result, "name");
        objectLDTShipperClienteData.phone = UtilSql.getValue(result, "phone");
        objectLDTShipperClienteData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLDTShipperClienteData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LDTShipperClienteData objectLDTShipperClienteData[] = new LDTShipperClienteData[vector.size()];
    vector.copyInto(objectLDTShipperClienteData);
    return(objectLDTShipperClienteData);
  }

  public static String selectRUC(ConnectionProvider connectionProvider, String cBpartnerId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		 SELECT CP.TAXID " +
      "		   FROM C_BPARTNER CP" +
      "		  WHERE CP.C_BPARTNER_ID = ?";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "taxid");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
