//Sqlc generated V1.O00-1
package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class LDTDireccionClienteData implements FieldProvider {
static Logger log4j = Logger.getLogger(LDTDireccionClienteData.class);
  private String InitRecordNumber="0";
  public String id;
  public String name;
  public String svCliente;
  public String vendedor;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("id"))
      return id;
    else if (fieldName.equalsIgnoreCase("name"))
      return name;
    else if (fieldName.equalsIgnoreCase("sv_cliente") || fieldName.equals("svCliente"))
      return svCliente;
    else if (fieldName.equalsIgnoreCase("vendedor"))
      return vendedor;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static LDTDireccionClienteData[] obtenerDireccionCliente(ConnectionProvider connectionProvider, String cBpartnerId)    throws ServletException {
    return obtenerDireccionCliente(connectionProvider, cBpartnerId, 0, 0);
  }

  public static LDTDireccionClienteData[] obtenerDireccionCliente(ConnectionProvider connectionProvider, String cBpartnerId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT BL.C_BPARTNER_LOCATION_ID AS ID, " +
      "       		   BL.NAME AS NAME," +
      "               COALESCE(BP.EM_PDM_SERVICIOCLIENTE_ID, '') AS SV_CLIENTE," +
      "               COALESCE(BP.SALESREP_ID,'') AS VENDEDOR" +
      "		  FROM C_BPARTNER_LOCATION BL, C_BPARTNER BP " +
      "		 WHERE BL.C_BPARTNER_ID = ?" +
      "		   AND BL.IsBillTo='Y' " +
      "		   AND BL.IsActive='Y'" +
      "           AND BL.C_BPARTNER_ID = BP.C_BPARTNER_ID";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LDTDireccionClienteData objectLDTDireccionClienteData = new LDTDireccionClienteData();
        objectLDTDireccionClienteData.id = UtilSql.getValue(result, "id");
        objectLDTDireccionClienteData.name = UtilSql.getValue(result, "name");
        objectLDTDireccionClienteData.svCliente = UtilSql.getValue(result, "sv_cliente");
        objectLDTDireccionClienteData.vendedor = UtilSql.getValue(result, "vendedor");
        objectLDTDireccionClienteData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLDTDireccionClienteData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LDTDireccionClienteData objectLDTDireccionClienteData[] = new LDTDireccionClienteData[vector.size()];
    vector.copyInto(objectLDTDireccionClienteData);
    return(objectLDTDireccionClienteData);
  }
}
