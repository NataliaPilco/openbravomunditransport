//Sqlc generated V1.O00-1
package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class LDTPrecioProductoChinaData implements FieldProvider {
static Logger log4j = Logger.getLogger(LDTPrecioProductoChinaData.class);
  private String InitRecordNumber="0";
  public String precio;
  public String versionTarifa;
  public String warehouse;
  public String minimo;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("precio"))
      return precio;
    else if (fieldName.equalsIgnoreCase("version_tarifa") || fieldName.equals("versionTarifa"))
      return versionTarifa;
    else if (fieldName.equalsIgnoreCase("warehouse"))
      return warehouse;
    else if (fieldName.equalsIgnoreCase("minimo"))
      return minimo;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static LDTPrecioProductoChinaData[] selectPrecio(ConnectionProvider connectionProvider, String mProductId, String mPricelistId, String ldtContratoPuertoId)    throws ServletException {
    return selectPrecio(connectionProvider, mProductId, mPricelistId, ldtContratoPuertoId, 0, 0);
  }

  public static LDTPrecioProductoChinaData[] selectPrecio(ConnectionProvider connectionProvider, String mProductId, String mPricelistId, String ldtContratoPuertoId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		 SELECT (COALESCE(MP.PRICELIST,0)+ COALESCE(MP.EM_LDT_OPA,0)) AS PRECIO," +
      "                 MP.M_PRICELIST_VERSION_ID AS VERSION_TARIFA," +
      "                 'F0DF1B44AD334E67827222317B41C840' AS WAREHOUSE, " +
      "                 P.EM_LDT_VALOR_MINIMO AS MINIMO " +
      "           FROM M_PRODUCTPRICE MP, M_PRICELIST_VERSION PL, M_PRODUCT P" +
      "          WHERE MP.M_PRICELIST_VERSION_ID = PL.M_PRICELIST_VERSION_ID" +
      "            AND MP.M_PRODUCT_ID = P.M_PRODUCT_ID" +
      "            AND MP.M_PRODUCT_ID = ?" +
      "            AND MP.M_PRICELIST_VERSION_ID = (SELECT PLV.M_PRICELIST_VERSION_ID" +
      "                                               FROM M_PRICELIST_VERSION PLV" +
      "                                              WHERE PLV.VALIDFROM = (SELECT MAX(A.VALIDFROM)" +
      "                                                                    FROM M_PRICELIST_VERSION A" +
      "                                                                   WHERE A.M_PRICELIST_ID = ?" +
      "                                                                     AND A.VALIDFROM <= NOW()" +
      "                                                                     AND (CASE WHEN TO_CHAR(?) IS NULL " +
      "                                                                               THEN A.EM_LDT_CONTRATO_PUERTO_ID IS NULL " +
      "                                                                               ELSE A.EM_LDT_CONTRATO_PUERTO_ID = TO_CHAR(?)" +
      "                                                                          END)" +
      "                                                                   LIMIT 1)" +
      "                                        AND (CASE WHEN TO_CHAR(?) IS NULL THEN PLV.EM_LDT_CONTRATO_PUERTO_ID IS NULL " +
      "                                                                               ELSE PLV.EM_LDT_CONTRATO_PUERTO_ID = TO_CHAR(?)" +
      "                                                                          END)" +
      "                                        AND PLV.M_PRICELIST_ID = ?" +
      "                                        )";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtContratoPuertoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtContratoPuertoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtContratoPuertoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtContratoPuertoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LDTPrecioProductoChinaData objectLDTPrecioProductoChinaData = new LDTPrecioProductoChinaData();
        objectLDTPrecioProductoChinaData.precio = UtilSql.getValue(result, "precio");
        objectLDTPrecioProductoChinaData.versionTarifa = UtilSql.getValue(result, "version_tarifa");
        objectLDTPrecioProductoChinaData.warehouse = UtilSql.getValue(result, "warehouse");
        objectLDTPrecioProductoChinaData.minimo = UtilSql.getValue(result, "minimo");
        objectLDTPrecioProductoChinaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLDTPrecioProductoChinaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LDTPrecioProductoChinaData objectLDTPrecioProductoChinaData[] = new LDTPrecioProductoChinaData[vector.size()];
    vector.copyInto(objectLDTPrecioProductoChinaData);
    return(objectLDTPrecioProductoChinaData);
  }
}
