//Sqlc generated V1.O00-1
package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class LDTFleteCostoLclData implements FieldProvider {
static Logger log4j = Logger.getLogger(LDTFleteCostoLclData.class);
  private String InitRecordNumber="0";
  public String numDet;
  public String fleteCosto;
  public String div;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("num_det") || fieldName.equals("numDet"))
      return numDet;
    else if (fieldName.equalsIgnoreCase("flete_costo") || fieldName.equals("fleteCosto"))
      return fleteCosto;
    else if (fieldName.equalsIgnoreCase("div"))
      return div;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static LDTFleteCostoLclData[] selectFleteCosto(ConnectionProvider connectionProvider, String ldtMblId)    throws ServletException {
    return selectFleteCosto(connectionProvider, ldtMblId, 0, 0);
  }

  public static LDTFleteCostoLclData[] selectFleteCosto(ConnectionProvider connectionProvider, String ldtMblId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "            SELECT COUNT(NUM_HBL) AS NUM_DET, SUM(FLETE_COSTO) AS FLETE_COSTO, (SUM(FLETE_COSTO)/COUNT(NUM_HBL)) AS DIV" +
      "            FROM" +
      "            (" +
      "            SELECT DISTINCT  C.DOCUMENTNO AS DOC_COTIZACION," +
      "                   (CASE WHEN RO.LDT_COTIZACION_ID IS NOT NULL THEN" +
      "                            (NP.CANTIDAD * (SELECT PP.PRICESTD FROM M_PRODUCTPRICE PP WHERE PP.M_PRICELIST_VERSION_ID = NP.M_PRICELIST_VERSION_ID AND PP.M_PRODUCT_ID = NP.M_PRODUCT_ID))" +
      "                         ELSE" +
      "                            (NR.CANTIDAD * (SELECT PP.PRICESTD FROM M_PRODUCTPRICE PP WHERE PP.M_PRICELIST_VERSION_ID = NR.M_PRICELIST_VERSION_ID AND PP.M_PRODUCT_ID = NR.M_PRODUCT_ID))" +
      "                         END)  AS FLETE_COSTO," +
      "                   H.NUM_HBL       " +
      "             FROM LDT_MBL M" +
      "                      INNER JOIN LDT_MBL_LINEAS ML ON M.LDT_MBL_ID = ML.LDT_MBL_ID" +
      "                      INNER JOIN LDT_HBL H ON ML.LDT_HBL_ID = H.LDT_HBL_ID" +
      "                      INNER JOIN LDT_HBL_LINEA HL ON HL.LDT_HBL_ID = H.LDT_HBL_ID" +
      "                       LEFT JOIN LDT_ROUTING_ORDER RO ON RO.LDT_ROUTING_ORDER_ID = H.LDT_ROUTING_ORDER_ID" +
      "                       LEFT JOIN LDT_COTIZACION C ON C.LDT_COTIZACION_ID = RO.LDT_COTIZACION_ID" +
      "                       LEFT JOIN LDT_COTIZACION_NAVIERA CN ON (C.LDT_COTIZACION_ID = CN.LDT_COTIZACION_ID AND CN.ISACTIVE = 'Y')" +
      "                       LEFT JOIN LDT_NAVIERA_PRODUCTOS NP ON (NP.LDT_COTIZACION_NAVIERA_ID = CN.LDT_COTIZACION_NAVIERA_ID AND NP.M_PRODUCT_ID IS NOT NULL)" +
      "                       LEFT JOIN LDT_NAVIERA_PRODUCTOS NPCO ON (NPCO.LDT_COTIZACION_NAVIERA_ID = CN.LDT_COTIZACION_NAVIERA_ID AND NPCO.M_PRODUCT_CO_ID IS NOT NULL)" +
      "                       LEFT JOIN LDT_NAVIERA_PRODUCTOS NR ON (NR.LDT_ROUTING_ORDER_ID = RO.LDT_ROUTING_ORDER_ID AND NR.M_PRODUCT_ID IS NOT NULL AND RO.LDT_COTIZACION_ID IS NULL)" +
      "                       LEFT JOIN LDT_NAVIERA_PRODUCTOS NRCO ON (NRCO.LDT_ROUTING_ORDER_ID = RO.LDT_ROUTING_ORDER_ID AND NRCO.M_PRODUCT_CO_ID IS NOT NULL AND RO.LDT_COTIZACION_ID IS NULL)" +
      "                       LEFT JOIN C_BPARTNER BP1 ON RO.CLIENTE_ID = BP1.C_BPARTNER_ID" +
      "                       LEFT JOIN C_INVOICE IFL ON (IFL.EM_LDT_ROUTING_ORDER_ID = RO.LDT_ROUTING_ORDER_ID AND IFL.POREFERENCE IS NOT NULL AND IFL.ISSOTRX = 'Y' AND IFL.DOCSTATUS = 'CO')" +
      "                       LEFT JOIN C_INVOICE ICL ON (ICL.EM_LDT_ROUTING_ORDER_ID = RO.LDT_ROUTING_ORDER_ID AND ICL.POREFERENCE IS NULL AND ICL.ISSOTRX = 'Y')" +
      "                       LEFT JOIN C_INVOICE IMBL ON (IMBL.EM_LDT_MBL_ID = M.LDT_MBL_ID AND IMBL.ISSOTRX = 'N' AND IMBL.C_DOCTYPETARGET_ID IN (SELECT C_DOCTYPE_ID FROM C_DOCTYPE WHERE NAME LIKE '%FACTURA MBL%' AND AD_ORG_ID = M.AD_ORG_ID))" +
      "                       LEFT JOIN C_INVOICE ICMBL ON (ICMBL.EM_LDT_MBL_ID = M.LDT_MBL_ID AND IMBL.ISSOTRX = 'N' AND ICMBL.C_DOCTYPETARGET_ID NOT IN (SELECT C_DOCTYPE_ID FROM C_DOCTYPE WHERE NAME LIKE '%FACTURA MBL%' AND AD_ORG_ID = M.AD_ORG_ID))" +
      "             WHERE M.LDT_MBL_ID = ?" +
      "            ) AS VAL";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ldtMblId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LDTFleteCostoLclData objectLDTFleteCostoLclData = new LDTFleteCostoLclData();
        objectLDTFleteCostoLclData.numDet = UtilSql.getValue(result, "num_det");
        objectLDTFleteCostoLclData.fleteCosto = UtilSql.getValue(result, "flete_costo");
        objectLDTFleteCostoLclData.div = UtilSql.getValue(result, "div");
        objectLDTFleteCostoLclData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLDTFleteCostoLclData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LDTFleteCostoLclData objectLDTFleteCostoLclData[] = new LDTFleteCostoLclData[vector.size()];
    vector.copyInto(objectLDTFleteCostoLclData);
    return(objectLDTFleteCostoLclData);
  }
}
