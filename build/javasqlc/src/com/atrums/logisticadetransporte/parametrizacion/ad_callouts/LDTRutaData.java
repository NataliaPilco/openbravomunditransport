//Sqlc generated V1.O00-1
package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class LDTRutaData implements FieldProvider {
static Logger log4j = Logger.getLogger(LDTRutaData.class);
  private String InitRecordNumber="0";
  public String ldtRutaId;
  public String descripcion;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("ldt_ruta_id") || fieldName.equals("ldtRutaId"))
      return ldtRutaId;
    else if (fieldName.equalsIgnoreCase("descripcion"))
      return descripcion;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static LDTRutaData[] selectRuta(ConnectionProvider connectionProvider, String cBpartnerId)    throws ServletException {
    return selectRuta(connectionProvider, cBpartnerId, 0, 0);
  }

  public static LDTRutaData[] selectRuta(ConnectionProvider connectionProvider, String cBpartnerId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		 SELECT LR.LDT_RUTA_ID, LR.DESCRIPCION" +
      "		   FROM LDT_RUTA LR, LDT_RUTA_PARTNER LRP" +
      "		  WHERE LR.LDT_RUTA_ID    = LRP.LDT_RUTA_ID" +
      "		    AND LRP.C_BPARTNER_ID = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LDTRutaData objectLDTRutaData = new LDTRutaData();
        objectLDTRutaData.ldtRutaId = UtilSql.getValue(result, "ldt_ruta_id");
        objectLDTRutaData.descripcion = UtilSql.getValue(result, "descripcion");
        objectLDTRutaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLDTRutaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LDTRutaData objectLDTRutaData[] = new LDTRutaData[vector.size()];
    vector.copyInto(objectLDTRutaData);
    return(objectLDTRutaData);
  }
}
