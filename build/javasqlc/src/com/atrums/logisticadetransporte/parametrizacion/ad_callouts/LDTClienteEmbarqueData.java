//Sqlc generated V1.O00-1
package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class LDTClienteEmbarqueData implements FieldProvider {
static Logger log4j = Logger.getLogger(LDTClienteEmbarqueData.class);
  private String InitRecordNumber="0";
  public String cliente;
  public String rucCliente;
  public String dirCliente;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("cliente"))
      return cliente;
    else if (fieldName.equalsIgnoreCase("ruc_cliente") || fieldName.equals("rucCliente"))
      return rucCliente;
    else if (fieldName.equalsIgnoreCase("dir_cliente") || fieldName.equals("dirCliente"))
      return dirCliente;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static LDTClienteEmbarqueData[] selectClienteEmbarque(ConnectionProvider connectionProvider, String emLdtRoutingOrderId)    throws ServletException {
    return selectClienteEmbarque(connectionProvider, emLdtRoutingOrderId, 0, 0);
  }

  public static LDTClienteEmbarqueData[] selectClienteEmbarque(ConnectionProvider connectionProvider, String emLdtRoutingOrderId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		 SELECT CLIENTE_ID AS CLIENTE," +
      "                CLIENTE_RUC AS RUC_CLIENTE," +
      "                CLIENTE_DIRECCION_ID AS DIR_CLIENTE" +
      "           FROM LDT_ROUTING_ORDER RO" +
      "          WHERE LDT_ROUTING_ORDER_ID = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emLdtRoutingOrderId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LDTClienteEmbarqueData objectLDTClienteEmbarqueData = new LDTClienteEmbarqueData();
        objectLDTClienteEmbarqueData.cliente = UtilSql.getValue(result, "cliente");
        objectLDTClienteEmbarqueData.rucCliente = UtilSql.getValue(result, "ruc_cliente");
        objectLDTClienteEmbarqueData.dirCliente = UtilSql.getValue(result, "dir_cliente");
        objectLDTClienteEmbarqueData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLDTClienteEmbarqueData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LDTClienteEmbarqueData objectLDTClienteEmbarqueData[] = new LDTClienteEmbarqueData[vector.size()];
    vector.copyInto(objectLDTClienteEmbarqueData);
    return(objectLDTClienteEmbarqueData);
  }
}
