//Sqlc generated V1.O00-1
package com.atrums.logisticadetransporte.parametrizacion.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class LDTFacturaData implements FieldProvider {
static Logger log4j = Logger.getLogger(LDTFacturaData.class);
  private String InitRecordNumber="0";
  public String totalLineas;
  public String grandTotal;
  public String total;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("total_lineas") || fieldName.equals("totalLineas"))
      return totalLineas;
    else if (fieldName.equalsIgnoreCase("grand_total") || fieldName.equals("grandTotal"))
      return grandTotal;
    else if (fieldName.equalsIgnoreCase("total"))
      return total;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static LDTFacturaData[] selectFactura(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return selectFactura(connectionProvider, cInvoiceId, 0, 0);
  }

  public static LDTFacturaData[] selectFactura(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		 SELECT CI.TOTALLINES AS TOTAL_LINEAS," +
      "		        CI.GRANDTOTAL AS GRAND_TOTAL," +
      "		        CI.TOTALPAID AS TOTAL" +
      "		   FROM C_INVOICE CI" +
      "		  WHERE C_INVOICE_ID = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LDTFacturaData objectLDTFacturaData = new LDTFacturaData();
        objectLDTFacturaData.totalLineas = UtilSql.getValue(result, "total_lineas");
        objectLDTFacturaData.grandTotal = UtilSql.getValue(result, "grand_total");
        objectLDTFacturaData.total = UtilSql.getValue(result, "total");
        objectLDTFacturaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLDTFacturaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LDTFacturaData objectLDTFacturaData[] = new LDTFacturaData[vector.size()];
    vector.copyInto(objectLDTFacturaData);
    return(objectLDTFacturaData);
  }
}
