//Sqlc generated V1.O00-1
package com.atrums.print.doctype.utility.reporting.printing;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class PocData implements FieldProvider {
static Logger log4j = Logger.getLogger(PocData.class);
  private String InitRecordNumber="0";
  public String documentId;
  public String docstatus;
  public String ourreference;
  public String yourreference;
  public String salesrepUserId;
  public String salesrepEmail;
  public String salesrepName;
  public String bpartnerId;
  public String bpartnerName;
  public String contactUserId;
  public String contactEmail;
  public String contactName;
  public String adUserId;
  public String userEmail;
  public String userName;
  public String reportLocation;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("document_id") || fieldName.equals("documentId"))
      return documentId;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("ourreference"))
      return ourreference;
    else if (fieldName.equalsIgnoreCase("yourreference"))
      return yourreference;
    else if (fieldName.equalsIgnoreCase("salesrep_user_id") || fieldName.equals("salesrepUserId"))
      return salesrepUserId;
    else if (fieldName.equalsIgnoreCase("salesrep_email") || fieldName.equals("salesrepEmail"))
      return salesrepEmail;
    else if (fieldName.equalsIgnoreCase("salesrep_name") || fieldName.equals("salesrepName"))
      return salesrepName;
    else if (fieldName.equalsIgnoreCase("bpartner_id") || fieldName.equals("bpartnerId"))
      return bpartnerId;
    else if (fieldName.equalsIgnoreCase("bpartner_name") || fieldName.equals("bpartnerName"))
      return bpartnerName;
    else if (fieldName.equalsIgnoreCase("contact_user_id") || fieldName.equals("contactUserId"))
      return contactUserId;
    else if (fieldName.equalsIgnoreCase("contact_email") || fieldName.equals("contactEmail"))
      return contactEmail;
    else if (fieldName.equalsIgnoreCase("contact_name") || fieldName.equals("contactName"))
      return contactName;
    else if (fieldName.equalsIgnoreCase("ad_user_id") || fieldName.equals("adUserId"))
      return adUserId;
    else if (fieldName.equalsIgnoreCase("user_email") || fieldName.equals("userEmail"))
      return userEmail;
    else if (fieldName.equalsIgnoreCase("user_name") || fieldName.equals("userName"))
      return userName;
    else if (fieldName.equalsIgnoreCase("report_location") || fieldName.equals("reportLocation"))
      return reportLocation;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static PocData[] dummy(ConnectionProvider connectionProvider)    throws ServletException {
    return dummy(connectionProvider, 0, 0);
  }

  public static PocData[] dummy(ConnectionProvider connectionProvider, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		select" +
      "			'' as document_id," +
      "			'' as docstatus," +
      "			'' as ourreference," +
      "			'' as yourreference," +
      "			'' as salesrep_user_id," +
      "			'' as salesrep_email," +
      "			'' as salesrep_name," +
      "			'' as bpartner_id," +
      "			'' as bpartner_name," +
      "			'' as contact_user_id," +
      "			'' as contact_email," +
      "			'' as contact_name," +
      "			'' as ad_user_id," +
      "			'' as user_email," +
      "			'' as user_name," +
      "			'' as report_location" +
      "		from" +
      "			dual";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        PocData objectPocData = new PocData();
        objectPocData.documentId = UtilSql.getValue(result, "document_id");
        objectPocData.docstatus = UtilSql.getValue(result, "docstatus");
        objectPocData.ourreference = UtilSql.getValue(result, "ourreference");
        objectPocData.yourreference = UtilSql.getValue(result, "yourreference");
        objectPocData.salesrepUserId = UtilSql.getValue(result, "salesrep_user_id");
        objectPocData.salesrepEmail = UtilSql.getValue(result, "salesrep_email");
        objectPocData.salesrepName = UtilSql.getValue(result, "salesrep_name");
        objectPocData.bpartnerId = UtilSql.getValue(result, "bpartner_id");
        objectPocData.bpartnerName = UtilSql.getValue(result, "bpartner_name");
        objectPocData.contactUserId = UtilSql.getValue(result, "contact_user_id");
        objectPocData.contactEmail = UtilSql.getValue(result, "contact_email");
        objectPocData.contactName = UtilSql.getValue(result, "contact_name");
        objectPocData.adUserId = UtilSql.getValue(result, "ad_user_id");
        objectPocData.userEmail = UtilSql.getValue(result, "user_email");
        objectPocData.userName = UtilSql.getValue(result, "user_name");
        objectPocData.reportLocation = UtilSql.getValue(result, "report_location");
        objectPocData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectPocData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    PocData objectPocData[] = new PocData[vector.size()];
    vector.copyInto(objectPocData);
    return(objectPocData);
  }

  public static PocData[] getContactDetailsForCotizacion(ConnectionProvider connectionProvider, String ldtCotizacionId)    throws ServletException {
    return getContactDetailsForCotizacion(connectionProvider, ldtCotizacionId, 0, 0);
  }

  public static PocData[] getContactDetailsForCotizacion(ConnectionProvider connectionProvider, String ldtCotizacionId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "                select c.ldt_cotizacion_id as document_id," +
      "                       c.estado as docstatus," +
      "                       c.documentno as ourreference," +
      "                       null as yourreference," +
      "                       salesrep.ad_user_id as salesrep_user_id," +
      "                       salesrep.email as salesrep_email," +
      "                       salesrep.name as salesrep_name," +
      "                       c.c_bpartner_id as bpartner_id," +
      "                       c_bpartner.name as bpartner_name," +
      "                       customercontact.ad_user_id as contact_user_id," +
      "                       customercontact.email as contact_email," +
      "                       customercontact.name as contact_name" +
      "                  from ldt_cotizacion c" +
      "                  left join c_bpartner on c.c_bpartner_id = c_bpartner.c_bpartner_id" +
      "                  left join ad_user customercontact on customercontact.c_bpartner_id = c_bpartner.c_bpartner_id" +
      "                  left join ad_user salesrep on c.vendedor_id = salesrep.ad_user_id" +
      "                 where 1=1";
    strSql = strSql + ((ldtCotizacionId==null || ldtCotizacionId.equals(""))?"":"              and c.ldt_cotizacion_id in          " + ldtCotizacionId);

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);
      if (ldtCotizacionId != null && !(ldtCotizacionId.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        PocData objectPocData = new PocData();
        objectPocData.documentId = UtilSql.getValue(result, "document_id");
        objectPocData.docstatus = UtilSql.getValue(result, "docstatus");
        objectPocData.ourreference = UtilSql.getValue(result, "ourreference");
        objectPocData.yourreference = UtilSql.getValue(result, "yourreference");
        objectPocData.salesrepUserId = UtilSql.getValue(result, "salesrep_user_id");
        objectPocData.salesrepEmail = UtilSql.getValue(result, "salesrep_email");
        objectPocData.salesrepName = UtilSql.getValue(result, "salesrep_name");
        objectPocData.bpartnerId = UtilSql.getValue(result, "bpartner_id");
        objectPocData.bpartnerName = UtilSql.getValue(result, "bpartner_name");
        objectPocData.contactUserId = UtilSql.getValue(result, "contact_user_id");
        objectPocData.contactEmail = UtilSql.getValue(result, "contact_email");
        objectPocData.contactName = UtilSql.getValue(result, "contact_name");
        objectPocData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectPocData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    PocData objectPocData[] = new PocData[vector.size()];
    vector.copyInto(objectPocData);
    return(objectPocData);
  }

  public static PocData[] getContactDetailsForRoutingOrder(ConnectionProvider connectionProvider, String ldtRoutingOrderId)    throws ServletException {
    return getContactDetailsForRoutingOrder(connectionProvider, ldtRoutingOrderId, 0, 0);
  }

  public static PocData[] getContactDetailsForRoutingOrder(ConnectionProvider connectionProvider, String ldtRoutingOrderId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "                select ro.ldt_routing_order_id as document_id," +
      "                       ro.estado as docstatus," +
      "                       ro.documentno as ourreference," +
      "                       null as yourreference," +
      "                       salesrep.ad_user_id as salesrep_user_id," +
      "                       salesrep.email as salesrep_email," +
      "                       salesrep.name as salesrep_name," +
      "                       ro.cliente_id as bpartner_id," +
      "                       c_bpartner.name as bpartner_name," +
      "                       customercontact.ad_user_id as contact_user_id," +
      "                       customercontact.email as contact_email," +
      "                       customercontact.name as contact_name" +
      "                  from ldt_routing_order ro" +
      "                  left join ldt_cotizacion c on ro.ldt_cotizacion_id = c.ldt_cotizacion_id" +
      "                  left join c_bpartner on ro.cliente_id = c_bpartner.c_bpartner_id" +
      "                  left join ad_user customercontact on customercontact.c_bpartner_id = c_bpartner.c_bpartner_id" +
      "                  left join ad_user salesrep on c.vendedor_id = salesrep.ad_user_id" +
      "                 where 1=1";
    strSql = strSql + ((ldtRoutingOrderId==null || ldtRoutingOrderId.equals(""))?"":"              and ro.ldt_routing_order_id in          " + ldtRoutingOrderId);

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);
      if (ldtRoutingOrderId != null && !(ldtRoutingOrderId.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        PocData objectPocData = new PocData();
        objectPocData.documentId = UtilSql.getValue(result, "document_id");
        objectPocData.docstatus = UtilSql.getValue(result, "docstatus");
        objectPocData.ourreference = UtilSql.getValue(result, "ourreference");
        objectPocData.yourreference = UtilSql.getValue(result, "yourreference");
        objectPocData.salesrepUserId = UtilSql.getValue(result, "salesrep_user_id");
        objectPocData.salesrepEmail = UtilSql.getValue(result, "salesrep_email");
        objectPocData.salesrepName = UtilSql.getValue(result, "salesrep_name");
        objectPocData.bpartnerId = UtilSql.getValue(result, "bpartner_id");
        objectPocData.bpartnerName = UtilSql.getValue(result, "bpartner_name");
        objectPocData.contactUserId = UtilSql.getValue(result, "contact_user_id");
        objectPocData.contactEmail = UtilSql.getValue(result, "contact_email");
        objectPocData.contactName = UtilSql.getValue(result, "contact_name");
        objectPocData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectPocData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    PocData objectPocData[] = new PocData[vector.size()];
    vector.copyInto(objectPocData);
    return(objectPocData);
  }

  public static PocData[] getContactDetailsForUser(ConnectionProvider connectionProvider, String adUserId)    throws ServletException {
    return getContactDetailsForUser(connectionProvider, adUserId, 0, 0);
  }

  public static PocData[] getContactDetailsForUser(ConnectionProvider connectionProvider, String adUserId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select" +
      "            ad_user.ad_user_id," +
      "            ad_user.email as user_email," +
      "            ad_user.name as user_name" +
      "        from" +
      "            ad_user" +
      "        where" +
      "            ad_user.ad_user_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        PocData objectPocData = new PocData();
        objectPocData.adUserId = UtilSql.getValue(result, "ad_user_id");
        objectPocData.userEmail = UtilSql.getValue(result, "user_email");
        objectPocData.userName = UtilSql.getValue(result, "user_name");
        objectPocData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectPocData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    PocData objectPocData[] = new PocData[vector.size()];
    vector.copyInto(objectPocData);
    return(objectPocData);
  }
}
