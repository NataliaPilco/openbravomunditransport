
package org.openbravo.erpWindows.com.atrums.logisticadetransporte.parametrizacion.MDTLiquidaciones;


import org.openbravo.erpCommon.reference.*;


import org.openbravo.erpCommon.ad_actionButton.*;


import org.codehaus.jettison.json.JSONObject;
import org.openbravo.erpCommon.utility.*;
import org.openbravo.data.FieldProvider;
import org.openbravo.utils.FormatUtilities;
import org.openbravo.utils.Replace;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.exception.OBException;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessRunner;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.xmlEngine.XmlDocument;
import java.util.Vector;
import java.util.StringTokenizer;
import org.openbravo.database.SessionInfo;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import java.sql.Connection;
import org.apache.log4j.Logger;

public class LiquidacionesDFDA384C51D24338B9070979CDF44876 extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;
  
  private static Logger log4j = Logger.getLogger(LiquidacionesDFDA384C51D24338B9070979CDF44876.class);
  
  private static final String windowId = "76E5AFD6A6A8435FB470FBF12625B7AD";
  private static final String tabId = "DFDA384C51D24338B9070979CDF44876";
  private static final String defaultTabView = "RELATION";
  private static final int accesslevel = 3;
  private static final String moduleId = "C5BE74383A264BF68316D6BF975E6EFC";
  
  @Override
  public void init(ServletConfig config) {
    setClassInfo("W", tabId, moduleId);
    super.init(config);
  }
  
  
  @Override
  public void service(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    String command = vars.getCommand();
    
    boolean securedProcess = false;
    if (command.contains("BUTTON")) {
      try {
        securedProcess = "Y".equals(org.openbravo.erpCommon.businessUtility.Preferences
            .getPreferenceValue("SecuredProcess", true, vars.getClient(), vars.getOrg(), vars
                .getUser(), vars.getRole(), windowId));
      } catch (PropertyException e) {
      }
    
     
      if (command.contains("E2B721F481554903ADB7F8658E76B368")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("E2B721F481554903ADB7F8658E76B368");
        SessionInfo.setModuleId("C5BE74383A264BF68316D6BF975E6EFC");
        if (securedProcess) {
          classInfo.type = "P";
          classInfo.id = "E2B721F481554903ADB7F8658E76B368";
        }
      }
     
      if (command.contains("B066D443B4EC4E68A8B9363702AB217B")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("B066D443B4EC4E68A8B9363702AB217B");
        SessionInfo.setModuleId("C5BE74383A264BF68316D6BF975E6EFC");
        if (securedProcess) {
          classInfo.type = "P";
          classInfo.id = "B066D443B4EC4E68A8B9363702AB217B";
        }
      }
     
      if (command.contains("C1A39A18FF9243A7964AF78580613C42")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("C1A39A18FF9243A7964AF78580613C42");
        SessionInfo.setModuleId("C5BE74383A264BF68316D6BF975E6EFC");
        if (securedProcess) {
          classInfo.type = "P";
          classInfo.id = "C1A39A18FF9243A7964AF78580613C42";
        }
      }
     
     
    }
    if (!securedProcess) {
      setClassInfo("W", tabId, moduleId);
    }
    super.service(request, response);
  }
  

  public void doPost (HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException {
    TableSQLData tableSQL = null;
    VariablesSecureApp vars = new VariablesSecureApp(request);
    Boolean saveRequest = (Boolean) request.getAttribute("autosave");
    
    if(saveRequest != null && saveRequest){
      String currentOrg = vars.getStringParameter("inpadOrgId");
      String currentClient = vars.getStringParameter("inpadClientId");
      boolean editableTab = (!org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)
                            && (currentOrg.equals("") || Utility.isElementInList(Utility.getContext(this, vars,"#User_Org", windowId, accesslevel), currentOrg)) 
                            && (currentClient.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),currentClient)));
    
        OBError myError = new OBError();
        String commandType = request.getParameter("inpCommandType");
        String strldtLiquidacionCabeceraId = request.getParameter("inpldtLiquidacionCabeceraId");
        
        if (editableTab) {
          int total = 0;
          
          if(commandType.equalsIgnoreCase("EDIT") && !strldtLiquidacionCabeceraId.equals(""))
              total = saveRecord(vars, myError, 'U');
          else
              total = saveRecord(vars, myError, 'I');
          
          if (!myError.isEmpty() && total == 0)     
            throw new OBException(myError.getMessage());
        }
        vars.setSessionValue(request.getParameter("mappingName") +"|hash", vars.getPostDataHash());
        vars.setSessionValue(tabId + "|Header.view", "EDIT");
        
        return;
    }
    
    try {
      tableSQL = new TableSQLData(vars, this, tabId, Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel), Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    String strOrderBy = vars.getSessionValue(tabId + "|orderby");
    if (!strOrderBy.equals("")) {
      vars.setSessionValue(tabId + "|newOrder", "1");
    }

    if (vars.commandIn("DEFAULT")) {

      String strLDT_Liquidacion_Cabecera_ID = vars.getGlobalVariable("inpldtLiquidacionCabeceraId", windowId + "|LDT_Liquidacion_Cabecera_ID", "");
      

      String strView = vars.getSessionValue(tabId + "|LiquidacionesDFDA384C51D24338B9070979CDF44876.view");
      if (strView.equals("")) {
        strView = defaultTabView;

        if (strView.equals("EDIT")) {
          if (strLDT_Liquidacion_Cabecera_ID.equals("")) strLDT_Liquidacion_Cabecera_ID = firstElement(vars, tableSQL);
          if (strLDT_Liquidacion_Cabecera_ID.equals("")) strView = "RELATION";
        }
      }
      if (strView.equals("EDIT")) 

        printPageEdit(response, request, vars, false, strLDT_Liquidacion_Cabecera_ID, tableSQL);

      else printPageDataSheet(response, vars, strLDT_Liquidacion_Cabecera_ID, tableSQL);
    } else if (vars.commandIn("DIRECT")) {
      String strLDT_Liquidacion_Cabecera_ID = vars.getStringParameter("inpDirectKey");
      
        
      if (strLDT_Liquidacion_Cabecera_ID.equals("")) strLDT_Liquidacion_Cabecera_ID = vars.getRequiredGlobalVariable("inpldtLiquidacionCabeceraId", windowId + "|LDT_Liquidacion_Cabecera_ID");
      else vars.setSessionValue(windowId + "|LDT_Liquidacion_Cabecera_ID", strLDT_Liquidacion_Cabecera_ID);
      
      vars.setSessionValue(tabId + "|LiquidacionesDFDA384C51D24338B9070979CDF44876.view", "EDIT");

      printPageEdit(response, request, vars, false, strLDT_Liquidacion_Cabecera_ID, tableSQL);

    } else if (vars.commandIn("TAB")) {


      String strView = vars.getSessionValue(tabId + "|LiquidacionesDFDA384C51D24338B9070979CDF44876.view");
      String strLDT_Liquidacion_Cabecera_ID = "";
      if (strView.equals("")) {
        strView = defaultTabView;
        if (strView.equals("EDIT")) {
          strLDT_Liquidacion_Cabecera_ID = firstElement(vars, tableSQL);
          if (strLDT_Liquidacion_Cabecera_ID.equals("")) strView = "RELATION";
        }
      }
      if (strView.equals("EDIT")) {

        if (strLDT_Liquidacion_Cabecera_ID.equals("")) strLDT_Liquidacion_Cabecera_ID = firstElement(vars, tableSQL);
        printPageEdit(response, request, vars, false, strLDT_Liquidacion_Cabecera_ID, tableSQL);

      } else printPageDataSheet(response, vars, "", tableSQL);
    } else if (vars.commandIn("SEARCH")) {
vars.getRequestGlobalVariable("inpParamldt_mbl_id", tabId + "|paramldt_mbl_id");

        vars.getRequestGlobalVariable("inpParamUpdated", tabId + "|paramUpdated");
        vars.getRequestGlobalVariable("inpParamUpdatedBy", tabId + "|paramUpdatedBy");
        vars.getRequestGlobalVariable("inpParamCreated", tabId + "|paramCreated");
        vars.getRequestGlobalVariable("inpparamCreatedBy", tabId + "|paramCreatedBy");
      
      
      vars.removeSessionValue(windowId + "|LDT_Liquidacion_Cabecera_ID");
      String strLDT_Liquidacion_Cabecera_ID="";

      String strView = vars.getSessionValue(tabId + "|LiquidacionesDFDA384C51D24338B9070979CDF44876.view");
      if (strView.equals("")) strView=defaultTabView;

      if (strView.equals("EDIT")) {
        strLDT_Liquidacion_Cabecera_ID = firstElement(vars, tableSQL);
        if (strLDT_Liquidacion_Cabecera_ID.equals("")) {
          // filter returns empty set
          strView = "RELATION";
          // switch to grid permanently until the user changes the view again
          vars.setSessionValue(tabId + "|LiquidacionesDFDA384C51D24338B9070979CDF44876.view", strView);
        }
      }

      if (strView.equals("EDIT")) 

        printPageEdit(response, request, vars, false, strLDT_Liquidacion_Cabecera_ID, tableSQL);

      else printPageDataSheet(response, vars, strLDT_Liquidacion_Cabecera_ID, tableSQL);
    } else if (vars.commandIn("RELATION")) {
      

      String strLDT_Liquidacion_Cabecera_ID = vars.getGlobalVariable("inpldtLiquidacionCabeceraId", windowId + "|LDT_Liquidacion_Cabecera_ID", "");
      vars.setSessionValue(tabId + "|LiquidacionesDFDA384C51D24338B9070979CDF44876.view", "RELATION");
      printPageDataSheet(response, vars, strLDT_Liquidacion_Cabecera_ID, tableSQL);
    } else if (vars.commandIn("NEW")) {


      printPageEdit(response, request, vars, true, "", tableSQL);

    } else if (vars.commandIn("EDIT")) {

      @SuppressWarnings("unused") // In Expense Invoice tab this variable is not used, to be fixed
      String strLDT_Liquidacion_Cabecera_ID = vars.getGlobalVariable("inpldtLiquidacionCabeceraId", windowId + "|LDT_Liquidacion_Cabecera_ID", "");
      vars.setSessionValue(tabId + "|LiquidacionesDFDA384C51D24338B9070979CDF44876.view", "EDIT");

      setHistoryCommand(request, "EDIT");
      printPageEdit(response, request, vars, false, strLDT_Liquidacion_Cabecera_ID, tableSQL);

    } else if (vars.commandIn("NEXT")) {

      String strLDT_Liquidacion_Cabecera_ID = vars.getRequiredStringParameter("inpldtLiquidacionCabeceraId");
      
      String strNext = nextElement(vars, strLDT_Liquidacion_Cabecera_ID, tableSQL);

      printPageEdit(response, request, vars, false, strNext, tableSQL);
    } else if (vars.commandIn("PREVIOUS")) {

      String strLDT_Liquidacion_Cabecera_ID = vars.getRequiredStringParameter("inpldtLiquidacionCabeceraId");
      
      String strPrevious = previousElement(vars, strLDT_Liquidacion_Cabecera_ID, tableSQL);

      printPageEdit(response, request, vars, false, strPrevious, tableSQL);
    } else if (vars.commandIn("FIRST_RELATION")) {

      vars.setSessionValue(tabId + "|LiquidacionesDFDA384C51D24338B9070979CDF44876.initRecordNumber", "0");
      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("PREVIOUS_RELATION")) {

      String strInitRecord = vars.getSessionValue(tabId + "|LiquidacionesDFDA384C51D24338B9070979CDF44876.initRecordNumber");
      String strRecordRange = Utility.getContext(this, vars, "#RecordRange", windowId);
      int intRecordRange = strRecordRange.equals("")?0:Integer.parseInt(strRecordRange);
      if (strInitRecord.equals("") || strInitRecord.equals("0")) {
        vars.setSessionValue(tabId + "|LiquidacionesDFDA384C51D24338B9070979CDF44876.initRecordNumber", "0");
      } else {
        int initRecord = (strInitRecord.equals("")?0:Integer.parseInt(strInitRecord));
        initRecord -= intRecordRange;
        strInitRecord = ((initRecord<0)?"0":Integer.toString(initRecord));
        vars.setSessionValue(tabId + "|LiquidacionesDFDA384C51D24338B9070979CDF44876.initRecordNumber", strInitRecord);
      }
      vars.removeSessionValue(windowId + "|LDT_Liquidacion_Cabecera_ID");

      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("NEXT_RELATION")) {

      String strInitRecord = vars.getSessionValue(tabId + "|LiquidacionesDFDA384C51D24338B9070979CDF44876.initRecordNumber");
      String strRecordRange = Utility.getContext(this, vars, "#RecordRange", windowId);
      int intRecordRange = strRecordRange.equals("")?0:Integer.parseInt(strRecordRange);
      int initRecord = (strInitRecord.equals("")?0:Integer.parseInt(strInitRecord));
      if (initRecord==0) initRecord=1;
      initRecord += intRecordRange;
      strInitRecord = ((initRecord<0)?"0":Integer.toString(initRecord));
      vars.setSessionValue(tabId + "|LiquidacionesDFDA384C51D24338B9070979CDF44876.initRecordNumber", strInitRecord);
      vars.removeSessionValue(windowId + "|LDT_Liquidacion_Cabecera_ID");

      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("FIRST")) {

      
      String strFirst = firstElement(vars, tableSQL);

      printPageEdit(response, request, vars, false, strFirst, tableSQL);
    } else if (vars.commandIn("LAST_RELATION")) {

      String strLast = lastElement(vars, tableSQL);
      printPageDataSheet(response, vars, strLast, tableSQL);
    } else if (vars.commandIn("LAST")) {

      
      String strLast = lastElement(vars, tableSQL);

      printPageEdit(response, request, vars, false, strLast, tableSQL);
    } else if (vars.commandIn("SAVE_NEW_RELATION", "SAVE_NEW_NEW", "SAVE_NEW_EDIT")) {

      OBError myError = new OBError();      
      int total = saveRecord(vars, myError, 'I');      
      if (!myError.isEmpty()) {        
        response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
      } 
      else {
		if (myError.isEmpty()) {
		  myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsInserted");
		  myError.setMessage(total + " " + myError.getMessage());
		  vars.setMessage(tabId, myError);
		}        
        if (vars.commandIn("SAVE_NEW_NEW")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
        else if (vars.commandIn("SAVE_NEW_EDIT")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        else response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
      }
    } else if (vars.commandIn("SAVE_EDIT_RELATION", "SAVE_EDIT_NEW", "SAVE_EDIT_EDIT", "SAVE_EDIT_NEXT")) {

      String strLDT_Liquidacion_Cabecera_ID = vars.getRequiredGlobalVariable("inpldtLiquidacionCabeceraId", windowId + "|LDT_Liquidacion_Cabecera_ID");
      OBError myError = new OBError();
      int total = saveRecord(vars, myError, 'U');      
      if (!myError.isEmpty()) {
        response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
      } 
      else {
        if (myError.isEmpty()) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsUpdated");
          myError.setMessage(total + " " + myError.getMessage());
          vars.setMessage(tabId, myError);
        }
        if (vars.commandIn("SAVE_EDIT_NEW")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
        else if (vars.commandIn("SAVE_EDIT_EDIT")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        else if (vars.commandIn("SAVE_EDIT_NEXT")) {
          String strNext = nextElement(vars, strLDT_Liquidacion_Cabecera_ID, tableSQL);
          vars.setSessionValue(windowId + "|LDT_Liquidacion_Cabecera_ID", strNext);
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        } else response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
      }
    } else if (vars.commandIn("DELETE")) {

      String strLDT_Liquidacion_Cabecera_ID = vars.getRequiredStringParameter("inpldtLiquidacionCabeceraId");
      //LiquidacionesDFDA384C51D24338B9070979CDF44876Data data = getEditVariables(vars);
      int total = 0;
      OBError myError = null;
      if (org.openbravo.erpCommon.utility.WindowAccessData.hasNotDeleteAccess(this, vars.getRole(), tabId)) {
        myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
        vars.setMessage(tabId, myError);
      } else {
        try {
          total = LiquidacionesDFDA384C51D24338B9070979CDF44876Data.delete(this, strLDT_Liquidacion_Cabecera_ID, Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), Utility.getContext(this, vars, "#User_Org", windowId, accesslevel));
        } catch(ServletException ex) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myError.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myError);
        }
        if (myError==null && total==0) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
        }
        vars.removeSessionValue(windowId + "|ldtLiquidacionCabeceraId");
        vars.setSessionValue(tabId + "|LiquidacionesDFDA384C51D24338B9070979CDF44876.view", "RELATION");
      }
      if (myError==null) {
        myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsDeleted");
        myError.setMessage(total + " " + myError.getMessage());
        vars.setMessage(tabId, myError);
      }
      response.sendRedirect(strDireccion + request.getServletPath());

     } else if (vars.commandIn("BUTTONdatos_liqE2B721F481554903ADB7F8658E76B368")) {
        vars.setSessionValue("buttonE2B721F481554903ADB7F8658E76B368.strdatosLiq", vars.getStringParameter("inpdatosLiq"));
        vars.setSessionValue("buttonE2B721F481554903ADB7F8658E76B368.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("buttonE2B721F481554903ADB7F8658E76B368.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("buttonE2B721F481554903ADB7F8658E76B368.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        
        
        //Save in session needed params for combos if needed
        vars.setSessionObject("buttonE2B721F481554903ADB7F8658E76B368.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "E2B721F481554903ADB7F8658E76B368", request.getServletPath());    
     } else if (vars.commandIn("BUTTONE2B721F481554903ADB7F8658E76B368")) {
        String strLDT_Liquidacion_Cabecera_ID = vars.getGlobalVariable("inpldtLiquidacionCabeceraId", windowId + "|LDT_Liquidacion_Cabecera_ID", "");
        String strdatosLiq = vars.getSessionValue("buttonE2B721F481554903ADB7F8658E76B368.strdatosLiq");
        String strProcessing = vars.getSessionValue("buttonE2B721F481554903ADB7F8658E76B368.strProcessing");
        String strOrg = vars.getSessionValue("buttonE2B721F481554903ADB7F8658E76B368.strOrg");
        String strClient = vars.getSessionValue("buttonE2B721F481554903ADB7F8658E76B368.strClient");
        
        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtondatos_liqE2B721F481554903ADB7F8658E76B368(response, vars, strLDT_Liquidacion_Cabecera_ID, strdatosLiq, strProcessing);
        }

     } else if (vars.commandIn("BUTTONgenera_liqB066D443B4EC4E68A8B9363702AB217B")) {
        vars.setSessionValue("buttonB066D443B4EC4E68A8B9363702AB217B.strgeneraLiq", vars.getStringParameter("inpgeneraLiq"));
        vars.setSessionValue("buttonB066D443B4EC4E68A8B9363702AB217B.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("buttonB066D443B4EC4E68A8B9363702AB217B.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("buttonB066D443B4EC4E68A8B9363702AB217B.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        
        
        //Save in session needed params for combos if needed
        vars.setSessionObject("buttonB066D443B4EC4E68A8B9363702AB217B.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "B066D443B4EC4E68A8B9363702AB217B", request.getServletPath());    
     } else if (vars.commandIn("BUTTONB066D443B4EC4E68A8B9363702AB217B")) {
        String strLDT_Liquidacion_Cabecera_ID = vars.getGlobalVariable("inpldtLiquidacionCabeceraId", windowId + "|LDT_Liquidacion_Cabecera_ID", "");
        String strgeneraLiq = vars.getSessionValue("buttonB066D443B4EC4E68A8B9363702AB217B.strgeneraLiq");
        String strProcessing = vars.getSessionValue("buttonB066D443B4EC4E68A8B9363702AB217B.strProcessing");
        String strOrg = vars.getSessionValue("buttonB066D443B4EC4E68A8B9363702AB217B.strOrg");
        String strClient = vars.getSessionValue("buttonB066D443B4EC4E68A8B9363702AB217B.strClient");
        
        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtongenera_liqB066D443B4EC4E68A8B9363702AB217B(response, vars, strLDT_Liquidacion_Cabecera_ID, strgeneraLiq, strProcessing);
        }

     } else if (vars.commandIn("BUTTONprocessedC1A39A18FF9243A7964AF78580613C42")) {
        vars.setSessionValue("buttonC1A39A18FF9243A7964AF78580613C42.strprocessed", vars.getStringParameter("inpprocessed"));
        vars.setSessionValue("buttonC1A39A18FF9243A7964AF78580613C42.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("buttonC1A39A18FF9243A7964AF78580613C42.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("buttonC1A39A18FF9243A7964AF78580613C42.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        p.put("Processed", vars.getStringParameter("inpprocessed"));

        
        //Save in session needed params for combos if needed
        vars.setSessionObject("buttonC1A39A18FF9243A7964AF78580613C42.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "C1A39A18FF9243A7964AF78580613C42", request.getServletPath());    
     } else if (vars.commandIn("BUTTONC1A39A18FF9243A7964AF78580613C42")) {
        String strLDT_Liquidacion_Cabecera_ID = vars.getGlobalVariable("inpldtLiquidacionCabeceraId", windowId + "|LDT_Liquidacion_Cabecera_ID", "");
        String strprocessed = vars.getSessionValue("buttonC1A39A18FF9243A7964AF78580613C42.strprocessed");
        String strProcessing = vars.getSessionValue("buttonC1A39A18FF9243A7964AF78580613C42.strProcessing");
        String strOrg = vars.getSessionValue("buttonC1A39A18FF9243A7964AF78580613C42.strOrg");
        String strClient = vars.getSessionValue("buttonC1A39A18FF9243A7964AF78580613C42.strClient");
        
        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtonprocessedC1A39A18FF9243A7964AF78580613C42(response, vars, strLDT_Liquidacion_Cabecera_ID, strprocessed, strProcessing);
        }


    } else if (vars.commandIn("SAVE_BUTTONdatos_liqE2B721F481554903ADB7F8658E76B368")) {
        String strLDT_Liquidacion_Cabecera_ID = vars.getGlobalVariable("inpKey", windowId + "|LDT_Liquidacion_Cabecera_ID", "");
        @SuppressWarnings("unused")
        String strdatosLiq = vars.getStringParameter("inpdatosLiq");
        String strProcessing = vars.getStringParameter("inpprocessing");
        OBError myMessage = null;
        try {
          String pinstance = SequenceIdData.getUUID();
          PInstanceProcessData.insertPInstance(this, pinstance, "E2B721F481554903ADB7F8658E76B368", (("LDT_Liquidacion_Cabecera_ID".equalsIgnoreCase("AD_Language"))?"0":strLDT_Liquidacion_Cabecera_ID), strProcessing, vars.getUser(), vars.getClient(), vars.getOrg());
          
          
          ProcessBundle bundle = ProcessBundle.pinstance(pinstance, vars, this);
          new ProcessRunner(bundle).execute(this);
          
          PInstanceProcessData[] pinstanceData = PInstanceProcessData.select(this, pinstance);
          myMessage = Utility.getProcessInstanceMessage(this, vars, pinstanceData);
        } catch (ServletException ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);
    } else if (vars.commandIn("SAVE_BUTTONgenera_liqB066D443B4EC4E68A8B9363702AB217B")) {
        String strLDT_Liquidacion_Cabecera_ID = vars.getGlobalVariable("inpKey", windowId + "|LDT_Liquidacion_Cabecera_ID", "");
        @SuppressWarnings("unused")
        String strgeneraLiq = vars.getStringParameter("inpgeneraLiq");
        String strProcessing = vars.getStringParameter("inpprocessing");
        OBError myMessage = null;
        try {
          String pinstance = SequenceIdData.getUUID();
          PInstanceProcessData.insertPInstance(this, pinstance, "B066D443B4EC4E68A8B9363702AB217B", (("LDT_Liquidacion_Cabecera_ID".equalsIgnoreCase("AD_Language"))?"0":strLDT_Liquidacion_Cabecera_ID), strProcessing, vars.getUser(), vars.getClient(), vars.getOrg());
          
          
          ProcessBundle bundle = ProcessBundle.pinstance(pinstance, vars, this);
          new ProcessRunner(bundle).execute(this);
          
          PInstanceProcessData[] pinstanceData = PInstanceProcessData.select(this, pinstance);
          myMessage = Utility.getProcessInstanceMessage(this, vars, pinstanceData);
        } catch (ServletException ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);
    } else if (vars.commandIn("SAVE_BUTTONprocessedC1A39A18FF9243A7964AF78580613C42")) {
        String strLDT_Liquidacion_Cabecera_ID = vars.getGlobalVariable("inpKey", windowId + "|LDT_Liquidacion_Cabecera_ID", "");
        @SuppressWarnings("unused")
        String strprocessed = vars.getStringParameter("inpprocessed");
        String strProcessing = vars.getStringParameter("inpprocessing");
        OBError myMessage = null;
        try {
          String pinstance = SequenceIdData.getUUID();
          PInstanceProcessData.insertPInstance(this, pinstance, "C1A39A18FF9243A7964AF78580613C42", (("LDT_Liquidacion_Cabecera_ID".equalsIgnoreCase("AD_Language"))?"0":strLDT_Liquidacion_Cabecera_ID), strProcessing, vars.getUser(), vars.getClient(), vars.getOrg());
          String straccionliquidacion = vars.getStringParameter("inpaccionliquidacion");
PInstanceProcessData.insertPInstanceParam(this, pinstance, "10", "AccionLiquidacion", straccionliquidacion, vars.getClient(), vars.getOrg(), vars.getUser());

          
          ProcessBundle bundle = ProcessBundle.pinstance(pinstance, vars, this);
          new ProcessRunner(bundle).execute(this);
          
          PInstanceProcessData[] pinstanceData = PInstanceProcessData.select(this, pinstance);
          myMessage = Utility.getProcessInstanceMessage(this, vars, pinstanceData);
        } catch (ServletException ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);






    } else if (vars.commandIn("SAVE_XHR")) {
      
      OBError myError = new OBError();
      JSONObject result = new JSONObject();
      String commandType = vars.getStringParameter("inpCommandType");
      char saveType = "NEW".equals(commandType) ? 'I' : 'U';
      try {
        int total = saveRecord(vars, myError, saveType);
        if (myError.isEmpty()) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsUpdated");
          myError.setMessage(total + " " + myError.getMessage());
          myError.setType("Success");
        }
        result.put("oberror", myError.toMap());
        result.put("tabid", vars.getStringParameter("tabID"));
        result.put("redirect", strDireccion + request.getServletPath() + "?Command=" + commandType);
      } catch (Exception e) {
        log4j.error("Error saving record (XHR request): " + e.getMessage(), e);
        myError.setType("Error");
        myError.setMessage(e.getMessage());
      }

      response.setContentType("application/json");
      PrintWriter out = response.getWriter();
      out.print(result.toString());
      out.flush();
      out.close();
    } else if (vars.getCommand().toUpperCase().startsWith("BUTTON") || vars.getCommand().toUpperCase().startsWith("SAVE_BUTTON")) {
      pageErrorPopUp(response);
    } else pageError(response);
  }
  private LiquidacionesDFDA384C51D24338B9070979CDF44876Data getEditVariables(Connection con, VariablesSecureApp vars) throws IOException,ServletException {
    LiquidacionesDFDA384C51D24338B9070979CDF44876Data data = new LiquidacionesDFDA384C51D24338B9070979CDF44876Data();
    ServletException ex = null;
    try {
    data.condicion = vars.getRequiredGlobalVariable("inpcondicion", windowId + "|condicion");     data.condicionr = vars.getStringParameter("inpcondicion_R");     data.ldtMblId = vars.getRequiredGlobalVariable("inpldtMblId", windowId + "|ldt_mbl_id");     data.adOrgId = vars.getRequiredGlobalVariable("inpadOrgId", windowId + "|AD_Org_ID");     data.adOrgIdr = vars.getStringParameter("inpadOrgId_R");    try {   data.ldtNdAgente = vars.getNumericParameter("inpldtNdAgente");  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.ldtNcAgente = vars.getNumericParameter("inpldtNcAgente");  } catch (ServletException paramEx) { ex = paramEx; }     data.datosLiq = vars.getRequestGlobalVariable("inpdatosLiq", windowId + "|datos_liq");     data.generaLiq = vars.getRequestGlobalVariable("inpgeneraLiq", windowId + "|genera_liq");     data.processed = vars.getRequiredGlobalVariable("inpprocessed", windowId + "|processed");     data.ldtLiquidacionCabeceraId = vars.getRequestGlobalVariable("inpldtLiquidacionCabeceraId", windowId + "|LDT_Liquidacion_Cabecera_ID");     data.adClientId = vars.getRequiredGlobalVariable("inpadClientId", windowId + "|AD_Client_ID");     data.docstatus = vars.getRequiredGlobalVariable("inpdocstatus", windowId + "|docstatus");     data.bloquear = vars.getRequiredInputGlobalVariable("inpbloquear", windowId + "|bloquear", "N");    try {   data.cantidadCont = vars.getNumericParameter("inpcantidadCont", vars.getSessionValue(windowId + "|cantidad_cont"));  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.fleteCostoDiv = vars.getNumericParameter("inpfleteCostoDiv", vars.getSessionValue(windowId + "|flete_costo_div"));  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.totalFleteCosto = vars.getNumericParameter("inptotalFleteCosto");  } catch (ServletException paramEx) { ex = paramEx; }     data.isactive = vars.getStringParameter("inpisactive", "N"); 
      data.createdby = vars.getUser();
      data.updatedby = vars.getUser();
      data.adUserClient = Utility.getContext(this, vars, "#User_Client", windowId, accesslevel);
      data.adOrgClient = Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel);
      data.updatedTimeStamp = vars.getStringParameter("updatedTimestamp");



    
    

    
    }
    catch(ServletException e) {
    	vars.setEditionData(tabId, data);
    	throw e;
    }
    // Behavior with exception for numeric fields is to catch last one if we have multiple ones
    if(ex != null) {
      vars.setEditionData(tabId, data);
      throw ex;
    }
    return data;
  }




    private void refreshSessionEdit(VariablesSecureApp vars, FieldProvider[] data) {
      if (data==null || data.length==0) return;
          vars.setSessionValue(windowId + "|Condicion", data[0].getField("condicion"));    vars.setSessionValue(windowId + "|LDT_Mbl_ID", data[0].getField("ldtMblId"));    vars.setSessionValue(windowId + "|AD_Org_ID", data[0].getField("adOrgId"));    vars.setSessionValue(windowId + "|datos_liq", data[0].getField("datosLiq"));    vars.setSessionValue(windowId + "|genera_liq", data[0].getField("generaLiq"));    vars.setSessionValue(windowId + "|Processed", data[0].getField("processed"));    vars.setSessionValue(windowId + "|bloquear", data[0].getField("bloquear"));    vars.setSessionValue(windowId + "|LDT_Liquidacion_Cabecera_ID", data[0].getField("ldtLiquidacionCabeceraId"));    vars.setSessionValue(windowId + "|cantidad_cont", data[0].getField("cantidadCont"));    vars.setSessionValue(windowId + "|AD_Client_ID", data[0].getField("adClientId"));    vars.setSessionValue(windowId + "|DocStatus", data[0].getField("docstatus"));    vars.setSessionValue(windowId + "|flete_costo_div", data[0].getField("fleteCostoDiv"));
    }

    private void refreshSessionNew(VariablesSecureApp vars) throws IOException,ServletException {
      LiquidacionesDFDA384C51D24338B9070979CDF44876Data[] data = LiquidacionesDFDA384C51D24338B9070979CDF44876Data.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), vars.getStringParameter("inpldtLiquidacionCabeceraId", ""), Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
      if (data==null || data.length==0) return;
      refreshSessionEdit(vars, data);
    }

  private String nextElement(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (strSelected == null || strSelected.equals("")) return firstElement(vars, tableSQL);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(), 0, 0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.NEXT, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting next element", e);
      }
      if (data!=null) {
        if (data!=null) return data;
      }
    }
    return strSelected;
  }

  private int getKeyPosition(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("getKeyPosition: " + strSelected);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.GETPOSITION, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting key position", e);
      }
      if (data!=null) {
        // split offset -> (page,relativeOffset)
        int absoluteOffset = Integer.valueOf(data);
        int page = absoluteOffset / TableSQLData.maxRowsPerGridPage;
        int relativeOffset = absoluteOffset % TableSQLData.maxRowsPerGridPage;
        log4j.debug("getKeyPosition: absOffset: " + absoluteOffset + "=> page: " + page + " relOffset: " + relativeOffset);
        String currPageKey = tabId + "|" + "currentPage";
        vars.setSessionValue(currPageKey, String.valueOf(page));
        return relativeOffset;
      }
    }
    return 0;
  }

  private String previousElement(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (strSelected == null || strSelected.equals("")) return firstElement(vars, tableSQL);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.PREVIOUS, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting previous element", e);
      }
      if (data!=null) {
        return data;
      }
    }
    return strSelected;
  }

  private String firstElement(VariablesSecureApp vars, TableSQLData tableSQL) throws IOException, ServletException {
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,1);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.FIRST, "", tableSQL.getKeyColumn());

      } catch (Exception e) { 
        log4j.debug("Error getting first element", e);
      }
      if (data!=null) return data;
    }
    return "";
  }

  private String lastElement(VariablesSecureApp vars, TableSQLData tableSQL) throws IOException, ServletException {
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.LAST, "", tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting last element", e);
      }
      if (data!=null) return data;
    }
    return "";
  }

  private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars, String strLDT_Liquidacion_Cabecera_ID, TableSQLData tableSQL)
    throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("Output: dataSheet");

    String strParamldt_mbl_id = vars.getSessionValue(tabId + "|paramldt_mbl_id");

    boolean hasSearchCondition=false;
    vars.removeEditionData(tabId);
    hasSearchCondition = (tableSQL.hasInternalFilter() && ("").equals(strParamldt_mbl_id)) || !(("").equals(strParamldt_mbl_id) || ("%").equals(strParamldt_mbl_id)) ;
    String strOffset = vars.getSessionValue(tabId + "|offset");
    String selectedRow = "0";
    if (!strLDT_Liquidacion_Cabecera_ID.equals("")) {
      selectedRow = Integer.toString(getKeyPosition(vars, strLDT_Liquidacion_Cabecera_ID, tableSQL));
    }

    String[] discard={"isNotFiltered","isNotTest"};
    if (hasSearchCondition) discard[0] = new String("isFiltered");
    if (vars.getSessionValue("#ShowTest", "N").equals("Y")) discard[1] = new String("isTest");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/logisticadetransporte/parametrizacion/MDTLiquidaciones/LiquidacionesDFDA384C51D24338B9070979CDF44876_Relation", discard).createXmlDocument();

    boolean hasReadOnlyAccess = org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId);
    ToolBar toolbar = new ToolBar(this, true, vars.getLanguage(), "LiquidacionesDFDA384C51D24338B9070979CDF44876", false, "document.frmMain.inpldtLiquidacionCabeceraId", "grid", "..", "".equals("Y"), "MDTLiquidaciones", strReplaceWith, false, false, false, false, !hasReadOnlyAccess);
    toolbar.setTabId(tabId);
    
    toolbar.setDeleteable(true && !hasReadOnlyAccess);
    toolbar.prepareRelationTemplate("N".equals("Y"), hasSearchCondition, !vars.getSessionValue("#ShowTest", "N").equals("Y"), false, Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    xmlDocument.setParameter("toolbar", toolbar.toString());



    StringBuffer orderByArray = new StringBuffer();
      vars.setSessionValue(tabId + "|newOrder", "1");
      String positions = vars.getSessionValue(tabId + "|orderbyPositions");
      orderByArray.append("var orderByPositions = new Array(\n");
      if (!positions.equals("")) {
        StringTokenizer tokens=new StringTokenizer(positions, ",");
        boolean firstOrder = true;
        while(tokens.hasMoreTokens()){
          if (!firstOrder) orderByArray.append(",\n");
          orderByArray.append("\"").append(tokens.nextToken()).append("\"");
          firstOrder = false;
        }
      }
      orderByArray.append(");\n");
      String directions = vars.getSessionValue(tabId + "|orderbyDirections");
      orderByArray.append("var orderByDirections = new Array(\n");
      if (!positions.equals("")) {
        StringTokenizer tokens=new StringTokenizer(directions, ",");
        boolean firstOrder = true;
        while(tokens.hasMoreTokens()){
          if (!firstOrder) orderByArray.append(",\n");
          orderByArray.append("\"").append(tokens.nextToken()).append("\"");
          firstOrder = false;
        }
      }
      orderByArray.append(");\n");
//    }

    xmlDocument.setParameter("selectedColumn", "\nvar selectedRow = " + selectedRow + ";\n" + orderByArray.toString());
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("windowId", windowId);
    xmlDocument.setParameter("KeyName", "ldtLiquidacionCabeceraId");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    //xmlDocument.setParameter("buttonReference", Utility.messageBD(this, "Reference", vars.getLanguage()));
    try {
      WindowTabs tabs = new WindowTabs(this, vars, tabId, windowId, false);
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      String hideBackButton = vars.getGlobalVariable("hideMenu", "#Hide_BackButton", "");
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "LiquidacionesDFDA384C51D24338B9070979CDF44876_Relation.html", "MDTLiquidaciones", "W", strReplaceWith, tabs.breadcrumb(), hideBackButton.equals("true"));
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "LiquidacionesDFDA384C51D24338B9070979CDF44876_Relation.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.relationTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    {
      OBError myMessage = vars.getMessage(tabId);
      vars.removeMessage(tabId);
      if (myMessage!=null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }


    xmlDocument.setParameter("grid", Utility.getContext(this, vars, "#RecordRange", windowId));
xmlDocument.setParameter("grid_Offset", strOffset);
xmlDocument.setParameter("grid_SortCols", positions);
xmlDocument.setParameter("grid_SortDirs", directions);
xmlDocument.setParameter("grid_Default", selectedRow);


    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageEdit(HttpServletResponse response, HttpServletRequest request, VariablesSecureApp vars,boolean boolNew, String strLDT_Liquidacion_Cabecera_ID, TableSQLData tableSQL)
    throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("Output: edit");
    
    HashMap<String, String> usedButtonShortCuts;
  
    HashMap<String, String> reservedButtonShortCuts;
  
    usedButtonShortCuts = new HashMap<String, String>();
    
    reservedButtonShortCuts = new HashMap<String, String>();
    
    
    
    String strOrderByFilter = vars.getSessionValue(tabId + "|orderby");
    String orderClause = " 1";
    if (strOrderByFilter==null || strOrderByFilter.equals("")) strOrderByFilter = orderClause;
    /*{
      if (!strOrderByFilter.equals("") && !orderClause.equals("")) strOrderByFilter += ", ";
      strOrderByFilter += orderClause;
    }*/
    
    
    String strCommand = null;
    LiquidacionesDFDA384C51D24338B9070979CDF44876Data[] data=null;
    XmlDocument xmlDocument=null;
    FieldProvider dataField = vars.getEditionData(tabId);
    vars.removeEditionData(tabId);
    String strParamldt_mbl_id = vars.getSessionValue(tabId + "|paramldt_mbl_id");

    boolean hasSearchCondition=false;
    hasSearchCondition = (tableSQL.hasInternalFilter() && ("").equals(strParamldt_mbl_id)) || !(("").equals(strParamldt_mbl_id) || ("%").equals(strParamldt_mbl_id)) ;

       String strParamSessionDate = vars.getGlobalVariable("inpParamSessionDate", Utility.getTransactionalDate(this, vars, windowId), "");
      String buscador = "";
      String[] discard = {"", "isNotTest"};
      
      if (vars.getSessionValue("#ShowTest", "N").equals("Y")) discard[1] = new String("isTest");
    if (dataField==null) {
      if (!boolNew) {
        discard[0] = new String("newDiscard");
        data = LiquidacionesDFDA384C51D24338B9070979CDF44876Data.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), strLDT_Liquidacion_Cabecera_ID, Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
  
        if (!strLDT_Liquidacion_Cabecera_ID.equals("") && (data == null || data.length==0)) {
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
          return;
        }
        refreshSessionEdit(vars, data);
        strCommand = "EDIT";
      }

      if (boolNew || data==null || data.length==0) {
        discard[0] = new String ("editDiscard");
        strCommand = "NEW";
        data = new LiquidacionesDFDA384C51D24338B9070979CDF44876Data[0];
      } else {
        discard[0] = new String ("newDiscard");
      }
    } else {
      if (dataField.getField("ldtLiquidacionCabeceraId") == null || dataField.getField("ldtLiquidacionCabeceraId").equals("")) {
        discard[0] = new String ("editDiscard");
        strCommand = "NEW";
        boolNew = true;
      } else {
        discard[0] = new String ("newDiscard");
        strCommand = "EDIT";
      }
    }
    
    
    
    if (dataField==null) {
      if (boolNew || data==null || data.length==0) {
        refreshSessionNew(vars);
        data = LiquidacionesDFDA384C51D24338B9070979CDF44876Data.set("Y", Utility.getDefault(this, vars, "AD_Org_ID", "", "76E5AFD6A6A8435FB470FBF12625B7AD", "", dataField), Utility.getDefault(this, vars, "genera_liq", "N", "76E5AFD6A6A8435FB470FBF12625B7AD", "N", dataField), Utility.getDefault(this, vars, "total_flete_costo", "0", "76E5AFD6A6A8435FB470FBF12625B7AD", "", dataField), Utility.getDefault(this, vars, "ldt_mbl_id", "", "76E5AFD6A6A8435FB470FBF12625B7AD", "", dataField), Utility.getDefault(this, vars, "processed", "CO", "76E5AFD6A6A8435FB470FBF12625B7AD", "N", dataField), (vars.getLanguage().equals("en_US")?ListData.selectName(this, "1F931E9A5FAB445A8FD2F069ADD2DBE4", Utility.getDefault(this, vars, "processed", "CO", "76E5AFD6A6A8435FB470FBF12625B7AD", "N", dataField)):ListData.selectNameTrl(this, vars.getLanguage(), "1F931E9A5FAB445A8FD2F069ADD2DBE4", Utility.getDefault(this, vars, "processed", "CO", "76E5AFD6A6A8435FB470FBF12625B7AD", "N", dataField))), Utility.getDefault(this, vars, "Updatedby", "", "76E5AFD6A6A8435FB470FBF12625B7AD", "", dataField), LiquidacionesDFDA384C51D24338B9070979CDF44876Data.selectDef474C49D137784BD8B2B1A1CBBC04A92B_0(this, Utility.getDefault(this, vars, "Updatedby", "", "76E5AFD6A6A8435FB470FBF12625B7AD", "", dataField)), Utility.getDefault(this, vars, "ldt_nd_agente", "", "76E5AFD6A6A8435FB470FBF12625B7AD", "", dataField), Utility.getDefault(this, vars, "flete_costo_div", "0", "76E5AFD6A6A8435FB470FBF12625B7AD", "", dataField), Utility.getDefault(this, vars, "condicion", "", "76E5AFD6A6A8435FB470FBF12625B7AD", "", dataField), Utility.getDefault(this, vars, "Createdby", "", "76E5AFD6A6A8435FB470FBF12625B7AD", "", dataField), LiquidacionesDFDA384C51D24338B9070979CDF44876Data.selectDef8E59C22FD0334042B7654F01FF637140_1(this, Utility.getDefault(this, vars, "Createdby", "", "76E5AFD6A6A8435FB470FBF12625B7AD", "", dataField)), Utility.getDefault(this, vars, "cantidad_cont", "0", "76E5AFD6A6A8435FB470FBF12625B7AD", "", dataField), Utility.getDefault(this, vars, "bloquear", "N", "76E5AFD6A6A8435FB470FBF12625B7AD", "N", dataField), Utility.getDefault(this, vars, "AD_Client_ID", "@AD_CLIENT_ID@", "76E5AFD6A6A8435FB470FBF12625B7AD", "", dataField), Utility.getDefault(this, vars, "datos_liq", "N", "76E5AFD6A6A8435FB470FBF12625B7AD", "N", dataField), Utility.getDefault(this, vars, "docstatus", "DR", "76E5AFD6A6A8435FB470FBF12625B7AD", "", dataField), "", Utility.getDefault(this, vars, "ldt_nc_agente", "", "76E5AFD6A6A8435FB470FBF12625B7AD", "", dataField));
        
      }
     }
      
    
    String currentOrg = (boolNew?"":(dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId")));
    if (!currentOrg.equals("") && !currentOrg.startsWith("'")) currentOrg = "'"+currentOrg+"'";
    String currentClient = (boolNew?"":(dataField!=null?dataField.getField("adClientId"):data[0].getField("adClientId")));
    if (!currentClient.equals("") && !currentClient.startsWith("'")) currentClient = "'"+currentClient+"'";
    
    boolean hasReadOnlyAccess = org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId);
    boolean editableTab = (!hasReadOnlyAccess && (currentOrg.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),currentOrg)) && (currentClient.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), currentClient)));
    if (editableTab)
      xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/logisticadetransporte/parametrizacion/MDTLiquidaciones/LiquidacionesDFDA384C51D24338B9070979CDF44876_Edition",discard).createXmlDocument();
    else
      xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/logisticadetransporte/parametrizacion/MDTLiquidaciones/LiquidacionesDFDA384C51D24338B9070979CDF44876_NonEditable",discard).createXmlDocument();

    xmlDocument.setParameter("tabId", tabId);
    ToolBar toolbar = new ToolBar(this, editableTab, vars.getLanguage(), "LiquidacionesDFDA384C51D24338B9070979CDF44876", (strCommand.equals("NEW") || boolNew || (dataField==null && (data==null || data.length==0))), "document.frmMain.inpldtLiquidacionCabeceraId", "", "..", "".equals("Y"), "MDTLiquidaciones", strReplaceWith, true, false, false, Utility.hasTabAttachments(this, vars, tabId, strLDT_Liquidacion_Cabecera_ID), !hasReadOnlyAccess);
    toolbar.setTabId(tabId);
    toolbar.setDeleteable(true);
    toolbar.prepareEditionTemplate("N".equals("Y"), hasSearchCondition, vars.getSessionValue("#ShowTest", "N").equals("Y"), "STD", Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    xmlDocument.setParameter("toolbar", toolbar.toString());

    // set updated timestamp to manage locking mechanism
    if (!boolNew) {
      xmlDocument.setParameter("updatedTimestamp", (dataField != null ? dataField
          .getField("updatedTimeStamp") : data[0].getField("updatedTimeStamp")));
    }
    
    boolean concurrentSave = vars.getSessionValue(tabId + "|concurrentSave").equals("true");
    if (concurrentSave) {
      //after concurrent save error, force autosave
      xmlDocument.setParameter("autosave", "Y");
    } else {
      xmlDocument.setParameter("autosave", "N");
    }
    vars.removeSessionValue(tabId + "|concurrentSave");

    try {
      WindowTabs tabs = new WindowTabs(this, vars, tabId, windowId, true, (strCommand.equalsIgnoreCase("NEW")));
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      // if (!strLDT_Liquidacion_Cabecera_ID.equals("")) xmlDocument.setParameter("childTabContainer", tabs.childTabs(false));
	  // else xmlDocument.setParameter("childTabContainer", tabs.childTabs(true));
	  xmlDocument.setParameter("childTabContainer", tabs.childTabs(false));
	  String hideBackButton = vars.getGlobalVariable("hideMenu", "#Hide_BackButton", "");
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "LiquidacionesDFDA384C51D24338B9070979CDF44876_Relation.html", "MDTLiquidaciones", "W", strReplaceWith, tabs.breadcrumb(), hideBackButton.equals("true"), !concurrentSave);
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "LiquidacionesDFDA384C51D24338B9070979CDF44876_Relation.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.editionTemplate(strCommand.equals("NEW")));
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
		
    
    
    xmlDocument.setParameter("commandType", strCommand);
    xmlDocument.setParameter("buscador",buscador);
    xmlDocument.setParameter("windowId", windowId);
    xmlDocument.setParameter("changed", "");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    final String strMappingName = Utility.getTabURL(tabId, "E", false);
    xmlDocument.setParameter("mappingName", strMappingName);
    xmlDocument.setParameter("confirmOnChanges", Utility.getJSConfirmOnChanges(vars, windowId));
    //xmlDocument.setParameter("buttonReference", Utility.messageBD(this, "Reference", vars.getLanguage()));

    xmlDocument.setParameter("paramSessionDate", strParamSessionDate);

    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    OBError myMessage = vars.getMessage(tabId);
    vars.removeMessage(tabId);
    if (myMessage!=null) {
      xmlDocument.setParameter("messageType", myMessage.getType());
      xmlDocument.setParameter("messageTitle", myMessage.getTitle());
      xmlDocument.setParameter("messageMessage", myMessage.getMessage());
    }
    xmlDocument.setParameter("displayLogic", getDisplayLogicContext(vars, boolNew));
    
    
     if (dataField==null) {
      xmlDocument.setData("structure1",data);
      
    } else {
      
        FieldProvider[] dataAux = new FieldProvider[1];
        dataAux[0] = dataField;
        
        xmlDocument.setData("structure1",dataAux);
      
    }
    
      
   
    try {
      ComboTableData comboTableData = null;
comboTableData = new ComboTableData(vars, this, "17", "condicion", "3F49F47A1C5A422FB113F8C41D747E74", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("condicion"):dataField.getField("condicion")));
xmlDocument.setData("reportcondicion","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
String userOrgList = "";
if (editableTab) 
  userOrgList=Utility.getContext(this, vars, "#User_Org", windowId, accesslevel); //editable record 
else 
  userOrgList=currentOrg;
comboTableData = new ComboTableData(vars, this, "19", "AD_Org_ID", "", "", userOrgList, Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("adOrgId"):dataField.getField("adOrgId")));
xmlDocument.setData("reportAD_Org_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("buttonldt_nd_agente", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonldt_nc_agente", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("datos_liq_BTNname", Utility.getButtonName(this, vars, "D0D6C462EA214F2BBF1776258FDD2D00", "datos_liq_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modaldatos_liq = org.openbravo.erpCommon.utility.Utility.isModalProcess("E2B721F481554903ADB7F8658E76B368"); 
xmlDocument.setParameter("datos_liq_Modal", modaldatos_liq?"true":"false");
xmlDocument.setParameter("genera_liq_BTNname", Utility.getButtonName(this, vars, "B96D6A7A98DF4B4B968077288383391C", "genera_liq_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalgenera_liq = org.openbravo.erpCommon.utility.Utility.isModalProcess("B066D443B4EC4E68A8B9363702AB217B"); 
xmlDocument.setParameter("genera_liq_Modal", modalgenera_liq?"true":"false");
xmlDocument.setParameter("processed_BTNname", Utility.getButtonName(this, vars, "1F931E9A5FAB445A8FD2F069ADD2DBE4", (dataField==null?data[0].getField("processed"):dataField.getField("processed")), "processed_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalprocessed = org.openbravo.erpCommon.utility.Utility.isModalProcess("C1A39A18FF9243A7964AF78580613C42"); 
xmlDocument.setParameter("processed_Modal", modalprocessed?"true":"false");
xmlDocument.setParameter("Created_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Created_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
xmlDocument.setParameter("Updated_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Updated_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
xmlDocument.setParameter("buttontotal_flete_costo", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttoncantidad_cont", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonflete_costo_div", Utility.messageBD(this, "Calc", vars.getLanguage()));
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new ServletException(ex);
    }

    xmlDocument.setParameter("scriptOnLoad", getShortcutScript(usedButtonShortCuts, reservedButtonShortCuts));
    
    final String refererURL = vars.getSessionValue(tabId + "|requestURL");
    vars.removeSessionValue(tabId + "|requestURL");
    if(!refererURL.equals("")) {
    	final Boolean failedAutosave = (Boolean) vars.getSessionObject(tabId + "|failedAutosave");
		vars.removeSessionValue(tabId + "|failedAutosave");
    	if(failedAutosave != null && failedAutosave) {
    		final String jsFunction = "continueUserAction('"+refererURL+"');";
    		xmlDocument.setParameter("failedAutosave", jsFunction);
    	}
    }

    if (strCommand.equalsIgnoreCase("NEW")) {
      vars.removeSessionValue(tabId + "|failedAutosave");
      vars.removeSessionValue(strMappingName + "|hash");
    }

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageButtonFS(HttpServletResponse response, VariablesSecureApp vars, String strProcessId, String path) throws IOException, ServletException {
    log4j.debug("Output: Frames action button");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "org/openbravo/erpCommon/ad_actionButton/ActionButtonDefaultFrames").createXmlDocument();
    xmlDocument.setParameter("processId", strProcessId);
    xmlDocument.setParameter("trlFormType", "PROCESS");
    xmlDocument.setParameter("language", "defaultLang = \"" + vars.getLanguage() + "\";\n");
    xmlDocument.setParameter("type", strDireccion + path);
    out.println(xmlDocument.print());
    out.close();
  }

    private void printPageButtondatos_liqE2B721F481554903ADB7F8658E76B368(HttpServletResponse response, VariablesSecureApp vars, String strLDT_Liquidacion_Cabecera_ID, String strdatosLiq, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process E2B721F481554903ADB7F8658E76B368");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/datos_liqE2B721F481554903ADB7F8658E76B368", discard).createXmlDocument();
      xmlDocument.setParameter("key", strLDT_Liquidacion_Cabecera_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "LiquidacionesDFDA384C51D24338B9070979CDF44876_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "E2B721F481554903ADB7F8658E76B368");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("E2B721F481554903ADB7F8658E76B368");
        vars.removeMessage("E2B721F481554903ADB7F8658E76B368");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }
    private void printPageButtongenera_liqB066D443B4EC4E68A8B9363702AB217B(HttpServletResponse response, VariablesSecureApp vars, String strLDT_Liquidacion_Cabecera_ID, String strgeneraLiq, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process B066D443B4EC4E68A8B9363702AB217B");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/genera_liqB066D443B4EC4E68A8B9363702AB217B", discard).createXmlDocument();
      xmlDocument.setParameter("key", strLDT_Liquidacion_Cabecera_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "LiquidacionesDFDA384C51D24338B9070979CDF44876_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "B066D443B4EC4E68A8B9363702AB217B");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("B066D443B4EC4E68A8B9363702AB217B");
        vars.removeMessage("B066D443B4EC4E68A8B9363702AB217B");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }
    private void printPageButtonprocessedC1A39A18FF9243A7964AF78580613C42(HttpServletResponse response, VariablesSecureApp vars, String strLDT_Liquidacion_Cabecera_ID, String strprocessed, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process C1A39A18FF9243A7964AF78580613C42");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/processedC1A39A18FF9243A7964AF78580613C42", discard).createXmlDocument();
      xmlDocument.setParameter("key", strLDT_Liquidacion_Cabecera_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "LiquidacionesDFDA384C51D24338B9070979CDF44876_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "C1A39A18FF9243A7964AF78580613C42");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("C1A39A18FF9243A7964AF78580613C42");
        vars.removeMessage("C1A39A18FF9243A7964AF78580613C42");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    ComboTableData comboTableData = null;
    xmlDocument.setParameter("AccionLiquidacion", "");
    comboTableData = new ComboTableData(vars, this, "17", "AccionLiquidacion", "1F931E9A5FAB445A8FD2F069ADD2DBE4", "90BA9BF1BDAA4E8985121FFECF6B6DAA", Utility.getContext(this, vars, "#AccessibleOrgTree", ""), Utility.getContext(this, vars, "#User_Client", ""), 0);
    Utility.fillSQLParameters(this, vars, (FieldProvider) vars.getSessionObject("buttonC1A39A18FF9243A7964AF78580613C42.originalParams"), comboTableData, windowId, "");
    xmlDocument.setData("reportAccionLiquidacion", "liststructure", comboTableData.select(false));
comboTableData = null;
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }




    private String getDisplayLogicContext(VariablesSecureApp vars, boolean isNew) throws IOException, ServletException {
      log4j.debug("Output: Display logic context fields");
      String result = "var strShowAudit=\"" +(isNew?"N":Utility.getContext(this, vars, "ShowAudit", windowId)) + "\";\n";
      return result;
    }


    private String getReadOnlyLogicContext(VariablesSecureApp vars) throws IOException, ServletException {
      log4j.debug("Output: Read Only logic context fields");
      String result = "";
      return result;
    }




 
  private String getShortcutScript( HashMap<String, String> usedButtonShortCuts, HashMap<String, String> reservedButtonShortCuts){
    StringBuffer shortcuts = new StringBuffer();
    shortcuts.append(" function buttonListShorcuts() {\n");
    Iterator<String> ik = usedButtonShortCuts.keySet().iterator();
    Iterator<String> iv = usedButtonShortCuts.values().iterator();
    while(ik.hasNext() && iv.hasNext()){
      shortcuts.append("  keyArray[keyArray.length] = new keyArrayItem(\"").append(ik.next()).append("\", \"").append(iv.next()).append("\", null, \"altKey\", false, \"onkeydown\");\n");
    }
    shortcuts.append(" return true;\n}");
    return shortcuts.toString();
  }
  
  private int saveRecord(VariablesSecureApp vars, OBError myError, char type) throws IOException, ServletException {
    LiquidacionesDFDA384C51D24338B9070979CDF44876Data data = null;
    int total = 0;
    if (org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) {
        OBError newError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
        myError.setError(newError);
        vars.setMessage(tabId, myError);
    }
    else {
        Connection con = null;
        try {
            con = this.getTransactionConnection();
            data = getEditVariables(con, vars);
            data.dateTimeFormat = vars.getSessionValue("#AD_SqlDateTimeFormat");            
            String strSequence = "";
            if(type == 'I') {                
        strSequence = SequenceIdData.getUUID();
                if(log4j.isDebugEnabled()) log4j.debug("Sequence: " + strSequence);
                data.ldtLiquidacionCabeceraId = strSequence;  
            }
            if (Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),data.adClientId)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),data.adOrgId)){
		     if(type == 'I') {
		       total = data.insert(con, this);
		     } else {
		       //Check the version of the record we are saving is the one in DB
		       if (LiquidacionesDFDA384C51D24338B9070979CDF44876Data.getCurrentDBTimestamp(this, data.ldtLiquidacionCabeceraId).equals(
                vars.getStringParameter("updatedTimestamp"))) {
                total = data.update(con, this);
               } else {
                 myError.setMessage(Replace.replace(Replace.replace(Utility.messageBD(this,
                    "SavingModifiedRecord", vars.getLanguage()), "\\n", "<br/>"), "&quot;", "\""));
                 myError.setType("Error");
                 vars.setSessionValue(tabId + "|concurrentSave", "true");
               } 
		     }		            
          
            }
                else {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
            myError.setError(newError);            
          }
          releaseCommitConnection(con);
        } catch(Exception ex) {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
            myError.setError(newError);   
            try {
              releaseRollbackConnection(con);
            } catch (final Exception e) { //do nothing 
            }           
        }
            
        if (myError.isEmpty() && total == 0) {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=DBExecuteError");
            myError.setError(newError);
        }
        vars.setMessage(tabId, myError);
            
        if(!myError.isEmpty()){
            if(data != null ) {
                if(type == 'I') {            			
                    data.ldtLiquidacionCabeceraId = "";
                }
                else {                    
                    
                        //BUTTON TEXT FILLING
                    data.processedBtn = ActionButtonDefaultData.getText(this, vars.getLanguage(), "1F931E9A5FAB445A8FD2F069ADD2DBE4", data.getField("processed"));
                    
                }
                vars.setEditionData(tabId, data);
            }            	
        }
        else {
            vars.setSessionValue(windowId + "|LDT_Liquidacion_Cabecera_ID", data.ldtLiquidacionCabeceraId);
        }
    }
    return total;
  }

  public String getServletInfo() {
    return "Servlet LiquidacionesDFDA384C51D24338B9070979CDF44876. This Servlet was made by Wad constructor";
  } // End of getServletInfo() method
}
