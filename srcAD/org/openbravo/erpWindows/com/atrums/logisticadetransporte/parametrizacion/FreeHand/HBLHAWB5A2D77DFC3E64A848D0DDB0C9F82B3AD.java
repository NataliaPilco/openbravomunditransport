
package org.openbravo.erpWindows.com.atrums.logisticadetransporte.parametrizacion.FreeHand;


import org.openbravo.erpCommon.reference.*;



import org.codehaus.jettison.json.JSONObject;
import org.openbravo.erpCommon.utility.*;
import org.openbravo.data.FieldProvider;
import org.openbravo.utils.FormatUtilities;
import org.openbravo.utils.Replace;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.exception.OBException;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessRunner;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.xmlEngine.XmlDocument;
import java.util.Vector;
import java.util.StringTokenizer;
import org.openbravo.database.SessionInfo;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import java.sql.Connection;
import org.apache.log4j.Logger;

public class HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;
  
  private static Logger log4j = Logger.getLogger(HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD.class);
  
  private static final String windowId = "53C75C9421F94C609CDC00C4BED0E7CD";
  private static final String tabId = "5A2D77DFC3E64A848D0DDB0C9F82B3AD";
  private static final String defaultTabView = "EDIT";
  private static final int accesslevel = 3;
  private static final String moduleId = "C5BE74383A264BF68316D6BF975E6EFC";
  
  @Override
  public void init(ServletConfig config) {
    setClassInfo("W", tabId, moduleId);
    super.init(config);
  }
  
  
  @Override
  public void service(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    String command = vars.getCommand();
    
    boolean securedProcess = false;
    if (command.contains("BUTTON")) {
      try {
        securedProcess = "Y".equals(org.openbravo.erpCommon.businessUtility.Preferences
            .getPreferenceValue("SecuredProcess", true, vars.getClient(), vars.getOrg(), vars
                .getUser(), vars.getRole(), windowId));
      } catch (PropertyException e) {
      }
    
     
      if (command.contains("48BC2C143FE34BD1B63A5C46829749A2")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("48BC2C143FE34BD1B63A5C46829749A2");
        SessionInfo.setModuleId("C5BE74383A264BF68316D6BF975E6EFC");
        if (securedProcess) {
          classInfo.type = "P";
          classInfo.id = "48BC2C143FE34BD1B63A5C46829749A2";
        }
      }
     
     
    }
    if (!securedProcess) {
      setClassInfo("W", tabId, moduleId);
    }
    super.service(request, response);
  }
  

  public void doPost (HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException {
    TableSQLData tableSQL = null;
    VariablesSecureApp vars = new VariablesSecureApp(request);
    Boolean saveRequest = (Boolean) request.getAttribute("autosave");
    
    if(saveRequest != null && saveRequest){
      String currentOrg = vars.getStringParameter("inpadOrgId");
      String currentClient = vars.getStringParameter("inpadClientId");
      boolean editableTab = (!org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)
                            && (currentOrg.equals("") || Utility.isElementInList(Utility.getContext(this, vars,"#User_Org", windowId, accesslevel), currentOrg)) 
                            && (currentClient.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),currentClient)));
    
        OBError myError = new OBError();
        String commandType = request.getParameter("inpCommandType");
        String strldtHblId = request.getParameter("inpldtHblId");
         String strPLDT_Routing_Order_ID = vars.getGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID");
        if (editableTab) {
          int total = 0;
          
          if(commandType.equalsIgnoreCase("EDIT") && !strldtHblId.equals(""))
              total = saveRecord(vars, myError, 'U', strPLDT_Routing_Order_ID);
          else
              total = saveRecord(vars, myError, 'I', strPLDT_Routing_Order_ID);
          
          if (!myError.isEmpty() && total == 0)     
            throw new OBException(myError.getMessage());
        }
        vars.setSessionValue(request.getParameter("mappingName") +"|hash", vars.getPostDataHash());
        vars.setSessionValue(tabId + "|Header.view", "EDIT");
        
        return;
    }
    
    try {
      tableSQL = new TableSQLData(vars, this, tabId, Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel), Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    String strOrderBy = vars.getSessionValue(tabId + "|orderby");
    if (!strOrderBy.equals("")) {
      vars.setSessionValue(tabId + "|newOrder", "1");
    }

    if (vars.commandIn("DEFAULT")) {
      String strPLDT_Routing_Order_ID = vars.getGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID", "");

      String strLDT_Hbl_ID = vars.getGlobalVariable("inpldtHblId", windowId + "|LDT_Hbl_ID", "");
            if (strPLDT_Routing_Order_ID.equals("")) {
        strPLDT_Routing_Order_ID = getParentID(vars, strLDT_Hbl_ID);
        if (strPLDT_Routing_Order_ID.equals("")) throw new ServletException("Required parameter :" + windowId + "|LDT_Routing_Order_ID");
        vars.setSessionValue(windowId + "|LDT_Routing_Order_ID", strPLDT_Routing_Order_ID);

        refreshParentSession(vars, strPLDT_Routing_Order_ID);
      }


      String strView = vars.getSessionValue(tabId + "|HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD.view");
      if (strView.equals("")) {
        strView = defaultTabView;

        if (strView.equals("EDIT")) {
          if (strLDT_Hbl_ID.equals("")) strLDT_Hbl_ID = firstElement(vars, tableSQL);
          if (strLDT_Hbl_ID.equals("")) strView = "RELATION";
        }
      }
      if (strView.equals("EDIT")) 

        printPageEdit(response, request, vars, false, strLDT_Hbl_ID, strPLDT_Routing_Order_ID, tableSQL);

      else printPageDataSheet(response, vars, strPLDT_Routing_Order_ID, strLDT_Hbl_ID, tableSQL);
    } else if (vars.commandIn("DIRECT")) {
      String strLDT_Hbl_ID = vars.getStringParameter("inpDirectKey");
      
        
      if (strLDT_Hbl_ID.equals("")) strLDT_Hbl_ID = vars.getRequiredGlobalVariable("inpldtHblId", windowId + "|LDT_Hbl_ID");
      else vars.setSessionValue(windowId + "|LDT_Hbl_ID", strLDT_Hbl_ID);
      
      
      String strPLDT_Routing_Order_ID = getParentID(vars, strLDT_Hbl_ID);
      
      vars.setSessionValue(windowId + "|LDT_Routing_Order_ID", strPLDT_Routing_Order_ID);
      vars.setSessionValue("873BB22017634E47876049F4C5489617|Free Hand.view", "EDIT");

      refreshParentSession(vars, strPLDT_Routing_Order_ID);

      vars.setSessionValue(tabId + "|HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD.view", "EDIT");

      printPageEdit(response, request, vars, false, strLDT_Hbl_ID, strPLDT_Routing_Order_ID, tableSQL);

    } else if (vars.commandIn("TAB")) {
      String strPLDT_Routing_Order_ID = vars.getGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID", false, false, true, "");
      vars.removeSessionValue(windowId + "|LDT_Hbl_ID");
      refreshParentSession(vars, strPLDT_Routing_Order_ID);


      String strView = vars.getSessionValue(tabId + "|HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD.view");
      String strLDT_Hbl_ID = "";
      if (strView.equals("")) {
        strView = defaultTabView;
        if (strView.equals("EDIT")) {
          strLDT_Hbl_ID = firstElement(vars, tableSQL);
          if (strLDT_Hbl_ID.equals("")) strView = "RELATION";
        }
      }
      if (strView.equals("EDIT")) {

        if (strLDT_Hbl_ID.equals("")) strLDT_Hbl_ID = firstElement(vars, tableSQL);
        printPageEdit(response, request, vars, false, strLDT_Hbl_ID, strPLDT_Routing_Order_ID, tableSQL);

      } else printPageDataSheet(response, vars, strPLDT_Routing_Order_ID, "", tableSQL);
    } else if (vars.commandIn("SEARCH")) {
vars.getRequestGlobalVariable("inpParamNUM_Hbl", tabId + "|paramNUM_Hbl");
vars.getRequestGlobalVariable("inpParamC_Bpartner_ID", tabId + "|paramC_Bpartner_ID");

        vars.getRequestGlobalVariable("inpParamUpdated", tabId + "|paramUpdated");
        vars.getRequestGlobalVariable("inpParamUpdatedBy", tabId + "|paramUpdatedBy");
        vars.getRequestGlobalVariable("inpParamCreated", tabId + "|paramCreated");
        vars.getRequestGlobalVariable("inpparamCreatedBy", tabId + "|paramCreatedBy");
            String strPLDT_Routing_Order_ID = vars.getGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID");

      
      vars.removeSessionValue(windowId + "|LDT_Hbl_ID");
      String strLDT_Hbl_ID="";

      String strView = vars.getSessionValue(tabId + "|HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD.view");
      if (strView.equals("")) strView=defaultTabView;

      if (strView.equals("EDIT")) {
        strLDT_Hbl_ID = firstElement(vars, tableSQL);
        if (strLDT_Hbl_ID.equals("")) {
          // filter returns empty set
          strView = "RELATION";
          // switch to grid permanently until the user changes the view again
          vars.setSessionValue(tabId + "|HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD.view", strView);
        }
      }

      if (strView.equals("EDIT")) 

        printPageEdit(response, request, vars, false, strLDT_Hbl_ID, strPLDT_Routing_Order_ID, tableSQL);

      else printPageDataSheet(response, vars, strPLDT_Routing_Order_ID, strLDT_Hbl_ID, tableSQL);
    } else if (vars.commandIn("RELATION")) {
            String strPLDT_Routing_Order_ID = vars.getGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID");
      

      String strLDT_Hbl_ID = vars.getGlobalVariable("inpldtHblId", windowId + "|LDT_Hbl_ID", "");
      vars.setSessionValue(tabId + "|HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD.view", "RELATION");
      printPageDataSheet(response, vars, strPLDT_Routing_Order_ID, strLDT_Hbl_ID, tableSQL);
    } else if (vars.commandIn("NEW")) {
      String strPLDT_Routing_Order_ID = vars.getGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID");


      printPageEdit(response, request, vars, true, "", strPLDT_Routing_Order_ID, tableSQL);

    } else if (vars.commandIn("EDIT")) {
      String strPLDT_Routing_Order_ID = vars.getGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID");

      @SuppressWarnings("unused") // In Expense Invoice tab this variable is not used, to be fixed
      String strLDT_Hbl_ID = vars.getGlobalVariable("inpldtHblId", windowId + "|LDT_Hbl_ID", "");
      vars.setSessionValue(tabId + "|HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD.view", "EDIT");

      setHistoryCommand(request, "EDIT");
      printPageEdit(response, request, vars, false, strLDT_Hbl_ID, strPLDT_Routing_Order_ID, tableSQL);

    } else if (vars.commandIn("NEXT")) {
      String strPLDT_Routing_Order_ID = vars.getGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID");
      String strLDT_Hbl_ID = vars.getRequiredStringParameter("inpldtHblId");
      
      String strNext = nextElement(vars, strLDT_Hbl_ID, tableSQL);

      printPageEdit(response, request, vars, false, strNext, strPLDT_Routing_Order_ID, tableSQL);
    } else if (vars.commandIn("PREVIOUS")) {
      String strPLDT_Routing_Order_ID = vars.getGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID");
      String strLDT_Hbl_ID = vars.getRequiredStringParameter("inpldtHblId");
      
      String strPrevious = previousElement(vars, strLDT_Hbl_ID, tableSQL);

      printPageEdit(response, request, vars, false, strPrevious, strPLDT_Routing_Order_ID, tableSQL);
    } else if (vars.commandIn("FIRST_RELATION")) {
vars.getGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID");

      vars.setSessionValue(tabId + "|HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD.initRecordNumber", "0");
      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("PREVIOUS_RELATION")) {
      String strPLDT_Routing_Order_ID = vars.getGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID");

      String strInitRecord = vars.getSessionValue(tabId + "|HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD.initRecordNumber");
      String strRecordRange = Utility.getContext(this, vars, "#RecordRange", windowId);
      int intRecordRange = strRecordRange.equals("")?0:Integer.parseInt(strRecordRange);
      if (strInitRecord.equals("") || strInitRecord.equals("0")) {
        vars.setSessionValue(tabId + "|HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD.initRecordNumber", "0");
      } else {
        int initRecord = (strInitRecord.equals("")?0:Integer.parseInt(strInitRecord));
        initRecord -= intRecordRange;
        strInitRecord = ((initRecord<0)?"0":Integer.toString(initRecord));
        vars.setSessionValue(tabId + "|HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD.initRecordNumber", strInitRecord);
      }
      vars.removeSessionValue(windowId + "|LDT_Hbl_ID");
      vars.setSessionValue(windowId + "|LDT_Routing_Order_ID", strPLDT_Routing_Order_ID);
      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("NEXT_RELATION")) {
      String strPLDT_Routing_Order_ID = vars.getGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID");

      String strInitRecord = vars.getSessionValue(tabId + "|HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD.initRecordNumber");
      String strRecordRange = Utility.getContext(this, vars, "#RecordRange", windowId);
      int intRecordRange = strRecordRange.equals("")?0:Integer.parseInt(strRecordRange);
      int initRecord = (strInitRecord.equals("")?0:Integer.parseInt(strInitRecord));
      if (initRecord==0) initRecord=1;
      initRecord += intRecordRange;
      strInitRecord = ((initRecord<0)?"0":Integer.toString(initRecord));
      vars.setSessionValue(tabId + "|HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD.initRecordNumber", strInitRecord);
      vars.removeSessionValue(windowId + "|LDT_Hbl_ID");
      vars.setSessionValue(windowId + "|LDT_Routing_Order_ID", strPLDT_Routing_Order_ID);
      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("FIRST")) {
      String strPLDT_Routing_Order_ID = vars.getGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID");
      
      String strFirst = firstElement(vars, tableSQL);

      printPageEdit(response, request, vars, false, strFirst, strPLDT_Routing_Order_ID, tableSQL);
    } else if (vars.commandIn("LAST_RELATION")) {
      String strPLDT_Routing_Order_ID = vars.getGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID");

      String strLast = lastElement(vars, tableSQL);
      printPageDataSheet(response, vars, strPLDT_Routing_Order_ID, strLast, tableSQL);
    } else if (vars.commandIn("LAST")) {
      String strPLDT_Routing_Order_ID = vars.getGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID");
      
      String strLast = lastElement(vars, tableSQL);

      printPageEdit(response, request, vars, false, strLast, strPLDT_Routing_Order_ID, tableSQL);
    } else if (vars.commandIn("SAVE_NEW_RELATION", "SAVE_NEW_NEW", "SAVE_NEW_EDIT")) {
      String strPLDT_Routing_Order_ID = vars.getGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID");
      OBError myError = new OBError();      
      int total = saveRecord(vars, myError, 'I', strPLDT_Routing_Order_ID);      
      if (!myError.isEmpty()) {        
        response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
      } 
      else {
		if (myError.isEmpty()) {
		  myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsInserted");
		  myError.setMessage(total + " " + myError.getMessage());
		  vars.setMessage(tabId, myError);
		}        
        if (vars.commandIn("SAVE_NEW_NEW")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
        else if (vars.commandIn("SAVE_NEW_EDIT")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        else response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
      }
    } else if (vars.commandIn("SAVE_EDIT_RELATION", "SAVE_EDIT_NEW", "SAVE_EDIT_EDIT", "SAVE_EDIT_NEXT")) {
      String strPLDT_Routing_Order_ID = vars.getGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID");
      String strLDT_Hbl_ID = vars.getRequiredGlobalVariable("inpldtHblId", windowId + "|LDT_Hbl_ID");
      OBError myError = new OBError();
      int total = saveRecord(vars, myError, 'U', strPLDT_Routing_Order_ID);      
      if (!myError.isEmpty()) {
        response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
      } 
      else {
        if (myError.isEmpty()) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsUpdated");
          myError.setMessage(total + " " + myError.getMessage());
          vars.setMessage(tabId, myError);
        }
        if (vars.commandIn("SAVE_EDIT_NEW")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
        else if (vars.commandIn("SAVE_EDIT_EDIT")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        else if (vars.commandIn("SAVE_EDIT_NEXT")) {
          String strNext = nextElement(vars, strLDT_Hbl_ID, tableSQL);
          vars.setSessionValue(windowId + "|LDT_Hbl_ID", strNext);
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        } else response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
      }
    } else if (vars.commandIn("DELETE")) {
      String strPLDT_Routing_Order_ID = vars.getGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID");

      String strLDT_Hbl_ID = vars.getRequiredStringParameter("inpldtHblId");
      //HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData data = getEditVariables(vars, strPLDT_Routing_Order_ID);
      int total = 0;
      OBError myError = null;
      if (org.openbravo.erpCommon.utility.WindowAccessData.hasNotDeleteAccess(this, vars.getRole(), tabId)) {
        myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
        vars.setMessage(tabId, myError);
      } else {
        try {
          total = HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.delete(this, strLDT_Hbl_ID, strPLDT_Routing_Order_ID, Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), Utility.getContext(this, vars, "#User_Org", windowId, accesslevel));
        } catch(ServletException ex) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myError.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myError);
        }
        if (myError==null && total==0) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
        }
        vars.removeSessionValue(windowId + "|ldtHblId");
        vars.setSessionValue(tabId + "|HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD.view", "RELATION");
      }
      if (myError==null) {
        myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsDeleted");
        myError.setMessage(total + " " + myError.getMessage());
        vars.setMessage(tabId, myError);
      }
      response.sendRedirect(strDireccion + request.getServletPath());

     } else if (vars.commandIn("BUTTONFactura48BC2C143FE34BD1B63A5C46829749A2")) {
        vars.setSessionValue("button48BC2C143FE34BD1B63A5C46829749A2.strfactura", vars.getStringParameter("inpfactura"));
        vars.setSessionValue("button48BC2C143FE34BD1B63A5C46829749A2.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("button48BC2C143FE34BD1B63A5C46829749A2.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("button48BC2C143FE34BD1B63A5C46829749A2.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        
        
        //Save in session needed params for combos if needed
        vars.setSessionObject("button48BC2C143FE34BD1B63A5C46829749A2.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "48BC2C143FE34BD1B63A5C46829749A2", request.getServletPath());    
     } else if (vars.commandIn("BUTTON48BC2C143FE34BD1B63A5C46829749A2")) {
        String strLDT_Hbl_ID = vars.getGlobalVariable("inpldtHblId", windowId + "|LDT_Hbl_ID", "");
        String strfactura = vars.getSessionValue("button48BC2C143FE34BD1B63A5C46829749A2.strfactura");
        String strProcessing = vars.getSessionValue("button48BC2C143FE34BD1B63A5C46829749A2.strProcessing");
        String strOrg = vars.getSessionValue("button48BC2C143FE34BD1B63A5C46829749A2.strOrg");
        String strClient = vars.getSessionValue("button48BC2C143FE34BD1B63A5C46829749A2.strClient");
        
        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtonFactura48BC2C143FE34BD1B63A5C46829749A2(response, vars, strLDT_Hbl_ID, strfactura, strProcessing);
        }


    } else if (vars.commandIn("SAVE_BUTTONFactura48BC2C143FE34BD1B63A5C46829749A2")) {
        String strLDT_Hbl_ID = vars.getGlobalVariable("inpKey", windowId + "|LDT_Hbl_ID", "");
        @SuppressWarnings("unused")
        String strfactura = vars.getStringParameter("inpfactura");
        String strProcessing = vars.getStringParameter("inpprocessing");
        OBError myMessage = null;
        try {
          String pinstance = SequenceIdData.getUUID();
          PInstanceProcessData.insertPInstance(this, pinstance, "48BC2C143FE34BD1B63A5C46829749A2", (("LDT_Hbl_ID".equalsIgnoreCase("AD_Language"))?"0":strLDT_Hbl_ID), strProcessing, vars.getUser(), vars.getClient(), vars.getOrg());
          
          
          ProcessBundle bundle = ProcessBundle.pinstance(pinstance, vars, this);
          new ProcessRunner(bundle).execute(this);
          
          PInstanceProcessData[] pinstanceData = PInstanceProcessData.select(this, pinstance);
          myMessage = Utility.getProcessInstanceMessage(this, vars, pinstanceData);
        } catch (ServletException ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);






    } else if (vars.commandIn("SAVE_XHR")) {
      String strPLDT_Routing_Order_ID = vars.getGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID");
      OBError myError = new OBError();
      JSONObject result = new JSONObject();
      String commandType = vars.getStringParameter("inpCommandType");
      char saveType = "NEW".equals(commandType) ? 'I' : 'U';
      try {
        int total = saveRecord(vars, myError, saveType, strPLDT_Routing_Order_ID);
        if (myError.isEmpty()) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsUpdated");
          myError.setMessage(total + " " + myError.getMessage());
          myError.setType("Success");
        }
        result.put("oberror", myError.toMap());
        result.put("tabid", vars.getStringParameter("tabID"));
        result.put("redirect", strDireccion + request.getServletPath() + "?Command=" + commandType);
      } catch (Exception e) {
        log4j.error("Error saving record (XHR request): " + e.getMessage(), e);
        myError.setType("Error");
        myError.setMessage(e.getMessage());
      }

      response.setContentType("application/json");
      PrintWriter out = response.getWriter();
      out.print(result.toString());
      out.flush();
      out.close();
    } else if (vars.getCommand().toUpperCase().startsWith("BUTTON") || vars.getCommand().toUpperCase().startsWith("SAVE_BUTTON")) {
      pageErrorPopUp(response);
    } else pageError(response);
  }
  private HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData getEditVariables(Connection con, VariablesSecureApp vars, String strPLDT_Routing_Order_ID) throws IOException,ServletException {
    HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData data = new HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData();
    ServletException ex = null;
    try {
    data.numHbl = vars.getRequiredStringParameter("inpnumHbl");     data.ldtRoutingOrderId = vars.getRequiredGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID");     data.secuencialhblHawb = vars.getStringParameter("inpsecuencialhblHawb");     data.cBpartnerId = vars.getRequiredStringParameter("inpcBpartnerId");     data.cBpartnerIdr = vars.getStringParameter("inpcBpartnerId_R");     data.isactive = vars.getStringParameter("inpisactive", "N");    try {   data.valorFlete = vars.getNumericParameter("inpvalorFlete");  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.fletePp = vars.getNumericParameter("inpfletePp");  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.valorFleteEur = vars.getNumericParameter("inpvalorFleteEur");  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.fletePpEur = vars.getNumericParameter("inpfletePpEur");  } catch (ServletException paramEx) { ex = paramEx; }     data.calcular = vars.getRequiredInputGlobalVariable("inpcalcular", windowId + "|calcular", "N");    try {   data.valorTotalUsd = vars.getNumericParameter("inpvalorTotalUsd");  } catch (ServletException paramEx) { ex = paramEx; }     data.factura = vars.getRequiredGlobalVariable("inpfactura", windowId + "|Factura");     data.cartas = vars.getStringParameter("inpcartas");     data.avisoarribo = vars.getStringParameter("inpavisoarribo");     data.ldtHblId = vars.getRequestGlobalVariable("inpldtHblId", windowId + "|LDT_Hbl_ID");     data.adClientId = vars.getRequiredGlobalVariable("inpadClientId", windowId + "|AD_Client_ID");     data.adOrgId = vars.getRequiredGlobalVariable("inpadOrgId", windowId + "|AD_Org_ID");     data.cInvoiceId = vars.getStringParameter("inpcInvoiceId");     data.lftFh = vars.getRequiredInputGlobalVariable("inplftFh", windowId + "|LFT_Fh", "N");     data.estado = vars.getRequiredGlobalVariable("inpestado", windowId + "|Estado");     data.processed = vars.getRequiredGlobalVariable("inpprocessed", windowId + "|Processed"); 
      data.createdby = vars.getUser();
      data.updatedby = vars.getUser();
      data.adUserClient = Utility.getContext(this, vars, "#User_Client", windowId, accesslevel);
      data.adOrgClient = Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel);
      data.updatedTimeStamp = vars.getStringParameter("updatedTimestamp");

      data.ldtRoutingOrderId = vars.getGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID");


    
    

    
    }
    catch(ServletException e) {
    	vars.setEditionData(tabId, data);
    	throw e;
    }
    // Behavior with exception for numeric fields is to catch last one if we have multiple ones
    if(ex != null) {
      vars.setEditionData(tabId, data);
      throw ex;
    }
    return data;
  }


  private void refreshParentSession(VariablesSecureApp vars, String strPLDT_Routing_Order_ID) throws IOException,ServletException {
      
      FreeHand873BB22017634E47876049F4C5489617Data[] data = FreeHand873BB22017634E47876049F4C5489617Data.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), strPLDT_Routing_Order_ID, Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
      if (data==null || data.length==0) return;
          vars.setSessionValue(windowId + "|AD_Org_ID", data[0].adOrgId);    vars.setSessionValue(windowId + "|Estado", data[0].estado);    vars.setSessionValue(windowId + "|Puerto_Terminal_Origen_ID", data[0].puertoTerminalOrigenId);    vars.setSessionValue(windowId + "|Puerto_Terminal_Destino_ID", data[0].puertoTerminalDestinoId);    vars.setSessionValue(windowId + "|c_bpartner_mt_id", data[0].cBpartnerMtId);    vars.setSessionValue(windowId + "|LDT_Contrato_Puerto_ID", data[0].ldtContratoPuertoId);    vars.setSessionValue(windowId + "|Vendedor_ID", data[0].vendedorId);    vars.setSessionValue(windowId + "|M_PriceList_Version_ID", data[0].mPricelistVersionId);    vars.setSessionValue(windowId + "|C_BPartner_ID", data[0].cBpartnerId);    vars.setSessionValue(windowId + "|C_BPartner_Location_ID", data[0].cBpartnerLocationId);    vars.setSessionValue(windowId + "|Cliente_ID", data[0].clienteId);    vars.setSessionValue(windowId + "|Modalidad", data[0].modalidad);    vars.setSessionValue(windowId + "|Tipo", data[0].tipo);    vars.setSessionValue(windowId + "|C_Incoterms_ID", data[0].cIncotermsId);    vars.setSessionValue(windowId + "|Agente_ID", data[0].agenteId);    vars.setSessionValue(windowId + "|Costos_Locales", data[0].costosLocales);    vars.setSessionValue(windowId + "|costos_locales_eur", data[0].costosLocalesEur);    vars.setSessionValue(windowId + "|total_negociado_eur", data[0].totalNegociadoEur);    vars.setSessionValue(windowId + "|Embarque", data[0].embarque);    vars.setSessionValue(windowId + "|ES_Fin_Embarque", data[0].esFinEmbarque);    vars.setSessionValue(windowId + "|Tarifa_Venta", data[0].tarifaVenta);    vars.setSessionValue(windowId + "|C_Country_ID", data[0].cCountryId);    vars.setSessionValue(windowId + "|M_PriceList_ID", data[0].mPricelistId);    vars.setSessionValue(windowId + "|AD_Client_ID", data[0].adClientId);    vars.setSessionValue(windowId + "|LDT_Routing_Order_ID", data[0].ldtRoutingOrderId);
      vars.setSessionValue(windowId + "|LDT_Routing_Order_ID", strPLDT_Routing_Order_ID); //to ensure key parent is set for EM_* cols

      FieldProvider dataField = null; // Define this so that auxiliar inputs using SQL will work
      
  }
  
  
  private String getParentID(VariablesSecureApp vars, String strLDT_Hbl_ID) throws ServletException {
    String strPLDT_Routing_Order_ID = HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.selectParentID(this, strLDT_Hbl_ID);
    if (strPLDT_Routing_Order_ID.equals("")) {
      log4j.error("Parent record not found for id: " + strLDT_Hbl_ID);
      throw new ServletException("Parent record not found");
    }
    return strPLDT_Routing_Order_ID;
  }

    private void refreshSessionEdit(VariablesSecureApp vars, FieldProvider[] data) {
      if (data==null || data.length==0) return;
          vars.setSessionValue(windowId + "|LDT_Routing_Order_ID", data[0].getField("ldtRoutingOrderId"));    vars.setSessionValue(windowId + "|calcular", data[0].getField("calcular"));    vars.setSessionValue(windowId + "|Factura", data[0].getField("factura"));    vars.setSessionValue(windowId + "|AD_Org_ID", data[0].getField("adOrgId"));    vars.setSessionValue(windowId + "|LFT_Fh", data[0].getField("lftFh"));    vars.setSessionValue(windowId + "|Estado", data[0].getField("estado"));    vars.setSessionValue(windowId + "|Processed", data[0].getField("processed"));    vars.setSessionValue(windowId + "|AD_Client_ID", data[0].getField("adClientId"));    vars.setSessionValue(windowId + "|LDT_Hbl_ID", data[0].getField("ldtHblId"));
    }

    private void refreshSessionNew(VariablesSecureApp vars, String strPLDT_Routing_Order_ID) throws IOException,ServletException {
      HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[] data = HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), strPLDT_Routing_Order_ID, vars.getStringParameter("inpldtHblId", ""), Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
      if (data==null || data.length==0) return;
      refreshSessionEdit(vars, data);
    }

  private String nextElement(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (strSelected == null || strSelected.equals("")) return firstElement(vars, tableSQL);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(), 0, 0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.NEXT, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting next element", e);
      }
      if (data!=null) {
        if (data!=null) return data;
      }
    }
    return strSelected;
  }

  private int getKeyPosition(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("getKeyPosition: " + strSelected);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.GETPOSITION, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting key position", e);
      }
      if (data!=null) {
        // split offset -> (page,relativeOffset)
        int absoluteOffset = Integer.valueOf(data);
        int page = absoluteOffset / TableSQLData.maxRowsPerGridPage;
        int relativeOffset = absoluteOffset % TableSQLData.maxRowsPerGridPage;
        log4j.debug("getKeyPosition: absOffset: " + absoluteOffset + "=> page: " + page + " relOffset: " + relativeOffset);
        String currPageKey = tabId + "|" + "currentPage";
        vars.setSessionValue(currPageKey, String.valueOf(page));
        return relativeOffset;
      }
    }
    return 0;
  }

  private String previousElement(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (strSelected == null || strSelected.equals("")) return firstElement(vars, tableSQL);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.PREVIOUS, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting previous element", e);
      }
      if (data!=null) {
        return data;
      }
    }
    return strSelected;
  }

  private String firstElement(VariablesSecureApp vars, TableSQLData tableSQL) throws IOException, ServletException {
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,1);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.FIRST, "", tableSQL.getKeyColumn());

      } catch (Exception e) { 
        log4j.debug("Error getting first element", e);
      }
      if (data!=null) return data;
    }
    return "";
  }

  private String lastElement(VariablesSecureApp vars, TableSQLData tableSQL) throws IOException, ServletException {
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.LAST, "", tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting last element", e);
      }
      if (data!=null) return data;
    }
    return "";
  }

  private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars, String strPLDT_Routing_Order_ID, String strLDT_Hbl_ID, TableSQLData tableSQL)
    throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("Output: dataSheet");

    String strParamNUM_Hbl = vars.getSessionValue(tabId + "|paramNUM_Hbl");
String strParamC_Bpartner_ID = vars.getSessionValue(tabId + "|paramC_Bpartner_ID");

    boolean hasSearchCondition=false;
    vars.removeEditionData(tabId);
    hasSearchCondition = (tableSQL.hasInternalFilter() && ("").equals(strParamNUM_Hbl) && ("").equals(strParamC_Bpartner_ID)) || !(("").equals(strParamNUM_Hbl) || ("%").equals(strParamNUM_Hbl))  || !(("").equals(strParamC_Bpartner_ID) || ("%").equals(strParamC_Bpartner_ID)) ;
    String strOffset = vars.getSessionValue(tabId + "|offset");
    String selectedRow = "0";
    if (!strLDT_Hbl_ID.equals("")) {
      selectedRow = Integer.toString(getKeyPosition(vars, strLDT_Hbl_ID, tableSQL));
    }

    String[] discard={"isNotFiltered","isNotTest"};
    if (hasSearchCondition) discard[0] = new String("isFiltered");
    if (vars.getSessionValue("#ShowTest", "N").equals("Y")) discard[1] = new String("isTest");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/logisticadetransporte/parametrizacion/FreeHand/HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD_Relation", discard).createXmlDocument();

    boolean hasReadOnlyAccess = org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId);
    ToolBar toolbar = new ToolBar(this, true, vars.getLanguage(), "HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD", false, "document.frmMain.inpldtHblId", "grid", "..", "".equals("Y"), "FreeHand", strReplaceWith, false, false, false, false, !hasReadOnlyAccess);
    toolbar.setTabId(tabId);
    
    toolbar.setDeleteable(true && !hasReadOnlyAccess);
    toolbar.prepareRelationTemplate("N".equals("Y"), hasSearchCondition, !vars.getSessionValue("#ShowTest", "N").equals("Y"), false, Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    xmlDocument.setParameter("toolbar", toolbar.toString());

    xmlDocument.setParameter("keyParent", strPLDT_Routing_Order_ID);
    xmlDocument.setParameter("parentFieldName", Utility.getFieldName("F49F5A04FD3147EBA77B19C8F7366F3F", vars.getLanguage()));


    StringBuffer orderByArray = new StringBuffer();
      vars.setSessionValue(tabId + "|newOrder", "1");
      String positions = vars.getSessionValue(tabId + "|orderbyPositions");
      orderByArray.append("var orderByPositions = new Array(\n");
      if (!positions.equals("")) {
        StringTokenizer tokens=new StringTokenizer(positions, ",");
        boolean firstOrder = true;
        while(tokens.hasMoreTokens()){
          if (!firstOrder) orderByArray.append(",\n");
          orderByArray.append("\"").append(tokens.nextToken()).append("\"");
          firstOrder = false;
        }
      }
      orderByArray.append(");\n");
      String directions = vars.getSessionValue(tabId + "|orderbyDirections");
      orderByArray.append("var orderByDirections = new Array(\n");
      if (!positions.equals("")) {
        StringTokenizer tokens=new StringTokenizer(directions, ",");
        boolean firstOrder = true;
        while(tokens.hasMoreTokens()){
          if (!firstOrder) orderByArray.append(",\n");
          orderByArray.append("\"").append(tokens.nextToken()).append("\"");
          firstOrder = false;
        }
      }
      orderByArray.append(");\n");
//    }

    xmlDocument.setParameter("selectedColumn", "\nvar selectedRow = " + selectedRow + ";\n" + orderByArray.toString());
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("windowId", windowId);
    xmlDocument.setParameter("KeyName", "ldtHblId");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    //xmlDocument.setParameter("buttonReference", Utility.messageBD(this, "Reference", vars.getLanguage()));
    try {
      WindowTabs tabs = new WindowTabs(this, vars, tabId, windowId, false);
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      String hideBackButton = vars.getGlobalVariable("hideMenu", "#Hide_BackButton", "");
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD_Relation.html", "FreeHand", "W", strReplaceWith, tabs.breadcrumb(), hideBackButton.equals("true"));
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD_Relation.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.relationTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    {
      OBError myMessage = vars.getMessage(tabId);
      vars.removeMessage(tabId);
      if (myMessage!=null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }
    if (vars.getLanguage().equals("en_US")) xmlDocument.setParameter("parent", HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.selectParent(this, strPLDT_Routing_Order_ID));
    else xmlDocument.setParameter("parent", HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.selectParentTrl(this, strPLDT_Routing_Order_ID));

    xmlDocument.setParameter("grid", Utility.getContext(this, vars, "#RecordRange", windowId));
xmlDocument.setParameter("grid_Offset", strOffset);
xmlDocument.setParameter("grid_SortCols", positions);
xmlDocument.setParameter("grid_SortDirs", directions);
xmlDocument.setParameter("grid_Default", selectedRow);


    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageEdit(HttpServletResponse response, HttpServletRequest request, VariablesSecureApp vars,boolean boolNew, String strLDT_Hbl_ID, String strPLDT_Routing_Order_ID, TableSQLData tableSQL)
    throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("Output: edit");
    
    HashMap<String, String> usedButtonShortCuts;
  
    HashMap<String, String> reservedButtonShortCuts;
  
    usedButtonShortCuts = new HashMap<String, String>();
    
    reservedButtonShortCuts = new HashMap<String, String>();
    
    
    
    String strOrderByFilter = vars.getSessionValue(tabId + "|orderby");
    String orderClause = " 1";
    if (strOrderByFilter==null || strOrderByFilter.equals("")) strOrderByFilter = orderClause;
    /*{
      if (!strOrderByFilter.equals("") && !orderClause.equals("")) strOrderByFilter += ", ";
      strOrderByFilter += orderClause;
    }*/
    
    
    String strCommand = null;
    HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[] data=null;
    XmlDocument xmlDocument=null;
    FieldProvider dataField = vars.getEditionData(tabId);
    vars.removeEditionData(tabId);
    String strParamNUM_Hbl = vars.getSessionValue(tabId + "|paramNUM_Hbl");
String strParamC_Bpartner_ID = vars.getSessionValue(tabId + "|paramC_Bpartner_ID");

    boolean hasSearchCondition=false;
    hasSearchCondition = (tableSQL.hasInternalFilter() && ("").equals(strParamNUM_Hbl) && ("").equals(strParamC_Bpartner_ID)) || !(("").equals(strParamNUM_Hbl) || ("%").equals(strParamNUM_Hbl))  || !(("").equals(strParamC_Bpartner_ID) || ("%").equals(strParamC_Bpartner_ID)) ;

       String strParamSessionDate = vars.getGlobalVariable("inpParamSessionDate", Utility.getTransactionalDate(this, vars, windowId), "");
      String buscador = "";
      String[] discard = {"", "isNotTest"};
      
      if (vars.getSessionValue("#ShowTest", "N").equals("Y")) discard[1] = new String("isTest");
    if (dataField==null) {
      if (!boolNew) {
        discard[0] = new String("newDiscard");
        data = HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), strPLDT_Routing_Order_ID, strLDT_Hbl_ID, Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
  
        if (!strLDT_Hbl_ID.equals("") && (data == null || data.length==0)) {
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
          return;
        }
        refreshSessionEdit(vars, data);
        strCommand = "EDIT";
      }

      if (boolNew || data==null || data.length==0) {
        discard[0] = new String ("editDiscard");
        strCommand = "NEW";
        data = new HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData[0];
      } else {
        discard[0] = new String ("newDiscard");
      }
    } else {
      if (dataField.getField("ldtHblId") == null || dataField.getField("ldtHblId").equals("")) {
        discard[0] = new String ("editDiscard");
        strCommand = "NEW";
        boolNew = true;
      } else {
        discard[0] = new String ("newDiscard");
        strCommand = "EDIT";
      }
    }
    
    
    
    if (dataField==null) {
      if (boolNew || data==null || data.length==0) {
        refreshSessionNew(vars, strPLDT_Routing_Order_ID);
        data = HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.set(strPLDT_Routing_Order_ID, Utility.getDefault(this, vars, "calcular", "N", "53C75C9421F94C609CDC00C4BED0E7CD", "N", dataField), Utility.getDefault(this, vars, "Secuencialhbl_Hawb", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "AD_Org_ID", "@AD_ORG_ID@", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "Cartas", "", "53C75C9421F94C609CDC00C4BED0E7CD", "N", dataField), Utility.getDefault(this, vars, "Processed", "CO", "53C75C9421F94C609CDC00C4BED0E7CD", "N", dataField), Utility.getDefault(this, vars, "valor_total_usd", "0", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "Estado", "DR", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "valor_flete_eur", "0", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "Valor_Flete", "0", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "C_Bpartner_ID", "@Cliente_ID@", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.selectDef7F025BCB6D1343EEB2624BB5DFBA7625_0(this, Utility.getDefault(this, vars, "C_Bpartner_ID", "@Cliente_ID@", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField)), Utility.getDefault(this, vars, "flete_pp_eur", "0", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "flete_pp", "0", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "C_Invoice_ID", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.selectDefAB621508B70E4E3ABB779427C57F9CEB(this, strPLDT_Routing_Order_ID), Utility.getDefault(this, vars, "Factura", "N", "53C75C9421F94C609CDC00C4BED0E7CD", "N", dataField), "", Utility.getDefault(this, vars, "NUM_Hbl", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "Updatedby", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.selectDefCEA41398F6F34FCAB387780EDC6B8E94_1(this, Utility.getDefault(this, vars, "Updatedby", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField)), Utility.getDefault(this, vars, "AD_Client_ID", "@AD_CLIENT_ID@", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), "Y", Utility.getDefault(this, vars, "Createdby", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.selectDefF4819B0004B141B3888DFF590C15FAEC_2(this, Utility.getDefault(this, vars, "Createdby", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField)), Utility.getDefault(this, vars, "Avisoarribo", "Y", "53C75C9421F94C609CDC00C4BED0E7CD", "N", dataField));
        
      }
     }
      
    String currentPOrg=FreeHand873BB22017634E47876049F4C5489617Data.selectOrg(this, strPLDT_Routing_Order_ID);
    String currentOrg = (boolNew?"":(dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId")));
    if (!currentOrg.equals("") && !currentOrg.startsWith("'")) currentOrg = "'"+currentOrg+"'";
    String currentClient = (boolNew?"":(dataField!=null?dataField.getField("adClientId"):data[0].getField("adClientId")));
    if (!currentClient.equals("") && !currentClient.startsWith("'")) currentClient = "'"+currentClient+"'";
    
    boolean hasReadOnlyAccess = org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId);
    boolean editableTab = (!hasReadOnlyAccess && (currentOrg.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),currentOrg)) && (currentClient.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), currentClient)));
    if (editableTab)
      xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/logisticadetransporte/parametrizacion/FreeHand/HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD_Edition",discard).createXmlDocument();
    else
      xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/logisticadetransporte/parametrizacion/FreeHand/HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD_NonEditable",discard).createXmlDocument();

    xmlDocument.setParameter("tabId", tabId);
    ToolBar toolbar = new ToolBar(this, editableTab, vars.getLanguage(), "HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD", (strCommand.equals("NEW") || boolNew || (dataField==null && (data==null || data.length==0))), "document.frmMain.inpldtHblId", "", "..", "".equals("Y"), "FreeHand", strReplaceWith, true, false, false, Utility.hasTabAttachments(this, vars, tabId, strLDT_Hbl_ID), !hasReadOnlyAccess);
    toolbar.setTabId(tabId);
    toolbar.setDeleteable(true);
    toolbar.prepareEditionTemplate("N".equals("Y"), hasSearchCondition, vars.getSessionValue("#ShowTest", "N").equals("Y"), "STD", Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    xmlDocument.setParameter("toolbar", toolbar.toString());

    // set updated timestamp to manage locking mechanism
    if (!boolNew) {
      xmlDocument.setParameter("updatedTimestamp", (dataField != null ? dataField
          .getField("updatedTimeStamp") : data[0].getField("updatedTimeStamp")));
    }
    
    boolean concurrentSave = vars.getSessionValue(tabId + "|concurrentSave").equals("true");
    if (concurrentSave) {
      //after concurrent save error, force autosave
      xmlDocument.setParameter("autosave", "Y");
    } else {
      xmlDocument.setParameter("autosave", "N");
    }
    vars.removeSessionValue(tabId + "|concurrentSave");

    try {
      WindowTabs tabs = new WindowTabs(this, vars, tabId, windowId, true, (strCommand.equalsIgnoreCase("NEW")));
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      // if (!strLDT_Hbl_ID.equals("")) xmlDocument.setParameter("childTabContainer", tabs.childTabs(false));
	  // else xmlDocument.setParameter("childTabContainer", tabs.childTabs(true));
	  xmlDocument.setParameter("childTabContainer", tabs.childTabs(false));
	  String hideBackButton = vars.getGlobalVariable("hideMenu", "#Hide_BackButton", "");
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD_Relation.html", "FreeHand", "W", strReplaceWith, tabs.breadcrumb(), hideBackButton.equals("true"), !concurrentSave);
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD_Relation.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.editionTemplate(strCommand.equals("NEW")));
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
		
    
    xmlDocument.setParameter("parentOrg", currentPOrg);
    xmlDocument.setParameter("commandType", strCommand);
    xmlDocument.setParameter("buscador",buscador);
    xmlDocument.setParameter("windowId", windowId);
    xmlDocument.setParameter("changed", "");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    final String strMappingName = Utility.getTabURL(tabId, "E", false);
    xmlDocument.setParameter("mappingName", strMappingName);
    xmlDocument.setParameter("confirmOnChanges", Utility.getJSConfirmOnChanges(vars, windowId));
    //xmlDocument.setParameter("buttonReference", Utility.messageBD(this, "Reference", vars.getLanguage()));

    xmlDocument.setParameter("paramSessionDate", strParamSessionDate);

    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    OBError myMessage = vars.getMessage(tabId);
    vars.removeMessage(tabId);
    if (myMessage!=null) {
      xmlDocument.setParameter("messageType", myMessage.getType());
      xmlDocument.setParameter("messageTitle", myMessage.getTitle());
      xmlDocument.setParameter("messageMessage", myMessage.getMessage());
    }
    xmlDocument.setParameter("displayLogic", getDisplayLogicContext(vars, boolNew));
    
    
     if (dataField==null) {
      xmlDocument.setData("structure1",data);
      
    } else {
      
        FieldProvider[] dataAux = new FieldProvider[1];
        dataAux[0] = dataField;
        
        xmlDocument.setData("structure1",dataAux);
      
    }
    
      
   
    try {
      ComboTableData comboTableData = null;
xmlDocument.setParameter("buttonValor_Flete", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonflete_pp", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonvalor_flete_eur", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonflete_pp_eur", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonvalor_total_usd", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("Factura_BTNname", Utility.getButtonName(this, vars, "35E4CB7E2DCD46F08A2F1EEDC3EB6B16", "Factura_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalFactura = org.openbravo.erpCommon.utility.Utility.isModalProcess("48BC2C143FE34BD1B63A5C46829749A2"); 
xmlDocument.setParameter("Factura_Modal", modalFactura?"true":"false");
xmlDocument.setParameter("Cartas_BTNname", Utility.getButtonName(this, vars, "EBC3794684EE48F1B36436B84167C088", "Cartas_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalCartas = org.openbravo.erpCommon.utility.Utility.isModalProcess("9CBB959CAF794C95959B6F32BF2B1639"); 
xmlDocument.setParameter("Cartas_Modal", modalCartas?"true":"false");
xmlDocument.setParameter("Avisoarribo_BTNname", Utility.getButtonName(this, vars, "00F5B6DE7084434EBC9ACDEE9B43CD3E", "Avisoarribo_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalAvisoarribo = org.openbravo.erpCommon.utility.Utility.isModalProcess("A650E755EA1D485680BDD179F7048845"); 
xmlDocument.setParameter("Avisoarribo_Modal", modalAvisoarribo?"true":"false");
xmlDocument.setParameter("Created_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Created_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
xmlDocument.setParameter("Updated_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Updated_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new ServletException(ex);
    }

    xmlDocument.setParameter("scriptOnLoad", getShortcutScript(usedButtonShortCuts, reservedButtonShortCuts));
    
    final String refererURL = vars.getSessionValue(tabId + "|requestURL");
    vars.removeSessionValue(tabId + "|requestURL");
    if(!refererURL.equals("")) {
    	final Boolean failedAutosave = (Boolean) vars.getSessionObject(tabId + "|failedAutosave");
		vars.removeSessionValue(tabId + "|failedAutosave");
    	if(failedAutosave != null && failedAutosave) {
    		final String jsFunction = "continueUserAction('"+refererURL+"');";
    		xmlDocument.setParameter("failedAutosave", jsFunction);
    	}
    }

    if (strCommand.equalsIgnoreCase("NEW")) {
      vars.removeSessionValue(tabId + "|failedAutosave");
      vars.removeSessionValue(strMappingName + "|hash");
    }

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageButtonFS(HttpServletResponse response, VariablesSecureApp vars, String strProcessId, String path) throws IOException, ServletException {
    log4j.debug("Output: Frames action button");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "org/openbravo/erpCommon/ad_actionButton/ActionButtonDefaultFrames").createXmlDocument();
    xmlDocument.setParameter("processId", strProcessId);
    xmlDocument.setParameter("trlFormType", "PROCESS");
    xmlDocument.setParameter("language", "defaultLang = \"" + vars.getLanguage() + "\";\n");
    xmlDocument.setParameter("type", strDireccion + path);
    out.println(xmlDocument.print());
    out.close();
  }

    private void printPageButtonFactura48BC2C143FE34BD1B63A5C46829749A2(HttpServletResponse response, VariablesSecureApp vars, String strLDT_Hbl_ID, String strfactura, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process 48BC2C143FE34BD1B63A5C46829749A2");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/Factura48BC2C143FE34BD1B63A5C46829749A2", discard).createXmlDocument();
      xmlDocument.setParameter("key", strLDT_Hbl_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "48BC2C143FE34BD1B63A5C46829749A2");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("48BC2C143FE34BD1B63A5C46829749A2");
        vars.removeMessage("48BC2C143FE34BD1B63A5C46829749A2");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }




    private String getDisplayLogicContext(VariablesSecureApp vars, boolean isNew) throws IOException, ServletException {
      log4j.debug("Output: Display logic context fields");
      String result = "var strC_Incoterms_ID=\"" +Utility.getContext(this, vars, "C_Incoterms_ID", windowId) + "\";\nvar strAD_Role_Id=\"" +Utility.getContext(this, vars, "#AD_Role_Id", windowId) + "\";\nvar strShowAudit=\"" +(isNew?"N":Utility.getContext(this, vars, "ShowAudit", windowId)) + "\";\n";
      return result;
    }


    private String getReadOnlyLogicContext(VariablesSecureApp vars) throws IOException, ServletException {
      log4j.debug("Output: Read Only logic context fields");
      String result = "var strES_Fin_Embarque=\"" + Utility.getContext(this, vars, "ES_Fin_Embarque", windowId) + "\";\n";
      return result;
    }




 
  private String getShortcutScript( HashMap<String, String> usedButtonShortCuts, HashMap<String, String> reservedButtonShortCuts){
    StringBuffer shortcuts = new StringBuffer();
    shortcuts.append(" function buttonListShorcuts() {\n");
    Iterator<String> ik = usedButtonShortCuts.keySet().iterator();
    Iterator<String> iv = usedButtonShortCuts.values().iterator();
    while(ik.hasNext() && iv.hasNext()){
      shortcuts.append("  keyArray[keyArray.length] = new keyArrayItem(\"").append(ik.next()).append("\", \"").append(iv.next()).append("\", null, \"altKey\", false, \"onkeydown\");\n");
    }
    shortcuts.append(" return true;\n}");
    return shortcuts.toString();
  }
  
  private int saveRecord(VariablesSecureApp vars, OBError myError, char type, String strPLDT_Routing_Order_ID) throws IOException, ServletException {
    HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData data = null;
    int total = 0;
    if (org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) {
        OBError newError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
        myError.setError(newError);
        vars.setMessage(tabId, myError);
    }
    else {
        Connection con = null;
        try {
            con = this.getTransactionConnection();
            data = getEditVariables(con, vars, strPLDT_Routing_Order_ID);
            data.dateTimeFormat = vars.getSessionValue("#AD_SqlDateTimeFormat");            
            String strSequence = "";
            if(type == 'I') {                
        strSequence = SequenceIdData.getUUID();
                if(log4j.isDebugEnabled()) log4j.debug("Sequence: " + strSequence);
                data.ldtHblId = strSequence;  
            }
            if (Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),data.adClientId)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),data.adOrgId)){
		     if(type == 'I') {
		       total = data.insert(con, this);
		     } else {
		       //Check the version of the record we are saving is the one in DB
		       if (HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3ADData.getCurrentDBTimestamp(this, data.ldtHblId).equals(
                vars.getStringParameter("updatedTimestamp"))) {
                total = data.update(con, this);
               } else {
                 myError.setMessage(Replace.replace(Replace.replace(Utility.messageBD(this,
                    "SavingModifiedRecord", vars.getLanguage()), "\\n", "<br/>"), "&quot;", "\""));
                 myError.setType("Error");
                 vars.setSessionValue(tabId + "|concurrentSave", "true");
               } 
		     }		            
          
            }
                else {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
            myError.setError(newError);            
          }
          releaseCommitConnection(con);
        } catch(Exception ex) {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
            myError.setError(newError);   
            try {
              releaseRollbackConnection(con);
            } catch (final Exception e) { //do nothing 
            }           
        }
            
        if (myError.isEmpty() && total == 0) {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=DBExecuteError");
            myError.setError(newError);
        }
        vars.setMessage(tabId, myError);
            
        if(!myError.isEmpty()){
            if(data != null ) {
                if(type == 'I') {            			
                    data.ldtHblId = "";
                }
                else {                    
                    
                }
                vars.setEditionData(tabId, data);
            }            	
        }
        else {
            vars.setSessionValue(windowId + "|LDT_Hbl_ID", data.ldtHblId);
        }
    }
    return total;
  }

  public String getServletInfo() {
    return "Servlet HBLHAWB5A2D77DFC3E64A848D0DDB0C9F82B3AD. This Servlet was made by Wad constructor";
  } // End of getServletInfo() method
}
