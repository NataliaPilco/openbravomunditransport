
package org.openbravo.erpWindows.com.atrums.logisticadetransporte.parametrizacion.MDTPreLiquidacion;


import org.openbravo.erpCommon.reference.*;


import org.openbravo.erpCommon.ad_actionButton.*;


import org.codehaus.jettison.json.JSONObject;
import org.openbravo.erpCommon.utility.*;
import org.openbravo.data.FieldProvider;
import org.openbravo.utils.FormatUtilities;
import org.openbravo.utils.Replace;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.exception.OBException;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessRunner;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.xmlEngine.XmlDocument;
import java.util.Vector;
import java.util.StringTokenizer;
import org.openbravo.database.SessionInfo;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import java.sql.Connection;
import org.apache.log4j.Logger;

public class PreLiquidacionB43F509219314D47B91836B76F1B8091 extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;
  
  private static Logger log4j = Logger.getLogger(PreLiquidacionB43F509219314D47B91836B76F1B8091.class);
  
  private static final String windowId = "CA2E3BFE1DE64BD1BF7E12A23B4C94DA";
  private static final String tabId = "B43F509219314D47B91836B76F1B8091";
  private static final String defaultTabView = "RELATION";
  private static final int accesslevel = 3;
  private static final String moduleId = "C5BE74383A264BF68316D6BF975E6EFC";
  
  @Override
  public void init(ServletConfig config) {
    setClassInfo("W", tabId, moduleId);
    super.init(config);
  }
  
  
  @Override
  public void service(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    String command = vars.getCommand();
    
    boolean securedProcess = false;
    if (command.contains("BUTTON")) {
      try {
        securedProcess = "Y".equals(org.openbravo.erpCommon.businessUtility.Preferences
            .getPreferenceValue("SecuredProcess", true, vars.getClient(), vars.getOrg(), vars
                .getUser(), vars.getRole(), windowId));
      } catch (PropertyException e) {
      }
    
     
      if (command.contains("24889AFB88CA4C0BB5EDF88BFE085E16")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("24889AFB88CA4C0BB5EDF88BFE085E16");
        SessionInfo.setModuleId("C5BE74383A264BF68316D6BF975E6EFC");
        if (securedProcess) {
          classInfo.type = "P";
          classInfo.id = "24889AFB88CA4C0BB5EDF88BFE085E16";
        }
      }
     
     
    }
    if (!securedProcess) {
      setClassInfo("W", tabId, moduleId);
    }
    super.service(request, response);
  }
  

  public void doPost (HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException {
    TableSQLData tableSQL = null;
    VariablesSecureApp vars = new VariablesSecureApp(request);
    Boolean saveRequest = (Boolean) request.getAttribute("autosave");
    
    if(saveRequest != null && saveRequest){
      String currentOrg = vars.getStringParameter("inpadOrgId");
      String currentClient = vars.getStringParameter("inpadClientId");
      boolean editableTab = (!org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)
                            && (currentOrg.equals("") || Utility.isElementInList(Utility.getContext(this, vars,"#User_Org", windowId, accesslevel), currentOrg)) 
                            && (currentClient.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),currentClient)));
    
        OBError myError = new OBError();
        String commandType = request.getParameter("inpCommandType");
        String strldtPreLiquidacionId = request.getParameter("inpldtPreLiquidacionId");
        
        if (editableTab) {
          int total = 0;
          
          if(commandType.equalsIgnoreCase("EDIT") && !strldtPreLiquidacionId.equals(""))
              total = saveRecord(vars, myError, 'U');
          else
              total = saveRecord(vars, myError, 'I');
          
          if (!myError.isEmpty() && total == 0)     
            throw new OBException(myError.getMessage());
        }
        vars.setSessionValue(request.getParameter("mappingName") +"|hash", vars.getPostDataHash());
        vars.setSessionValue(tabId + "|Header.view", "EDIT");
        
        return;
    }
    
    try {
      tableSQL = new TableSQLData(vars, this, tabId, Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel), Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    String strOrderBy = vars.getSessionValue(tabId + "|orderby");
    if (!strOrderBy.equals("")) {
      vars.setSessionValue(tabId + "|newOrder", "1");
    }

    if (vars.commandIn("DEFAULT")) {

      String strLDT_Pre_Liquidacion_ID = vars.getGlobalVariable("inpldtPreLiquidacionId", windowId + "|LDT_Pre_Liquidacion_ID", "");
      

      String strView = vars.getSessionValue(tabId + "|PreLiquidacionB43F509219314D47B91836B76F1B8091.view");
      if (strView.equals("")) {
        strView = defaultTabView;

        if (strView.equals("EDIT")) {
          if (strLDT_Pre_Liquidacion_ID.equals("")) strLDT_Pre_Liquidacion_ID = firstElement(vars, tableSQL);
          if (strLDT_Pre_Liquidacion_ID.equals("")) strView = "RELATION";
        }
      }
      if (strView.equals("EDIT")) 

        printPageEdit(response, request, vars, false, strLDT_Pre_Liquidacion_ID, tableSQL);

      else printPageDataSheet(response, vars, strLDT_Pre_Liquidacion_ID, tableSQL);
    } else if (vars.commandIn("DIRECT")) {
      String strLDT_Pre_Liquidacion_ID = vars.getStringParameter("inpDirectKey");
      
        
      if (strLDT_Pre_Liquidacion_ID.equals("")) strLDT_Pre_Liquidacion_ID = vars.getRequiredGlobalVariable("inpldtPreLiquidacionId", windowId + "|LDT_Pre_Liquidacion_ID");
      else vars.setSessionValue(windowId + "|LDT_Pre_Liquidacion_ID", strLDT_Pre_Liquidacion_ID);
      
      vars.setSessionValue(tabId + "|PreLiquidacionB43F509219314D47B91836B76F1B8091.view", "EDIT");

      printPageEdit(response, request, vars, false, strLDT_Pre_Liquidacion_ID, tableSQL);

    } else if (vars.commandIn("TAB")) {


      String strView = vars.getSessionValue(tabId + "|PreLiquidacionB43F509219314D47B91836B76F1B8091.view");
      String strLDT_Pre_Liquidacion_ID = "";
      if (strView.equals("")) {
        strView = defaultTabView;
        if (strView.equals("EDIT")) {
          strLDT_Pre_Liquidacion_ID = firstElement(vars, tableSQL);
          if (strLDT_Pre_Liquidacion_ID.equals("")) strView = "RELATION";
        }
      }
      if (strView.equals("EDIT")) {

        if (strLDT_Pre_Liquidacion_ID.equals("")) strLDT_Pre_Liquidacion_ID = firstElement(vars, tableSQL);
        printPageEdit(response, request, vars, false, strLDT_Pre_Liquidacion_ID, tableSQL);

      } else printPageDataSheet(response, vars, "", tableSQL);
    } else if (vars.commandIn("SEARCH")) {
vars.getRequestGlobalVariable("inpParamLDT_Routing_Order_ID", tabId + "|paramLDT_Routing_Order_ID");

        vars.getRequestGlobalVariable("inpParamUpdated", tabId + "|paramUpdated");
        vars.getRequestGlobalVariable("inpParamUpdatedBy", tabId + "|paramUpdatedBy");
        vars.getRequestGlobalVariable("inpParamCreated", tabId + "|paramCreated");
        vars.getRequestGlobalVariable("inpparamCreatedBy", tabId + "|paramCreatedBy");
      
      
      vars.removeSessionValue(windowId + "|LDT_Pre_Liquidacion_ID");
      String strLDT_Pre_Liquidacion_ID="";

      String strView = vars.getSessionValue(tabId + "|PreLiquidacionB43F509219314D47B91836B76F1B8091.view");
      if (strView.equals("")) strView=defaultTabView;

      if (strView.equals("EDIT")) {
        strLDT_Pre_Liquidacion_ID = firstElement(vars, tableSQL);
        if (strLDT_Pre_Liquidacion_ID.equals("")) {
          // filter returns empty set
          strView = "RELATION";
          // switch to grid permanently until the user changes the view again
          vars.setSessionValue(tabId + "|PreLiquidacionB43F509219314D47B91836B76F1B8091.view", strView);
        }
      }

      if (strView.equals("EDIT")) 

        printPageEdit(response, request, vars, false, strLDT_Pre_Liquidacion_ID, tableSQL);

      else printPageDataSheet(response, vars, strLDT_Pre_Liquidacion_ID, tableSQL);
    } else if (vars.commandIn("RELATION")) {
      

      String strLDT_Pre_Liquidacion_ID = vars.getGlobalVariable("inpldtPreLiquidacionId", windowId + "|LDT_Pre_Liquidacion_ID", "");
      vars.setSessionValue(tabId + "|PreLiquidacionB43F509219314D47B91836B76F1B8091.view", "RELATION");
      printPageDataSheet(response, vars, strLDT_Pre_Liquidacion_ID, tableSQL);
    } else if (vars.commandIn("NEW")) {


      printPageEdit(response, request, vars, true, "", tableSQL);

    } else if (vars.commandIn("EDIT")) {

      @SuppressWarnings("unused") // In Expense Invoice tab this variable is not used, to be fixed
      String strLDT_Pre_Liquidacion_ID = vars.getGlobalVariable("inpldtPreLiquidacionId", windowId + "|LDT_Pre_Liquidacion_ID", "");
      vars.setSessionValue(tabId + "|PreLiquidacionB43F509219314D47B91836B76F1B8091.view", "EDIT");

      setHistoryCommand(request, "EDIT");
      printPageEdit(response, request, vars, false, strLDT_Pre_Liquidacion_ID, tableSQL);

    } else if (vars.commandIn("NEXT")) {

      String strLDT_Pre_Liquidacion_ID = vars.getRequiredStringParameter("inpldtPreLiquidacionId");
      
      String strNext = nextElement(vars, strLDT_Pre_Liquidacion_ID, tableSQL);

      printPageEdit(response, request, vars, false, strNext, tableSQL);
    } else if (vars.commandIn("PREVIOUS")) {

      String strLDT_Pre_Liquidacion_ID = vars.getRequiredStringParameter("inpldtPreLiquidacionId");
      
      String strPrevious = previousElement(vars, strLDT_Pre_Liquidacion_ID, tableSQL);

      printPageEdit(response, request, vars, false, strPrevious, tableSQL);
    } else if (vars.commandIn("FIRST_RELATION")) {

      vars.setSessionValue(tabId + "|PreLiquidacionB43F509219314D47B91836B76F1B8091.initRecordNumber", "0");
      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("PREVIOUS_RELATION")) {

      String strInitRecord = vars.getSessionValue(tabId + "|PreLiquidacionB43F509219314D47B91836B76F1B8091.initRecordNumber");
      String strRecordRange = Utility.getContext(this, vars, "#RecordRange", windowId);
      int intRecordRange = strRecordRange.equals("")?0:Integer.parseInt(strRecordRange);
      if (strInitRecord.equals("") || strInitRecord.equals("0")) {
        vars.setSessionValue(tabId + "|PreLiquidacionB43F509219314D47B91836B76F1B8091.initRecordNumber", "0");
      } else {
        int initRecord = (strInitRecord.equals("")?0:Integer.parseInt(strInitRecord));
        initRecord -= intRecordRange;
        strInitRecord = ((initRecord<0)?"0":Integer.toString(initRecord));
        vars.setSessionValue(tabId + "|PreLiquidacionB43F509219314D47B91836B76F1B8091.initRecordNumber", strInitRecord);
      }
      vars.removeSessionValue(windowId + "|LDT_Pre_Liquidacion_ID");

      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("NEXT_RELATION")) {

      String strInitRecord = vars.getSessionValue(tabId + "|PreLiquidacionB43F509219314D47B91836B76F1B8091.initRecordNumber");
      String strRecordRange = Utility.getContext(this, vars, "#RecordRange", windowId);
      int intRecordRange = strRecordRange.equals("")?0:Integer.parseInt(strRecordRange);
      int initRecord = (strInitRecord.equals("")?0:Integer.parseInt(strInitRecord));
      if (initRecord==0) initRecord=1;
      initRecord += intRecordRange;
      strInitRecord = ((initRecord<0)?"0":Integer.toString(initRecord));
      vars.setSessionValue(tabId + "|PreLiquidacionB43F509219314D47B91836B76F1B8091.initRecordNumber", strInitRecord);
      vars.removeSessionValue(windowId + "|LDT_Pre_Liquidacion_ID");

      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("FIRST")) {

      
      String strFirst = firstElement(vars, tableSQL);

      printPageEdit(response, request, vars, false, strFirst, tableSQL);
    } else if (vars.commandIn("LAST_RELATION")) {

      String strLast = lastElement(vars, tableSQL);
      printPageDataSheet(response, vars, strLast, tableSQL);
    } else if (vars.commandIn("LAST")) {

      
      String strLast = lastElement(vars, tableSQL);

      printPageEdit(response, request, vars, false, strLast, tableSQL);
    } else if (vars.commandIn("SAVE_NEW_RELATION", "SAVE_NEW_NEW", "SAVE_NEW_EDIT")) {

      OBError myError = new OBError();      
      int total = saveRecord(vars, myError, 'I');      
      if (!myError.isEmpty()) {        
        response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
      } 
      else {
		if (myError.isEmpty()) {
		  myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsInserted");
		  myError.setMessage(total + " " + myError.getMessage());
		  vars.setMessage(tabId, myError);
		}        
        if (vars.commandIn("SAVE_NEW_NEW")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
        else if (vars.commandIn("SAVE_NEW_EDIT")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        else response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
      }
    } else if (vars.commandIn("SAVE_EDIT_RELATION", "SAVE_EDIT_NEW", "SAVE_EDIT_EDIT", "SAVE_EDIT_NEXT")) {

      String strLDT_Pre_Liquidacion_ID = vars.getRequiredGlobalVariable("inpldtPreLiquidacionId", windowId + "|LDT_Pre_Liquidacion_ID");
      OBError myError = new OBError();
      int total = saveRecord(vars, myError, 'U');      
      if (!myError.isEmpty()) {
        response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
      } 
      else {
        if (myError.isEmpty()) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsUpdated");
          myError.setMessage(total + " " + myError.getMessage());
          vars.setMessage(tabId, myError);
        }
        if (vars.commandIn("SAVE_EDIT_NEW")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
        else if (vars.commandIn("SAVE_EDIT_EDIT")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        else if (vars.commandIn("SAVE_EDIT_NEXT")) {
          String strNext = nextElement(vars, strLDT_Pre_Liquidacion_ID, tableSQL);
          vars.setSessionValue(windowId + "|LDT_Pre_Liquidacion_ID", strNext);
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        } else response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
      }
    } else if (vars.commandIn("DELETE")) {

      String strLDT_Pre_Liquidacion_ID = vars.getRequiredStringParameter("inpldtPreLiquidacionId");
      //PreLiquidacionB43F509219314D47B91836B76F1B8091Data data = getEditVariables(vars);
      int total = 0;
      OBError myError = null;
      if (org.openbravo.erpCommon.utility.WindowAccessData.hasNotDeleteAccess(this, vars.getRole(), tabId)) {
        myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
        vars.setMessage(tabId, myError);
      } else {
        try {
          total = PreLiquidacionB43F509219314D47B91836B76F1B8091Data.delete(this, strLDT_Pre_Liquidacion_ID, Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), Utility.getContext(this, vars, "#User_Org", windowId, accesslevel));
        } catch(ServletException ex) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myError.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myError);
        }
        if (myError==null && total==0) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
        }
        vars.removeSessionValue(windowId + "|ldtPreLiquidacionId");
        vars.setSessionValue(tabId + "|PreLiquidacionB43F509219314D47B91836B76F1B8091.view", "RELATION");
      }
      if (myError==null) {
        myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsDeleted");
        myError.setMessage(total + " " + myError.getMessage());
        vars.setMessage(tabId, myError);
      }
      response.sendRedirect(strDireccion + request.getServletPath());

     } else if (vars.commandIn("BUTTONprocesar24889AFB88CA4C0BB5EDF88BFE085E16")) {
        vars.setSessionValue("button24889AFB88CA4C0BB5EDF88BFE085E16.strprocesar", vars.getStringParameter("inpprocesar"));
        vars.setSessionValue("button24889AFB88CA4C0BB5EDF88BFE085E16.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("button24889AFB88CA4C0BB5EDF88BFE085E16.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("button24889AFB88CA4C0BB5EDF88BFE085E16.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        p.put("procesar", vars.getStringParameter("inpprocesar"));

        
        //Save in session needed params for combos if needed
        vars.setSessionObject("button24889AFB88CA4C0BB5EDF88BFE085E16.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "24889AFB88CA4C0BB5EDF88BFE085E16", request.getServletPath());    
     } else if (vars.commandIn("BUTTON24889AFB88CA4C0BB5EDF88BFE085E16")) {
        String strLDT_Pre_Liquidacion_ID = vars.getGlobalVariable("inpldtPreLiquidacionId", windowId + "|LDT_Pre_Liquidacion_ID", "");
        String strprocesar = vars.getSessionValue("button24889AFB88CA4C0BB5EDF88BFE085E16.strprocesar");
        String strProcessing = vars.getSessionValue("button24889AFB88CA4C0BB5EDF88BFE085E16.strProcessing");
        String strOrg = vars.getSessionValue("button24889AFB88CA4C0BB5EDF88BFE085E16.strOrg");
        String strClient = vars.getSessionValue("button24889AFB88CA4C0BB5EDF88BFE085E16.strClient");
        
        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtonprocesar24889AFB88CA4C0BB5EDF88BFE085E16(response, vars, strLDT_Pre_Liquidacion_ID, strprocesar, strProcessing);
        }


    } else if (vars.commandIn("SAVE_BUTTONprocesar24889AFB88CA4C0BB5EDF88BFE085E16")) {
        String strLDT_Pre_Liquidacion_ID = vars.getGlobalVariable("inpKey", windowId + "|LDT_Pre_Liquidacion_ID", "");
        @SuppressWarnings("unused")
        String strprocesar = vars.getStringParameter("inpprocesar");
        String strProcessing = vars.getStringParameter("inpprocessing");
        OBError myMessage = null;
        try {
          String pinstance = SequenceIdData.getUUID();
          PInstanceProcessData.insertPInstance(this, pinstance, "24889AFB88CA4C0BB5EDF88BFE085E16", (("LDT_Pre_Liquidacion_ID".equalsIgnoreCase("AD_Language"))?"0":strLDT_Pre_Liquidacion_ID), strProcessing, vars.getUser(), vars.getClient(), vars.getOrg());
          String straprobarpreliquidacion = vars.getStringParameter("inpaprobarpreliquidacion");
PInstanceProcessData.insertPInstanceParam(this, pinstance, "10", "AprobarPreliquidacion", straprobarpreliquidacion, vars.getClient(), vars.getOrg(), vars.getUser());

          
          ProcessBundle bundle = ProcessBundle.pinstance(pinstance, vars, this);
          new ProcessRunner(bundle).execute(this);
          
          PInstanceProcessData[] pinstanceData = PInstanceProcessData.select(this, pinstance);
          myMessage = Utility.getProcessInstanceMessage(this, vars, pinstanceData);
        } catch (ServletException ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);






    } else if (vars.commandIn("SAVE_XHR")) {
      
      OBError myError = new OBError();
      JSONObject result = new JSONObject();
      String commandType = vars.getStringParameter("inpCommandType");
      char saveType = "NEW".equals(commandType) ? 'I' : 'U';
      try {
        int total = saveRecord(vars, myError, saveType);
        if (myError.isEmpty()) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsUpdated");
          myError.setMessage(total + " " + myError.getMessage());
          myError.setType("Success");
        }
        result.put("oberror", myError.toMap());
        result.put("tabid", vars.getStringParameter("tabID"));
        result.put("redirect", strDireccion + request.getServletPath() + "?Command=" + commandType);
      } catch (Exception e) {
        log4j.error("Error saving record (XHR request): " + e.getMessage(), e);
        myError.setType("Error");
        myError.setMessage(e.getMessage());
      }

      response.setContentType("application/json");
      PrintWriter out = response.getWriter();
      out.print(result.toString());
      out.flush();
      out.close();
    } else if (vars.getCommand().toUpperCase().startsWith("BUTTON") || vars.getCommand().toUpperCase().startsWith("SAVE_BUTTON")) {
      pageErrorPopUp(response);
    } else pageError(response);
  }
  private PreLiquidacionB43F509219314D47B91836B76F1B8091Data getEditVariables(Connection con, VariablesSecureApp vars) throws IOException,ServletException {
    PreLiquidacionB43F509219314D47B91836B76F1B8091Data data = new PreLiquidacionB43F509219314D47B91836B76F1B8091Data();
    ServletException ex = null;
    try {
    data.adOrgId = vars.getRequiredGlobalVariable("inpadOrgId", windowId + "|AD_Org_ID");     data.adOrgIdr = vars.getStringParameter("inpadOrgId_R");     data.adUserId = vars.getRequiredGlobalVariable("inpadUserId", windowId + "|AD_User_ID");     data.servicioClienteId = vars.getRequiredGlobalVariable("inpservicioClienteId", windowId + "|Servicio_Cliente_ID");     data.servicioClienteIdr = vars.getStringParameter("inpservicioClienteId_R");     data.vendedorId = vars.getRequiredGlobalVariable("inpvendedorId", windowId + "|Vendedor_ID");     data.vendedorIdr = vars.getStringParameter("inpvendedorId_R");     data.ldtRoutingOrderId = vars.getRequiredGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID");     data.ldtTipo = vars.getRequestGlobalVariable("inpldtTipo", windowId + "|LDT_Tipo");     data.ldtTipor = vars.getStringParameter("inpldtTipo_R");     data.cBpartnerId = vars.getRequestGlobalVariable("inpcBpartnerId", windowId + "|C_Bpartner_ID");     data.cBpartnerIdr = vars.getStringParameter("inpcBpartnerId_R");    try {   data.resultado = vars.getNumericParameter("inpresultado");  } catch (ServletException paramEx) { ex = paramEx; }     data.agenteId = vars.getStringParameter("inpagenteId");     data.docstatus = vars.getRequiredGlobalVariable("inpdocstatus", windowId + "|Docstatus");     data.docstatusr = vars.getStringParameter("inpdocstatus_R");    try {   data.cantidad = vars.getNumericParameter("inpcantidad");  } catch (ServletException paramEx) { ex = paramEx; }     data.procesar = vars.getRequiredGlobalVariable("inpprocesar", windowId + "|procesar");     data.preLiquidacion = vars.getStringParameter("inppreLiquidacion");     data.adUserVendedorId = vars.getRequiredGlobalVariable("inpadUserVendedorId", windowId + "|AD_User_Vendedor_ID");     data.isactive = vars.getStringParameter("inpisactive", "N");     data.adClientId = vars.getRequiredGlobalVariable("inpadClientId", windowId + "|AD_Client_ID");     data.ldtPreLiquidacionId = vars.getRequestGlobalVariable("inpldtPreLiquidacionId", windowId + "|LDT_Pre_Liquidacion_ID"); 
      data.createdby = vars.getUser();
      data.updatedby = vars.getUser();
      data.adUserClient = Utility.getContext(this, vars, "#User_Client", windowId, accesslevel);
      data.adOrgClient = Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel);
      data.updatedTimeStamp = vars.getStringParameter("updatedTimestamp");



    
    

    
    }
    catch(ServletException e) {
    	vars.setEditionData(tabId, data);
    	throw e;
    }
    // Behavior with exception for numeric fields is to catch last one if we have multiple ones
    if(ex != null) {
      vars.setEditionData(tabId, data);
      throw ex;
    }
    return data;
  }




    private void refreshSessionEdit(VariablesSecureApp vars, FieldProvider[] data) {
      if (data==null || data.length==0) return;
          vars.setSessionValue(windowId + "|AD_Org_ID", data[0].getField("adOrgId"));    vars.setSessionValue(windowId + "|AD_User_ID", data[0].getField("adUserId"));    vars.setSessionValue(windowId + "|Servicio_Cliente_ID", data[0].getField("servicioClienteId"));    vars.setSessionValue(windowId + "|Vendedor_ID", data[0].getField("vendedorId"));    vars.setSessionValue(windowId + "|LDT_Routing_Order_ID", data[0].getField("ldtRoutingOrderId"));    vars.setSessionValue(windowId + "|LDT_Tipo", data[0].getField("ldtTipo"));    vars.setSessionValue(windowId + "|C_BPartner_ID", data[0].getField("cBpartnerId"));    vars.setSessionValue(windowId + "|DocStatus", data[0].getField("docstatus"));    vars.setSessionValue(windowId + "|Procesar", data[0].getField("procesar"));    vars.setSessionValue(windowId + "|ad_user_vendedor_id", data[0].getField("adUserVendedorId"));    vars.setSessionValue(windowId + "|AD_Client_ID", data[0].getField("adClientId"));    vars.setSessionValue(windowId + "|LDT_Pre_Liquidacion_ID", data[0].getField("ldtPreLiquidacionId"));
    }

    private void refreshSessionNew(VariablesSecureApp vars) throws IOException,ServletException {
      PreLiquidacionB43F509219314D47B91836B76F1B8091Data[] data = PreLiquidacionB43F509219314D47B91836B76F1B8091Data.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), vars.getStringParameter("inpldtPreLiquidacionId", ""), Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
      if (data==null || data.length==0) return;
      refreshSessionEdit(vars, data);
    }

  private String nextElement(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (strSelected == null || strSelected.equals("")) return firstElement(vars, tableSQL);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(), 0, 0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.NEXT, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting next element", e);
      }
      if (data!=null) {
        if (data!=null) return data;
      }
    }
    return strSelected;
  }

  private int getKeyPosition(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("getKeyPosition: " + strSelected);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.GETPOSITION, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting key position", e);
      }
      if (data!=null) {
        // split offset -> (page,relativeOffset)
        int absoluteOffset = Integer.valueOf(data);
        int page = absoluteOffset / TableSQLData.maxRowsPerGridPage;
        int relativeOffset = absoluteOffset % TableSQLData.maxRowsPerGridPage;
        log4j.debug("getKeyPosition: absOffset: " + absoluteOffset + "=> page: " + page + " relOffset: " + relativeOffset);
        String currPageKey = tabId + "|" + "currentPage";
        vars.setSessionValue(currPageKey, String.valueOf(page));
        return relativeOffset;
      }
    }
    return 0;
  }

  private String previousElement(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (strSelected == null || strSelected.equals("")) return firstElement(vars, tableSQL);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.PREVIOUS, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting previous element", e);
      }
      if (data!=null) {
        return data;
      }
    }
    return strSelected;
  }

  private String firstElement(VariablesSecureApp vars, TableSQLData tableSQL) throws IOException, ServletException {
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,1);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.FIRST, "", tableSQL.getKeyColumn());

      } catch (Exception e) { 
        log4j.debug("Error getting first element", e);
      }
      if (data!=null) return data;
    }
    return "";
  }

  private String lastElement(VariablesSecureApp vars, TableSQLData tableSQL) throws IOException, ServletException {
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.LAST, "", tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting last element", e);
      }
      if (data!=null) return data;
    }
    return "";
  }

  private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars, String strLDT_Pre_Liquidacion_ID, TableSQLData tableSQL)
    throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("Output: dataSheet");

    String strParamLDT_Routing_Order_ID = vars.getSessionValue(tabId + "|paramLDT_Routing_Order_ID");

    boolean hasSearchCondition=false;
    vars.removeEditionData(tabId);
    hasSearchCondition = (tableSQL.hasInternalFilter() && ("").equals(strParamLDT_Routing_Order_ID)) || !(("").equals(strParamLDT_Routing_Order_ID) || ("%").equals(strParamLDT_Routing_Order_ID)) ;
    String strOffset = vars.getSessionValue(tabId + "|offset");
    String selectedRow = "0";
    if (!strLDT_Pre_Liquidacion_ID.equals("")) {
      selectedRow = Integer.toString(getKeyPosition(vars, strLDT_Pre_Liquidacion_ID, tableSQL));
    }

    String[] discard={"isNotFiltered","isNotTest"};
    if (hasSearchCondition) discard[0] = new String("isFiltered");
    if (vars.getSessionValue("#ShowTest", "N").equals("Y")) discard[1] = new String("isTest");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/logisticadetransporte/parametrizacion/MDTPreLiquidacion/PreLiquidacionB43F509219314D47B91836B76F1B8091_Relation", discard).createXmlDocument();

    boolean hasReadOnlyAccess = org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId);
    ToolBar toolbar = new ToolBar(this, true, vars.getLanguage(), "PreLiquidacionB43F509219314D47B91836B76F1B8091", false, "document.frmMain.inpldtPreLiquidacionId", "grid", "..", "".equals("Y"), "MDTPreLiquidacion", strReplaceWith, false, false, false, false, !hasReadOnlyAccess);
    toolbar.setTabId(tabId);
    
    toolbar.setDeleteable(true && !hasReadOnlyAccess);
    toolbar.prepareRelationTemplate("N".equals("Y"), hasSearchCondition, !vars.getSessionValue("#ShowTest", "N").equals("Y"), false, Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    xmlDocument.setParameter("toolbar", toolbar.toString());



    StringBuffer orderByArray = new StringBuffer();
      vars.setSessionValue(tabId + "|newOrder", "1");
      String positions = vars.getSessionValue(tabId + "|orderbyPositions");
      orderByArray.append("var orderByPositions = new Array(\n");
      if (!positions.equals("")) {
        StringTokenizer tokens=new StringTokenizer(positions, ",");
        boolean firstOrder = true;
        while(tokens.hasMoreTokens()){
          if (!firstOrder) orderByArray.append(",\n");
          orderByArray.append("\"").append(tokens.nextToken()).append("\"");
          firstOrder = false;
        }
      }
      orderByArray.append(");\n");
      String directions = vars.getSessionValue(tabId + "|orderbyDirections");
      orderByArray.append("var orderByDirections = new Array(\n");
      if (!positions.equals("")) {
        StringTokenizer tokens=new StringTokenizer(directions, ",");
        boolean firstOrder = true;
        while(tokens.hasMoreTokens()){
          if (!firstOrder) orderByArray.append(",\n");
          orderByArray.append("\"").append(tokens.nextToken()).append("\"");
          firstOrder = false;
        }
      }
      orderByArray.append(");\n");
//    }

    xmlDocument.setParameter("selectedColumn", "\nvar selectedRow = " + selectedRow + ";\n" + orderByArray.toString());
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("windowId", windowId);
    xmlDocument.setParameter("KeyName", "ldtPreLiquidacionId");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    //xmlDocument.setParameter("buttonReference", Utility.messageBD(this, "Reference", vars.getLanguage()));
    try {
      WindowTabs tabs = new WindowTabs(this, vars, tabId, windowId, false);
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      String hideBackButton = vars.getGlobalVariable("hideMenu", "#Hide_BackButton", "");
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "PreLiquidacionB43F509219314D47B91836B76F1B8091_Relation.html", "MDTPreLiquidacion", "W", strReplaceWith, tabs.breadcrumb(), hideBackButton.equals("true"));
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "PreLiquidacionB43F509219314D47B91836B76F1B8091_Relation.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.relationTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    {
      OBError myMessage = vars.getMessage(tabId);
      vars.removeMessage(tabId);
      if (myMessage!=null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }


    xmlDocument.setParameter("grid", Utility.getContext(this, vars, "#RecordRange", windowId));
xmlDocument.setParameter("grid_Offset", strOffset);
xmlDocument.setParameter("grid_SortCols", positions);
xmlDocument.setParameter("grid_SortDirs", directions);
xmlDocument.setParameter("grid_Default", selectedRow);


    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageEdit(HttpServletResponse response, HttpServletRequest request, VariablesSecureApp vars,boolean boolNew, String strLDT_Pre_Liquidacion_ID, TableSQLData tableSQL)
    throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("Output: edit");
    
    HashMap<String, String> usedButtonShortCuts;
  
    HashMap<String, String> reservedButtonShortCuts;
  
    usedButtonShortCuts = new HashMap<String, String>();
    
    reservedButtonShortCuts = new HashMap<String, String>();
    
    
    
    String strOrderByFilter = vars.getSessionValue(tabId + "|orderby");
    String orderClause = " 1";
    if (strOrderByFilter==null || strOrderByFilter.equals("")) strOrderByFilter = orderClause;
    /*{
      if (!strOrderByFilter.equals("") && !orderClause.equals("")) strOrderByFilter += ", ";
      strOrderByFilter += orderClause;
    }*/
    
    
    String strCommand = null;
    PreLiquidacionB43F509219314D47B91836B76F1B8091Data[] data=null;
    XmlDocument xmlDocument=null;
    FieldProvider dataField = vars.getEditionData(tabId);
    vars.removeEditionData(tabId);
    String strParamLDT_Routing_Order_ID = vars.getSessionValue(tabId + "|paramLDT_Routing_Order_ID");

    boolean hasSearchCondition=false;
    hasSearchCondition = (tableSQL.hasInternalFilter() && ("").equals(strParamLDT_Routing_Order_ID)) || !(("").equals(strParamLDT_Routing_Order_ID) || ("%").equals(strParamLDT_Routing_Order_ID)) ;

       String strParamSessionDate = vars.getGlobalVariable("inpParamSessionDate", Utility.getTransactionalDate(this, vars, windowId), "");
      String buscador = "";
      String[] discard = {"", "isNotTest"};
      
      if (vars.getSessionValue("#ShowTest", "N").equals("Y")) discard[1] = new String("isTest");
    if (dataField==null) {
      if (!boolNew) {
        discard[0] = new String("newDiscard");
        data = PreLiquidacionB43F509219314D47B91836B76F1B8091Data.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), strLDT_Pre_Liquidacion_ID, Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
  
        if (!strLDT_Pre_Liquidacion_ID.equals("") && (data == null || data.length==0)) {
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
          return;
        }
        refreshSessionEdit(vars, data);
        strCommand = "EDIT";
      }

      if (boolNew || data==null || data.length==0) {
        discard[0] = new String ("editDiscard");
        strCommand = "NEW";
        data = new PreLiquidacionB43F509219314D47B91836B76F1B8091Data[0];
      } else {
        discard[0] = new String ("newDiscard");
      }
    } else {
      if (dataField.getField("ldtPreLiquidacionId") == null || dataField.getField("ldtPreLiquidacionId").equals("")) {
        discard[0] = new String ("editDiscard");
        strCommand = "NEW";
        boolNew = true;
      } else {
        discard[0] = new String ("newDiscard");
        strCommand = "EDIT";
      }
    }
    
    
    
    if (dataField==null) {
      if (boolNew || data==null || data.length==0) {
        refreshSessionNew(vars);
        data = PreLiquidacionB43F509219314D47B91836B76F1B8091Data.set(Utility.getDefault(this, vars, "procesar", "CO", "CA2E3BFE1DE64BD1BF7E12A23B4C94DA", "N", dataField), (vars.getLanguage().equals("en_US")?ListData.selectName(this, "2B056D3A40B349EBAD59FB72E029656F", Utility.getDefault(this, vars, "procesar", "CO", "CA2E3BFE1DE64BD1BF7E12A23B4C94DA", "N", dataField)):ListData.selectNameTrl(this, vars.getLanguage(), "2B056D3A40B349EBAD59FB72E029656F", Utility.getDefault(this, vars, "procesar", "CO", "CA2E3BFE1DE64BD1BF7E12A23B4C94DA", "N", dataField))), Utility.getDefault(this, vars, "Resultado", "0", "CA2E3BFE1DE64BD1BF7E12A23B4C94DA", "", dataField), Utility.getDefault(this, vars, "LDT_Routing_Order_ID", "", "CA2E3BFE1DE64BD1BF7E12A23B4C94DA", "", dataField), Utility.getDefault(this, vars, "C_Bpartner_ID", "", "CA2E3BFE1DE64BD1BF7E12A23B4C94DA", "", dataField), PreLiquidacionB43F509219314D47B91836B76F1B8091Data.selectDef17A2CA4B13AA4C6AADE8D91571E2C74F_0(this, Utility.getDefault(this, vars, "C_Bpartner_ID", "", "CA2E3BFE1DE64BD1BF7E12A23B4C94DA", "", dataField)), Utility.getDefault(this, vars, "Agente_ID", "", "CA2E3BFE1DE64BD1BF7E12A23B4C94DA", "", dataField), PreLiquidacionB43F509219314D47B91836B76F1B8091Data.selectDef2BBC62038FCF4A5DBEE6CF7356E72C56(this, Utility.getContext(this, vars, "#AD_Role_Id", windowId), Utility.getContext(this, vars, "#Ad_User_Id", windowId)), Utility.getDefault(this, vars, "Docstatus", "BR", "CA2E3BFE1DE64BD1BF7E12A23B4C94DA", "", dataField), PreLiquidacionB43F509219314D47B91836B76F1B8091Data.selectDef407220E71CF44A22B97E6968BB053C5A(this, Utility.getContext(this, vars, "#AD_Role_Id", windowId), Utility.getContext(this, vars, "#Ad_User_Id", windowId)), Utility.getDefault(this, vars, "Createdby", "", "CA2E3BFE1DE64BD1BF7E12A23B4C94DA", "", dataField), PreLiquidacionB43F509219314D47B91836B76F1B8091Data.selectDef47F27CAFC3124DFCB9719234C31B0696_1(this, Utility.getDefault(this, vars, "Createdby", "", "CA2E3BFE1DE64BD1BF7E12A23B4C94DA", "", dataField)), Utility.getDefault(this, vars, "AD_Org_ID", "@AD_ORG_ID@", "CA2E3BFE1DE64BD1BF7E12A23B4C94DA", "", dataField), Utility.getDefault(this, vars, "Updatedby", "", "CA2E3BFE1DE64BD1BF7E12A23B4C94DA", "", dataField), PreLiquidacionB43F509219314D47B91836B76F1B8091Data.selectDef4F370AE6B7FA4E069CB976287C9E5BEF_2(this, Utility.getDefault(this, vars, "Updatedby", "", "CA2E3BFE1DE64BD1BF7E12A23B4C94DA", "", dataField)), Utility.getDefault(this, vars, "Cantidad", "", "CA2E3BFE1DE64BD1BF7E12A23B4C94DA", "", dataField), Utility.getDefault(this, vars, "AD_User_ID", "@#AD_User_Id@", "CA2E3BFE1DE64BD1BF7E12A23B4C94DA", "", dataField), Utility.getDefault(this, vars, "AD_Client_ID", "@AD_CLIENT_ID@", "CA2E3BFE1DE64BD1BF7E12A23B4C94DA", "", dataField), Utility.getDefault(this, vars, "AD_User_Vendedor_ID", "@#AD_User_Id@", "CA2E3BFE1DE64BD1BF7E12A23B4C94DA", "", dataField), Utility.getDefault(this, vars, "PRE_Liquidacion", "N", "CA2E3BFE1DE64BD1BF7E12A23B4C94DA", "N", dataField), "Y", "", Utility.getDefault(this, vars, "LDT_Tipo", "", "CA2E3BFE1DE64BD1BF7E12A23B4C94DA", "", dataField));
        
      }
     }
      
    
    String currentOrg = (boolNew?"":(dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId")));
    if (!currentOrg.equals("") && !currentOrg.startsWith("'")) currentOrg = "'"+currentOrg+"'";
    String currentClient = (boolNew?"":(dataField!=null?dataField.getField("adClientId"):data[0].getField("adClientId")));
    if (!currentClient.equals("") && !currentClient.startsWith("'")) currentClient = "'"+currentClient+"'";
    
    boolean hasReadOnlyAccess = org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId);
    boolean editableTab = (!hasReadOnlyAccess && (currentOrg.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),currentOrg)) && (currentClient.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), currentClient)));
    if (editableTab)
      xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/logisticadetransporte/parametrizacion/MDTPreLiquidacion/PreLiquidacionB43F509219314D47B91836B76F1B8091_Edition",discard).createXmlDocument();
    else
      xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/logisticadetransporte/parametrizacion/MDTPreLiquidacion/PreLiquidacionB43F509219314D47B91836B76F1B8091_NonEditable",discard).createXmlDocument();

    xmlDocument.setParameter("tabId", tabId);
    ToolBar toolbar = new ToolBar(this, editableTab, vars.getLanguage(), "PreLiquidacionB43F509219314D47B91836B76F1B8091", (strCommand.equals("NEW") || boolNew || (dataField==null && (data==null || data.length==0))), "document.frmMain.inpldtPreLiquidacionId", "", "..", "".equals("Y"), "MDTPreLiquidacion", strReplaceWith, true, false, false, Utility.hasTabAttachments(this, vars, tabId, strLDT_Pre_Liquidacion_ID), !hasReadOnlyAccess);
    toolbar.setTabId(tabId);
    toolbar.setDeleteable(true);
    toolbar.prepareEditionTemplate("N".equals("Y"), hasSearchCondition, vars.getSessionValue("#ShowTest", "N").equals("Y"), "STD", Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    xmlDocument.setParameter("toolbar", toolbar.toString());

    // set updated timestamp to manage locking mechanism
    if (!boolNew) {
      xmlDocument.setParameter("updatedTimestamp", (dataField != null ? dataField
          .getField("updatedTimeStamp") : data[0].getField("updatedTimeStamp")));
    }
    
    boolean concurrentSave = vars.getSessionValue(tabId + "|concurrentSave").equals("true");
    if (concurrentSave) {
      //after concurrent save error, force autosave
      xmlDocument.setParameter("autosave", "Y");
    } else {
      xmlDocument.setParameter("autosave", "N");
    }
    vars.removeSessionValue(tabId + "|concurrentSave");

    try {
      WindowTabs tabs = new WindowTabs(this, vars, tabId, windowId, true, (strCommand.equalsIgnoreCase("NEW")));
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      // if (!strLDT_Pre_Liquidacion_ID.equals("")) xmlDocument.setParameter("childTabContainer", tabs.childTabs(false));
	  // else xmlDocument.setParameter("childTabContainer", tabs.childTabs(true));
	  xmlDocument.setParameter("childTabContainer", tabs.childTabs(false));
	  String hideBackButton = vars.getGlobalVariable("hideMenu", "#Hide_BackButton", "");
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "PreLiquidacionB43F509219314D47B91836B76F1B8091_Relation.html", "MDTPreLiquidacion", "W", strReplaceWith, tabs.breadcrumb(), hideBackButton.equals("true"), !concurrentSave);
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "PreLiquidacionB43F509219314D47B91836B76F1B8091_Relation.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.editionTemplate(strCommand.equals("NEW")));
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
		
    
    
    xmlDocument.setParameter("commandType", strCommand);
    xmlDocument.setParameter("buscador",buscador);
    xmlDocument.setParameter("windowId", windowId);
    xmlDocument.setParameter("changed", "");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    final String strMappingName = Utility.getTabURL(tabId, "E", false);
    xmlDocument.setParameter("mappingName", strMappingName);
    xmlDocument.setParameter("confirmOnChanges", Utility.getJSConfirmOnChanges(vars, windowId));
    //xmlDocument.setParameter("buttonReference", Utility.messageBD(this, "Reference", vars.getLanguage()));

    xmlDocument.setParameter("paramSessionDate", strParamSessionDate);

    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    OBError myMessage = vars.getMessage(tabId);
    vars.removeMessage(tabId);
    if (myMessage!=null) {
      xmlDocument.setParameter("messageType", myMessage.getType());
      xmlDocument.setParameter("messageTitle", myMessage.getTitle());
      xmlDocument.setParameter("messageMessage", myMessage.getMessage());
    }
    xmlDocument.setParameter("displayLogic", getDisplayLogicContext(vars, boolNew));
    
    
     if (dataField==null) {
      xmlDocument.setData("structure1",data);
      
    } else {
      
        FieldProvider[] dataAux = new FieldProvider[1];
        dataAux[0] = dataField;
        
        xmlDocument.setData("structure1",dataAux);
      
    }
    
      
   
    try {
      ComboTableData comboTableData = null;
String userOrgList = "";
if (editableTab) 
  userOrgList=Utility.getContext(this, vars, "#User_Org", windowId, accesslevel); //editable record 
else 
  userOrgList=currentOrg;
comboTableData = new ComboTableData(vars, this, "19", "AD_Org_ID", "", "", userOrgList, Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("adOrgId"):dataField.getField("adOrgId")));
xmlDocument.setData("reportAD_Org_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "18", "Servicio_Cliente_ID", "EC3C5F382289469A83B4B32369DF6EBB", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("servicioClienteId"):dataField.getField("servicioClienteId")));
xmlDocument.setData("reportServicio_Cliente_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "18", "Vendedor_ID", "2B60113683DF450C969C42FD6D6DD8D6", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("vendedorId"):dataField.getField("vendedorId")));
xmlDocument.setData("reportVendedor_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "17", "LDT_Tipo", "336BA96AA2694CB0BD2689AB13A88F03", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("ldtTipo"):dataField.getField("ldtTipo")));
xmlDocument.setData("reportLDT_Tipo","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("buttonResultado", Utility.messageBD(this, "Calc", vars.getLanguage()));
comboTableData = new ComboTableData(vars, this, "17", "Docstatus", "81620756ABEB429EB58A014C9F3ABB3E", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("docstatus"):dataField.getField("docstatus")));
xmlDocument.setData("reportDocstatus","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("buttonCantidad", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("procesar_BTNname", Utility.getButtonName(this, vars, "2B056D3A40B349EBAD59FB72E029656F", (dataField==null?data[0].getField("procesar"):dataField.getField("procesar")), "procesar_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalprocesar = org.openbravo.erpCommon.utility.Utility.isModalProcess("24889AFB88CA4C0BB5EDF88BFE085E16"); 
xmlDocument.setParameter("procesar_Modal", modalprocesar?"true":"false");
xmlDocument.setParameter("PRE_Liquidacion_BTNname", Utility.getButtonName(this, vars, "9D0DBD06217A464FB85DAD7885A35413", "PRE_Liquidacion_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalPRE_Liquidacion = org.openbravo.erpCommon.utility.Utility.isModalProcess("295EBD91736244B688A70865CF09D078"); 
xmlDocument.setParameter("PRE_Liquidacion_Modal", modalPRE_Liquidacion?"true":"false");
xmlDocument.setParameter("Created_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Created_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
xmlDocument.setParameter("Updated_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Updated_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new ServletException(ex);
    }

    xmlDocument.setParameter("scriptOnLoad", getShortcutScript(usedButtonShortCuts, reservedButtonShortCuts));
    
    final String refererURL = vars.getSessionValue(tabId + "|requestURL");
    vars.removeSessionValue(tabId + "|requestURL");
    if(!refererURL.equals("")) {
    	final Boolean failedAutosave = (Boolean) vars.getSessionObject(tabId + "|failedAutosave");
		vars.removeSessionValue(tabId + "|failedAutosave");
    	if(failedAutosave != null && failedAutosave) {
    		final String jsFunction = "continueUserAction('"+refererURL+"');";
    		xmlDocument.setParameter("failedAutosave", jsFunction);
    	}
    }

    if (strCommand.equalsIgnoreCase("NEW")) {
      vars.removeSessionValue(tabId + "|failedAutosave");
      vars.removeSessionValue(strMappingName + "|hash");
    }

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageButtonFS(HttpServletResponse response, VariablesSecureApp vars, String strProcessId, String path) throws IOException, ServletException {
    log4j.debug("Output: Frames action button");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "org/openbravo/erpCommon/ad_actionButton/ActionButtonDefaultFrames").createXmlDocument();
    xmlDocument.setParameter("processId", strProcessId);
    xmlDocument.setParameter("trlFormType", "PROCESS");
    xmlDocument.setParameter("language", "defaultLang = \"" + vars.getLanguage() + "\";\n");
    xmlDocument.setParameter("type", strDireccion + path);
    out.println(xmlDocument.print());
    out.close();
  }

    private void printPageButtonprocesar24889AFB88CA4C0BB5EDF88BFE085E16(HttpServletResponse response, VariablesSecureApp vars, String strLDT_Pre_Liquidacion_ID, String strprocesar, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process 24889AFB88CA4C0BB5EDF88BFE085E16");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/procesar24889AFB88CA4C0BB5EDF88BFE085E16", discard).createXmlDocument();
      xmlDocument.setParameter("key", strLDT_Pre_Liquidacion_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "PreLiquidacionB43F509219314D47B91836B76F1B8091_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "24889AFB88CA4C0BB5EDF88BFE085E16");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("24889AFB88CA4C0BB5EDF88BFE085E16");
        vars.removeMessage("24889AFB88CA4C0BB5EDF88BFE085E16");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    ComboTableData comboTableData = null;
    xmlDocument.setParameter("AprobarPreliquidacion", "");
    comboTableData = new ComboTableData(vars, this, "17", "AprobarPreliquidacion", "2B056D3A40B349EBAD59FB72E029656F", "25105E6087C74C94BE7E83EBD81919AD", Utility.getContext(this, vars, "#AccessibleOrgTree", ""), Utility.getContext(this, vars, "#User_Client", ""), 0);
    Utility.fillSQLParameters(this, vars, (FieldProvider) vars.getSessionObject("button24889AFB88CA4C0BB5EDF88BFE085E16.originalParams"), comboTableData, windowId, "");
    xmlDocument.setData("reportAprobarPreliquidacion", "liststructure", comboTableData.select(false));
comboTableData = null;
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }




    private String getDisplayLogicContext(VariablesSecureApp vars, boolean isNew) throws IOException, ServletException {
      log4j.debug("Output: Display logic context fields");
      String result = "var strShowAudit=\"" +(isNew?"N":Utility.getContext(this, vars, "ShowAudit", windowId)) + "\";\n";
      return result;
    }


    private String getReadOnlyLogicContext(VariablesSecureApp vars) throws IOException, ServletException {
      log4j.debug("Output: Read Only logic context fields");
      String result = "";
      return result;
    }




 
  private String getShortcutScript( HashMap<String, String> usedButtonShortCuts, HashMap<String, String> reservedButtonShortCuts){
    StringBuffer shortcuts = new StringBuffer();
    shortcuts.append(" function buttonListShorcuts() {\n");
    Iterator<String> ik = usedButtonShortCuts.keySet().iterator();
    Iterator<String> iv = usedButtonShortCuts.values().iterator();
    while(ik.hasNext() && iv.hasNext()){
      shortcuts.append("  keyArray[keyArray.length] = new keyArrayItem(\"").append(ik.next()).append("\", \"").append(iv.next()).append("\", null, \"altKey\", false, \"onkeydown\");\n");
    }
    shortcuts.append(" return true;\n}");
    return shortcuts.toString();
  }
  
  private int saveRecord(VariablesSecureApp vars, OBError myError, char type) throws IOException, ServletException {
    PreLiquidacionB43F509219314D47B91836B76F1B8091Data data = null;
    int total = 0;
    if (org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) {
        OBError newError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
        myError.setError(newError);
        vars.setMessage(tabId, myError);
    }
    else {
        Connection con = null;
        try {
            con = this.getTransactionConnection();
            data = getEditVariables(con, vars);
            data.dateTimeFormat = vars.getSessionValue("#AD_SqlDateTimeFormat");            
            String strSequence = "";
            if(type == 'I') {                
        strSequence = SequenceIdData.getUUID();
                if(log4j.isDebugEnabled()) log4j.debug("Sequence: " + strSequence);
                data.ldtPreLiquidacionId = strSequence;  
            }
            if (Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),data.adClientId)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),data.adOrgId)){
		     if(type == 'I') {
		       total = data.insert(con, this);
		     } else {
		       //Check the version of the record we are saving is the one in DB
		       if (PreLiquidacionB43F509219314D47B91836B76F1B8091Data.getCurrentDBTimestamp(this, data.ldtPreLiquidacionId).equals(
                vars.getStringParameter("updatedTimestamp"))) {
                total = data.update(con, this);
               } else {
                 myError.setMessage(Replace.replace(Replace.replace(Utility.messageBD(this,
                    "SavingModifiedRecord", vars.getLanguage()), "\\n", "<br/>"), "&quot;", "\""));
                 myError.setType("Error");
                 vars.setSessionValue(tabId + "|concurrentSave", "true");
               } 
		     }		            
          
            }
                else {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
            myError.setError(newError);            
          }
          releaseCommitConnection(con);
        } catch(Exception ex) {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
            myError.setError(newError);   
            try {
              releaseRollbackConnection(con);
            } catch (final Exception e) { //do nothing 
            }           
        }
            
        if (myError.isEmpty() && total == 0) {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=DBExecuteError");
            myError.setError(newError);
        }
        vars.setMessage(tabId, myError);
            
        if(!myError.isEmpty()){
            if(data != null ) {
                if(type == 'I') {            			
                    data.ldtPreLiquidacionId = "";
                }
                else {                    
                    
                        //BUTTON TEXT FILLING
                    data.procesarBtn = ActionButtonDefaultData.getText(this, vars.getLanguage(), "2B056D3A40B349EBAD59FB72E029656F", data.getField("procesar"));
                    
                }
                vars.setEditionData(tabId, data);
            }            	
        }
        else {
            vars.setSessionValue(windowId + "|LDT_Pre_Liquidacion_ID", data.ldtPreLiquidacionId);
        }
    }
    return total;
  }

  public String getServletInfo() {
    return "Servlet PreLiquidacionB43F509219314D47B91836B76F1B8091. This Servlet was made by Wad constructor";
  } // End of getServletInfo() method
}
