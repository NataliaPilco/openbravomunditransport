
package org.openbravo.erpWindows.com.atrums.logisticadetransporte.parametrizacion.Cotizacion;


import org.openbravo.erpCommon.reference.*;


import org.openbravo.erpCommon.ad_actionButton.*;


import org.codehaus.jettison.json.JSONObject;
import org.openbravo.erpCommon.utility.*;
import org.openbravo.data.FieldProvider;
import org.openbravo.utils.FormatUtilities;
import org.openbravo.utils.Replace;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.exception.OBException;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessRunner;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.xmlEngine.XmlDocument;
import java.util.Vector;
import java.util.StringTokenizer;
import org.openbravo.database.SessionInfo;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import java.sql.Connection;
import org.apache.log4j.Logger;

public class Cotizacion221A13EAEB064B3387B2BB9203B1E3F3 extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;
  
  private static Logger log4j = Logger.getLogger(Cotizacion221A13EAEB064B3387B2BB9203B1E3F3.class);
  
  private static final String windowId = "4D8F2DAE17EB4578AD6EC29C0C34F806";
  private static final String tabId = "221A13EAEB064B3387B2BB9203B1E3F3";
  private static final String defaultTabView = "RELATION";
  private static final int accesslevel = 3;
  private static final String moduleId = "C5BE74383A264BF68316D6BF975E6EFC";
  
  @Override
  public void init(ServletConfig config) {
    setClassInfo("W", tabId, moduleId);
    super.init(config);
  }
  
  
  @Override
  public void service(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    String command = vars.getCommand();
    
    boolean securedProcess = false;
    if (command.contains("BUTTON")) {
      try {
        securedProcess = "Y".equals(org.openbravo.erpCommon.businessUtility.Preferences
            .getPreferenceValue("SecuredProcess", true, vars.getClient(), vars.getOrg(), vars
                .getUser(), vars.getRole(), windowId));
      } catch (PropertyException e) {
      }
    
     
      if (command.contains("35AEE2126EB54D1E9A7D3A1BE1A18852")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("35AEE2126EB54D1E9A7D3A1BE1A18852");
        SessionInfo.setModuleId("C5BE74383A264BF68316D6BF975E6EFC");
        if (securedProcess) {
          classInfo.type = "P";
          classInfo.id = "35AEE2126EB54D1E9A7D3A1BE1A18852";
        }
      }
     
      if (command.contains("7E5DB05C594E4CC4AFFC0B4F78FC8051")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("7E5DB05C594E4CC4AFFC0B4F78FC8051");
        SessionInfo.setModuleId("C5BE74383A264BF68316D6BF975E6EFC");
        if (securedProcess) {
          classInfo.type = "P";
          classInfo.id = "7E5DB05C594E4CC4AFFC0B4F78FC8051";
        }
      }
     
     
    }
    if (!securedProcess) {
      setClassInfo("W", tabId, moduleId);
    }
    super.service(request, response);
  }
  

  public void doPost (HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException {
    TableSQLData tableSQL = null;
    VariablesSecureApp vars = new VariablesSecureApp(request);
    Boolean saveRequest = (Boolean) request.getAttribute("autosave");
    
    if(saveRequest != null && saveRequest){
      String currentOrg = vars.getStringParameter("inpadOrgId");
      String currentClient = vars.getStringParameter("inpadClientId");
      boolean editableTab = (!org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)
                            && (currentOrg.equals("") || Utility.isElementInList(Utility.getContext(this, vars,"#User_Org", windowId, accesslevel), currentOrg)) 
                            && (currentClient.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),currentClient)));
    
        OBError myError = new OBError();
        String commandType = request.getParameter("inpCommandType");
        String strldtCotizacionId = request.getParameter("inpldtCotizacionId");
        
        if (editableTab) {
          int total = 0;
          
          if(commandType.equalsIgnoreCase("EDIT") && !strldtCotizacionId.equals(""))
              total = saveRecord(vars, myError, 'U');
          else
              total = saveRecord(vars, myError, 'I');
          
          if (!myError.isEmpty() && total == 0)     
            throw new OBException(myError.getMessage());
        }
        vars.setSessionValue(request.getParameter("mappingName") +"|hash", vars.getPostDataHash());
        vars.setSessionValue(tabId + "|Header.view", "EDIT");
        
        return;
    }
    
    try {
      tableSQL = new TableSQLData(vars, this, tabId, Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel), Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    String strOrderBy = vars.getSessionValue(tabId + "|orderby");
    if (!strOrderBy.equals("")) {
      vars.setSessionValue(tabId + "|newOrder", "1");
    }

    if (vars.commandIn("DEFAULT")) {

      String strLDT_Cotizacion_ID = vars.getGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID", "");
      

      String strView = vars.getSessionValue(tabId + "|Cotizacion221A13EAEB064B3387B2BB9203B1E3F3.view");
      if (strView.equals("")) {
        strView = defaultTabView;

        if (strView.equals("EDIT")) {
          if (strLDT_Cotizacion_ID.equals("")) strLDT_Cotizacion_ID = firstElement(vars, tableSQL);
          if (strLDT_Cotizacion_ID.equals("")) strView = "RELATION";
        }
      }
      if (strView.equals("EDIT")) 

        printPageEdit(response, request, vars, false, strLDT_Cotizacion_ID, tableSQL);

      else printPageDataSheet(response, vars, strLDT_Cotizacion_ID, tableSQL);
    } else if (vars.commandIn("DIRECT")) {
      String strLDT_Cotizacion_ID = vars.getStringParameter("inpDirectKey");
      
        
      if (strLDT_Cotizacion_ID.equals("")) strLDT_Cotizacion_ID = vars.getRequiredGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID");
      else vars.setSessionValue(windowId + "|LDT_Cotizacion_ID", strLDT_Cotizacion_ID);
      
      vars.setSessionValue(tabId + "|Cotizacion221A13EAEB064B3387B2BB9203B1E3F3.view", "EDIT");

      printPageEdit(response, request, vars, false, strLDT_Cotizacion_ID, tableSQL);

    } else if (vars.commandIn("TAB")) {


      String strView = vars.getSessionValue(tabId + "|Cotizacion221A13EAEB064B3387B2BB9203B1E3F3.view");
      String strLDT_Cotizacion_ID = "";
      if (strView.equals("")) {
        strView = defaultTabView;
        if (strView.equals("EDIT")) {
          strLDT_Cotizacion_ID = firstElement(vars, tableSQL);
          if (strLDT_Cotizacion_ID.equals("")) strView = "RELATION";
        }
      }
      if (strView.equals("EDIT")) {

        if (strLDT_Cotizacion_ID.equals("")) strLDT_Cotizacion_ID = firstElement(vars, tableSQL);
        printPageEdit(response, request, vars, false, strLDT_Cotizacion_ID, tableSQL);

      } else printPageDataSheet(response, vars, "", tableSQL);
    } else if (vars.commandIn("SEARCH")) {
vars.getRequestGlobalVariable("inpParamC_Bpartner_ID", tabId + "|paramC_Bpartner_ID");
vars.getRequestGlobalVariable("inpParamDocumentno", tabId + "|paramDocumentno");

        vars.getRequestGlobalVariable("inpParamUpdated", tabId + "|paramUpdated");
        vars.getRequestGlobalVariable("inpParamUpdatedBy", tabId + "|paramUpdatedBy");
        vars.getRequestGlobalVariable("inpParamCreated", tabId + "|paramCreated");
        vars.getRequestGlobalVariable("inpparamCreatedBy", tabId + "|paramCreatedBy");
      
      
      vars.removeSessionValue(windowId + "|LDT_Cotizacion_ID");
      String strLDT_Cotizacion_ID="";

      String strView = vars.getSessionValue(tabId + "|Cotizacion221A13EAEB064B3387B2BB9203B1E3F3.view");
      if (strView.equals("")) strView=defaultTabView;

      if (strView.equals("EDIT")) {
        strLDT_Cotizacion_ID = firstElement(vars, tableSQL);
        if (strLDT_Cotizacion_ID.equals("")) {
          // filter returns empty set
          strView = "RELATION";
          // switch to grid permanently until the user changes the view again
          vars.setSessionValue(tabId + "|Cotizacion221A13EAEB064B3387B2BB9203B1E3F3.view", strView);
        }
      }

      if (strView.equals("EDIT")) 

        printPageEdit(response, request, vars, false, strLDT_Cotizacion_ID, tableSQL);

      else printPageDataSheet(response, vars, strLDT_Cotizacion_ID, tableSQL);
    } else if (vars.commandIn("RELATION")) {
      

      String strLDT_Cotizacion_ID = vars.getGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID", "");
      vars.setSessionValue(tabId + "|Cotizacion221A13EAEB064B3387B2BB9203B1E3F3.view", "RELATION");
      printPageDataSheet(response, vars, strLDT_Cotizacion_ID, tableSQL);
    } else if (vars.commandIn("NEW")) {


      printPageEdit(response, request, vars, true, "", tableSQL);

    } else if (vars.commandIn("EDIT")) {

      @SuppressWarnings("unused") // In Expense Invoice tab this variable is not used, to be fixed
      String strLDT_Cotizacion_ID = vars.getGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID", "");
      vars.setSessionValue(tabId + "|Cotizacion221A13EAEB064B3387B2BB9203B1E3F3.view", "EDIT");

      setHistoryCommand(request, "EDIT");
      printPageEdit(response, request, vars, false, strLDT_Cotizacion_ID, tableSQL);

    } else if (vars.commandIn("NEXT")) {

      String strLDT_Cotizacion_ID = vars.getRequiredStringParameter("inpldtCotizacionId");
      
      String strNext = nextElement(vars, strLDT_Cotizacion_ID, tableSQL);

      printPageEdit(response, request, vars, false, strNext, tableSQL);
    } else if (vars.commandIn("PREVIOUS")) {

      String strLDT_Cotizacion_ID = vars.getRequiredStringParameter("inpldtCotizacionId");
      
      String strPrevious = previousElement(vars, strLDT_Cotizacion_ID, tableSQL);

      printPageEdit(response, request, vars, false, strPrevious, tableSQL);
    } else if (vars.commandIn("FIRST_RELATION")) {

      vars.setSessionValue(tabId + "|Cotizacion221A13EAEB064B3387B2BB9203B1E3F3.initRecordNumber", "0");
      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("PREVIOUS_RELATION")) {

      String strInitRecord = vars.getSessionValue(tabId + "|Cotizacion221A13EAEB064B3387B2BB9203B1E3F3.initRecordNumber");
      String strRecordRange = Utility.getContext(this, vars, "#RecordRange", windowId);
      int intRecordRange = strRecordRange.equals("")?0:Integer.parseInt(strRecordRange);
      if (strInitRecord.equals("") || strInitRecord.equals("0")) {
        vars.setSessionValue(tabId + "|Cotizacion221A13EAEB064B3387B2BB9203B1E3F3.initRecordNumber", "0");
      } else {
        int initRecord = (strInitRecord.equals("")?0:Integer.parseInt(strInitRecord));
        initRecord -= intRecordRange;
        strInitRecord = ((initRecord<0)?"0":Integer.toString(initRecord));
        vars.setSessionValue(tabId + "|Cotizacion221A13EAEB064B3387B2BB9203B1E3F3.initRecordNumber", strInitRecord);
      }
      vars.removeSessionValue(windowId + "|LDT_Cotizacion_ID");

      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("NEXT_RELATION")) {

      String strInitRecord = vars.getSessionValue(tabId + "|Cotizacion221A13EAEB064B3387B2BB9203B1E3F3.initRecordNumber");
      String strRecordRange = Utility.getContext(this, vars, "#RecordRange", windowId);
      int intRecordRange = strRecordRange.equals("")?0:Integer.parseInt(strRecordRange);
      int initRecord = (strInitRecord.equals("")?0:Integer.parseInt(strInitRecord));
      if (initRecord==0) initRecord=1;
      initRecord += intRecordRange;
      strInitRecord = ((initRecord<0)?"0":Integer.toString(initRecord));
      vars.setSessionValue(tabId + "|Cotizacion221A13EAEB064B3387B2BB9203B1E3F3.initRecordNumber", strInitRecord);
      vars.removeSessionValue(windowId + "|LDT_Cotizacion_ID");

      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("FIRST")) {

      
      String strFirst = firstElement(vars, tableSQL);

      printPageEdit(response, request, vars, false, strFirst, tableSQL);
    } else if (vars.commandIn("LAST_RELATION")) {

      String strLast = lastElement(vars, tableSQL);
      printPageDataSheet(response, vars, strLast, tableSQL);
    } else if (vars.commandIn("LAST")) {

      
      String strLast = lastElement(vars, tableSQL);

      printPageEdit(response, request, vars, false, strLast, tableSQL);
    } else if (vars.commandIn("SAVE_NEW_RELATION", "SAVE_NEW_NEW", "SAVE_NEW_EDIT")) {

      OBError myError = new OBError();      
      int total = saveRecord(vars, myError, 'I');      
      if (!myError.isEmpty()) {        
        response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
      } 
      else {
		if (myError.isEmpty()) {
		  myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsInserted");
		  myError.setMessage(total + " " + myError.getMessage());
		  vars.setMessage(tabId, myError);
		}        
        if (vars.commandIn("SAVE_NEW_NEW")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
        else if (vars.commandIn("SAVE_NEW_EDIT")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        else response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
      }
    } else if (vars.commandIn("SAVE_EDIT_RELATION", "SAVE_EDIT_NEW", "SAVE_EDIT_EDIT", "SAVE_EDIT_NEXT")) {

      String strLDT_Cotizacion_ID = vars.getRequiredGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID");
      OBError myError = new OBError();
      int total = saveRecord(vars, myError, 'U');      
      if (!myError.isEmpty()) {
        response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
      } 
      else {
        if (myError.isEmpty()) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsUpdated");
          myError.setMessage(total + " " + myError.getMessage());
          vars.setMessage(tabId, myError);
        }
        if (vars.commandIn("SAVE_EDIT_NEW")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
        else if (vars.commandIn("SAVE_EDIT_EDIT")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        else if (vars.commandIn("SAVE_EDIT_NEXT")) {
          String strNext = nextElement(vars, strLDT_Cotizacion_ID, tableSQL);
          vars.setSessionValue(windowId + "|LDT_Cotizacion_ID", strNext);
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        } else response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
      }
    } else if (vars.commandIn("DELETE")) {

      String strLDT_Cotizacion_ID = vars.getRequiredStringParameter("inpldtCotizacionId");
      //Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data data = getEditVariables(vars);
      int total = 0;
      OBError myError = null;
      if (org.openbravo.erpCommon.utility.WindowAccessData.hasNotDeleteAccess(this, vars.getRole(), tabId)) {
        myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
        vars.setMessage(tabId, myError);
      } else {
        try {
          total = Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.delete(this, strLDT_Cotizacion_ID, Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), Utility.getContext(this, vars, "#User_Org", windowId, accesslevel));
        } catch(ServletException ex) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myError.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myError);
        }
        if (myError==null && total==0) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
        }
        vars.removeSessionValue(windowId + "|ldtCotizacionId");
        vars.setSessionValue(tabId + "|Cotizacion221A13EAEB064B3387B2BB9203B1E3F3.view", "RELATION");
      }
      if (myError==null) {
        myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsDeleted");
        myError.setMessage(total + " " + myError.getMessage());
        vars.setMessage(tabId, myError);
      }
      response.sendRedirect(strDireccion + request.getServletPath());

     } else if (vars.commandIn("BUTTONProcessed35AEE2126EB54D1E9A7D3A1BE1A18852")) {
        vars.setSessionValue("button35AEE2126EB54D1E9A7D3A1BE1A18852.strprocessed", vars.getStringParameter("inpprocessed"));
        vars.setSessionValue("button35AEE2126EB54D1E9A7D3A1BE1A18852.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("button35AEE2126EB54D1E9A7D3A1BE1A18852.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("button35AEE2126EB54D1E9A7D3A1BE1A18852.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        p.put("Processed", vars.getStringParameter("inpprocessed"));

        
        //Save in session needed params for combos if needed
        vars.setSessionObject("button35AEE2126EB54D1E9A7D3A1BE1A18852.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "35AEE2126EB54D1E9A7D3A1BE1A18852", request.getServletPath());    
     } else if (vars.commandIn("BUTTON35AEE2126EB54D1E9A7D3A1BE1A18852")) {
        String strLDT_Cotizacion_ID = vars.getGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID", "");
        String strprocessed = vars.getSessionValue("button35AEE2126EB54D1E9A7D3A1BE1A18852.strprocessed");
        String strProcessing = vars.getSessionValue("button35AEE2126EB54D1E9A7D3A1BE1A18852.strProcessing");
        String strOrg = vars.getSessionValue("button35AEE2126EB54D1E9A7D3A1BE1A18852.strOrg");
        String strClient = vars.getSessionValue("button35AEE2126EB54D1E9A7D3A1BE1A18852.strClient");
        
        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtonProcessed35AEE2126EB54D1E9A7D3A1BE1A18852(response, vars, strLDT_Cotizacion_ID, strprocessed, strProcessing);
        }

     } else if (vars.commandIn("BUTTONBTN_Generaro7E5DB05C594E4CC4AFFC0B4F78FC8051")) {
        vars.setSessionValue("button7E5DB05C594E4CC4AFFC0B4F78FC8051.strbtnGeneraro", vars.getStringParameter("inpbtnGeneraro"));
        vars.setSessionValue("button7E5DB05C594E4CC4AFFC0B4F78FC8051.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("button7E5DB05C594E4CC4AFFC0B4F78FC8051.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("button7E5DB05C594E4CC4AFFC0B4F78FC8051.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        
        
        //Save in session needed params for combos if needed
        vars.setSessionObject("button7E5DB05C594E4CC4AFFC0B4F78FC8051.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "7E5DB05C594E4CC4AFFC0B4F78FC8051", request.getServletPath());    
     } else if (vars.commandIn("BUTTON7E5DB05C594E4CC4AFFC0B4F78FC8051")) {
        String strLDT_Cotizacion_ID = vars.getGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID", "");
        String strbtnGeneraro = vars.getSessionValue("button7E5DB05C594E4CC4AFFC0B4F78FC8051.strbtnGeneraro");
        String strProcessing = vars.getSessionValue("button7E5DB05C594E4CC4AFFC0B4F78FC8051.strProcessing");
        String strOrg = vars.getSessionValue("button7E5DB05C594E4CC4AFFC0B4F78FC8051.strOrg");
        String strClient = vars.getSessionValue("button7E5DB05C594E4CC4AFFC0B4F78FC8051.strClient");
        
        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtonBTN_Generaro7E5DB05C594E4CC4AFFC0B4F78FC8051(response, vars, strLDT_Cotizacion_ID, strbtnGeneraro, strProcessing);
        }


    } else if (vars.commandIn("SAVE_BUTTONProcessed35AEE2126EB54D1E9A7D3A1BE1A18852")) {
        String strLDT_Cotizacion_ID = vars.getGlobalVariable("inpKey", windowId + "|LDT_Cotizacion_ID", "");
        @SuppressWarnings("unused")
        String strprocessed = vars.getStringParameter("inpprocessed");
        String strProcessing = vars.getStringParameter("inpprocessing");
        OBError myMessage = null;
        try {
          String pinstance = SequenceIdData.getUUID();
          PInstanceProcessData.insertPInstance(this, pinstance, "35AEE2126EB54D1E9A7D3A1BE1A18852", (("LDT_Cotizacion_ID".equalsIgnoreCase("AD_Language"))?"0":strLDT_Cotizacion_ID), strProcessing, vars.getUser(), vars.getClient(), vars.getOrg());
          String straccioncotizacion = vars.getStringParameter("inpaccioncotizacion");
PInstanceProcessData.insertPInstanceParam(this, pinstance, "10", "AccionCotizacion", straccioncotizacion, vars.getClient(), vars.getOrg(), vars.getUser());

          
          ProcessBundle bundle = ProcessBundle.pinstance(pinstance, vars, this);
          new ProcessRunner(bundle).execute(this);
          
          PInstanceProcessData[] pinstanceData = PInstanceProcessData.select(this, pinstance);
          myMessage = Utility.getProcessInstanceMessage(this, vars, pinstanceData);
        } catch (ServletException ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);
    } else if (vars.commandIn("SAVE_BUTTONBTN_Generaro7E5DB05C594E4CC4AFFC0B4F78FC8051")) {
        String strLDT_Cotizacion_ID = vars.getGlobalVariable("inpKey", windowId + "|LDT_Cotizacion_ID", "");
        @SuppressWarnings("unused")
        String strbtnGeneraro = vars.getStringParameter("inpbtnGeneraro");
        String strProcessing = vars.getStringParameter("inpprocessing");
        OBError myMessage = null;
        try {
          String pinstance = SequenceIdData.getUUID();
          PInstanceProcessData.insertPInstance(this, pinstance, "7E5DB05C594E4CC4AFFC0B4F78FC8051", (("LDT_Cotizacion_ID".equalsIgnoreCase("AD_Language"))?"0":strLDT_Cotizacion_ID), strProcessing, vars.getUser(), vars.getClient(), vars.getOrg());
          
          
          ProcessBundle bundle = ProcessBundle.pinstance(pinstance, vars, this);
          new ProcessRunner(bundle).execute(this);
          
          PInstanceProcessData[] pinstanceData = PInstanceProcessData.select(this, pinstance);
          myMessage = Utility.getProcessInstanceMessage(this, vars, pinstanceData);
        } catch (ServletException ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);






    } else if (vars.commandIn("SAVE_XHR")) {
      
      OBError myError = new OBError();
      JSONObject result = new JSONObject();
      String commandType = vars.getStringParameter("inpCommandType");
      char saveType = "NEW".equals(commandType) ? 'I' : 'U';
      try {
        int total = saveRecord(vars, myError, saveType);
        if (myError.isEmpty()) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsUpdated");
          myError.setMessage(total + " " + myError.getMessage());
          myError.setType("Success");
        }
        result.put("oberror", myError.toMap());
        result.put("tabid", vars.getStringParameter("tabID"));
        result.put("redirect", strDireccion + request.getServletPath() + "?Command=" + commandType);
      } catch (Exception e) {
        log4j.error("Error saving record (XHR request): " + e.getMessage(), e);
        myError.setType("Error");
        myError.setMessage(e.getMessage());
      }

      response.setContentType("application/json");
      PrintWriter out = response.getWriter();
      out.print(result.toString());
      out.flush();
      out.close();
    } else if (vars.getCommand().toUpperCase().startsWith("BUTTON") || vars.getCommand().toUpperCase().startsWith("SAVE_BUTTON")) {
      pageErrorPopUp(response);
    } else pageError(response);
  }
  private Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data getEditVariables(Connection con, VariablesSecureApp vars) throws IOException,ServletException {
    Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data data = new Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data();
    ServletException ex = null;
    try {
    data.adOrgId = vars.getRequiredGlobalVariable("inpadOrgId", windowId + "|AD_Org_ID");     data.adOrgIdr = vars.getStringParameter("inpadOrgId_R");     data.cDoctypeId = vars.getRequiredStringParameter("inpcDoctypeId");     data.cDoctypeIdr = vars.getStringParameter("inpcDoctypeId_R");     data.documentno = vars.getRequiredStringParameter("inpdocumentno");     data.fecha = vars.getRequiredStringParameter("inpfecha");     data.cBpartnerId = vars.getRequiredGlobalVariable("inpcBpartnerId", windowId + "|C_Bpartner_ID");     data.cBpartnerLocationId = vars.getRequiredStringParameter("inpcBpartnerLocationId");     data.cBpartnerLocationIdr = vars.getStringParameter("inpcBpartnerLocationId_R");     data.vendedorId = vars.getRequiredGlobalVariable("inpvendedorId", windowId + "|Vendedor_ID");     data.vendedorIdr = vars.getStringParameter("inpvendedorId_R");     data.servicioClienteId = vars.getRequiredGlobalVariable("inpservicioClienteId", windowId + "|Servicio_Cliente_ID");     data.servicioClienteIdr = vars.getStringParameter("inpservicioClienteId_R");     data.descripcion = vars.getStringParameter("inpdescripcion");     data.modalidad = vars.getRequiredGlobalVariable("inpmodalidad", windowId + "|Modalidad");     data.modalidadr = vars.getStringParameter("inpmodalidad_R");     data.tipo = vars.getRequiredGlobalVariable("inptipo", windowId + "|Tipo");     data.tipor = vars.getStringParameter("inptipo_R");     data.frecuencia = vars.getStringParameter("inpfrecuencia");     data.frecuenciar = vars.getStringParameter("inpfrecuencia_R");     data.processed = vars.getRequiredGlobalVariable("inpprocessed", windowId + "|Processed");     data.cIncotermsId = vars.getRequiredGlobalVariable("inpcIncotermsId", windowId + "|C_Incoterms_ID");     data.cIncotermsIdr = vars.getStringParameter("inpcIncotermsId_R");     data.isactive = vars.getStringParameter("inpisactive", "N");     data.btnGeneraro = vars.getStringParameter("inpbtnGeneraro");     data.tarifaProductos = vars.getStringParameter("inptarifaProductos");     data.opcion = vars.getRequiredInputGlobalVariable("inpopcion", windowId + "|opcion", "N");     data.adUserVendedorId = vars.getStringParameter("inpadUserVendedorId");     data.adUserSclienteId = vars.getStringParameter("inpadUserSclienteId");     data.ldtCotizacionId = vars.getRequestGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID");     data.adClientId = vars.getRequiredGlobalVariable("inpadClientId", windowId + "|AD_Client_ID");     data.estado = vars.getRequiredGlobalVariable("inpestado", windowId + "|Estado"); 
      data.createdby = vars.getUser();
      data.updatedby = vars.getUser();
      data.adUserClient = Utility.getContext(this, vars, "#User_Client", windowId, accesslevel);
      data.adOrgClient = Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel);
      data.updatedTimeStamp = vars.getStringParameter("updatedTimestamp");



    
    

    
    }
    catch(ServletException e) {
    	vars.setEditionData(tabId, data);
    	throw e;
    }
    // Behavior with exception for numeric fields is to catch last one if we have multiple ones
    if(ex != null) {
      vars.setEditionData(tabId, data);
      throw ex;
    }
    return data;
  }




    private void refreshSessionEdit(VariablesSecureApp vars, FieldProvider[] data) {
      if (data==null || data.length==0) return;
          vars.setSessionValue(windowId + "|AD_Org_ID", data[0].getField("adOrgId"));    vars.setSessionValue(windowId + "|C_BPartner_ID", data[0].getField("cBpartnerId"));    vars.setSessionValue(windowId + "|Vendedor_ID", data[0].getField("vendedorId"));    vars.setSessionValue(windowId + "|Servicio_Cliente_ID", data[0].getField("servicioClienteId"));    vars.setSessionValue(windowId + "|Modalidad", data[0].getField("modalidad"));    vars.setSessionValue(windowId + "|Tipo", data[0].getField("tipo"));    vars.setSessionValue(windowId + "|Processed", data[0].getField("processed"));    vars.setSessionValue(windowId + "|C_Incoterms_ID", data[0].getField("cIncotermsId"));    vars.setSessionValue(windowId + "|opcion", data[0].getField("opcion"));    vars.setSessionValue(windowId + "|AD_Client_ID", data[0].getField("adClientId"));    vars.setSessionValue(windowId + "|LDT_Cotizacion_ID", data[0].getField("ldtCotizacionId"));    vars.setSessionValue(windowId + "|Estado", data[0].getField("estado"));
    }

    private void refreshSessionNew(VariablesSecureApp vars) throws IOException,ServletException {
      Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[] data = Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), vars.getStringParameter("inpldtCotizacionId", ""), Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
      if (data==null || data.length==0) return;
      refreshSessionEdit(vars, data);
    }

  private String nextElement(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (strSelected == null || strSelected.equals("")) return firstElement(vars, tableSQL);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(), 0, 0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.NEXT, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting next element", e);
      }
      if (data!=null) {
        if (data!=null) return data;
      }
    }
    return strSelected;
  }

  private int getKeyPosition(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("getKeyPosition: " + strSelected);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.GETPOSITION, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting key position", e);
      }
      if (data!=null) {
        // split offset -> (page,relativeOffset)
        int absoluteOffset = Integer.valueOf(data);
        int page = absoluteOffset / TableSQLData.maxRowsPerGridPage;
        int relativeOffset = absoluteOffset % TableSQLData.maxRowsPerGridPage;
        log4j.debug("getKeyPosition: absOffset: " + absoluteOffset + "=> page: " + page + " relOffset: " + relativeOffset);
        String currPageKey = tabId + "|" + "currentPage";
        vars.setSessionValue(currPageKey, String.valueOf(page));
        return relativeOffset;
      }
    }
    return 0;
  }

  private String previousElement(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (strSelected == null || strSelected.equals("")) return firstElement(vars, tableSQL);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.PREVIOUS, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting previous element", e);
      }
      if (data!=null) {
        return data;
      }
    }
    return strSelected;
  }

  private String firstElement(VariablesSecureApp vars, TableSQLData tableSQL) throws IOException, ServletException {
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,1);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.FIRST, "", tableSQL.getKeyColumn());

      } catch (Exception e) { 
        log4j.debug("Error getting first element", e);
      }
      if (data!=null) return data;
    }
    return "";
  }

  private String lastElement(VariablesSecureApp vars, TableSQLData tableSQL) throws IOException, ServletException {
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.LAST, "", tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting last element", e);
      }
      if (data!=null) return data;
    }
    return "";
  }

  private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars, String strLDT_Cotizacion_ID, TableSQLData tableSQL)
    throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("Output: dataSheet");

    String strParamC_Bpartner_ID = vars.getSessionValue(tabId + "|paramC_Bpartner_ID");
String strParamDocumentno = vars.getSessionValue(tabId + "|paramDocumentno");

    boolean hasSearchCondition=false;
    vars.removeEditionData(tabId);
    hasSearchCondition = (tableSQL.hasInternalFilter() && ("").equals(strParamC_Bpartner_ID) && ("").equals(strParamDocumentno)) || !(("").equals(strParamC_Bpartner_ID) || ("%").equals(strParamC_Bpartner_ID))  || !(("").equals(strParamDocumentno) || ("%").equals(strParamDocumentno)) ;
    String strOffset = vars.getSessionValue(tabId + "|offset");
    String selectedRow = "0";
    if (!strLDT_Cotizacion_ID.equals("")) {
      selectedRow = Integer.toString(getKeyPosition(vars, strLDT_Cotizacion_ID, tableSQL));
    }

    String[] discard={"isNotFiltered","isNotTest"};
    if (hasSearchCondition) discard[0] = new String("isFiltered");
    if (vars.getSessionValue("#ShowTest", "N").equals("Y")) discard[1] = new String("isTest");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/logisticadetransporte/parametrizacion/Cotizacion/Cotizacion221A13EAEB064B3387B2BB9203B1E3F3_Relation", discard).createXmlDocument();

    boolean hasReadOnlyAccess = org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId);
    ToolBar toolbar = new ToolBar(this, true, vars.getLanguage(), "Cotizacion221A13EAEB064B3387B2BB9203B1E3F3", false, "document.frmMain.inpldtCotizacionId", "grid", "../com.atrums.logisticadetransporte.parametrizacion/orders/print.html", "N".equals("Y"), "Cotizacion", strReplaceWith, false, false, false, false, !hasReadOnlyAccess);
    toolbar.setTabId(tabId);
    
    toolbar.setDeleteable(true && !hasReadOnlyAccess);
    toolbar.prepareRelationTemplate("N".equals("Y"), hasSearchCondition, !vars.getSessionValue("#ShowTest", "N").equals("Y"), false, Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    xmlDocument.setParameter("toolbar", toolbar.toString());



    StringBuffer orderByArray = new StringBuffer();
      vars.setSessionValue(tabId + "|newOrder", "1");
      String positions = vars.getSessionValue(tabId + "|orderbyPositions");
      orderByArray.append("var orderByPositions = new Array(\n");
      if (!positions.equals("")) {
        StringTokenizer tokens=new StringTokenizer(positions, ",");
        boolean firstOrder = true;
        while(tokens.hasMoreTokens()){
          if (!firstOrder) orderByArray.append(",\n");
          orderByArray.append("\"").append(tokens.nextToken()).append("\"");
          firstOrder = false;
        }
      }
      orderByArray.append(");\n");
      String directions = vars.getSessionValue(tabId + "|orderbyDirections");
      orderByArray.append("var orderByDirections = new Array(\n");
      if (!positions.equals("")) {
        StringTokenizer tokens=new StringTokenizer(directions, ",");
        boolean firstOrder = true;
        while(tokens.hasMoreTokens()){
          if (!firstOrder) orderByArray.append(",\n");
          orderByArray.append("\"").append(tokens.nextToken()).append("\"");
          firstOrder = false;
        }
      }
      orderByArray.append(");\n");
//    }

    xmlDocument.setParameter("selectedColumn", "\nvar selectedRow = " + selectedRow + ";\n" + orderByArray.toString());
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("windowId", windowId);
    xmlDocument.setParameter("KeyName", "ldtCotizacionId");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    //xmlDocument.setParameter("buttonReference", Utility.messageBD(this, "Reference", vars.getLanguage()));
    try {
      WindowTabs tabs = new WindowTabs(this, vars, tabId, windowId, false);
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      String hideBackButton = vars.getGlobalVariable("hideMenu", "#Hide_BackButton", "");
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "Cotizacion221A13EAEB064B3387B2BB9203B1E3F3_Relation.html", "Cotizacion", "W", strReplaceWith, tabs.breadcrumb(), hideBackButton.equals("true"));
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "Cotizacion221A13EAEB064B3387B2BB9203B1E3F3_Relation.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.relationTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    {
      OBError myMessage = vars.getMessage(tabId);
      vars.removeMessage(tabId);
      if (myMessage!=null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }


    xmlDocument.setParameter("grid", Utility.getContext(this, vars, "#RecordRange", windowId));
xmlDocument.setParameter("grid_Offset", strOffset);
xmlDocument.setParameter("grid_SortCols", positions);
xmlDocument.setParameter("grid_SortDirs", directions);
xmlDocument.setParameter("grid_Default", selectedRow);


    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageEdit(HttpServletResponse response, HttpServletRequest request, VariablesSecureApp vars,boolean boolNew, String strLDT_Cotizacion_ID, TableSQLData tableSQL)
    throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("Output: edit");
    
    HashMap<String, String> usedButtonShortCuts;
  
    HashMap<String, String> reservedButtonShortCuts;
  
    usedButtonShortCuts = new HashMap<String, String>();
    
    reservedButtonShortCuts = new HashMap<String, String>();
    
    
    
    String strOrderByFilter = vars.getSessionValue(tabId + "|orderby");
    String orderClause = " 1";
    if (strOrderByFilter==null || strOrderByFilter.equals("")) strOrderByFilter = orderClause;
    /*{
      if (!strOrderByFilter.equals("") && !orderClause.equals("")) strOrderByFilter += ", ";
      strOrderByFilter += orderClause;
    }*/
    
    
    String strCommand = null;
    Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[] data=null;
    XmlDocument xmlDocument=null;
    FieldProvider dataField = vars.getEditionData(tabId);
    vars.removeEditionData(tabId);
    String strParamC_Bpartner_ID = vars.getSessionValue(tabId + "|paramC_Bpartner_ID");
String strParamDocumentno = vars.getSessionValue(tabId + "|paramDocumentno");

    boolean hasSearchCondition=false;
    hasSearchCondition = (tableSQL.hasInternalFilter() && ("").equals(strParamC_Bpartner_ID) && ("").equals(strParamDocumentno)) || !(("").equals(strParamC_Bpartner_ID) || ("%").equals(strParamC_Bpartner_ID))  || !(("").equals(strParamDocumentno) || ("%").equals(strParamDocumentno)) ;

       String strParamSessionDate = vars.getGlobalVariable("inpParamSessionDate", Utility.getTransactionalDate(this, vars, windowId), "");
      String buscador = "";
      String[] discard = {"", "isNotTest"};
      
      if (vars.getSessionValue("#ShowTest", "N").equals("Y")) discard[1] = new String("isTest");
    if (dataField==null) {
      if (!boolNew) {
        discard[0] = new String("newDiscard");
        data = Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), strLDT_Cotizacion_ID, Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
  
        if (!strLDT_Cotizacion_ID.equals("") && (data == null || data.length==0)) {
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
          return;
        }
        refreshSessionEdit(vars, data);
        strCommand = "EDIT";
      }

      if (boolNew || data==null || data.length==0) {
        discard[0] = new String ("editDiscard");
        strCommand = "NEW";
        data = new Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[0];
      } else {
        discard[0] = new String ("newDiscard");
      }
    } else {
      if (dataField.getField("ldtCotizacionId") == null || dataField.getField("ldtCotizacionId").equals("")) {
        discard[0] = new String ("editDiscard");
        strCommand = "NEW";
        boolNew = true;
      } else {
        discard[0] = new String ("newDiscard");
        strCommand = "EDIT";
      }
    }
    
    
    
    if (dataField==null) {
      if (boolNew || data==null || data.length==0) {
        refreshSessionNew(vars);
        data = Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.set(Utility.getDefault(this, vars, "Tarifa_Productos", "N", "4D8F2DAE17EB4578AD6EC29C0C34F806", "N", dataField), Utility.getDefault(this, vars, "opcion", "N", "4D8F2DAE17EB4578AD6EC29C0C34F806", "N", dataField), Utility.getDefault(this, vars, "Descripcion", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), "", Utility.getDefault(this, vars, "Tipo", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "Modalidad", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "Createdby", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.selectDef2AAFF521796A41ADA2CF0765D4495FF0_0(this, Utility.getDefault(this, vars, "Createdby", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField)), Utility.getDefault(this, vars, "Estado", "DR", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "C_Incoterms_ID", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "ad_user_vendedor_id", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "ad_user_scliente_id", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "Fecha", "@#Date@", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), "Y", Utility.getDefault(this, vars, "frecuencia", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "Servicio_Cliente_ID", "select e.pdmServicioCliente  from BusinessPartner e,  ADUser uwhere e.id = u.businessPartner.id     and u.id = @#AD_User_Id@", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "C_Bpartner_Location_ID", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "Processed", "CO", "4D8F2DAE17EB4578AD6EC29C0C34F806", "N", dataField), (vars.getLanguage().equals("en_US")?ListData.selectName(this, "1F931E9A5FAB445A8FD2F069ADD2DBE4", Utility.getDefault(this, vars, "Processed", "CO", "4D8F2DAE17EB4578AD6EC29C0C34F806", "N", dataField)):ListData.selectNameTrl(this, vars.getLanguage(), "1F931E9A5FAB445A8FD2F069ADD2DBE4", Utility.getDefault(this, vars, "Processed", "CO", "4D8F2DAE17EB4578AD6EC29C0C34F806", "N", dataField))), Utility.getDefault(this, vars, "AD_Org_ID", "@AD_ORG_ID@", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "Updatedby", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.selectDefA27D34A306B043B8B2498F79549DF109_1(this, Utility.getDefault(this, vars, "Updatedby", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField)), Utility.getDefault(this, vars, "Vendedor_ID", "select e.salesRepresentative  from BusinessPartner e,  ADUser uwhere e.id = u.businessPartner.id     and u.id = @#AD_User_Id@", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "Documentno", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "C_Doctype_ID", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "C_Bpartner_ID", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "BTN_Generaro", "N", "4D8F2DAE17EB4578AD6EC29C0C34F806", "N", dataField), Utility.getDefault(this, vars, "AD_Client_ID", "@AD_CLIENT_ID@", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField));
        
      }
     }
      
    
    String currentOrg = (boolNew?"":(dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId")));
    if (!currentOrg.equals("") && !currentOrg.startsWith("'")) currentOrg = "'"+currentOrg+"'";
    String currentClient = (boolNew?"":(dataField!=null?dataField.getField("adClientId"):data[0].getField("adClientId")));
    if (!currentClient.equals("") && !currentClient.startsWith("'")) currentClient = "'"+currentClient+"'";
    
    boolean hasReadOnlyAccess = org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId);
    boolean editableTab = (!hasReadOnlyAccess && (currentOrg.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),currentOrg)) && (currentClient.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), currentClient)));
    if (editableTab)
      xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/logisticadetransporte/parametrizacion/Cotizacion/Cotizacion221A13EAEB064B3387B2BB9203B1E3F3_Edition",discard).createXmlDocument();
    else
      xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/logisticadetransporte/parametrizacion/Cotizacion/Cotizacion221A13EAEB064B3387B2BB9203B1E3F3_NonEditable",discard).createXmlDocument();

    xmlDocument.setParameter("tabId", tabId);
    ToolBar toolbar = new ToolBar(this, editableTab, vars.getLanguage(), "Cotizacion221A13EAEB064B3387B2BB9203B1E3F3", (strCommand.equals("NEW") || boolNew || (dataField==null && (data==null || data.length==0))), "document.frmMain.inpldtCotizacionId", "", "../com.atrums.logisticadetransporte.parametrizacion/orders/print.html", "N".equals("Y"), "Cotizacion", strReplaceWith, true, false, false, Utility.hasTabAttachments(this, vars, tabId, strLDT_Cotizacion_ID), !hasReadOnlyAccess);
    toolbar.setTabId(tabId);
    toolbar.setDeleteable(true);
    toolbar.prepareEditionTemplate("N".equals("Y"), hasSearchCondition, vars.getSessionValue("#ShowTest", "N").equals("Y"), "STD", Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    xmlDocument.setParameter("toolbar", toolbar.toString());

    // set updated timestamp to manage locking mechanism
    if (!boolNew) {
      xmlDocument.setParameter("updatedTimestamp", (dataField != null ? dataField
          .getField("updatedTimeStamp") : data[0].getField("updatedTimeStamp")));
    }
    
    boolean concurrentSave = vars.getSessionValue(tabId + "|concurrentSave").equals("true");
    if (concurrentSave) {
      //after concurrent save error, force autosave
      xmlDocument.setParameter("autosave", "Y");
    } else {
      xmlDocument.setParameter("autosave", "N");
    }
    vars.removeSessionValue(tabId + "|concurrentSave");

    try {
      WindowTabs tabs = new WindowTabs(this, vars, tabId, windowId, true, (strCommand.equalsIgnoreCase("NEW")));
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      // if (!strLDT_Cotizacion_ID.equals("")) xmlDocument.setParameter("childTabContainer", tabs.childTabs(false));
	  // else xmlDocument.setParameter("childTabContainer", tabs.childTabs(true));
	  xmlDocument.setParameter("childTabContainer", tabs.childTabs(false));
	  String hideBackButton = vars.getGlobalVariable("hideMenu", "#Hide_BackButton", "");
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "Cotizacion221A13EAEB064B3387B2BB9203B1E3F3_Relation.html", "Cotizacion", "W", strReplaceWith, tabs.breadcrumb(), hideBackButton.equals("true"), !concurrentSave);
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "Cotizacion221A13EAEB064B3387B2BB9203B1E3F3_Relation.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.editionTemplate(strCommand.equals("NEW")));
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
		
    
    
    xmlDocument.setParameter("commandType", strCommand);
    xmlDocument.setParameter("buscador",buscador);
    xmlDocument.setParameter("windowId", windowId);
    xmlDocument.setParameter("changed", "");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    final String strMappingName = Utility.getTabURL(tabId, "E", false);
    xmlDocument.setParameter("mappingName", strMappingName);
    xmlDocument.setParameter("confirmOnChanges", Utility.getJSConfirmOnChanges(vars, windowId));
    //xmlDocument.setParameter("buttonReference", Utility.messageBD(this, "Reference", vars.getLanguage()));

    xmlDocument.setParameter("paramSessionDate", strParamSessionDate);

    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    OBError myMessage = vars.getMessage(tabId);
    vars.removeMessage(tabId);
    if (myMessage!=null) {
      xmlDocument.setParameter("messageType", myMessage.getType());
      xmlDocument.setParameter("messageTitle", myMessage.getTitle());
      xmlDocument.setParameter("messageMessage", myMessage.getMessage());
    }
    xmlDocument.setParameter("displayLogic", getDisplayLogicContext(vars, boolNew));
    
    
     if (dataField==null) {
      xmlDocument.setData("structure1",data);
      
    } else {
      
        FieldProvider[] dataAux = new FieldProvider[1];
        dataAux[0] = dataField;
        
        xmlDocument.setData("structure1",dataAux);
      
    }
    
      
   
    try {
      ComboTableData comboTableData = null;
String userOrgList = "";
if (editableTab) 
  userOrgList=Utility.getContext(this, vars, "#User_Org", windowId, accesslevel); //editable record 
else 
  userOrgList=currentOrg;
comboTableData = new ComboTableData(vars, this, "19", "AD_Org_ID", "", "", userOrgList, Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("adOrgId"):dataField.getField("adOrgId")));
xmlDocument.setData("reportAD_Org_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "18", "C_Doctype_ID", "170", "262543586271415F9AFB635D3006B498", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("cDoctypeId"):dataField.getField("cDoctypeId")));
xmlDocument.setData("reportC_Doctype_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("Fecha_Format", vars.getSessionValue("#AD_SqlDateFormat"));
comboTableData = new ComboTableData(vars, this, "19", "C_Bpartner_Location_ID", "", "119", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("cBpartnerLocationId"):dataField.getField("cBpartnerLocationId")));
xmlDocument.setData("reportC_Bpartner_Location_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "18", "Vendedor_ID", "2B60113683DF450C969C42FD6D6DD8D6", "8B085A70BA80485FBD13FC71F12E2625", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("vendedorId"):dataField.getField("vendedorId")));
xmlDocument.setData("reportVendedor_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "18", "Servicio_Cliente_ID", "443D51697F39405C8E68D7A5291681CD", "6BD050AE9FEA456B97A067258C9F4A89", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("servicioClienteId"):dataField.getField("servicioClienteId")));
xmlDocument.setData("reportServicio_Cliente_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "17", "Modalidad", "084746C4C4934924AABBD1EAB1DB9BF5", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("modalidad"):dataField.getField("modalidad")));
xmlDocument.setData("reportModalidad","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "17", "Tipo", "336BA96AA2694CB0BD2689AB13A88F03", "05782A3C8CCE4197AB6EE369D03DB062", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("tipo"):dataField.getField("tipo")));
xmlDocument.setData("reportTipo","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "17", "frecuencia", "78F4041249B043B194408A0829BA35EC", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("frecuencia"):dataField.getField("frecuencia")));
xmlDocument.setData("reportfrecuencia","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("Processed_BTNname", Utility.getButtonName(this, vars, "1F931E9A5FAB445A8FD2F069ADD2DBE4", (dataField==null?data[0].getField("processed"):dataField.getField("processed")), "Processed_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalProcessed = org.openbravo.erpCommon.utility.Utility.isModalProcess("35AEE2126EB54D1E9A7D3A1BE1A18852"); 
xmlDocument.setParameter("Processed_Modal", modalProcessed?"true":"false");
comboTableData = new ComboTableData(vars, this, "19", "C_Incoterms_ID", "", "CB0C96AED59E475F9DA45EADCFCDB74E", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("cIncotermsId"):dataField.getField("cIncotermsId")));
xmlDocument.setData("reportC_Incoterms_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("BTN_Generaro_BTNname", Utility.getButtonName(this, vars, "2BCE8F9397BA47FBBC2DDF84EB0A78EB", "BTN_Generaro_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalBTN_Generaro = org.openbravo.erpCommon.utility.Utility.isModalProcess("7E5DB05C594E4CC4AFFC0B4F78FC8051"); 
xmlDocument.setParameter("BTN_Generaro_Modal", modalBTN_Generaro?"true":"false");
xmlDocument.setParameter("Tarifa_Productos_BTNname", Utility.getButtonName(this, vars, "F81EE533800845F3A674EDBFA21AE705", "Tarifa_Productos_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalTarifa_Productos = org.openbravo.erpCommon.utility.Utility.isModalProcess(""); 
xmlDocument.setParameter("Tarifa_Productos_Modal", modalTarifa_Productos?"true":"false");
xmlDocument.setParameter("Created_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Created_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
xmlDocument.setParameter("Updated_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Updated_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new ServletException(ex);
    }

    xmlDocument.setParameter("scriptOnLoad", getShortcutScript(usedButtonShortCuts, reservedButtonShortCuts));
    
    final String refererURL = vars.getSessionValue(tabId + "|requestURL");
    vars.removeSessionValue(tabId + "|requestURL");
    if(!refererURL.equals("")) {
    	final Boolean failedAutosave = (Boolean) vars.getSessionObject(tabId + "|failedAutosave");
		vars.removeSessionValue(tabId + "|failedAutosave");
    	if(failedAutosave != null && failedAutosave) {
    		final String jsFunction = "continueUserAction('"+refererURL+"');";
    		xmlDocument.setParameter("failedAutosave", jsFunction);
    	}
    }

    if (strCommand.equalsIgnoreCase("NEW")) {
      vars.removeSessionValue(tabId + "|failedAutosave");
      vars.removeSessionValue(strMappingName + "|hash");
    }

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageButtonFS(HttpServletResponse response, VariablesSecureApp vars, String strProcessId, String path) throws IOException, ServletException {
    log4j.debug("Output: Frames action button");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "org/openbravo/erpCommon/ad_actionButton/ActionButtonDefaultFrames").createXmlDocument();
    xmlDocument.setParameter("processId", strProcessId);
    xmlDocument.setParameter("trlFormType", "PROCESS");
    xmlDocument.setParameter("language", "defaultLang = \"" + vars.getLanguage() + "\";\n");
    xmlDocument.setParameter("type", strDireccion + path);
    out.println(xmlDocument.print());
    out.close();
  }

    private void printPageButtonProcessed35AEE2126EB54D1E9A7D3A1BE1A18852(HttpServletResponse response, VariablesSecureApp vars, String strLDT_Cotizacion_ID, String strprocessed, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process 35AEE2126EB54D1E9A7D3A1BE1A18852");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/Processed35AEE2126EB54D1E9A7D3A1BE1A18852", discard).createXmlDocument();
      xmlDocument.setParameter("key", strLDT_Cotizacion_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "Cotizacion221A13EAEB064B3387B2BB9203B1E3F3_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "35AEE2126EB54D1E9A7D3A1BE1A18852");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("35AEE2126EB54D1E9A7D3A1BE1A18852");
        vars.removeMessage("35AEE2126EB54D1E9A7D3A1BE1A18852");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    ComboTableData comboTableData = null;
    xmlDocument.setParameter("AccionCotizacion", "");
    comboTableData = new ComboTableData(vars, this, "17", "AccionCotizacion", "1F931E9A5FAB445A8FD2F069ADD2DBE4", "C7FB980DC18F482AABB67E659E2E9EB6", Utility.getContext(this, vars, "#AccessibleOrgTree", ""), Utility.getContext(this, vars, "#User_Client", ""), 0);
    Utility.fillSQLParameters(this, vars, (FieldProvider) vars.getSessionObject("button35AEE2126EB54D1E9A7D3A1BE1A18852.originalParams"), comboTableData, windowId, "");
    xmlDocument.setData("reportAccionCotizacion", "liststructure", comboTableData.select(false));
comboTableData = null;
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }
    private void printPageButtonBTN_Generaro7E5DB05C594E4CC4AFFC0B4F78FC8051(HttpServletResponse response, VariablesSecureApp vars, String strLDT_Cotizacion_ID, String strbtnGeneraro, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process 7E5DB05C594E4CC4AFFC0B4F78FC8051");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/BTN_Generaro7E5DB05C594E4CC4AFFC0B4F78FC8051", discard).createXmlDocument();
      xmlDocument.setParameter("key", strLDT_Cotizacion_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "Cotizacion221A13EAEB064B3387B2BB9203B1E3F3_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "7E5DB05C594E4CC4AFFC0B4F78FC8051");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("7E5DB05C594E4CC4AFFC0B4F78FC8051");
        vars.removeMessage("7E5DB05C594E4CC4AFFC0B4F78FC8051");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }




    private String getDisplayLogicContext(VariablesSecureApp vars, boolean isNew) throws IOException, ServletException {
      log4j.debug("Output: Display logic context fields");
      String result = "var strShowAudit=\"" +(isNew?"N":Utility.getContext(this, vars, "ShowAudit", windowId)) + "\";\n";
      return result;
    }


    private String getReadOnlyLogicContext(VariablesSecureApp vars) throws IOException, ServletException {
      log4j.debug("Output: Read Only logic context fields");
      String result = "var strAD_Role_Id=\"" + Utility.getContext(this, vars, "#AD_Role_Id", windowId) + "\";\n";
      return result;
    }




 
  private String getShortcutScript( HashMap<String, String> usedButtonShortCuts, HashMap<String, String> reservedButtonShortCuts){
    StringBuffer shortcuts = new StringBuffer();
    shortcuts.append(" function buttonListShorcuts() {\n");
    Iterator<String> ik = usedButtonShortCuts.keySet().iterator();
    Iterator<String> iv = usedButtonShortCuts.values().iterator();
    while(ik.hasNext() && iv.hasNext()){
      shortcuts.append("  keyArray[keyArray.length] = new keyArrayItem(\"").append(ik.next()).append("\", \"").append(iv.next()).append("\", null, \"altKey\", false, \"onkeydown\");\n");
    }
    shortcuts.append(" return true;\n}");
    return shortcuts.toString();
  }
  
  private int saveRecord(VariablesSecureApp vars, OBError myError, char type) throws IOException, ServletException {
    Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data data = null;
    int total = 0;
    if (org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) {
        OBError newError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
        myError.setError(newError);
        vars.setMessage(tabId, myError);
    }
    else {
        Connection con = null;
        try {
            con = this.getTransactionConnection();
            data = getEditVariables(con, vars);
            data.dateTimeFormat = vars.getSessionValue("#AD_SqlDateTimeFormat");            
            String strSequence = "";
            if(type == 'I') {                
        strSequence = SequenceIdData.getUUID();
                if(log4j.isDebugEnabled()) log4j.debug("Sequence: " + strSequence);
                data.ldtCotizacionId = strSequence;  
            }
            if (Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),data.adClientId)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),data.adOrgId)){
		     if(type == 'I') {
		       total = data.insert(con, this);
		     } else {
		       //Check the version of the record we are saving is the one in DB
		       if (Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.getCurrentDBTimestamp(this, data.ldtCotizacionId).equals(
                vars.getStringParameter("updatedTimestamp"))) {
                total = data.update(con, this);
               } else {
                 myError.setMessage(Replace.replace(Replace.replace(Utility.messageBD(this,
                    "SavingModifiedRecord", vars.getLanguage()), "\\n", "<br/>"), "&quot;", "\""));
                 myError.setType("Error");
                 vars.setSessionValue(tabId + "|concurrentSave", "true");
               } 
		     }		            
          
            }
                else {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
            myError.setError(newError);            
          }
          releaseCommitConnection(con);
        } catch(Exception ex) {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
            myError.setError(newError);   
            try {
              releaseRollbackConnection(con);
            } catch (final Exception e) { //do nothing 
            }           
        }
            
        if (myError.isEmpty() && total == 0) {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=DBExecuteError");
            myError.setError(newError);
        }
        vars.setMessage(tabId, myError);
            
        if(!myError.isEmpty()){
            if(data != null ) {
                if(type == 'I') {            			
                    data.ldtCotizacionId = "";
                }
                else {                    
                    
                        //BUTTON TEXT FILLING
                    data.processedBtn = ActionButtonDefaultData.getText(this, vars.getLanguage(), "1F931E9A5FAB445A8FD2F069ADD2DBE4", data.getField("Processed"));
                    
                }
                vars.setEditionData(tabId, data);
            }            	
        }
        else {
            vars.setSessionValue(windowId + "|LDT_Cotizacion_ID", data.ldtCotizacionId);
        }
    }
    return total;
  }

  public String getServletInfo() {
    return "Servlet Cotizacion221A13EAEB064B3387B2BB9203B1E3F3. This Servlet was made by Wad constructor";
  } // End of getServletInfo() method
}
