
package org.openbravo.erpWindows.com.atrums.logisticadetransporte.parametrizacion.Cotizacion;


import org.openbravo.erpCommon.reference.*;



import org.codehaus.jettison.json.JSONObject;
import org.openbravo.erpCommon.utility.*;
import org.openbravo.data.FieldProvider;
import org.openbravo.utils.FormatUtilities;
import org.openbravo.utils.Replace;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.exception.OBException;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessRunner;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.xmlEngine.XmlDocument;
import java.util.Vector;
import java.util.StringTokenizer;
import org.openbravo.database.SessionInfo;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import java.sql.Connection;
import org.apache.log4j.Logger;

public class MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;
  
  private static Logger log4j = Logger.getLogger(MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA.class);
  
  private static final String windowId = "4D8F2DAE17EB4578AD6EC29C0C34F806";
  private static final String tabId = "CC5374351D0E44B1904CA61623941AEA";
  private static final String defaultTabView = "RELATION";
  private static final int accesslevel = 3;
  private static final String moduleId = "C5BE74383A264BF68316D6BF975E6EFC";
  
  @Override
  public void init(ServletConfig config) {
    setClassInfo("W", tabId, moduleId);
    super.init(config);
  }
  
  
  @Override
  public void service(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    String command = vars.getCommand();
    
    boolean securedProcess = false;
    if (command.contains("BUTTON")) {
      try {
        securedProcess = "Y".equals(org.openbravo.erpCommon.businessUtility.Preferences
            .getPreferenceValue("SecuredProcess", true, vars.getClient(), vars.getOrg(), vars
                .getUser(), vars.getRole(), windowId));
      } catch (PropertyException e) {
      }
    
     
      if (command.contains("34CDC7B016F648E6B29055C8A8236FE5")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("34CDC7B016F648E6B29055C8A8236FE5");
        SessionInfo.setModuleId("C5BE74383A264BF68316D6BF975E6EFC");
        if (securedProcess) {
          classInfo.type = "P";
          classInfo.id = "34CDC7B016F648E6B29055C8A8236FE5";
        }
      }
     
     
    }
    if (!securedProcess) {
      setClassInfo("W", tabId, moduleId);
    }
    super.service(request, response);
  }
  

  public void doPost (HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException {
    TableSQLData tableSQL = null;
    VariablesSecureApp vars = new VariablesSecureApp(request);
    Boolean saveRequest = (Boolean) request.getAttribute("autosave");
    
    if(saveRequest != null && saveRequest){
      String currentOrg = vars.getStringParameter("inpadOrgId");
      String currentClient = vars.getStringParameter("inpadClientId");
      boolean editableTab = (!org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)
                            && (currentOrg.equals("") || Utility.isElementInList(Utility.getContext(this, vars,"#User_Org", windowId, accesslevel), currentOrg)) 
                            && (currentClient.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),currentClient)));
    
        OBError myError = new OBError();
        String commandType = request.getParameter("inpCommandType");
        String strldtCotizacionNavieraId = request.getParameter("inpldtCotizacionNavieraId");
         String strPLDT_Cotizacion_ID = vars.getGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID");
        if (editableTab) {
          int total = 0;
          
          if(commandType.equalsIgnoreCase("EDIT") && !strldtCotizacionNavieraId.equals(""))
              total = saveRecord(vars, myError, 'U', strPLDT_Cotizacion_ID);
          else
              total = saveRecord(vars, myError, 'I', strPLDT_Cotizacion_ID);
          
          if (!myError.isEmpty() && total == 0)     
            throw new OBException(myError.getMessage());
        }
        vars.setSessionValue(request.getParameter("mappingName") +"|hash", vars.getPostDataHash());
        vars.setSessionValue(tabId + "|Header.view", "EDIT");
        
        return;
    }
    
    try {
      tableSQL = new TableSQLData(vars, this, tabId, Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel), Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    String strOrderBy = vars.getSessionValue(tabId + "|orderby");
    if (!strOrderBy.equals("")) {
      vars.setSessionValue(tabId + "|newOrder", "1");
    }

    if (vars.commandIn("DEFAULT")) {
      String strPLDT_Cotizacion_ID = vars.getGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID", "");

      String strLDT_Cotizacion_Naviera_ID = vars.getGlobalVariable("inpldtCotizacionNavieraId", windowId + "|LDT_Cotizacion_Naviera_ID", "");
            if (strPLDT_Cotizacion_ID.equals("")) {
        strPLDT_Cotizacion_ID = getParentID(vars, strLDT_Cotizacion_Naviera_ID);
        if (strPLDT_Cotizacion_ID.equals("")) throw new ServletException("Required parameter :" + windowId + "|LDT_Cotizacion_ID");
        vars.setSessionValue(windowId + "|LDT_Cotizacion_ID", strPLDT_Cotizacion_ID);

        refreshParentSession(vars, strPLDT_Cotizacion_ID);
      }


      String strView = vars.getSessionValue(tabId + "|MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA.view");
      if (strView.equals("")) {
        strView = defaultTabView;

        if (strView.equals("EDIT")) {
          if (strLDT_Cotizacion_Naviera_ID.equals("")) strLDT_Cotizacion_Naviera_ID = firstElement(vars, tableSQL);
          if (strLDT_Cotizacion_Naviera_ID.equals("")) strView = "RELATION";
        }
      }
      if (strView.equals("EDIT")) 

        printPageEdit(response, request, vars, false, strLDT_Cotizacion_Naviera_ID, strPLDT_Cotizacion_ID, tableSQL);

      else printPageDataSheet(response, vars, strPLDT_Cotizacion_ID, strLDT_Cotizacion_Naviera_ID, tableSQL);
    } else if (vars.commandIn("DIRECT")) {
      String strLDT_Cotizacion_Naviera_ID = vars.getStringParameter("inpDirectKey");
      
        
      if (strLDT_Cotizacion_Naviera_ID.equals("")) strLDT_Cotizacion_Naviera_ID = vars.getRequiredGlobalVariable("inpldtCotizacionNavieraId", windowId + "|LDT_Cotizacion_Naviera_ID");
      else vars.setSessionValue(windowId + "|LDT_Cotizacion_Naviera_ID", strLDT_Cotizacion_Naviera_ID);
      
      
      String strPLDT_Cotizacion_ID = getParentID(vars, strLDT_Cotizacion_Naviera_ID);
      
      vars.setSessionValue(windowId + "|LDT_Cotizacion_ID", strPLDT_Cotizacion_ID);
      vars.setSessionValue("221A13EAEB064B3387B2BB9203B1E3F3|Cotizacion.view", "EDIT");

      refreshParentSession(vars, strPLDT_Cotizacion_ID);

      vars.setSessionValue(tabId + "|MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA.view", "EDIT");

      printPageEdit(response, request, vars, false, strLDT_Cotizacion_Naviera_ID, strPLDT_Cotizacion_ID, tableSQL);

    } else if (vars.commandIn("TAB")) {
      String strPLDT_Cotizacion_ID = vars.getGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID", false, false, true, "");
      vars.removeSessionValue(windowId + "|LDT_Cotizacion_Naviera_ID");
      refreshParentSession(vars, strPLDT_Cotizacion_ID);


      String strView = vars.getSessionValue(tabId + "|MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA.view");
      String strLDT_Cotizacion_Naviera_ID = "";
      if (strView.equals("")) {
        strView = defaultTabView;
        if (strView.equals("EDIT")) {
          strLDT_Cotizacion_Naviera_ID = firstElement(vars, tableSQL);
          if (strLDT_Cotizacion_Naviera_ID.equals("")) strView = "RELATION";
        }
      }
      if (strView.equals("EDIT")) {

        if (strLDT_Cotizacion_Naviera_ID.equals("")) strLDT_Cotizacion_Naviera_ID = firstElement(vars, tableSQL);
        printPageEdit(response, request, vars, false, strLDT_Cotizacion_Naviera_ID, strPLDT_Cotizacion_ID, tableSQL);

      } else printPageDataSheet(response, vars, strPLDT_Cotizacion_ID, "", tableSQL);
    } else if (vars.commandIn("SEARCH")) {
vars.getRequestGlobalVariable("inpParamLine", tabId + "|paramLine");
vars.getRequestGlobalVariable("inpParamC_Bpartner_ID", tabId + "|paramC_Bpartner_ID");
vars.getRequestGlobalVariable("inpParamLine_f", tabId + "|paramLine_f");

        vars.getRequestGlobalVariable("inpParamUpdated", tabId + "|paramUpdated");
        vars.getRequestGlobalVariable("inpParamUpdatedBy", tabId + "|paramUpdatedBy");
        vars.getRequestGlobalVariable("inpParamCreated", tabId + "|paramCreated");
        vars.getRequestGlobalVariable("inpparamCreatedBy", tabId + "|paramCreatedBy");
            String strPLDT_Cotizacion_ID = vars.getGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID");

      
      vars.removeSessionValue(windowId + "|LDT_Cotizacion_Naviera_ID");
      String strLDT_Cotizacion_Naviera_ID="";

      String strView = vars.getSessionValue(tabId + "|MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA.view");
      if (strView.equals("")) strView=defaultTabView;

      if (strView.equals("EDIT")) {
        strLDT_Cotizacion_Naviera_ID = firstElement(vars, tableSQL);
        if (strLDT_Cotizacion_Naviera_ID.equals("")) {
          // filter returns empty set
          strView = "RELATION";
          // switch to grid permanently until the user changes the view again
          vars.setSessionValue(tabId + "|MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA.view", strView);
        }
      }

      if (strView.equals("EDIT")) 

        printPageEdit(response, request, vars, false, strLDT_Cotizacion_Naviera_ID, strPLDT_Cotizacion_ID, tableSQL);

      else printPageDataSheet(response, vars, strPLDT_Cotizacion_ID, strLDT_Cotizacion_Naviera_ID, tableSQL);
    } else if (vars.commandIn("RELATION")) {
            String strPLDT_Cotizacion_ID = vars.getGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID");
      

      String strLDT_Cotizacion_Naviera_ID = vars.getGlobalVariable("inpldtCotizacionNavieraId", windowId + "|LDT_Cotizacion_Naviera_ID", "");
      vars.setSessionValue(tabId + "|MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA.view", "RELATION");
      printPageDataSheet(response, vars, strPLDT_Cotizacion_ID, strLDT_Cotizacion_Naviera_ID, tableSQL);
    } else if (vars.commandIn("NEW")) {
      String strPLDT_Cotizacion_ID = vars.getGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID");


      printPageEdit(response, request, vars, true, "", strPLDT_Cotizacion_ID, tableSQL);

    } else if (vars.commandIn("EDIT")) {
      String strPLDT_Cotizacion_ID = vars.getGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID");

      @SuppressWarnings("unused") // In Expense Invoice tab this variable is not used, to be fixed
      String strLDT_Cotizacion_Naviera_ID = vars.getGlobalVariable("inpldtCotizacionNavieraId", windowId + "|LDT_Cotizacion_Naviera_ID", "");
      vars.setSessionValue(tabId + "|MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA.view", "EDIT");

      setHistoryCommand(request, "EDIT");
      printPageEdit(response, request, vars, false, strLDT_Cotizacion_Naviera_ID, strPLDT_Cotizacion_ID, tableSQL);

    } else if (vars.commandIn("NEXT")) {
      String strPLDT_Cotizacion_ID = vars.getGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID");
      String strLDT_Cotizacion_Naviera_ID = vars.getRequiredStringParameter("inpldtCotizacionNavieraId");
      
      String strNext = nextElement(vars, strLDT_Cotizacion_Naviera_ID, tableSQL);

      printPageEdit(response, request, vars, false, strNext, strPLDT_Cotizacion_ID, tableSQL);
    } else if (vars.commandIn("PREVIOUS")) {
      String strPLDT_Cotizacion_ID = vars.getGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID");
      String strLDT_Cotizacion_Naviera_ID = vars.getRequiredStringParameter("inpldtCotizacionNavieraId");
      
      String strPrevious = previousElement(vars, strLDT_Cotizacion_Naviera_ID, tableSQL);

      printPageEdit(response, request, vars, false, strPrevious, strPLDT_Cotizacion_ID, tableSQL);
    } else if (vars.commandIn("FIRST_RELATION")) {
vars.getGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID");

      vars.setSessionValue(tabId + "|MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA.initRecordNumber", "0");
      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("PREVIOUS_RELATION")) {
      String strPLDT_Cotizacion_ID = vars.getGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID");

      String strInitRecord = vars.getSessionValue(tabId + "|MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA.initRecordNumber");
      String strRecordRange = Utility.getContext(this, vars, "#RecordRange", windowId);
      int intRecordRange = strRecordRange.equals("")?0:Integer.parseInt(strRecordRange);
      if (strInitRecord.equals("") || strInitRecord.equals("0")) {
        vars.setSessionValue(tabId + "|MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA.initRecordNumber", "0");
      } else {
        int initRecord = (strInitRecord.equals("")?0:Integer.parseInt(strInitRecord));
        initRecord -= intRecordRange;
        strInitRecord = ((initRecord<0)?"0":Integer.toString(initRecord));
        vars.setSessionValue(tabId + "|MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA.initRecordNumber", strInitRecord);
      }
      vars.removeSessionValue(windowId + "|LDT_Cotizacion_Naviera_ID");
      vars.setSessionValue(windowId + "|LDT_Cotizacion_ID", strPLDT_Cotizacion_ID);
      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("NEXT_RELATION")) {
      String strPLDT_Cotizacion_ID = vars.getGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID");

      String strInitRecord = vars.getSessionValue(tabId + "|MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA.initRecordNumber");
      String strRecordRange = Utility.getContext(this, vars, "#RecordRange", windowId);
      int intRecordRange = strRecordRange.equals("")?0:Integer.parseInt(strRecordRange);
      int initRecord = (strInitRecord.equals("")?0:Integer.parseInt(strInitRecord));
      if (initRecord==0) initRecord=1;
      initRecord += intRecordRange;
      strInitRecord = ((initRecord<0)?"0":Integer.toString(initRecord));
      vars.setSessionValue(tabId + "|MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA.initRecordNumber", strInitRecord);
      vars.removeSessionValue(windowId + "|LDT_Cotizacion_Naviera_ID");
      vars.setSessionValue(windowId + "|LDT_Cotizacion_ID", strPLDT_Cotizacion_ID);
      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("FIRST")) {
      String strPLDT_Cotizacion_ID = vars.getGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID");
      
      String strFirst = firstElement(vars, tableSQL);

      printPageEdit(response, request, vars, false, strFirst, strPLDT_Cotizacion_ID, tableSQL);
    } else if (vars.commandIn("LAST_RELATION")) {
      String strPLDT_Cotizacion_ID = vars.getGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID");

      String strLast = lastElement(vars, tableSQL);
      printPageDataSheet(response, vars, strPLDT_Cotizacion_ID, strLast, tableSQL);
    } else if (vars.commandIn("LAST")) {
      String strPLDT_Cotizacion_ID = vars.getGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID");
      
      String strLast = lastElement(vars, tableSQL);

      printPageEdit(response, request, vars, false, strLast, strPLDT_Cotizacion_ID, tableSQL);
    } else if (vars.commandIn("SAVE_NEW_RELATION", "SAVE_NEW_NEW", "SAVE_NEW_EDIT")) {
      String strPLDT_Cotizacion_ID = vars.getGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID");
      OBError myError = new OBError();      
      int total = saveRecord(vars, myError, 'I', strPLDT_Cotizacion_ID);      
      if (!myError.isEmpty()) {        
        response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
      } 
      else {
		if (myError.isEmpty()) {
		  myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsInserted");
		  myError.setMessage(total + " " + myError.getMessage());
		  vars.setMessage(tabId, myError);
		}        
        if (vars.commandIn("SAVE_NEW_NEW")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
        else if (vars.commandIn("SAVE_NEW_EDIT")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        else response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
      }
    } else if (vars.commandIn("SAVE_EDIT_RELATION", "SAVE_EDIT_NEW", "SAVE_EDIT_EDIT", "SAVE_EDIT_NEXT")) {
      String strPLDT_Cotizacion_ID = vars.getGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID");
      String strLDT_Cotizacion_Naviera_ID = vars.getRequiredGlobalVariable("inpldtCotizacionNavieraId", windowId + "|LDT_Cotizacion_Naviera_ID");
      OBError myError = new OBError();
      int total = saveRecord(vars, myError, 'U', strPLDT_Cotizacion_ID);      
      if (!myError.isEmpty()) {
        response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
      } 
      else {
        if (myError.isEmpty()) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsUpdated");
          myError.setMessage(total + " " + myError.getMessage());
          vars.setMessage(tabId, myError);
        }
        if (vars.commandIn("SAVE_EDIT_NEW")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
        else if (vars.commandIn("SAVE_EDIT_EDIT")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        else if (vars.commandIn("SAVE_EDIT_NEXT")) {
          String strNext = nextElement(vars, strLDT_Cotizacion_Naviera_ID, tableSQL);
          vars.setSessionValue(windowId + "|LDT_Cotizacion_Naviera_ID", strNext);
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        } else response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
      }
    } else if (vars.commandIn("DELETE")) {
      String strPLDT_Cotizacion_ID = vars.getGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID");

      String strLDT_Cotizacion_Naviera_ID = vars.getRequiredStringParameter("inpldtCotizacionNavieraId");
      //MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData data = getEditVariables(vars, strPLDT_Cotizacion_ID);
      int total = 0;
      OBError myError = null;
      if (org.openbravo.erpCommon.utility.WindowAccessData.hasNotDeleteAccess(this, vars.getRole(), tabId)) {
        myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
        vars.setMessage(tabId, myError);
      } else {
        try {
          total = MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.delete(this, strLDT_Cotizacion_Naviera_ID, strPLDT_Cotizacion_ID, Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), Utility.getContext(this, vars, "#User_Org", windowId, accesslevel));
        } catch(ServletException ex) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myError.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myError);
        }
        if (myError==null && total==0) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
        }
        vars.removeSessionValue(windowId + "|ldtCotizacionNavieraId");
        vars.setSessionValue(tabId + "|MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA.view", "RELATION");
      }
      if (myError==null) {
        myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsDeleted");
        myError.setMessage(total + " " + myError.getMessage());
        vars.setMessage(tabId, myError);
      }
      response.sendRedirect(strDireccion + request.getServletPath());

     } else if (vars.commandIn("BUTTONSeleccionar_Op34CDC7B016F648E6B29055C8A8236FE5")) {
        vars.setSessionValue("button34CDC7B016F648E6B29055C8A8236FE5.strseleccionarOp", vars.getStringParameter("inpseleccionarOp"));
        vars.setSessionValue("button34CDC7B016F648E6B29055C8A8236FE5.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("button34CDC7B016F648E6B29055C8A8236FE5.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("button34CDC7B016F648E6B29055C8A8236FE5.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        
        
        //Save in session needed params for combos if needed
        vars.setSessionObject("button34CDC7B016F648E6B29055C8A8236FE5.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "34CDC7B016F648E6B29055C8A8236FE5", request.getServletPath());    
     } else if (vars.commandIn("BUTTON34CDC7B016F648E6B29055C8A8236FE5")) {
        String strLDT_Cotizacion_Naviera_ID = vars.getGlobalVariable("inpldtCotizacionNavieraId", windowId + "|LDT_Cotizacion_Naviera_ID", "");
        String strseleccionarOp = vars.getSessionValue("button34CDC7B016F648E6B29055C8A8236FE5.strseleccionarOp");
        String strProcessing = vars.getSessionValue("button34CDC7B016F648E6B29055C8A8236FE5.strProcessing");
        String strOrg = vars.getSessionValue("button34CDC7B016F648E6B29055C8A8236FE5.strOrg");
        String strClient = vars.getSessionValue("button34CDC7B016F648E6B29055C8A8236FE5.strClient");
        
        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtonSeleccionar_Op34CDC7B016F648E6B29055C8A8236FE5(response, vars, strLDT_Cotizacion_Naviera_ID, strseleccionarOp, strProcessing);
        }


    } else if (vars.commandIn("SAVE_BUTTONSeleccionar_Op34CDC7B016F648E6B29055C8A8236FE5")) {
        String strLDT_Cotizacion_Naviera_ID = vars.getGlobalVariable("inpKey", windowId + "|LDT_Cotizacion_Naviera_ID", "");
        @SuppressWarnings("unused")
        String strseleccionarOp = vars.getStringParameter("inpseleccionarOp");
        String strProcessing = vars.getStringParameter("inpprocessing");
        OBError myMessage = null;
        try {
          String pinstance = SequenceIdData.getUUID();
          PInstanceProcessData.insertPInstance(this, pinstance, "34CDC7B016F648E6B29055C8A8236FE5", (("LDT_Cotizacion_Naviera_ID".equalsIgnoreCase("AD_Language"))?"0":strLDT_Cotizacion_Naviera_ID), strProcessing, vars.getUser(), vars.getClient(), vars.getOrg());
          
          
          ProcessBundle bundle = ProcessBundle.pinstance(pinstance, vars, this);
          new ProcessRunner(bundle).execute(this);
          
          PInstanceProcessData[] pinstanceData = PInstanceProcessData.select(this, pinstance);
          myMessage = Utility.getProcessInstanceMessage(this, vars, pinstanceData);
        } catch (ServletException ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);






    } else if (vars.commandIn("SAVE_XHR")) {
      String strPLDT_Cotizacion_ID = vars.getGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID");
      OBError myError = new OBError();
      JSONObject result = new JSONObject();
      String commandType = vars.getStringParameter("inpCommandType");
      char saveType = "NEW".equals(commandType) ? 'I' : 'U';
      try {
        int total = saveRecord(vars, myError, saveType, strPLDT_Cotizacion_ID);
        if (myError.isEmpty()) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsUpdated");
          myError.setMessage(total + " " + myError.getMessage());
          myError.setType("Success");
        }
        result.put("oberror", myError.toMap());
        result.put("tabid", vars.getStringParameter("tabID"));
        result.put("redirect", strDireccion + request.getServletPath() + "?Command=" + commandType);
      } catch (Exception e) {
        log4j.error("Error saving record (XHR request): " + e.getMessage(), e);
        myError.setType("Error");
        myError.setMessage(e.getMessage());
      }

      response.setContentType("application/json");
      PrintWriter out = response.getWriter();
      out.print(result.toString());
      out.flush();
      out.close();
    } else if (vars.getCommand().toUpperCase().startsWith("BUTTON") || vars.getCommand().toUpperCase().startsWith("SAVE_BUTTON")) {
      pageErrorPopUp(response);
    } else pageError(response);
  }
  private MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData getEditVariables(Connection con, VariablesSecureApp vars, String strPLDT_Cotizacion_ID) throws IOException,ServletException {
    MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData data = new MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData();
    ServletException ex = null;
    try {
    data.puertoTerminalOrigenId = vars.getRequiredGlobalVariable("inppuertoTerminalOrigenId", windowId + "|Puerto_Terminal_Origen_ID");     data.puertoTerminalDestinoId = vars.getRequiredGlobalVariable("inppuertoTerminalDestinoId", windowId + "|Puerto_Terminal_Destino_ID");     data.cBpartnerId = vars.getRequiredGlobalVariable("inpcBpartnerId", windowId + "|C_Bpartner_ID");     data.cCurrencyId = vars.getRequestGlobalVariable("inpcCurrencyId", windowId + "|C_Currency_ID");     data.cCurrencyIdr = vars.getStringParameter("inpcCurrencyId_R");     data.mVersionTarifaId = vars.getStringParameter("inpmVersionTarifaId");     data.ldtContratoPuertoId = vars.getRequestGlobalVariable("inpldtContratoPuertoId", windowId + "|ldt_contrato_puerto_id");     data.ldtContratoPuertoIdr = vars.getStringParameter("inpldtContratoPuertoId_R");    try {   data.grandTotal = vars.getNumericParameter("inpgrandTotal", vars.getSessionValue(windowId + "|Grand_Total"));  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.totalCostosEur = vars.getNumericParameter("inptotalCostosEur", vars.getSessionValue(windowId + "|total_costos_eur"));  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.totalNegociado = vars.getNumericParameter("inptotalNegociado", vars.getSessionValue(windowId + "|Total_Negociado"));  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.totalNegociadoEur = vars.getNumericParameter("inptotalNegociadoEur", vars.getSessionValue(windowId + "|total_negociado_eur"));  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.total = vars.getNumericParameter("inptotal", vars.getSessionValue(windowId + "|Total"));  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.totalOpcion = vars.getNumericParameter("inptotalOpcion");  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.totalEur = vars.getNumericParameter("inptotalEur", vars.getSessionValue(windowId + "|total_eur"));  } catch (ServletException paramEx) { ex = paramEx; }     data.seleccionarOp = vars.getRequestGlobalVariable("inpseleccionarOp", windowId + "|Seleccionar_Op");     data.fechaValidez = vars.getStringParameter("inpfechaValidez");     data.ldtDiasTransito = vars.getStringParameter("inpldtDiasTransito");    try {   data.ldtDiasLibres = vars.getNumericParameter("inpldtDiasLibres");  } catch (ServletException paramEx) { ex = paramEx; }     data.isactive = vars.getStringParameter("inpisactive", "N");     data.mPricelistId = vars.getRequestGlobalVariable("inpmPricelistId", windowId + "|M_Pricelist_ID");     data.mPricelistVersionId = vars.getRequestGlobalVariable("inpmPricelistVersionId", windowId + "|m_pricelist_version_id");    try {   data.line = vars.getRequiredNumericParameter("inpline");  } catch (ServletException paramEx) { ex = paramEx; }     data.adOrgId = vars.getRequiredGlobalVariable("inpadOrgId", windowId + "|AD_Org_ID");     data.ldtCotizacionId = vars.getRequiredStringParameter("inpldtCotizacionId");     data.adClientId = vars.getRequiredGlobalVariable("inpadClientId", windowId + "|AD_Client_ID");     data.ldtCotizacionNavieraId = vars.getRequestGlobalVariable("inpldtCotizacionNavieraId", windowId + "|LDT_Cotizacion_Naviera_ID");     data.cCountryId = vars.getRequestGlobalVariable("inpcCountryId", windowId + "|c_country_id"); 
      data.createdby = vars.getUser();
      data.updatedby = vars.getUser();
      data.adUserClient = Utility.getContext(this, vars, "#User_Client", windowId, accesslevel);
      data.adOrgClient = Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel);
      data.updatedTimeStamp = vars.getStringParameter("updatedTimestamp");

      data.ldtCotizacionId = vars.getGlobalVariable("inpldtCotizacionId", windowId + "|LDT_Cotizacion_ID");


    
    

    
    }
    catch(ServletException e) {
    	vars.setEditionData(tabId, data);
    	throw e;
    }
    // Behavior with exception for numeric fields is to catch last one if we have multiple ones
    if(ex != null) {
      vars.setEditionData(tabId, data);
      throw ex;
    }
    return data;
  }


  private void refreshParentSession(VariablesSecureApp vars, String strPLDT_Cotizacion_ID) throws IOException,ServletException {
      
      Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data[] data = Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), strPLDT_Cotizacion_ID, Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
      if (data==null || data.length==0) return;
          vars.setSessionValue(windowId + "|AD_Org_ID", data[0].adOrgId);    vars.setSessionValue(windowId + "|C_BPartner_ID", data[0].cBpartnerId);    vars.setSessionValue(windowId + "|Vendedor_ID", data[0].vendedorId);    vars.setSessionValue(windowId + "|Servicio_Cliente_ID", data[0].servicioClienteId);    vars.setSessionValue(windowId + "|Modalidad", data[0].modalidad);    vars.setSessionValue(windowId + "|Tipo", data[0].tipo);    vars.setSessionValue(windowId + "|Processed", data[0].processed);    vars.setSessionValue(windowId + "|C_Incoterms_ID", data[0].cIncotermsId);    vars.setSessionValue(windowId + "|opcion", data[0].opcion);    vars.setSessionValue(windowId + "|AD_Client_ID", data[0].adClientId);    vars.setSessionValue(windowId + "|LDT_Cotizacion_ID", data[0].ldtCotizacionId);    vars.setSessionValue(windowId + "|Estado", data[0].estado);
      vars.setSessionValue(windowId + "|LDT_Cotizacion_ID", strPLDT_Cotizacion_ID); //to ensure key parent is set for EM_* cols

      FieldProvider dataField = null; // Define this so that auxiliar inputs using SQL will work
      
  }
  
  
  private String getParentID(VariablesSecureApp vars, String strLDT_Cotizacion_Naviera_ID) throws ServletException {
    String strPLDT_Cotizacion_ID = MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.selectParentID(this, strLDT_Cotizacion_Naviera_ID);
    if (strPLDT_Cotizacion_ID.equals("")) {
      log4j.error("Parent record not found for id: " + strLDT_Cotizacion_Naviera_ID);
      throw new ServletException("Parent record not found");
    }
    return strPLDT_Cotizacion_ID;
  }

    private void refreshSessionEdit(VariablesSecureApp vars, FieldProvider[] data) {
      if (data==null || data.length==0) return;
          vars.setSessionValue(windowId + "|Puerto_Terminal_Origen_ID", data[0].getField("puertoTerminalOrigenId"));    vars.setSessionValue(windowId + "|Puerto_Terminal_Destino_ID", data[0].getField("puertoTerminalDestinoId"));    vars.setSessionValue(windowId + "|C_BPartner_ID", data[0].getField("cBpartnerId"));    vars.setSessionValue(windowId + "|C_Currency_ID", data[0].getField("cCurrencyId"));    vars.setSessionValue(windowId + "|LDT_Contrato_Puerto_ID", data[0].getField("ldtContratoPuertoId"));    vars.setSessionValue(windowId + "|Grand_Total", data[0].getField("grandTotal"));    vars.setSessionValue(windowId + "|total_costos_eur", data[0].getField("totalCostosEur"));    vars.setSessionValue(windowId + "|Total_Negociado", data[0].getField("totalNegociado"));    vars.setSessionValue(windowId + "|total_negociado_eur", data[0].getField("totalNegociadoEur"));    vars.setSessionValue(windowId + "|Total", data[0].getField("total"));    vars.setSessionValue(windowId + "|total_eur", data[0].getField("totalEur"));    vars.setSessionValue(windowId + "|Seleccionar Opcion", data[0].getField("seleccionar opcion"));    vars.setSessionValue(windowId + "|M_PriceList_ID", data[0].getField("mPricelistId"));    vars.setSessionValue(windowId + "|M_PriceList_Version_ID", data[0].getField("mPricelistVersionId"));    vars.setSessionValue(windowId + "|C_Country_ID", data[0].getField("cCountryId"));    vars.setSessionValue(windowId + "|LDT_Cotizacion_Naviera_ID", data[0].getField("ldtCotizacionNavieraId"));    vars.setSessionValue(windowId + "|AD_Org_ID", data[0].getField("adOrgId"));    vars.setSessionValue(windowId + "|AD_Client_ID", data[0].getField("adClientId"));
    }

    private void refreshSessionNew(VariablesSecureApp vars, String strPLDT_Cotizacion_ID) throws IOException,ServletException {
      MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[] data = MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), strPLDT_Cotizacion_ID, vars.getStringParameter("inpldtCotizacionNavieraId", ""), Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
      if (data==null || data.length==0) return;
      refreshSessionEdit(vars, data);
    }

  private String nextElement(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (strSelected == null || strSelected.equals("")) return firstElement(vars, tableSQL);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(), 0, 0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.NEXT, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting next element", e);
      }
      if (data!=null) {
        if (data!=null) return data;
      }
    }
    return strSelected;
  }

  private int getKeyPosition(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("getKeyPosition: " + strSelected);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.GETPOSITION, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting key position", e);
      }
      if (data!=null) {
        // split offset -> (page,relativeOffset)
        int absoluteOffset = Integer.valueOf(data);
        int page = absoluteOffset / TableSQLData.maxRowsPerGridPage;
        int relativeOffset = absoluteOffset % TableSQLData.maxRowsPerGridPage;
        log4j.debug("getKeyPosition: absOffset: " + absoluteOffset + "=> page: " + page + " relOffset: " + relativeOffset);
        String currPageKey = tabId + "|" + "currentPage";
        vars.setSessionValue(currPageKey, String.valueOf(page));
        return relativeOffset;
      }
    }
    return 0;
  }

  private String previousElement(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (strSelected == null || strSelected.equals("")) return firstElement(vars, tableSQL);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.PREVIOUS, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting previous element", e);
      }
      if (data!=null) {
        return data;
      }
    }
    return strSelected;
  }

  private String firstElement(VariablesSecureApp vars, TableSQLData tableSQL) throws IOException, ServletException {
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,1);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.FIRST, "", tableSQL.getKeyColumn());

      } catch (Exception e) { 
        log4j.debug("Error getting first element", e);
      }
      if (data!=null) return data;
    }
    return "";
  }

  private String lastElement(VariablesSecureApp vars, TableSQLData tableSQL) throws IOException, ServletException {
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.LAST, "", tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting last element", e);
      }
      if (data!=null) return data;
    }
    return "";
  }

  private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars, String strPLDT_Cotizacion_ID, String strLDT_Cotizacion_Naviera_ID, TableSQLData tableSQL)
    throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("Output: dataSheet");

    String strParamLine = vars.getSessionValue(tabId + "|paramLine");
String strParamC_Bpartner_ID = vars.getSessionValue(tabId + "|paramC_Bpartner_ID");
String strParamLine_f = vars.getSessionValue(tabId + "|paramLine_f");

    boolean hasSearchCondition=false;
    vars.removeEditionData(tabId);
    hasSearchCondition = (tableSQL.hasInternalFilter() && ("").equals(strParamLine) && ("").equals(strParamC_Bpartner_ID) && ("").equals(strParamLine_f)) || !(("").equals(strParamLine) || ("%").equals(strParamLine))  || !(("").equals(strParamC_Bpartner_ID) || ("%").equals(strParamC_Bpartner_ID))  || !(("").equals(strParamLine_f) || ("%").equals(strParamLine_f)) ;
    String strOffset = vars.getSessionValue(tabId + "|offset");
    String selectedRow = "0";
    if (!strLDT_Cotizacion_Naviera_ID.equals("")) {
      selectedRow = Integer.toString(getKeyPosition(vars, strLDT_Cotizacion_Naviera_ID, tableSQL));
    }

    String[] discard={"isNotFiltered","isNotTest"};
    if (hasSearchCondition) discard[0] = new String("isFiltered");
    if (vars.getSessionValue("#ShowTest", "N").equals("Y")) discard[1] = new String("isTest");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/logisticadetransporte/parametrizacion/Cotizacion/MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA_Relation", discard).createXmlDocument();

    boolean hasReadOnlyAccess = org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId);
    ToolBar toolbar = new ToolBar(this, true, vars.getLanguage(), "MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA", false, "document.frmMain.inpldtCotizacionNavieraId", "grid", "..", "".equals("Y"), "Cotizacion", strReplaceWith, false, false, false, false, !hasReadOnlyAccess);
    toolbar.setTabId(tabId);
    
    toolbar.setDeleteable(true && !hasReadOnlyAccess);
    toolbar.prepareRelationTemplate("N".equals("Y"), hasSearchCondition, !vars.getSessionValue("#ShowTest", "N").equals("Y"), false, Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    xmlDocument.setParameter("toolbar", toolbar.toString());

    xmlDocument.setParameter("keyParent", strPLDT_Cotizacion_ID);
    xmlDocument.setParameter("parentFieldName", Utility.getFieldName("AFFE726FB55F4056B5BF8C7A623D1BD8", vars.getLanguage()));


    StringBuffer orderByArray = new StringBuffer();
      vars.setSessionValue(tabId + "|newOrder", "1");
      String positions = vars.getSessionValue(tabId + "|orderbyPositions");
      orderByArray.append("var orderByPositions = new Array(\n");
      if (!positions.equals("")) {
        StringTokenizer tokens=new StringTokenizer(positions, ",");
        boolean firstOrder = true;
        while(tokens.hasMoreTokens()){
          if (!firstOrder) orderByArray.append(",\n");
          orderByArray.append("\"").append(tokens.nextToken()).append("\"");
          firstOrder = false;
        }
      }
      orderByArray.append(");\n");
      String directions = vars.getSessionValue(tabId + "|orderbyDirections");
      orderByArray.append("var orderByDirections = new Array(\n");
      if (!positions.equals("")) {
        StringTokenizer tokens=new StringTokenizer(directions, ",");
        boolean firstOrder = true;
        while(tokens.hasMoreTokens()){
          if (!firstOrder) orderByArray.append(",\n");
          orderByArray.append("\"").append(tokens.nextToken()).append("\"");
          firstOrder = false;
        }
      }
      orderByArray.append(");\n");
//    }

    xmlDocument.setParameter("selectedColumn", "\nvar selectedRow = " + selectedRow + ";\n" + orderByArray.toString());
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("windowId", windowId);
    xmlDocument.setParameter("KeyName", "ldtCotizacionNavieraId");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    //xmlDocument.setParameter("buttonReference", Utility.messageBD(this, "Reference", vars.getLanguage()));
    try {
      WindowTabs tabs = new WindowTabs(this, vars, tabId, windowId, false);
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      String hideBackButton = vars.getGlobalVariable("hideMenu", "#Hide_BackButton", "");
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA_Relation.html", "Cotizacion", "W", strReplaceWith, tabs.breadcrumb(), hideBackButton.equals("true"));
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA_Relation.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.relationTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    {
      OBError myMessage = vars.getMessage(tabId);
      vars.removeMessage(tabId);
      if (myMessage!=null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }
    if (vars.getLanguage().equals("en_US")) xmlDocument.setParameter("parent", MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.selectParent(this, strPLDT_Cotizacion_ID));
    else xmlDocument.setParameter("parent", MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.selectParentTrl(this, strPLDT_Cotizacion_ID));

    xmlDocument.setParameter("grid", Utility.getContext(this, vars, "#RecordRange", windowId));
xmlDocument.setParameter("grid_Offset", strOffset);
xmlDocument.setParameter("grid_SortCols", positions);
xmlDocument.setParameter("grid_SortDirs", directions);
xmlDocument.setParameter("grid_Default", selectedRow);


    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageEdit(HttpServletResponse response, HttpServletRequest request, VariablesSecureApp vars,boolean boolNew, String strLDT_Cotizacion_Naviera_ID, String strPLDT_Cotizacion_ID, TableSQLData tableSQL)
    throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("Output: edit");
    
    HashMap<String, String> usedButtonShortCuts;
  
    HashMap<String, String> reservedButtonShortCuts;
  
    usedButtonShortCuts = new HashMap<String, String>();
    
    reservedButtonShortCuts = new HashMap<String, String>();
    
    
    
    String strOrderByFilter = vars.getSessionValue(tabId + "|orderby");
    String orderClause = " 1";
    if (strOrderByFilter==null || strOrderByFilter.equals("")) strOrderByFilter = orderClause;
    /*{
      if (!strOrderByFilter.equals("") && !orderClause.equals("")) strOrderByFilter += ", ";
      strOrderByFilter += orderClause;
    }*/
    
    
    String strCommand = null;
    MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[] data=null;
    XmlDocument xmlDocument=null;
    FieldProvider dataField = vars.getEditionData(tabId);
    vars.removeEditionData(tabId);
    String strParamLine = vars.getSessionValue(tabId + "|paramLine");
String strParamC_Bpartner_ID = vars.getSessionValue(tabId + "|paramC_Bpartner_ID");
String strParamLine_f = vars.getSessionValue(tabId + "|paramLine_f");

    boolean hasSearchCondition=false;
    hasSearchCondition = (tableSQL.hasInternalFilter() && ("").equals(strParamLine) && ("").equals(strParamC_Bpartner_ID) && ("").equals(strParamLine_f)) || !(("").equals(strParamLine) || ("%").equals(strParamLine))  || !(("").equals(strParamC_Bpartner_ID) || ("%").equals(strParamC_Bpartner_ID))  || !(("").equals(strParamLine_f) || ("%").equals(strParamLine_f)) ;

       String strParamSessionDate = vars.getGlobalVariable("inpParamSessionDate", Utility.getTransactionalDate(this, vars, windowId), "");
      String buscador = "";
      String[] discard = {"", "isNotTest"};
      
      if (vars.getSessionValue("#ShowTest", "N").equals("Y")) discard[1] = new String("isTest");
    if (dataField==null) {
      if (!boolNew) {
        discard[0] = new String("newDiscard");
        data = MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), strPLDT_Cotizacion_ID, strLDT_Cotizacion_Naviera_ID, Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
  
        if (!strLDT_Cotizacion_Naviera_ID.equals("") && (data == null || data.length==0)) {
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
          return;
        }
        refreshSessionEdit(vars, data);
        strCommand = "EDIT";
      }

      if (boolNew || data==null || data.length==0) {
        discard[0] = new String ("editDiscard");
        strCommand = "NEW";
        data = new MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData[0];
      } else {
        discard[0] = new String ("newDiscard");
      }
    } else {
      if (dataField.getField("ldtCotizacionNavieraId") == null || dataField.getField("ldtCotizacionNavieraId").equals("")) {
        discard[0] = new String ("editDiscard");
        strCommand = "NEW";
        boolNew = true;
      } else {
        discard[0] = new String ("newDiscard");
        strCommand = "EDIT";
      }
    }
    
    
    
    if (dataField==null) {
      if (boolNew || data==null || data.length==0) {
        refreshSessionNew(vars, strPLDT_Cotizacion_ID);
        data = MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.set(strPLDT_Cotizacion_ID, Utility.getDefault(this, vars, "ldt_dias_transito", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "Puerto_Terminal_Origen_ID", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "Fecha_Validez", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "Total_Opcion", "0.00", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "c_country_id", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "Total", "0.00", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "C_Bpartner_ID", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "m_pricelist_version_id", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "ldt_contrato_puerto_id", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), "Y", Utility.getDefault(this, vars, "Updatedby", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.selectDef7939AB039A104E06A45430FA7C5425D4_0(this, Utility.getDefault(this, vars, "Updatedby", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField)), Utility.getDefault(this, vars, "Grand_Total", "0.00", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "M_Version_Tarifa_ID", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "Total_Negociado", "0.00", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.selectDef9F4F2DA3436A4125B6793D632DE0052E(this, strPLDT_Cotizacion_ID), Utility.getDefault(this, vars, "M_Pricelist_ID", "0", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "C_Currency_ID", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "AD_Client_ID", "@AD_CLIENT_ID@", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "Puerto_Terminal_Destino_ID", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "Createdby", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.selectDefCE837539DA6F4C7BB58AAFB177F8AD2D_1(this, Utility.getDefault(this, vars, "Createdby", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField)), Utility.getDefault(this, vars, "AD_Org_ID", "@AD_ORG_ID@", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "total_costos_eur", "0", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "Seleccionar_Op", "N", "4D8F2DAE17EB4578AD6EC29C0C34F806", "N", dataField), "", Utility.getDefault(this, vars, "total_eur", "0", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "ldt_dias_libres", "", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField), Utility.getDefault(this, vars, "total_negociado_eur", "0", "4D8F2DAE17EB4578AD6EC29C0C34F806", "", dataField));
        
      }
     }
      
    String currentPOrg=Cotizacion221A13EAEB064B3387B2BB9203B1E3F3Data.selectOrg(this, strPLDT_Cotizacion_ID);
    String currentOrg = (boolNew?"":(dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId")));
    if (!currentOrg.equals("") && !currentOrg.startsWith("'")) currentOrg = "'"+currentOrg+"'";
    String currentClient = (boolNew?"":(dataField!=null?dataField.getField("adClientId"):data[0].getField("adClientId")));
    if (!currentClient.equals("") && !currentClient.startsWith("'")) currentClient = "'"+currentClient+"'";
    
    boolean hasReadOnlyAccess = org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId);
    boolean editableTab = (!hasReadOnlyAccess && (currentOrg.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),currentOrg)) && (currentClient.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), currentClient)));
    if (editableTab)
      xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/logisticadetransporte/parametrizacion/Cotizacion/MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA_Edition",discard).createXmlDocument();
    else
      xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/logisticadetransporte/parametrizacion/Cotizacion/MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA_NonEditable",discard).createXmlDocument();

    xmlDocument.setParameter("tabId", tabId);
    ToolBar toolbar = new ToolBar(this, editableTab, vars.getLanguage(), "MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA", (strCommand.equals("NEW") || boolNew || (dataField==null && (data==null || data.length==0))), "document.frmMain.inpldtCotizacionNavieraId", "", "..", "".equals("Y"), "Cotizacion", strReplaceWith, true, false, false, Utility.hasTabAttachments(this, vars, tabId, strLDT_Cotizacion_Naviera_ID), !hasReadOnlyAccess);
    toolbar.setTabId(tabId);
    toolbar.setDeleteable(true);
    toolbar.prepareEditionTemplate("N".equals("Y"), hasSearchCondition, vars.getSessionValue("#ShowTest", "N").equals("Y"), "STD", Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    xmlDocument.setParameter("toolbar", toolbar.toString());

    // set updated timestamp to manage locking mechanism
    if (!boolNew) {
      xmlDocument.setParameter("updatedTimestamp", (dataField != null ? dataField
          .getField("updatedTimeStamp") : data[0].getField("updatedTimeStamp")));
    }
    
    boolean concurrentSave = vars.getSessionValue(tabId + "|concurrentSave").equals("true");
    if (concurrentSave) {
      //after concurrent save error, force autosave
      xmlDocument.setParameter("autosave", "Y");
    } else {
      xmlDocument.setParameter("autosave", "N");
    }
    vars.removeSessionValue(tabId + "|concurrentSave");

    try {
      WindowTabs tabs = new WindowTabs(this, vars, tabId, windowId, true, (strCommand.equalsIgnoreCase("NEW")));
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      // if (!strLDT_Cotizacion_Naviera_ID.equals("")) xmlDocument.setParameter("childTabContainer", tabs.childTabs(false));
	  // else xmlDocument.setParameter("childTabContainer", tabs.childTabs(true));
	  xmlDocument.setParameter("childTabContainer", tabs.childTabs(false));
	  String hideBackButton = vars.getGlobalVariable("hideMenu", "#Hide_BackButton", "");
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA_Relation.html", "Cotizacion", "W", strReplaceWith, tabs.breadcrumb(), hideBackButton.equals("true"), !concurrentSave);
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA_Relation.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.editionTemplate(strCommand.equals("NEW")));
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
		
    
    xmlDocument.setParameter("parentOrg", currentPOrg);
    xmlDocument.setParameter("commandType", strCommand);
    xmlDocument.setParameter("buscador",buscador);
    xmlDocument.setParameter("windowId", windowId);
    xmlDocument.setParameter("changed", "");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    final String strMappingName = Utility.getTabURL(tabId, "E", false);
    xmlDocument.setParameter("mappingName", strMappingName);
    xmlDocument.setParameter("confirmOnChanges", Utility.getJSConfirmOnChanges(vars, windowId));
    //xmlDocument.setParameter("buttonReference", Utility.messageBD(this, "Reference", vars.getLanguage()));

    xmlDocument.setParameter("paramSessionDate", strParamSessionDate);

    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    OBError myMessage = vars.getMessage(tabId);
    vars.removeMessage(tabId);
    if (myMessage!=null) {
      xmlDocument.setParameter("messageType", myMessage.getType());
      xmlDocument.setParameter("messageTitle", myMessage.getTitle());
      xmlDocument.setParameter("messageMessage", myMessage.getMessage());
    }
    xmlDocument.setParameter("displayLogic", getDisplayLogicContext(vars, boolNew));
    
    
     if (dataField==null) {
      xmlDocument.setData("structure1",data);
      
    } else {
      
        FieldProvider[] dataAux = new FieldProvider[1];
        dataAux[0] = dataField;
        
        xmlDocument.setData("structure1",dataAux);
      
    }
    
      
   
    try {
      ComboTableData comboTableData = null;
comboTableData = new ComboTableData(vars, this, "19", "C_Currency_ID", "", "527DAA7C05504E449E86724FE0EFD8A4", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("cCurrencyId"):dataField.getField("cCurrencyId")));
xmlDocument.setData("reportC_Currency_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "18", "ldt_contrato_puerto_id", "DB63E0789DBB4CFCB2298011FE4A8F5D", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("ldtContratoPuertoId"):dataField.getField("ldtContratoPuertoId")));
xmlDocument.setData("reportldt_contrato_puerto_id","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("buttonGrand_Total", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttontotal_costos_eur", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonTotal_Negociado", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttontotal_negociado_eur", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonTotal", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonTotal_Opcion", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttontotal_eur", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("Seleccionar_Op_BTNname", Utility.getButtonName(this, vars, "41ACCD1338B449319BC7B4541F45B66A", "Seleccionar_Op_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalSeleccionar_Op = org.openbravo.erpCommon.utility.Utility.isModalProcess("34CDC7B016F648E6B29055C8A8236FE5"); 
xmlDocument.setParameter("Seleccionar_Op_Modal", modalSeleccionar_Op?"true":"false");
xmlDocument.setParameter("Fecha_Validez_Format", vars.getSessionValue("#AD_SqlDateFormat"));
xmlDocument.setParameter("Created_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Created_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
xmlDocument.setParameter("Updated_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Updated_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new ServletException(ex);
    }

    xmlDocument.setParameter("scriptOnLoad", getShortcutScript(usedButtonShortCuts, reservedButtonShortCuts));
    
    final String refererURL = vars.getSessionValue(tabId + "|requestURL");
    vars.removeSessionValue(tabId + "|requestURL");
    if(!refererURL.equals("")) {
    	final Boolean failedAutosave = (Boolean) vars.getSessionObject(tabId + "|failedAutosave");
		vars.removeSessionValue(tabId + "|failedAutosave");
    	if(failedAutosave != null && failedAutosave) {
    		final String jsFunction = "continueUserAction('"+refererURL+"');";
    		xmlDocument.setParameter("failedAutosave", jsFunction);
    	}
    }

    if (strCommand.equalsIgnoreCase("NEW")) {
      vars.removeSessionValue(tabId + "|failedAutosave");
      vars.removeSessionValue(strMappingName + "|hash");
    }

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageButtonFS(HttpServletResponse response, VariablesSecureApp vars, String strProcessId, String path) throws IOException, ServletException {
    log4j.debug("Output: Frames action button");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "org/openbravo/erpCommon/ad_actionButton/ActionButtonDefaultFrames").createXmlDocument();
    xmlDocument.setParameter("processId", strProcessId);
    xmlDocument.setParameter("trlFormType", "PROCESS");
    xmlDocument.setParameter("language", "defaultLang = \"" + vars.getLanguage() + "\";\n");
    xmlDocument.setParameter("type", strDireccion + path);
    out.println(xmlDocument.print());
    out.close();
  }

    private void printPageButtonSeleccionar_Op34CDC7B016F648E6B29055C8A8236FE5(HttpServletResponse response, VariablesSecureApp vars, String strLDT_Cotizacion_Naviera_ID, String strseleccionarOp, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process 34CDC7B016F648E6B29055C8A8236FE5");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/Seleccionar_Op34CDC7B016F648E6B29055C8A8236FE5", discard).createXmlDocument();
      xmlDocument.setParameter("key", strLDT_Cotizacion_Naviera_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "34CDC7B016F648E6B29055C8A8236FE5");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("34CDC7B016F648E6B29055C8A8236FE5");
        vars.removeMessage("34CDC7B016F648E6B29055C8A8236FE5");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }




    private String getDisplayLogicContext(VariablesSecureApp vars, boolean isNew) throws IOException, ServletException {
      log4j.debug("Output: Display logic context fields");
      String result = "var strTipo=\"" +Utility.getContext(this, vars, "Tipo", windowId) + "\";\nvar strShowAudit=\"" +(isNew?"N":Utility.getContext(this, vars, "ShowAudit", windowId)) + "\";\n";
      return result;
    }


    private String getReadOnlyLogicContext(VariablesSecureApp vars) throws IOException, ServletException {
      log4j.debug("Output: Read Only logic context fields");
      String result = "var strestado=\"" + Utility.getContext(this, vars, "estado", windowId) + "\";\n";
      return result;
    }




 
  private String getShortcutScript( HashMap<String, String> usedButtonShortCuts, HashMap<String, String> reservedButtonShortCuts){
    StringBuffer shortcuts = new StringBuffer();
    shortcuts.append(" function buttonListShorcuts() {\n");
    Iterator<String> ik = usedButtonShortCuts.keySet().iterator();
    Iterator<String> iv = usedButtonShortCuts.values().iterator();
    while(ik.hasNext() && iv.hasNext()){
      shortcuts.append("  keyArray[keyArray.length] = new keyArrayItem(\"").append(ik.next()).append("\", \"").append(iv.next()).append("\", null, \"altKey\", false, \"onkeydown\");\n");
    }
    shortcuts.append(" return true;\n}");
    return shortcuts.toString();
  }
  
  private int saveRecord(VariablesSecureApp vars, OBError myError, char type, String strPLDT_Cotizacion_ID) throws IOException, ServletException {
    MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData data = null;
    int total = 0;
    if (org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) {
        OBError newError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
        myError.setError(newError);
        vars.setMessage(tabId, myError);
    }
    else {
        Connection con = null;
        try {
            con = this.getTransactionConnection();
            data = getEditVariables(con, vars, strPLDT_Cotizacion_ID);
            data.dateTimeFormat = vars.getSessionValue("#AD_SqlDateTimeFormat");            
            String strSequence = "";
            if(type == 'I') {                
        strSequence = SequenceIdData.getUUID();
                if(log4j.isDebugEnabled()) log4j.debug("Sequence: " + strSequence);
                data.ldtCotizacionNavieraId = strSequence;  
            }
            if (Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),data.adClientId)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),data.adOrgId)){
		     if(type == 'I') {
		       total = data.insert(con, this);
		     } else {
		       //Check the version of the record we are saving is the one in DB
		       if (MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEAData.getCurrentDBTimestamp(this, data.ldtCotizacionNavieraId).equals(
                vars.getStringParameter("updatedTimestamp"))) {
                total = data.update(con, this);
               } else {
                 myError.setMessage(Replace.replace(Replace.replace(Utility.messageBD(this,
                    "SavingModifiedRecord", vars.getLanguage()), "\\n", "<br/>"), "&quot;", "\""));
                 myError.setType("Error");
                 vars.setSessionValue(tabId + "|concurrentSave", "true");
               } 
		     }		            
          
            }
                else {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
            myError.setError(newError);            
          }
          releaseCommitConnection(con);
        } catch(Exception ex) {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
            myError.setError(newError);   
            try {
              releaseRollbackConnection(con);
            } catch (final Exception e) { //do nothing 
            }           
        }
            
        if (myError.isEmpty() && total == 0) {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=DBExecuteError");
            myError.setError(newError);
        }
        vars.setMessage(tabId, myError);
            
        if(!myError.isEmpty()){
            if(data != null ) {
                if(type == 'I') {            			
                    data.ldtCotizacionNavieraId = "";
                }
                else {                    
                    
                }
                vars.setEditionData(tabId, data);
            }            	
        }
        else {
            vars.setSessionValue(windowId + "|LDT_Cotizacion_Naviera_ID", data.ldtCotizacionNavieraId);
        }
    }
    return total;
  }

  public String getServletInfo() {
    return "Servlet MediodeTransporteMeansofTransportCC5374351D0E44B1904CA61623941AEA. This Servlet was made by Wad constructor";
  } // End of getServletInfo() method
}
