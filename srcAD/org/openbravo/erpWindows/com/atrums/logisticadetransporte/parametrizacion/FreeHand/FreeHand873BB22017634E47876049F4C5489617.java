
package org.openbravo.erpWindows.com.atrums.logisticadetransporte.parametrizacion.FreeHand;


import org.openbravo.erpCommon.reference.*;



import org.codehaus.jettison.json.JSONObject;
import org.openbravo.erpCommon.utility.*;
import org.openbravo.data.FieldProvider;
import org.openbravo.utils.FormatUtilities;
import org.openbravo.utils.Replace;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.exception.OBException;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessRunner;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.xmlEngine.XmlDocument;
import java.util.Vector;
import java.util.StringTokenizer;
import org.openbravo.database.SessionInfo;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import java.sql.Connection;
import org.apache.log4j.Logger;

public class FreeHand873BB22017634E47876049F4C5489617 extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;
  
  private static Logger log4j = Logger.getLogger(FreeHand873BB22017634E47876049F4C5489617.class);
  
  private static final String windowId = "53C75C9421F94C609CDC00C4BED0E7CD";
  private static final String tabId = "873BB22017634E47876049F4C5489617";
  private static final String defaultTabView = "RELATION";
  private static final int accesslevel = 3;
  private static final String moduleId = "C5BE74383A264BF68316D6BF975E6EFC";
  
  @Override
  public void init(ServletConfig config) {
    setClassInfo("W", tabId, moduleId);
    super.init(config);
  }
  
  
  @Override
  public void service(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    String command = vars.getCommand();
    
    boolean securedProcess = false;
    if (command.contains("BUTTON")) {
      try {
        securedProcess = "Y".equals(org.openbravo.erpCommon.businessUtility.Preferences
            .getPreferenceValue("SecuredProcess", true, vars.getClient(), vars.getOrg(), vars
                .getUser(), vars.getRole(), windowId));
      } catch (PropertyException e) {
      }
    
     
      if (command.contains("AF6D04358D444CDD894FEEBECF2F181F")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("AF6D04358D444CDD894FEEBECF2F181F");
        SessionInfo.setModuleId("C5BE74383A264BF68316D6BF975E6EFC");
        if (securedProcess) {
          classInfo.type = "P";
          classInfo.id = "AF6D04358D444CDD894FEEBECF2F181F";
        }
      }
     
      if (command.contains("5050917887A34C71B80BFC7464BD0467")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("5050917887A34C71B80BFC7464BD0467");
        SessionInfo.setModuleId("C5BE74383A264BF68316D6BF975E6EFC");
        if (securedProcess) {
          classInfo.type = "P";
          classInfo.id = "5050917887A34C71B80BFC7464BD0467";
        }
      }
     
     
    }
    if (!securedProcess) {
      setClassInfo("W", tabId, moduleId);
    }
    super.service(request, response);
  }
  

  public void doPost (HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException {
    TableSQLData tableSQL = null;
    VariablesSecureApp vars = new VariablesSecureApp(request);
    Boolean saveRequest = (Boolean) request.getAttribute("autosave");
    
    if(saveRequest != null && saveRequest){
      String currentOrg = vars.getStringParameter("inpadOrgId");
      String currentClient = vars.getStringParameter("inpadClientId");
      boolean editableTab = (!org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)
                            && (currentOrg.equals("") || Utility.isElementInList(Utility.getContext(this, vars,"#User_Org", windowId, accesslevel), currentOrg)) 
                            && (currentClient.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),currentClient)));
    
        OBError myError = new OBError();
        String commandType = request.getParameter("inpCommandType");
        String strldtRoutingOrderId = request.getParameter("inpldtRoutingOrderId");
        
        if (editableTab) {
          int total = 0;
          
          if(commandType.equalsIgnoreCase("EDIT") && !strldtRoutingOrderId.equals(""))
              total = saveRecord(vars, myError, 'U');
          else
              total = saveRecord(vars, myError, 'I');
          
          if (!myError.isEmpty() && total == 0)     
            throw new OBException(myError.getMessage());
        }
        vars.setSessionValue(request.getParameter("mappingName") +"|hash", vars.getPostDataHash());
        vars.setSessionValue(tabId + "|Header.view", "EDIT");
        
        return;
    }
    
    try {
      tableSQL = new TableSQLData(vars, this, tabId, Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel), Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    String strOrderBy = vars.getSessionValue(tabId + "|orderby");
    if (!strOrderBy.equals("")) {
      vars.setSessionValue(tabId + "|newOrder", "1");
    }

    if (vars.commandIn("DEFAULT")) {

      String strLDT_Routing_Order_ID = vars.getGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID", "");
      

      String strView = vars.getSessionValue(tabId + "|FreeHand873BB22017634E47876049F4C5489617.view");
      if (strView.equals("")) {
        strView = defaultTabView;

        if (strView.equals("EDIT")) {
          if (strLDT_Routing_Order_ID.equals("")) strLDT_Routing_Order_ID = firstElement(vars, tableSQL);
          if (strLDT_Routing_Order_ID.equals("")) strView = "RELATION";
        }
      }
      if (strView.equals("EDIT")) 

        printPageEdit(response, request, vars, false, strLDT_Routing_Order_ID, tableSQL);

      else printPageDataSheet(response, vars, strLDT_Routing_Order_ID, tableSQL);
    } else if (vars.commandIn("DIRECT")) {
      String strLDT_Routing_Order_ID = vars.getStringParameter("inpDirectKey");
      
        
      if (strLDT_Routing_Order_ID.equals("")) strLDT_Routing_Order_ID = vars.getRequiredGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID");
      else vars.setSessionValue(windowId + "|LDT_Routing_Order_ID", strLDT_Routing_Order_ID);
      
      vars.setSessionValue(tabId + "|FreeHand873BB22017634E47876049F4C5489617.view", "EDIT");

      printPageEdit(response, request, vars, false, strLDT_Routing_Order_ID, tableSQL);

    } else if (vars.commandIn("TAB")) {


      String strView = vars.getSessionValue(tabId + "|FreeHand873BB22017634E47876049F4C5489617.view");
      String strLDT_Routing_Order_ID = "";
      if (strView.equals("")) {
        strView = defaultTabView;
        if (strView.equals("EDIT")) {
          strLDT_Routing_Order_ID = firstElement(vars, tableSQL);
          if (strLDT_Routing_Order_ID.equals("")) strView = "RELATION";
        }
      }
      if (strView.equals("EDIT")) {

        if (strLDT_Routing_Order_ID.equals("")) strLDT_Routing_Order_ID = firstElement(vars, tableSQL);
        printPageEdit(response, request, vars, false, strLDT_Routing_Order_ID, tableSQL);

      } else printPageDataSheet(response, vars, "", tableSQL);
    } else if (vars.commandIn("SEARCH")) {
vars.getRequestGlobalVariable("inpParamDocumentno", tabId + "|paramDocumentno");

        vars.getRequestGlobalVariable("inpParamUpdated", tabId + "|paramUpdated");
        vars.getRequestGlobalVariable("inpParamUpdatedBy", tabId + "|paramUpdatedBy");
        vars.getRequestGlobalVariable("inpParamCreated", tabId + "|paramCreated");
        vars.getRequestGlobalVariable("inpparamCreatedBy", tabId + "|paramCreatedBy");
      
      
      vars.removeSessionValue(windowId + "|LDT_Routing_Order_ID");
      String strLDT_Routing_Order_ID="";

      String strView = vars.getSessionValue(tabId + "|FreeHand873BB22017634E47876049F4C5489617.view");
      if (strView.equals("")) strView=defaultTabView;

      if (strView.equals("EDIT")) {
        strLDT_Routing_Order_ID = firstElement(vars, tableSQL);
        if (strLDT_Routing_Order_ID.equals("")) {
          // filter returns empty set
          strView = "RELATION";
          // switch to grid permanently until the user changes the view again
          vars.setSessionValue(tabId + "|FreeHand873BB22017634E47876049F4C5489617.view", strView);
        }
      }

      if (strView.equals("EDIT")) 

        printPageEdit(response, request, vars, false, strLDT_Routing_Order_ID, tableSQL);

      else printPageDataSheet(response, vars, strLDT_Routing_Order_ID, tableSQL);
    } else if (vars.commandIn("RELATION")) {
      

      String strLDT_Routing_Order_ID = vars.getGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID", "");
      vars.setSessionValue(tabId + "|FreeHand873BB22017634E47876049F4C5489617.view", "RELATION");
      printPageDataSheet(response, vars, strLDT_Routing_Order_ID, tableSQL);
    } else if (vars.commandIn("NEW")) {


      printPageEdit(response, request, vars, true, "", tableSQL);

    } else if (vars.commandIn("EDIT")) {

      @SuppressWarnings("unused") // In Expense Invoice tab this variable is not used, to be fixed
      String strLDT_Routing_Order_ID = vars.getGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID", "");
      vars.setSessionValue(tabId + "|FreeHand873BB22017634E47876049F4C5489617.view", "EDIT");

      setHistoryCommand(request, "EDIT");
      printPageEdit(response, request, vars, false, strLDT_Routing_Order_ID, tableSQL);

    } else if (vars.commandIn("NEXT")) {

      String strLDT_Routing_Order_ID = vars.getRequiredStringParameter("inpldtRoutingOrderId");
      
      String strNext = nextElement(vars, strLDT_Routing_Order_ID, tableSQL);

      printPageEdit(response, request, vars, false, strNext, tableSQL);
    } else if (vars.commandIn("PREVIOUS")) {

      String strLDT_Routing_Order_ID = vars.getRequiredStringParameter("inpldtRoutingOrderId");
      
      String strPrevious = previousElement(vars, strLDT_Routing_Order_ID, tableSQL);

      printPageEdit(response, request, vars, false, strPrevious, tableSQL);
    } else if (vars.commandIn("FIRST_RELATION")) {

      vars.setSessionValue(tabId + "|FreeHand873BB22017634E47876049F4C5489617.initRecordNumber", "0");
      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("PREVIOUS_RELATION")) {

      String strInitRecord = vars.getSessionValue(tabId + "|FreeHand873BB22017634E47876049F4C5489617.initRecordNumber");
      String strRecordRange = Utility.getContext(this, vars, "#RecordRange", windowId);
      int intRecordRange = strRecordRange.equals("")?0:Integer.parseInt(strRecordRange);
      if (strInitRecord.equals("") || strInitRecord.equals("0")) {
        vars.setSessionValue(tabId + "|FreeHand873BB22017634E47876049F4C5489617.initRecordNumber", "0");
      } else {
        int initRecord = (strInitRecord.equals("")?0:Integer.parseInt(strInitRecord));
        initRecord -= intRecordRange;
        strInitRecord = ((initRecord<0)?"0":Integer.toString(initRecord));
        vars.setSessionValue(tabId + "|FreeHand873BB22017634E47876049F4C5489617.initRecordNumber", strInitRecord);
      }
      vars.removeSessionValue(windowId + "|LDT_Routing_Order_ID");

      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("NEXT_RELATION")) {

      String strInitRecord = vars.getSessionValue(tabId + "|FreeHand873BB22017634E47876049F4C5489617.initRecordNumber");
      String strRecordRange = Utility.getContext(this, vars, "#RecordRange", windowId);
      int intRecordRange = strRecordRange.equals("")?0:Integer.parseInt(strRecordRange);
      int initRecord = (strInitRecord.equals("")?0:Integer.parseInt(strInitRecord));
      if (initRecord==0) initRecord=1;
      initRecord += intRecordRange;
      strInitRecord = ((initRecord<0)?"0":Integer.toString(initRecord));
      vars.setSessionValue(tabId + "|FreeHand873BB22017634E47876049F4C5489617.initRecordNumber", strInitRecord);
      vars.removeSessionValue(windowId + "|LDT_Routing_Order_ID");

      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("FIRST")) {

      
      String strFirst = firstElement(vars, tableSQL);

      printPageEdit(response, request, vars, false, strFirst, tableSQL);
    } else if (vars.commandIn("LAST_RELATION")) {

      String strLast = lastElement(vars, tableSQL);
      printPageDataSheet(response, vars, strLast, tableSQL);
    } else if (vars.commandIn("LAST")) {

      
      String strLast = lastElement(vars, tableSQL);

      printPageEdit(response, request, vars, false, strLast, tableSQL);
    } else if (vars.commandIn("SAVE_NEW_RELATION", "SAVE_NEW_NEW", "SAVE_NEW_EDIT")) {

      OBError myError = new OBError();      
      int total = saveRecord(vars, myError, 'I');      
      if (!myError.isEmpty()) {        
        response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
      } 
      else {
		if (myError.isEmpty()) {
		  myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsInserted");
		  myError.setMessage(total + " " + myError.getMessage());
		  vars.setMessage(tabId, myError);
		}        
        if (vars.commandIn("SAVE_NEW_NEW")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
        else if (vars.commandIn("SAVE_NEW_EDIT")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        else response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
      }
    } else if (vars.commandIn("SAVE_EDIT_RELATION", "SAVE_EDIT_NEW", "SAVE_EDIT_EDIT", "SAVE_EDIT_NEXT")) {

      String strLDT_Routing_Order_ID = vars.getRequiredGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID");
      OBError myError = new OBError();
      int total = saveRecord(vars, myError, 'U');      
      if (!myError.isEmpty()) {
        response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
      } 
      else {
        if (myError.isEmpty()) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsUpdated");
          myError.setMessage(total + " " + myError.getMessage());
          vars.setMessage(tabId, myError);
        }
        if (vars.commandIn("SAVE_EDIT_NEW")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
        else if (vars.commandIn("SAVE_EDIT_EDIT")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        else if (vars.commandIn("SAVE_EDIT_NEXT")) {
          String strNext = nextElement(vars, strLDT_Routing_Order_ID, tableSQL);
          vars.setSessionValue(windowId + "|LDT_Routing_Order_ID", strNext);
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        } else response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
      }
    } else if (vars.commandIn("DELETE")) {

      String strLDT_Routing_Order_ID = vars.getRequiredStringParameter("inpldtRoutingOrderId");
      //FreeHand873BB22017634E47876049F4C5489617Data data = getEditVariables(vars);
      int total = 0;
      OBError myError = null;
      if (org.openbravo.erpCommon.utility.WindowAccessData.hasNotDeleteAccess(this, vars.getRole(), tabId)) {
        myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
        vars.setMessage(tabId, myError);
      } else {
        try {
          total = FreeHand873BB22017634E47876049F4C5489617Data.delete(this, strLDT_Routing_Order_ID, Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), Utility.getContext(this, vars, "#User_Org", windowId, accesslevel));
        } catch(ServletException ex) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myError.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myError);
        }
        if (myError==null && total==0) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
        }
        vars.removeSessionValue(windowId + "|ldtRoutingOrderId");
        vars.setSessionValue(tabId + "|FreeHand873BB22017634E47876049F4C5489617.view", "RELATION");
      }
      if (myError==null) {
        myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsDeleted");
        myError.setMessage(total + " " + myError.getMessage());
        vars.setMessage(tabId, myError);
      }
      response.sendRedirect(strDireccion + request.getServletPath());

     } else if (vars.commandIn("BUTTONEmbarqueAF6D04358D444CDD894FEEBECF2F181F")) {
        vars.setSessionValue("buttonAF6D04358D444CDD894FEEBECF2F181F.strembarque", vars.getStringParameter("inpembarque"));
        vars.setSessionValue("buttonAF6D04358D444CDD894FEEBECF2F181F.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("buttonAF6D04358D444CDD894FEEBECF2F181F.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("buttonAF6D04358D444CDD894FEEBECF2F181F.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        
        
        //Save in session needed params for combos if needed
        vars.setSessionObject("buttonAF6D04358D444CDD894FEEBECF2F181F.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "AF6D04358D444CDD894FEEBECF2F181F", request.getServletPath());    
     } else if (vars.commandIn("BUTTONAF6D04358D444CDD894FEEBECF2F181F")) {
        String strLDT_Routing_Order_ID = vars.getGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID", "");
        String strembarque = vars.getSessionValue("buttonAF6D04358D444CDD894FEEBECF2F181F.strembarque");
        String strProcessing = vars.getSessionValue("buttonAF6D04358D444CDD894FEEBECF2F181F.strProcessing");
        String strOrg = vars.getSessionValue("buttonAF6D04358D444CDD894FEEBECF2F181F.strOrg");
        String strClient = vars.getSessionValue("buttonAF6D04358D444CDD894FEEBECF2F181F.strClient");
        
        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtonEmbarqueAF6D04358D444CDD894FEEBECF2F181F(response, vars, strLDT_Routing_Order_ID, strembarque, strProcessing);
        }

     } else if (vars.commandIn("BUTTONES_Fin_Embarque5050917887A34C71B80BFC7464BD0467")) {
        vars.setSessionValue("button5050917887A34C71B80BFC7464BD0467.stresFinEmbarque", vars.getStringParameter("inpesFinEmbarque"));
        vars.setSessionValue("button5050917887A34C71B80BFC7464BD0467.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("button5050917887A34C71B80BFC7464BD0467.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("button5050917887A34C71B80BFC7464BD0467.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        
        
        //Save in session needed params for combos if needed
        vars.setSessionObject("button5050917887A34C71B80BFC7464BD0467.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "5050917887A34C71B80BFC7464BD0467", request.getServletPath());    
     } else if (vars.commandIn("BUTTON5050917887A34C71B80BFC7464BD0467")) {
        String strLDT_Routing_Order_ID = vars.getGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID", "");
        String stresFinEmbarque = vars.getSessionValue("button5050917887A34C71B80BFC7464BD0467.stresFinEmbarque");
        String strProcessing = vars.getSessionValue("button5050917887A34C71B80BFC7464BD0467.strProcessing");
        String strOrg = vars.getSessionValue("button5050917887A34C71B80BFC7464BD0467.strOrg");
        String strClient = vars.getSessionValue("button5050917887A34C71B80BFC7464BD0467.strClient");
        
        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtonES_Fin_Embarque5050917887A34C71B80BFC7464BD0467(response, vars, strLDT_Routing_Order_ID, stresFinEmbarque, strProcessing);
        }


    } else if (vars.commandIn("SAVE_BUTTONEmbarqueAF6D04358D444CDD894FEEBECF2F181F")) {
        String strLDT_Routing_Order_ID = vars.getGlobalVariable("inpKey", windowId + "|LDT_Routing_Order_ID", "");
        @SuppressWarnings("unused")
        String strembarque = vars.getStringParameter("inpembarque");
        String strProcessing = vars.getStringParameter("inpprocessing");
        OBError myMessage = null;
        try {
          String pinstance = SequenceIdData.getUUID();
          PInstanceProcessData.insertPInstance(this, pinstance, "AF6D04358D444CDD894FEEBECF2F181F", (("LDT_Routing_Order_ID".equalsIgnoreCase("AD_Language"))?"0":strLDT_Routing_Order_ID), strProcessing, vars.getUser(), vars.getClient(), vars.getOrg());
          
          
          ProcessBundle bundle = ProcessBundle.pinstance(pinstance, vars, this);
          new ProcessRunner(bundle).execute(this);
          
          PInstanceProcessData[] pinstanceData = PInstanceProcessData.select(this, pinstance);
          myMessage = Utility.getProcessInstanceMessage(this, vars, pinstanceData);
        } catch (ServletException ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);
    } else if (vars.commandIn("SAVE_BUTTONES_Fin_Embarque5050917887A34C71B80BFC7464BD0467")) {
        String strLDT_Routing_Order_ID = vars.getGlobalVariable("inpKey", windowId + "|LDT_Routing_Order_ID", "");
        @SuppressWarnings("unused")
        String stresFinEmbarque = vars.getStringParameter("inpesFinEmbarque");
        String strProcessing = vars.getStringParameter("inpprocessing");
        OBError myMessage = null;
        try {
          String pinstance = SequenceIdData.getUUID();
          PInstanceProcessData.insertPInstance(this, pinstance, "5050917887A34C71B80BFC7464BD0467", (("LDT_Routing_Order_ID".equalsIgnoreCase("AD_Language"))?"0":strLDT_Routing_Order_ID), strProcessing, vars.getUser(), vars.getClient(), vars.getOrg());
          
          
          ProcessBundle bundle = ProcessBundle.pinstance(pinstance, vars, this);
          new ProcessRunner(bundle).execute(this);
          
          PInstanceProcessData[] pinstanceData = PInstanceProcessData.select(this, pinstance);
          myMessage = Utility.getProcessInstanceMessage(this, vars, pinstanceData);
        } catch (ServletException ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);






    } else if (vars.commandIn("SAVE_XHR")) {
      
      OBError myError = new OBError();
      JSONObject result = new JSONObject();
      String commandType = vars.getStringParameter("inpCommandType");
      char saveType = "NEW".equals(commandType) ? 'I' : 'U';
      try {
        int total = saveRecord(vars, myError, saveType);
        if (myError.isEmpty()) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsUpdated");
          myError.setMessage(total + " " + myError.getMessage());
          myError.setType("Success");
        }
        result.put("oberror", myError.toMap());
        result.put("tabid", vars.getStringParameter("tabID"));
        result.put("redirect", strDireccion + request.getServletPath() + "?Command=" + commandType);
      } catch (Exception e) {
        log4j.error("Error saving record (XHR request): " + e.getMessage(), e);
        myError.setType("Error");
        myError.setMessage(e.getMessage());
      }

      response.setContentType("application/json");
      PrintWriter out = response.getWriter();
      out.print(result.toString());
      out.flush();
      out.close();
    } else if (vars.getCommand().toUpperCase().startsWith("BUTTON") || vars.getCommand().toUpperCase().startsWith("SAVE_BUTTON")) {
      pageErrorPopUp(response);
    } else pageError(response);
  }
  private FreeHand873BB22017634E47876049F4C5489617Data getEditVariables(Connection con, VariablesSecureApp vars) throws IOException,ServletException {
    FreeHand873BB22017634E47876049F4C5489617Data data = new FreeHand873BB22017634E47876049F4C5489617Data();
    ServletException ex = null;
    try {
    data.adOrgId = vars.getRequiredGlobalVariable("inpadOrgId", windowId + "|AD_Org_ID");     data.adOrgIdr = vars.getStringParameter("inpadOrgId_R");     data.cDoctypeId = vars.getRequiredStringParameter("inpcDoctypeId");     data.cDoctypeIdr = vars.getStringParameter("inpcDoctypeId_R");     data.numembarque = vars.getStringParameter("inpnumembarque");     data.fecha = vars.getRequiredStringParameter("inpfecha");     data.estado = vars.getRequiredGlobalVariable("inpestado", windowId + "|Estado");     data.puertoTerminalOrigenId = vars.getRequiredGlobalVariable("inppuertoTerminalOrigenId", windowId + "|Puerto_Terminal_Origen_ID");     data.puertoTerminalDestinoId = vars.getRequiredGlobalVariable("inppuertoTerminalDestinoId", windowId + "|Puerto_Terminal_Destino_ID");     data.cBpartnerMtId = vars.getRequiredGlobalVariable("inpcBpartnerMtId", windowId + "|c_bpartner_mt_id");     data.ldtContratoPuertoId = vars.getRequestGlobalVariable("inpldtContratoPuertoId", windowId + "|ldt_contrato_puerto_id");     data.fechaEstimada = vars.getStringParameter("inpfechaEstimada");     data.vendedorId = vars.getRequiredGlobalVariable("inpvendedorId", windowId + "|Vendedor_ID");     data.vendedorIdr = vars.getStringParameter("inpvendedorId_R");     data.mPricelistVersionId = vars.getRequiredGlobalVariable("inpmPricelistVersionId", windowId + "|M_Pricelist_Version_ID");     data.mPricelistVersionIdr = vars.getStringParameter("inpmPricelistVersionId_R");     data.cBpartnerId = vars.getRequestGlobalVariable("inpcBpartnerId", windowId + "|C_Bpartner_ID");     data.cBpartnerRuc = vars.getStringParameter("inpcBpartnerRuc");     data.cBpartnerLocationId = vars.getRequestGlobalVariable("inpcBpartnerLocationId", windowId + "|C_Bpartner_Location_ID");     data.telfShipper = vars.getStringParameter("inptelfShipper");     data.adUserId = vars.getStringParameter("inpadUserId");     data.adUserIdr = vars.getStringParameter("inpadUserId_R");     data.mail = vars.getStringParameter("inpmail");     data.clienteId = vars.getRequiredGlobalVariable("inpclienteId", windowId + "|Cliente_ID");     data.clienteIdr = vars.getStringParameter("inpclienteId_R");     data.clienteRuc = vars.getRequiredStringParameter("inpclienteRuc");     data.clienteDireccionId = vars.getRequiredStringParameter("inpclienteDireccionId");     data.clienteDireccionIdr = vars.getStringParameter("inpclienteDireccionId_R");     data.modalidad = vars.getRequiredGlobalVariable("inpmodalidad", windowId + "|Modalidad");     data.modalidadr = vars.getStringParameter("inpmodalidad_R");     data.tipo = vars.getRequiredGlobalVariable("inptipo", windowId + "|Tipo");     data.tipor = vars.getStringParameter("inptipo_R");     data.cIncotermsId = vars.getRequiredGlobalVariable("inpcIncotermsId", windowId + "|C_Incoterms_ID");     data.cIncotermsIdr = vars.getStringParameter("inpcIncotermsId_R");     data.agenteId = vars.getRequestGlobalVariable("inpagenteId", windowId + "|Agente_ID");    try {   data.costosLocales = vars.getNumericParameter("inpcostosLocales", vars.getSessionValue(windowId + "|Costos_Locales"));  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.costosLocalesEur = vars.getNumericParameter("inpcostosLocalesEur", vars.getSessionValue(windowId + "|costos_locales_eur"));  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.totalNegociado = vars.getNumericParameter("inptotalNegociado");  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.totalNegociadoEur = vars.getNumericParameter("inptotalNegociadoEur", vars.getSessionValue(windowId + "|total_negociado_eur"));  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.cod = vars.getNumericParameter("inpcod");  } catch (ServletException paramEx) { ex = paramEx; }     data.observacion = vars.getStringParameter("inpobservacion");     data.embarque = vars.getRequestGlobalVariable("inpembarque", windowId + "|Embarque");     data.esFinEmbarque = vars.getRequestGlobalVariable("inpesFinEmbarque", windowId + "|ES_Fin_Embarque");     data.isactive = vars.getStringParameter("inpisactive", "N");     data.adClientId = vars.getRequiredGlobalVariable("inpadClientId", windowId + "|AD_Client_ID");     data.mPricelistId = vars.getRequestGlobalVariable("inpmPricelistId", windowId + "|M_Pricelist_ID");     data.ldtRoutingOrderId = vars.getRequestGlobalVariable("inpldtRoutingOrderId", windowId + "|LDT_Routing_Order_ID");     data.cCountryId = vars.getRequiredGlobalVariable("inpcCountryId", windowId + "|c_country_id");    try {   data.tarifaVenta = vars.getNumericParameter("inptarifaVenta", vars.getSessionValue(windowId + "|Tarifa_Venta"));  } catch (ServletException paramEx) { ex = paramEx; }     data.documentno = vars.getRequiredStringParameter("inpdocumentno"); 
      data.createdby = vars.getUser();
      data.updatedby = vars.getUser();
      data.adUserClient = Utility.getContext(this, vars, "#User_Client", windowId, accesslevel);
      data.adOrgClient = Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel);
      data.updatedTimeStamp = vars.getStringParameter("updatedTimestamp");



    
    

    
    }
    catch(ServletException e) {
    	vars.setEditionData(tabId, data);
    	throw e;
    }
    // Behavior with exception for numeric fields is to catch last one if we have multiple ones
    if(ex != null) {
      vars.setEditionData(tabId, data);
      throw ex;
    }
    return data;
  }




    private void refreshSessionEdit(VariablesSecureApp vars, FieldProvider[] data) {
      if (data==null || data.length==0) return;
          vars.setSessionValue(windowId + "|AD_Org_ID", data[0].getField("adOrgId"));    vars.setSessionValue(windowId + "|Estado", data[0].getField("estado"));    vars.setSessionValue(windowId + "|Puerto_Terminal_Origen_ID", data[0].getField("puertoTerminalOrigenId"));    vars.setSessionValue(windowId + "|Puerto_Terminal_Destino_ID", data[0].getField("puertoTerminalDestinoId"));    vars.setSessionValue(windowId + "|c_bpartner_mt_id", data[0].getField("cBpartnerMtId"));    vars.setSessionValue(windowId + "|LDT_Contrato_Puerto_ID", data[0].getField("ldtContratoPuertoId"));    vars.setSessionValue(windowId + "|Vendedor_ID", data[0].getField("vendedorId"));    vars.setSessionValue(windowId + "|M_PriceList_Version_ID", data[0].getField("mPricelistVersionId"));    vars.setSessionValue(windowId + "|C_BPartner_ID", data[0].getField("cBpartnerId"));    vars.setSessionValue(windowId + "|C_BPartner_Location_ID", data[0].getField("cBpartnerLocationId"));    vars.setSessionValue(windowId + "|Cliente_ID", data[0].getField("clienteId"));    vars.setSessionValue(windowId + "|Modalidad", data[0].getField("modalidad"));    vars.setSessionValue(windowId + "|Tipo", data[0].getField("tipo"));    vars.setSessionValue(windowId + "|C_Incoterms_ID", data[0].getField("cIncotermsId"));    vars.setSessionValue(windowId + "|Agente_ID", data[0].getField("agenteId"));    vars.setSessionValue(windowId + "|Costos_Locales", data[0].getField("costosLocales"));    vars.setSessionValue(windowId + "|costos_locales_eur", data[0].getField("costosLocalesEur"));    vars.setSessionValue(windowId + "|total_negociado_eur", data[0].getField("totalNegociadoEur"));    vars.setSessionValue(windowId + "|Embarque", data[0].getField("embarque"));    vars.setSessionValue(windowId + "|ES_Fin_Embarque", data[0].getField("esFinEmbarque"));    vars.setSessionValue(windowId + "|Tarifa_Venta", data[0].getField("tarifaVenta"));    vars.setSessionValue(windowId + "|C_Country_ID", data[0].getField("cCountryId"));    vars.setSessionValue(windowId + "|M_PriceList_ID", data[0].getField("mPricelistId"));    vars.setSessionValue(windowId + "|AD_Client_ID", data[0].getField("adClientId"));    vars.setSessionValue(windowId + "|LDT_Routing_Order_ID", data[0].getField("ldtRoutingOrderId"));
    }

    private void refreshSessionNew(VariablesSecureApp vars) throws IOException,ServletException {
      FreeHand873BB22017634E47876049F4C5489617Data[] data = FreeHand873BB22017634E47876049F4C5489617Data.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), vars.getStringParameter("inpldtRoutingOrderId", ""), Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
      if (data==null || data.length==0) return;
      refreshSessionEdit(vars, data);
    }

  private String nextElement(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (strSelected == null || strSelected.equals("")) return firstElement(vars, tableSQL);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(), 0, 0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.NEXT, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting next element", e);
      }
      if (data!=null) {
        if (data!=null) return data;
      }
    }
    return strSelected;
  }

  private int getKeyPosition(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("getKeyPosition: " + strSelected);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.GETPOSITION, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting key position", e);
      }
      if (data!=null) {
        // split offset -> (page,relativeOffset)
        int absoluteOffset = Integer.valueOf(data);
        int page = absoluteOffset / TableSQLData.maxRowsPerGridPage;
        int relativeOffset = absoluteOffset % TableSQLData.maxRowsPerGridPage;
        log4j.debug("getKeyPosition: absOffset: " + absoluteOffset + "=> page: " + page + " relOffset: " + relativeOffset);
        String currPageKey = tabId + "|" + "currentPage";
        vars.setSessionValue(currPageKey, String.valueOf(page));
        return relativeOffset;
      }
    }
    return 0;
  }

  private String previousElement(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (strSelected == null || strSelected.equals("")) return firstElement(vars, tableSQL);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.PREVIOUS, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting previous element", e);
      }
      if (data!=null) {
        return data;
      }
    }
    return strSelected;
  }

  private String firstElement(VariablesSecureApp vars, TableSQLData tableSQL) throws IOException, ServletException {
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,1);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.FIRST, "", tableSQL.getKeyColumn());

      } catch (Exception e) { 
        log4j.debug("Error getting first element", e);
      }
      if (data!=null) return data;
    }
    return "";
  }

  private String lastElement(VariablesSecureApp vars, TableSQLData tableSQL) throws IOException, ServletException {
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.LAST, "", tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting last element", e);
      }
      if (data!=null) return data;
    }
    return "";
  }

  private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars, String strLDT_Routing_Order_ID, TableSQLData tableSQL)
    throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("Output: dataSheet");

    String strParamDocumentno = vars.getSessionValue(tabId + "|paramDocumentno");

    boolean hasSearchCondition=false;
    vars.removeEditionData(tabId);
    hasSearchCondition = (tableSQL.hasInternalFilter() && ("").equals(strParamDocumentno)) || !(("").equals(strParamDocumentno) || ("%").equals(strParamDocumentno)) ;
    String strOffset = vars.getSessionValue(tabId + "|offset");
    String selectedRow = "0";
    if (!strLDT_Routing_Order_ID.equals("")) {
      selectedRow = Integer.toString(getKeyPosition(vars, strLDT_Routing_Order_ID, tableSQL));
    }

    String[] discard={"isNotFiltered","isNotTest"};
    if (hasSearchCondition) discard[0] = new String("isFiltered");
    if (vars.getSessionValue("#ShowTest", "N").equals("Y")) discard[1] = new String("isTest");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/logisticadetransporte/parametrizacion/FreeHand/FreeHand873BB22017634E47876049F4C5489617_Relation", discard).createXmlDocument();

    boolean hasReadOnlyAccess = org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId);
    ToolBar toolbar = new ToolBar(this, true, vars.getLanguage(), "FreeHand873BB22017634E47876049F4C5489617", false, "document.frmMain.inpldtRoutingOrderId", "grid", "..", "".equals("Y"), "FreeHand", strReplaceWith, false, false, false, false, !hasReadOnlyAccess);
    toolbar.setTabId(tabId);
    
    toolbar.setDeleteable(true && !hasReadOnlyAccess);
    toolbar.prepareRelationTemplate("N".equals("Y"), hasSearchCondition, !vars.getSessionValue("#ShowTest", "N").equals("Y"), false, Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    xmlDocument.setParameter("toolbar", toolbar.toString());



    StringBuffer orderByArray = new StringBuffer();
      vars.setSessionValue(tabId + "|newOrder", "1");
      String positions = vars.getSessionValue(tabId + "|orderbyPositions");
      orderByArray.append("var orderByPositions = new Array(\n");
      if (!positions.equals("")) {
        StringTokenizer tokens=new StringTokenizer(positions, ",");
        boolean firstOrder = true;
        while(tokens.hasMoreTokens()){
          if (!firstOrder) orderByArray.append(",\n");
          orderByArray.append("\"").append(tokens.nextToken()).append("\"");
          firstOrder = false;
        }
      }
      orderByArray.append(");\n");
      String directions = vars.getSessionValue(tabId + "|orderbyDirections");
      orderByArray.append("var orderByDirections = new Array(\n");
      if (!positions.equals("")) {
        StringTokenizer tokens=new StringTokenizer(directions, ",");
        boolean firstOrder = true;
        while(tokens.hasMoreTokens()){
          if (!firstOrder) orderByArray.append(",\n");
          orderByArray.append("\"").append(tokens.nextToken()).append("\"");
          firstOrder = false;
        }
      }
      orderByArray.append(");\n");
//    }

    xmlDocument.setParameter("selectedColumn", "\nvar selectedRow = " + selectedRow + ";\n" + orderByArray.toString());
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("windowId", windowId);
    xmlDocument.setParameter("KeyName", "ldtRoutingOrderId");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    //xmlDocument.setParameter("buttonReference", Utility.messageBD(this, "Reference", vars.getLanguage()));
    try {
      WindowTabs tabs = new WindowTabs(this, vars, tabId, windowId, false);
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      String hideBackButton = vars.getGlobalVariable("hideMenu", "#Hide_BackButton", "");
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "FreeHand873BB22017634E47876049F4C5489617_Relation.html", "FreeHand", "W", strReplaceWith, tabs.breadcrumb(), hideBackButton.equals("true"));
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "FreeHand873BB22017634E47876049F4C5489617_Relation.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.relationTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    {
      OBError myMessage = vars.getMessage(tabId);
      vars.removeMessage(tabId);
      if (myMessage!=null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }


    xmlDocument.setParameter("grid", Utility.getContext(this, vars, "#RecordRange", windowId));
xmlDocument.setParameter("grid_Offset", strOffset);
xmlDocument.setParameter("grid_SortCols", positions);
xmlDocument.setParameter("grid_SortDirs", directions);
xmlDocument.setParameter("grid_Default", selectedRow);


    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageEdit(HttpServletResponse response, HttpServletRequest request, VariablesSecureApp vars,boolean boolNew, String strLDT_Routing_Order_ID, TableSQLData tableSQL)
    throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("Output: edit");
    
    HashMap<String, String> usedButtonShortCuts;
  
    HashMap<String, String> reservedButtonShortCuts;
  
    usedButtonShortCuts = new HashMap<String, String>();
    
    reservedButtonShortCuts = new HashMap<String, String>();
    
    
    
    String strOrderByFilter = vars.getSessionValue(tabId + "|orderby");
    String orderClause = " 1";
    if (strOrderByFilter==null || strOrderByFilter.equals("")) strOrderByFilter = orderClause;
    /*{
      if (!strOrderByFilter.equals("") && !orderClause.equals("")) strOrderByFilter += ", ";
      strOrderByFilter += orderClause;
    }*/
    
    
    String strCommand = null;
    FreeHand873BB22017634E47876049F4C5489617Data[] data=null;
    XmlDocument xmlDocument=null;
    FieldProvider dataField = vars.getEditionData(tabId);
    vars.removeEditionData(tabId);
    String strParamDocumentno = vars.getSessionValue(tabId + "|paramDocumentno");

    boolean hasSearchCondition=false;
    hasSearchCondition = (tableSQL.hasInternalFilter() && ("").equals(strParamDocumentno)) || !(("").equals(strParamDocumentno) || ("%").equals(strParamDocumentno)) ;

       String strParamSessionDate = vars.getGlobalVariable("inpParamSessionDate", Utility.getTransactionalDate(this, vars, windowId), "");
      String buscador = "";
      String[] discard = {"", "isNotTest"};
      
      if (vars.getSessionValue("#ShowTest", "N").equals("Y")) discard[1] = new String("isTest");
    if (dataField==null) {
      if (!boolNew) {
        discard[0] = new String("newDiscard");
        data = FreeHand873BB22017634E47876049F4C5489617Data.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), strLDT_Routing_Order_ID, Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
  
        if (!strLDT_Routing_Order_ID.equals("") && (data == null || data.length==0)) {
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
          return;
        }
        refreshSessionEdit(vars, data);
        strCommand = "EDIT";
      }

      if (boolNew || data==null || data.length==0) {
        discard[0] = new String ("editDiscard");
        strCommand = "NEW";
        data = new FreeHand873BB22017634E47876049F4C5489617Data[0];
      } else {
        discard[0] = new String ("newDiscard");
      }
    } else {
      if (dataField.getField("ldtRoutingOrderId") == null || dataField.getField("ldtRoutingOrderId").equals("")) {
        discard[0] = new String ("editDiscard");
        strCommand = "NEW";
        boolNew = true;
      } else {
        discard[0] = new String ("newDiscard");
        strCommand = "EDIT";
      }
    }
    
    
    
    if (dataField==null) {
      if (boolNew || data==null || data.length==0) {
        refreshSessionNew(vars);
        data = FreeHand873BB22017634E47876049F4C5489617Data.set(Utility.getDefault(this, vars, "C_Bpartner_Ruc", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "Mail", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "Fecha_Estimada", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "C_Bpartner_Location_ID", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "ES_Fin_Embarque", "N", "53C75C9421F94C609CDC00C4BED0E7CD", "N", dataField), Utility.getDefault(this, vars, "Cliente_ID", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), FreeHand873BB22017634E47876049F4C5489617Data.selectDef288AAF7724AB4659AC3EA3CAB946388E_0(this, Utility.getDefault(this, vars, "Cliente_ID", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField)), "", Utility.getDefault(this, vars, "Tipo", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "AD_Client_ID", "@AD_CLIENT_ID@", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "Embarque", "N", "53C75C9421F94C609CDC00C4BED0E7CD", "N", dataField), Utility.getDefault(this, vars, "C_Incoterms_ID", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "Updatedby", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), FreeHand873BB22017634E47876049F4C5489617Data.selectDef3C3EE0E1BB8041268CB1EF103CC6E8D0_1(this, Utility.getDefault(this, vars, "Updatedby", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField)), Utility.getDefault(this, vars, "Tarifa_Venta", "0.00", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "ldt_contrato_puerto_id", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "Vendedor_ID", "F52FCB3ABDB947E89C711C85B72ED558", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "Telf_Shipper", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "AD_Org_ID", "@AD_ORG_ID@", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "AD_User_ID", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "Puerto_Terminal_Destino_ID", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "Modalidad", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "M_Pricelist_Version_ID", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "Observacion", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "c_bpartner_mt_id", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "Estado", "DR", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "Costos_Locales", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "costos_locales_eur", "0", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "M_Pricelist_ID", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "Cliente_Direccion_ID", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "Documentno", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "c_country_id", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "Createdby", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), FreeHand873BB22017634E47876049F4C5489617Data.selectDefA68E17A6693C4D78B7972B1419D3E24E_2(this, Utility.getDefault(this, vars, "Createdby", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField)), Utility.getDefault(this, vars, "Numembarque", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "Cod", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "Cliente_Ruc", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "total_negociado_eur", "0", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "Fecha", "@#Date@", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), "Y", Utility.getDefault(this, vars, "Total_Negociado", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "C_Doctype_ID", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "Puerto_Terminal_Origen_ID", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "C_Bpartner_ID", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField), Utility.getDefault(this, vars, "Agente_ID", "", "53C75C9421F94C609CDC00C4BED0E7CD", "", dataField));
        
      }
     }
      
    
    String currentOrg = (boolNew?"":(dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId")));
    if (!currentOrg.equals("") && !currentOrg.startsWith("'")) currentOrg = "'"+currentOrg+"'";
    String currentClient = (boolNew?"":(dataField!=null?dataField.getField("adClientId"):data[0].getField("adClientId")));
    if (!currentClient.equals("") && !currentClient.startsWith("'")) currentClient = "'"+currentClient+"'";
    
    boolean hasReadOnlyAccess = org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId);
    boolean editableTab = (!hasReadOnlyAccess && (currentOrg.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),currentOrg)) && (currentClient.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), currentClient)));
    if (editableTab)
      xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/logisticadetransporte/parametrizacion/FreeHand/FreeHand873BB22017634E47876049F4C5489617_Edition",discard).createXmlDocument();
    else
      xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/logisticadetransporte/parametrizacion/FreeHand/FreeHand873BB22017634E47876049F4C5489617_NonEditable",discard).createXmlDocument();

    xmlDocument.setParameter("tabId", tabId);
    ToolBar toolbar = new ToolBar(this, editableTab, vars.getLanguage(), "FreeHand873BB22017634E47876049F4C5489617", (strCommand.equals("NEW") || boolNew || (dataField==null && (data==null || data.length==0))), "document.frmMain.inpldtRoutingOrderId", "", "..", "".equals("Y"), "FreeHand", strReplaceWith, true, false, false, Utility.hasTabAttachments(this, vars, tabId, strLDT_Routing_Order_ID), !hasReadOnlyAccess);
    toolbar.setTabId(tabId);
    toolbar.setDeleteable(true);
    toolbar.prepareEditionTemplate("N".equals("Y"), hasSearchCondition, vars.getSessionValue("#ShowTest", "N").equals("Y"), "STD", Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    xmlDocument.setParameter("toolbar", toolbar.toString());

    // set updated timestamp to manage locking mechanism
    if (!boolNew) {
      xmlDocument.setParameter("updatedTimestamp", (dataField != null ? dataField
          .getField("updatedTimeStamp") : data[0].getField("updatedTimeStamp")));
    }
    
    boolean concurrentSave = vars.getSessionValue(tabId + "|concurrentSave").equals("true");
    if (concurrentSave) {
      //after concurrent save error, force autosave
      xmlDocument.setParameter("autosave", "Y");
    } else {
      xmlDocument.setParameter("autosave", "N");
    }
    vars.removeSessionValue(tabId + "|concurrentSave");

    try {
      WindowTabs tabs = new WindowTabs(this, vars, tabId, windowId, true, (strCommand.equalsIgnoreCase("NEW")));
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      // if (!strLDT_Routing_Order_ID.equals("")) xmlDocument.setParameter("childTabContainer", tabs.childTabs(false));
	  // else xmlDocument.setParameter("childTabContainer", tabs.childTabs(true));
	  xmlDocument.setParameter("childTabContainer", tabs.childTabs(false));
	  String hideBackButton = vars.getGlobalVariable("hideMenu", "#Hide_BackButton", "");
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "FreeHand873BB22017634E47876049F4C5489617_Relation.html", "FreeHand", "W", strReplaceWith, tabs.breadcrumb(), hideBackButton.equals("true"), !concurrentSave);
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "FreeHand873BB22017634E47876049F4C5489617_Relation.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.editionTemplate(strCommand.equals("NEW")));
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
		
    
    
    xmlDocument.setParameter("commandType", strCommand);
    xmlDocument.setParameter("buscador",buscador);
    xmlDocument.setParameter("windowId", windowId);
    xmlDocument.setParameter("changed", "");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    final String strMappingName = Utility.getTabURL(tabId, "E", false);
    xmlDocument.setParameter("mappingName", strMappingName);
    xmlDocument.setParameter("confirmOnChanges", Utility.getJSConfirmOnChanges(vars, windowId));
    //xmlDocument.setParameter("buttonReference", Utility.messageBD(this, "Reference", vars.getLanguage()));

    xmlDocument.setParameter("paramSessionDate", strParamSessionDate);

    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    OBError myMessage = vars.getMessage(tabId);
    vars.removeMessage(tabId);
    if (myMessage!=null) {
      xmlDocument.setParameter("messageType", myMessage.getType());
      xmlDocument.setParameter("messageTitle", myMessage.getTitle());
      xmlDocument.setParameter("messageMessage", myMessage.getMessage());
    }
    xmlDocument.setParameter("displayLogic", getDisplayLogicContext(vars, boolNew));
    
    
     if (dataField==null) {
      xmlDocument.setData("structure1",data);
      
    } else {
      
        FieldProvider[] dataAux = new FieldProvider[1];
        dataAux[0] = dataField;
        
        xmlDocument.setData("structure1",dataAux);
      
    }
    
      
   
    try {
      ComboTableData comboTableData = null;
String userOrgList = "";
if (editableTab) 
  userOrgList=Utility.getContext(this, vars, "#User_Org", windowId, accesslevel); //editable record 
else 
  userOrgList=currentOrg;
comboTableData = new ComboTableData(vars, this, "19", "AD_Org_ID", "", "", userOrgList, Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("adOrgId"):dataField.getField("adOrgId")));
xmlDocument.setData("reportAD_Org_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "19", "C_Doctype_ID", "", "49AA2B2244CE4DA798E583F5FA4D6C2E", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("cDoctypeId"):dataField.getField("cDoctypeId")));
xmlDocument.setData("reportC_Doctype_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("Fecha_Format", vars.getSessionValue("#AD_SqlDateFormat"));
xmlDocument.setParameter("Fecha_Estimada_Format", vars.getSessionValue("#AD_SqlDateFormat"));
comboTableData = new ComboTableData(vars, this, "18", "Vendedor_ID", "2B60113683DF450C969C42FD6D6DD8D6", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("vendedorId"):dataField.getField("vendedorId")));
xmlDocument.setData("reportVendedor_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "18", "M_Pricelist_Version_ID", "813B906D1C2B45AD9270DBB1BEFA3AE4", "28BD3BC3351C45EDA83E4230BD0DD493", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("mPricelistVersionId"):dataField.getField("mPricelistVersionId")));
xmlDocument.setData("reportM_Pricelist_Version_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "19", "AD_User_ID", "", "C84F212F2A0D48EBA606172C596C76CC", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("adUserId"):dataField.getField("adUserId")));
xmlDocument.setData("reportAD_User_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "18", "Cliente_Direccion_ID", "159", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("clienteDireccionId"):dataField.getField("clienteDireccionId")));
xmlDocument.setData("reportCliente_Direccion_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "17", "Modalidad", "084746C4C4934924AABBD1EAB1DB9BF5", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("modalidad"):dataField.getField("modalidad")));
xmlDocument.setData("reportModalidad","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "17", "Tipo", "336BA96AA2694CB0BD2689AB13A88F03", "05782A3C8CCE4197AB6EE369D03DB062", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("tipo"):dataField.getField("tipo")));
xmlDocument.setData("reportTipo","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "19", "C_Incoterms_ID", "", "CB0C96AED59E475F9DA45EADCFCDB74E", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("cIncotermsId"):dataField.getField("cIncotermsId")));
xmlDocument.setData("reportC_Incoterms_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("buttonCostos_Locales", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttoncostos_locales_eur", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonTotal_Negociado", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttontotal_negociado_eur", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonCod", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("Embarque_BTNname", Utility.getButtonName(this, vars, "D7A7840B007F4C7A89D7C7E4AA46B692", "Embarque_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalEmbarque = org.openbravo.erpCommon.utility.Utility.isModalProcess("AF6D04358D444CDD894FEEBECF2F181F"); 
xmlDocument.setParameter("Embarque_Modal", modalEmbarque?"true":"false");
xmlDocument.setParameter("ES_Fin_Embarque_BTNname", Utility.getButtonName(this, vars, "077386FC34C840A0BD5863D8A0311252", "ES_Fin_Embarque_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalES_Fin_Embarque = org.openbravo.erpCommon.utility.Utility.isModalProcess("5050917887A34C71B80BFC7464BD0467"); 
xmlDocument.setParameter("ES_Fin_Embarque_Modal", modalES_Fin_Embarque?"true":"false");
xmlDocument.setParameter("Created_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Created_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
xmlDocument.setParameter("Updated_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Updated_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
xmlDocument.setParameter("buttonTarifa_Venta", Utility.messageBD(this, "Calc", vars.getLanguage()));
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new ServletException(ex);
    }

    xmlDocument.setParameter("scriptOnLoad", getShortcutScript(usedButtonShortCuts, reservedButtonShortCuts));
    
    final String refererURL = vars.getSessionValue(tabId + "|requestURL");
    vars.removeSessionValue(tabId + "|requestURL");
    if(!refererURL.equals("")) {
    	final Boolean failedAutosave = (Boolean) vars.getSessionObject(tabId + "|failedAutosave");
		vars.removeSessionValue(tabId + "|failedAutosave");
    	if(failedAutosave != null && failedAutosave) {
    		final String jsFunction = "continueUserAction('"+refererURL+"');";
    		xmlDocument.setParameter("failedAutosave", jsFunction);
    	}
    }

    if (strCommand.equalsIgnoreCase("NEW")) {
      vars.removeSessionValue(tabId + "|failedAutosave");
      vars.removeSessionValue(strMappingName + "|hash");
    }

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageButtonFS(HttpServletResponse response, VariablesSecureApp vars, String strProcessId, String path) throws IOException, ServletException {
    log4j.debug("Output: Frames action button");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "org/openbravo/erpCommon/ad_actionButton/ActionButtonDefaultFrames").createXmlDocument();
    xmlDocument.setParameter("processId", strProcessId);
    xmlDocument.setParameter("trlFormType", "PROCESS");
    xmlDocument.setParameter("language", "defaultLang = \"" + vars.getLanguage() + "\";\n");
    xmlDocument.setParameter("type", strDireccion + path);
    out.println(xmlDocument.print());
    out.close();
  }

    private void printPageButtonEmbarqueAF6D04358D444CDD894FEEBECF2F181F(HttpServletResponse response, VariablesSecureApp vars, String strLDT_Routing_Order_ID, String strembarque, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process AF6D04358D444CDD894FEEBECF2F181F");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/EmbarqueAF6D04358D444CDD894FEEBECF2F181F", discard).createXmlDocument();
      xmlDocument.setParameter("key", strLDT_Routing_Order_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "FreeHand873BB22017634E47876049F4C5489617_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "AF6D04358D444CDD894FEEBECF2F181F");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("AF6D04358D444CDD894FEEBECF2F181F");
        vars.removeMessage("AF6D04358D444CDD894FEEBECF2F181F");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }
    private void printPageButtonES_Fin_Embarque5050917887A34C71B80BFC7464BD0467(HttpServletResponse response, VariablesSecureApp vars, String strLDT_Routing_Order_ID, String stresFinEmbarque, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process 5050917887A34C71B80BFC7464BD0467");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/ES_Fin_Embarque5050917887A34C71B80BFC7464BD0467", discard).createXmlDocument();
      xmlDocument.setParameter("key", strLDT_Routing_Order_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "FreeHand873BB22017634E47876049F4C5489617_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "5050917887A34C71B80BFC7464BD0467");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("5050917887A34C71B80BFC7464BD0467");
        vars.removeMessage("5050917887A34C71B80BFC7464BD0467");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }




    private String getDisplayLogicContext(VariablesSecureApp vars, boolean isNew) throws IOException, ServletException {
      log4j.debug("Output: Display logic context fields");
      String result = "var strTarifa_Negociada=\"" +Utility.getContext(this, vars, "Tarifa_Negociada", windowId) + "\";\nvar strtarifa_negociada_eur=\"" +Utility.getContext(this, vars, "tarifa_negociada_eur", windowId) + "\";\nvar strShowAudit=\"" +(isNew?"N":Utility.getContext(this, vars, "ShowAudit", windowId)) + "\";\n";
      return result;
    }


    private String getReadOnlyLogicContext(VariablesSecureApp vars) throws IOException, ServletException {
      log4j.debug("Output: Read Only logic context fields");
      String result = "var strldt_cod=\"" + Utility.getContext(this, vars, "ldt_cod", windowId) + "\";\nvar strreferencia_cantidad=\"" + Utility.getContext(this, vars, "referencia_cantidad", windowId) + "\";\n";
      return result;
    }




 
  private String getShortcutScript( HashMap<String, String> usedButtonShortCuts, HashMap<String, String> reservedButtonShortCuts){
    StringBuffer shortcuts = new StringBuffer();
    shortcuts.append(" function buttonListShorcuts() {\n");
    Iterator<String> ik = usedButtonShortCuts.keySet().iterator();
    Iterator<String> iv = usedButtonShortCuts.values().iterator();
    while(ik.hasNext() && iv.hasNext()){
      shortcuts.append("  keyArray[keyArray.length] = new keyArrayItem(\"").append(ik.next()).append("\", \"").append(iv.next()).append("\", null, \"altKey\", false, \"onkeydown\");\n");
    }
    shortcuts.append(" return true;\n}");
    return shortcuts.toString();
  }
  
  private int saveRecord(VariablesSecureApp vars, OBError myError, char type) throws IOException, ServletException {
    FreeHand873BB22017634E47876049F4C5489617Data data = null;
    int total = 0;
    if (org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) {
        OBError newError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
        myError.setError(newError);
        vars.setMessage(tabId, myError);
    }
    else {
        Connection con = null;
        try {
            con = this.getTransactionConnection();
            data = getEditVariables(con, vars);
            data.dateTimeFormat = vars.getSessionValue("#AD_SqlDateTimeFormat");            
            String strSequence = "";
            if(type == 'I') {                
        strSequence = SequenceIdData.getUUID();
                if(log4j.isDebugEnabled()) log4j.debug("Sequence: " + strSequence);
                data.ldtRoutingOrderId = strSequence;  
            }
            if (Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),data.adClientId)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),data.adOrgId)){
		     if(type == 'I') {
		       total = data.insert(con, this);
		     } else {
		       //Check the version of the record we are saving is the one in DB
		       if (FreeHand873BB22017634E47876049F4C5489617Data.getCurrentDBTimestamp(this, data.ldtRoutingOrderId).equals(
                vars.getStringParameter("updatedTimestamp"))) {
                total = data.update(con, this);
               } else {
                 myError.setMessage(Replace.replace(Replace.replace(Utility.messageBD(this,
                    "SavingModifiedRecord", vars.getLanguage()), "\\n", "<br/>"), "&quot;", "\""));
                 myError.setType("Error");
                 vars.setSessionValue(tabId + "|concurrentSave", "true");
               } 
		     }		            
          
            }
                else {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
            myError.setError(newError);            
          }
          releaseCommitConnection(con);
        } catch(Exception ex) {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
            myError.setError(newError);   
            try {
              releaseRollbackConnection(con);
            } catch (final Exception e) { //do nothing 
            }           
        }
            
        if (myError.isEmpty() && total == 0) {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=DBExecuteError");
            myError.setError(newError);
        }
        vars.setMessage(tabId, myError);
            
        if(!myError.isEmpty()){
            if(data != null ) {
                if(type == 'I') {            			
                    data.ldtRoutingOrderId = "";
                }
                else {                    
                    
                }
                vars.setEditionData(tabId, data);
            }            	
        }
        else {
            vars.setSessionValue(windowId + "|LDT_Routing_Order_ID", data.ldtRoutingOrderId);
        }
    }
    return total;
  }

  public String getServletInfo() {
    return "Servlet FreeHand873BB22017634E47876049F4C5489617. This Servlet was made by Wad constructor";
  } // End of getServletInfo() method
}
